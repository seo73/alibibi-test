<?php
class ControllerRevolutionRevmenu extends Controller {
	public function index() {
		
		$this->load->language('revolution/revolution');
		$data['heading_title'] = $this->language->get('text_header_menu2_heading');
		$data['text_revmenu_manufs'] = $this->language->get('text_revmenu_manufs');
		$this->document->addScript('catalog/view/javascript/revolution/aim.js');
		$this->document->addScript('catalog/view/javascript/revolution/amazoncategory.js');

		$setting = $this->config->get('revtheme_header_menu');
		if ($setting['inhome']) {
			$data['module_class'] = 'inhome';
		} else {
			$data['module_class'] = false;
		}
		
		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}
		
		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}
		
		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}
		
		if (isset($parts[2])) {
            $data['child2_id'] = $parts[2];
        } else {
            $data['child2_id'] = 0;
        }

        if (isset($parts[3])) {
            $data['child3_id'] = $parts[3];
        } else {
            $data['child3_id'] = 0;
        }
			
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');
		
		$data['categories'] = array();
		
		$categories = $this->model_catalog_category->getCategories(0);
		

		foreach ($categories as $category) {
			if ($category['top']) {
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {

					$children2_data = array();
					$children2 = $this->model_catalog_category->getCategories($child['category_id']);

					foreach ($children2 as $child2) {

						$data2 = array(
							'filter_category_id'  => $child2['category_id'],
							'filter_sub_category' => true
						);

						$children3_data = array();
						$children3 = $this->model_catalog_category->getCategories($child2['category_id']);

						foreach ($children3 as $child3) {

							$data3 = array(
								'filter_category_id'  => $child3['category_id'],
								'filter_sub_category' => true
							);

							$children3_data[] = array(
								'category_id' => $child3['category_id'],
								'name'        => $child3['name'],
								'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $child2['category_id'] . '_' . $child3['category_id'])
							);
						}

						$filter_data_2 = array(
							'filter_category_id'  => $child2['category_id'],
							'filter_sub_category' => true
						);
						
						$children2_data[] = array(
							'category_id' => $child2['category_id'],
							'child3_id'   => $children3_data,                    
							'name'        => $child2['name'] . ($this->config->get('config_product_count') ? ' <sup>' . $this->model_catalog_product->getTotalProducts($filter_data_2) . '</sup>' : ''),
							'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $child2['category_id'])
						);
					}
					
					$filter_data_1 = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);
					
					$children_data[] = array(
						'category_id' => $child['category_id'],
						'child2_id'   => $children2_data,
						'name'        => $child['name'] . ($this->config->get('config_product_count') ? ' <sup>' . $this->model_catalog_product->getTotalProducts($filter_data_1) . '</sup>' : ''),
						'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])	
					);		
				}
				$this->load->model('tool/image');
				$category_info = $this->model_catalog_category->getCategory($category['category_id']);
				if ($category_info) {
					if ($category_info['image2']) {
						$thumb2 = $this->model_tool_image->resize($category_info['image2'], 300, 300);
					} else {
						$thumb2 = '';
					}
				}
				
				$filter_data = array(
					'filter_category_id'  => $category['category_id'],
					'filter_sub_category' => true
				);
				
				$data['categories'][] = array(
					'category_id' => $category['category_id'],
					'name'        => $category['name'] . ($this->config->get('config_product_count') ? ' <sup>' . $this->model_catalog_product->getTotalProducts($filter_data) . '</sup>' : ''),
					'thumb2'      => $thumb2,
					'children'    => $children_data,
					'href'        => $this->url->link('product/category', 'path=' . $category['category_id']),
					'column'      => $category['column'] ? $category['column'] : 1,
				);
			}
		}
		
		$data['manuf_status'] = $setting['manuf'];
		$data['n_column'] = $setting['n_column'];
		$this->load->model('catalog/manufacturer');
		$data['categories_m'] = array();
		$results = $this->model_catalog_manufacturer->getManufacturers();
		foreach ($results as $result) {
			$name = $result['name'];
			if (is_numeric(utf8_substr($name, 0, 1))) {
				$key = '0 - 9';
			} else {
				$key = utf8_substr(utf8_strtoupper($name), 0, 1);
			}
			if (!isset($data['categories_m'][$key])) {
				$data['categories_m'][$key]['name'] = $key;
			}
			$data['categories_m'][$key]['manufacturer'][] = array(
				'name' => $name,
				'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'])
			);
		}

		return $this->load->view('revolution/template/revolution/revmenu.tpl', $data);
  	}
}