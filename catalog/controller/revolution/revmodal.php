<?php
class ControllerRevolutionRevmodal extends Controller {
	public function index() {

		$data = array();

		$this->load->model('catalog/product');
		$this->load->language('revolution/revolution');
		
		$settings = $this->config->get('revtheme_all_settings');

		if (!$settings['modal_status']) {
			return false;
		}
		
		$data['modal_heading_title'] = $settings['modal_header'];
		$data['modal_message'] = html_entity_decode((str_replace("<img", "<img class='img-responsive'", $settings['modal_text'])), ENT_QUOTES, 'UTF-8');
		$data['modal_time'] = $settings['modal_time'];
		
		$this->response->setOutput($this->load->view('revolution/template/revolution/revmodal.tpl', $data));

	}
}
