<?php
class ControllerRevolutionRevBlog extends Controller {
	public function index() {
		
		$this->load->language('revolution/revblog');
		
		$this->load->model('revolution/revolution');
		$this->load->model('tool/image');

		$setting = $this->config->get('revtheme_home_blog');
		$setting2 = $this->config->get('revblog_settings');
		
		$this->document->setTitle($setting['title']);
		$data['heading_title'] = $setting['title'];

		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_blog'] = $this->language->get('text_blog');
		$data['text_sort'] = $this->language->get('text_sort');
		$data['text_limit'] = $this->language->get('text_limit');
		$data['text_data_added'] = $this->language->get('text_data_added');

		$data['button_continue'] = $this->language->get('button_continue');

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'b.date_available';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_product_limit');
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $setting['title'],
			'href' => $this->url->link('revolution/revblog')
		);

		$data['blogs'] = array();

		$data_blogs = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $limit,
			'limit' => $limit
		);	
		
		$results = $this->model_revolution_revolution->getBlogs($data_blogs);

		$blog_total = $this->model_revolution_revolution->getTotalBlogs();

		foreach ($results as $result) {

			$img_w = $setting2['list_image_width'];
			$img_h = $setting2['list_image_height'];
			$desc_l = $setting2['list_desc_limit'];

			if (!empty($img_w)) {
				$image_width = $img_w;
			} else {
				$image_width = 228;
			}
			
			if (!empty($img_h)) {
				$image_height = $img_h;
			} else {
				$image_height = 228;
			}

			if (!empty($desc_l)) {
				$description_limit = $desc_l;
			} else {
				$description_limit = 400;
			}

			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], $image_width, $image_height);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', $image_width, $image_height);
			}

			$data['blogs'][] = array(
				'title' 	  => $result['title'],
				'thumb'       => $image,
				'data_added'  => date($this->language->get('date_format_short'), strtotime($result['date_available'])),
				'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $description_limit) . '..',
				'href' 		  => $this->url->link('revolution/revblog/info', 'blog_id=' . $result['blog_id'])
			);
		}

		$url = '';

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['sorts'] = array();

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_date_desc'),
			'value' => 'b.date_available-DESC',
			'href'  => $this->url->link('revolution/revblog', '&sort=b.date_available&order=DESC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_date_asc'),
			'value' => 'b.date_available-ASC',
			'href'  => $this->url->link('revolution/revblog', '&sort=b.date_available&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_name_asc'),
			'value' => 'bd.title-ASC',
			'href'  => $this->url->link('revolution/revblog', '&sort=bd.title&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_name_desc'),
			'value' => 'bd.title-DESC',
			'href'  => $this->url->link('revolution/revblog', '&sort=bd.title&order=DESC' . $url)
		);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['limits'] = array();

		$limits = array_unique(array($this->config->get('config_product_limit'), 25, 50, 75, 100));

		sort($limits);

		foreach($limits as $value) {
			$data['limits'][] = array(
				'text'  => $value,
				'value' => $value,
				'href'  => $this->url->link('revolution/revblog', $url . '&limit=' . $value)
			);
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$pagination = new Pagination();
		$pagination->total = $blog_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('revolution/revblog', $url . '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($blog_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($blog_total - $limit)) ? $blog_total : ((($page - 1) * $limit) + $limit), $blog_total, ceil($blog_total / $limit));

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['limit'] = $limit;

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/revolution/revblog_list.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/revolution/revblog_list.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/revolution/revblog_list.tpl', $data));
		}
	}

	public function info() {
		
		$this->load->language('revolution/revblog');
		$this->load->language('revolution/revolution');

		$this->load->model('revolution/revolution');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->document->addStyle('catalog/view/javascript/revolution/owl-carousel/owl.carousel.css');
		$this->document->addScript('catalog/view/javascript/revolution/owl-carousel/owl.carousel.min.js');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['text_data_added'] = $this->language->get('text_data_added');
		$data['text_tax'] = $this->language->get('text_tax');
		
		$setting = $this->config->get('revtheme_home_blog');
		$setting2 = $this->config->get('revblog_settings');
		$data['share_status'] = $setting2['share_status'];
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $setting['title'],
			'href' => $this->url->link('revolution/revblog')
		);

		if (isset($this->request->get['blog_id'])) {
			$blog_id = (int)$this->request->get['blog_id'];
		} else {
			$blog_id = 0;
		}

		$blog_info = $this->model_revolution_revolution->getBlog($blog_id);

		if ($blog_info['title_pr'] != '') {
			$data['heading_products_title'] = $blog_info['title_pr'];
		} else {
			$data['heading_products_title'] = $this->language->get('heading_products_title');
		}
		
		if ($blog_info) {
			$this->document->setTitle($blog_info['meta_title']);
			$this->document->setDescription($blog_info['meta_description']);
			$this->document->setKeywords($blog_info['meta_keyword']);

			$data['breadcrumbs'][] = array(
				'text' => $blog_info['title'],
				'href' => $this->url->link('revolution/revblog/info', 'blog_id=' . $blog_id)
			);

			$data['heading_title'] = $blog_info['title'];

			$data['button_continue'] = $this->language->get('button_continue');

			$data['description'] = html_entity_decode($blog_info['description'], ENT_QUOTES, 'UTF-8');
			$data['data_added'] = date($this->language->get('date_format_short'), strtotime($blog_info['date_available']));
			$revpopuporder_settings = $this->config->get('revtheme_catalog_popuporder');
			$data['revpopuporder'] = $revpopuporder_settings['status'];
			
			
			$img_w = $setting2['form_image_width'];
			$img_h = $setting2['form_image_height'];

			if (!empty($img_w)) {
				$image_width = $img_w;
			} else {
				$image_width = 228;
			}
			
			if (!empty($img_h)) {
				$image_height = $img_h;
			} else {
				$image_height = 228;
			}

			$data['image'] = ( $blog_info['image'] ) ? $this->model_tool_image->resize($blog_info['image'], $image_width, $image_height) : false;

			$data['continue'] = $this->url->link('common/home');
			
			$this->load->model('revolution/revolution');
			$settings_stikers = $this->config->get('revtheme_catalog_stiker');
			if ($settings_stikers['status']) {
				$data['stikers_status'] = true;

				if ($settings_stikers['new_status']) {
					$settings_last = $this->config->get('revtheme_catalog_last');
					$data_last = array(
						'sort'  => 'p.date_added',
						'order' => 'DESC',
						'start' => 0,
						'limit' => $settings_last['limit']
						);
					$date_added = $this->model_catalog_product->getProducts($data_last);
				}

				if ($settings_stikers['best_status']) {
					$settings_best = $this->config->get('revtheme_catalog_best');
					$data_best = array(
						'sort'  => 'p.sales',
						'order' => 'DESC',
						'start' => 0,
						'limit' => $settings_best['limit']
					);
					$best_seller = $this->model_revolution_revolution->getBestProducts($data_best);
				}
			} else {
				$data['stikers_status'] = false;
			}
			
			$product_settings = $this->config->get('revtheme_product_all');
			$data['zakaz'] = $product_settings['zakaz'];
			$setting_catalog_all = $this->config->get('revtheme_catalog_all');
			$data['popup_view'] = $setting_catalog_all['popup_view'];
			$data['img_slider'] = $setting_catalog_all['img_slider'];
			$data['rev_srav_prod'] = $setting_catalog_all['rev_srav_prod'];
			$data['rev_wish_prod'] = $setting_catalog_all['rev_wish_prod'];
			$data['currency_code'] = $this->currency->getCode();
			$data['text_catalog_stiker_netu'] = $this->language->get('text_catalog_stiker_netu');
			$data['text_catalog_stiker_last'] = $this->language->get('text_catalog_stiker_last');
			$data['text_catalog_stiker_best'] = $this->language->get('text_catalog_stiker_best');
			$data['text_catalog_revpopup_purchase'] = $this->language->get('text_catalog_revpopup_purchase');
			$data['text_catalog_revpopup_view'] = $this->language->get('text_catalog_revpopup_view');
			$data['text_catalog_price_na_zakaz'] = $this->language->get('text_catalog_price_na_zakaz');
			
			
			$data['logo'] = DIR_IMAGE . $this->config->get('config_logo');
			$logo_size = @getimagesize($data['logo']);
			$data['logo_width'] = isset($logo_size[0])?$logo_size[0]:'150';
			$data['logo_height'] = isset($logo_size[1])?$logo_size[1]:'150';
			$data['microdata_author'] = $data['microdata_name'] = $this->config->get('config_name');
			$data['microdata_date_info'] = date('Y-m-d', filectime(DIR_APPLICATION . 'model/revolution/revolution.php'));
			$data['microdata_url_info'] = $this->url->link('revolution/revblog/info', 'blog_id=' . $blog_id);
			
			$data['products'] = array();

			$results = $this->model_revolution_revolution->getBlogProducts($blog_id);

			foreach ($results as $result) {
				
				if (isset($this->session->data['compare'])) {
					if (in_array($result['product_id'], $this->session->data['compare'])) {
						$compare_class = 'in_compare';
						$button_compare = $this->language->get('button_compare_out');
					} else {
						$compare_class = '';
						$button_compare = $this->language->get('button_compare');
					}
				} else {
					$compare_class = '';
					$button_compare = $this->language->get('button_compare');
				}
				if (isset($this->session->data['wishlist'])) {
					if (in_array($result['product_id'], $this->session->data['wishlist'])) {
						$wishlist_class = 'in_wishlist';
						$button_wishlist = $this->language->get('button_wishlist_out');
					} else {
						$wishlist_class = '';
						$button_wishlist = $this->language->get('button_wishlist');
					}
				} else {
					$wishlist_class = '';
					$button_wishlist = $this->language->get('button_wishlist');
				}
				
				if ($this->customer->isLogged()) {
					$this->load->model('account/wishlist');
					$wishlist_register = $this->model_account_wishlist->getWishlist();
					if ($wishlist_register) {
						$wishlist_register2 = array();
						foreach ($wishlist_register as $result_wishlist_register_id) {
							$wishlist_register_id[] = $result_wishlist_register_id['product_id'];
						}
						if (in_array($result['product_id'], $wishlist_register_id)) {
							$wishlist_class = 'in_wishlist';
							$button_wishlist = $this->language->get('button_wishlist_out');
						} else {
							$wishlist_class = '';
							$button_wishlist = $this->language->get('button_wishlist');
						}
					}
				}
				
				if ($settings_stikers['new_status']) {
					if (isset($date_added[$result['product_id']])) {
						$stiker_last = true;
					} else {
						$stiker_last = false;
					}
				} else {
					$stiker_last = false;
				}
				
				if ($settings_stikers['best_status']) {
					if (isset($best_seller[$result['product_id']])) {
						$stiker_best = true;	
					} else {
						$stiker_best = false;
					}
				} else {
					$stiker_best = false;
				}
				
				if ($settings_stikers['spec_status']) {
					$stiker_spec = true;
				} else {
					$stiker_spec = false;
				}
				
				if ($settings_stikers['stock_status']) {
					$stiker_stock = true;
				} else {
					$stiker_stock = false;
				}
				
				if ($settings_stikers['upc']) {
					$stiker_upc = $result['upc'];
				} else {
					$stiker_upc = false;
				}
				if ($settings_stikers['ean']) {
					$stiker_ean = $result['ean'];
				} else {
					$stiker_ean = false;
				}
				if ($settings_stikers['jan']) {
					$stiker_jan = $result['jan'];
				} else {
					$stiker_jan = false;
				}
				if ($settings_stikers['isbn']) {
					$stiker_isbn = $result['isbn'];
				} else {
					$stiker_isbn = false;
				}
				if ($settings_stikers['mpn']) {
					$stiker_mpn = $result['mpn'];
				} else {
					$stiker_mpn = false;
				}
				
				$settings = $this->config->get('revtheme_cat_compare');
				if ($settings['main_cat']) {
					$query_brand_main_cat_true = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$result['product_id'] . "' AND main_category = 1");
					if ($query_brand_main_cat_true->num_rows) {
						$query_brand = $this->db->query("SELECT cd.name FROM " . DB_PREFIX . "product_to_category ptc LEFT JOIN " . DB_PREFIX . "category_description cd ON (cd.category_id = ptc.category_id) WHERE ptc.product_id = '" . (int)$result['product_id'] . "' AND main_category = 1");
						$brand = $query_brand->row['name'];
					} else {					
						$brand = 'Все товары';
					}
				} else {
					$query_brand = $this->db->query("SELECT cd.name FROM " . DB_PREFIX . "product_to_category ptc LEFT JOIN " . DB_PREFIX . "category_description cd ON (cd.category_id = ptc.category_id) WHERE ptc.product_id = '" . (int)$result['product_id'] . "'");
					if ($query_brand->num_rows) {
						$brand = $query_brand->row['name'];
					} else {					
						$brand = 'Все товары';
					}
				}
				
				if ($result['image']) {
					$images = array();
					$add_images = $this->model_catalog_product->getProductImages($result['product_id']);
					foreach ($add_images as $add_image) {
						$images[] = array(
							'thumb' => $this->model_tool_image->resize($add_image['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'))
						);
					}
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$images = false;
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
					$price_number = $result['price'];
				} else {
					$price = false;
					$price_number = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
					$special_number = $result['special'];
				} else {
					$special = false;
					$special_number = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],
					'href'        => $this->url->link('product/product', '&product_id=' . $result['product_id']),
					'compare_class' => $compare_class,
					'wishlist_class' => $wishlist_class,
					'button_compare' => $button_compare,
					'button_wishlist' => $button_wishlist,
					'brand' => $brand,
					'images' => $images,
					'stiker_last' => $stiker_last,
					'stiker_best' => $stiker_best,
					'stiker_spec' => $stiker_spec,
					'stiker_stock' => $stiker_stock,
					'stiker_upc' => $stiker_upc,
					'stiker_ean' => $stiker_ean,
					'stiker_jan' => $stiker_jan,
					'stiker_isbn' => $stiker_isbn,
					'stiker_mpn' => $stiker_mpn,
					'price_number' => $price_number,
					'special_number' => $special_number,
					'quantity' => $result['quantity']
				);
			}

			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('revolution/template/revolution/revblog_info.tpl', $data));
			
		} else {
			$this->load->language('error/not_found');
			
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('revolution/revblog/info', 'blog_id=' . $blog_id)
			);

			$this->document->setTitle($this->language->get('text_error'));
			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('text_all_news');
			$data['continue'] = $this->url->link('revolution/revblog');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('revolution/template/error/not_found.tpl', $data));			
		}
	}
}