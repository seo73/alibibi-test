<?php
class ControllerRevolutionRevaboutstore extends Controller {
	public function index() {
		
		$setting = $this->config->get('revtheme_aboutstore_home');
		
		if ($setting['status']) {
			$data['heading_title'] = html_entity_decode($setting['title'], ENT_QUOTES, 'UTF-8');
			$data['html'] = html_entity_decode($setting['description'], ENT_QUOTES, 'UTF-8');

			return $this->load->view('revolution/template/revolution/revaboutstore.tpl', $data);
		}
	}
}