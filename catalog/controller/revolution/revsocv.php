<?php
class ControllerRevolutionRevsocv extends Controller {
	public function index() {

		$this->document->addScript('//vk.com/js/api/openapi.js?121');

		$setting = $this->config->get('revtheme_home_socv');

		if (!$setting['status']) {
			return false;
		}
		
		$data['width'] = $setting['width'];
		$data['height'] = $setting['height'];
		$data['id'] = $setting['id'];

		return $this->load->view('revolution/template/revolution/revsocv.tpl', $data);
		
	}
}
