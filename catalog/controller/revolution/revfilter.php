<?php  
class ControllerRevolutionRevfilter extends Controller {
	
	public function index() {

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}
			
		$category_id = end($parts);
		$this->load->model('catalog/category');
		
		$this->document->addScript('catalog/view/javascript/revolution/revfilter/revfilter.js');
		$this->document->addStyle('catalog/view/javascript/revolution/revfilter/nouislider/jquery.nouislider.css');
		$this->document->addScript('catalog/view/javascript/revolution/revfilter/nouislider/jquery.nouislider.js');
		
		$this->document->addStyle('catalog/view/theme/revolution/stylesheet/revfilter.css');
		
		$setting = $this->config->get('revtheme_catalog_filter');
		$revtheme_catalog_filter_categories = $this->config->get('revtheme_catalog_filter_categories');

		if (!$setting['status']) {
			return false;
		}

		$category_info = $this->model_catalog_category->getCategory($category_id);
		
		if ($category_info && in_array($category_id, $revtheme_catalog_filter_categories)) {
			
			$this->load->model('revolution/revfilter');
			$this->load->language('revolution/revfilter');
			
			$data['heading_title'] = $this->language->get('heading_title');
			
			$data['text_clear'] = $this->language->get('text_clear');
			$data['text_more'] = $this->language->get('text_more');
			$data['text_less'] = $this->language->get('text_less');
			$data['text_price'] = $this->language->get('text_price');
			
			$this->language->load('product/category');
			$data['text_empty'] = $this->language->get('text_empty');			
			$data['text_tax'] = $this->language->get('text_tax');
					
			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_continue'] = $this->language->get('button_continue');
			
			$data['currency'] = '&nbsp;' . $this->currency->getCode();
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}		
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
			
			$data['action'] = str_replace('&amp;', '&', $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url));			
			
			if (isset($this->request->get['filter_price'])) {
				$filter_price = $this->request->get['filter_price'];
			} else {
				$filter_price = NULL;
			}
			
			$data['filter_price'] =  $filter_price;
			
			$data['data'] = array(
				'filter_price' 			=> $filter_price,
				'filter_filter' 		=> isset($this->request->get['filter']) ? $this->request->get['filter'] : FALSE,
				'tax' 					=> $setting['tax'],
				'filter_category_id' 	=> $category_id
			);	
			
			if(!empty($filter_price)) {
				$data['price_range'] = $this->model_revolution_revfilter->getCategoryPriceRange2($data['data']);
			} else {
				$data['price_range'] = $this->model_revolution_revfilter->getCategoryPriceRange1($data['data']);
			}
			
			if (isset($this->request->get['filter'])) {
				if (substr($this->request->get['filter'], -1) == ",") 
					$this->request->get['filter'] = substr($this->request->get['filter'], 0, -1);
				$filter_category = $this->request->get['filter'];
				$data['filter_category'] =  explode(',', $filter_category);
			} else {
				$filter_category = array();
				$data['filter_category'] =  array();
			}

			$this->load->model('catalog/product');
			$data['filter_groups'] = array();
			$data['data'] = array(
				'filter_category_id' 	=> $category_id,
				'filter_price' 			=> $filter_price,
				'filter_filter' 		=> isset($this->request->get['filter']) ? $this->request->get['filter'] : FALSE 
			);
			$filter_groups = $this->model_revolution_revfilter->getCategoryFilters($data['data']);			
			if ($filter_groups) {
				foreach ($filter_groups as $filter_group) {
					$filter_data = array();
					
					foreach ($filter_group['filter'] as $filter) {
						$data['data'] = array(
							'filter_category_id'  	=> $category_id,
							'filter_price' 			=> $filter_price,
							'filter_filter'      	=> $filter['filter_id'].(isset($this->request->get['filter']) ? ','.$this->request->get['filter'] : FALSE )
						);
						
						if ($setting['metod'] == 'and') { 
							$total = $this->model_revolution_revfilter->getTotalProductsFiltered($data['data']); // AND Version
						} else {
							$total = $this->model_catalog_product->getTotalProducts($data['data']); // AND/OR Version	
						}
						
						if($total > 0) {
							$filter_data[] = array(
								'filter_id' 	=> $filter['filter_id'],
								'checked' 		=> in_array($filter['filter_id'], $data['filter_category']) ? TRUE : FALSE,
								'name'      	=> $filter['name'] .' <i class="total_count">' . $total . '</i>'
							);
						}
					}
					
					$data['filter_groups'][] = array(
						'filter_group_id' 	=> $filter_group['filter_group_id'],
						'name'            	=> $filter_group['name'],
						'filter'          	=> $filter_data
					);
				} 
			}
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/revolution/revfilter.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/revolution/revfilter.tpl', $data);
			} else {
				return $this->load->view('default/template/revolution/revfilter.tpl', $data);
			}
			
		} else {return $this->load->view($this->config->get('config_template') . '/template/revolution/test.tpl', $data); }
  	}
	
	public function ajaxFilter() {
		
		$this->load->model('catalog/category');
		
		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}
		
		$category_id = end($parts);
		$category_info = $this->model_catalog_category->getCategory($category_id);
		
		if ($category_info) {
			
			$this->load->language('revolution/revfilter');
			$this->load->model('revolution/revfilter');
			$this->load->model('catalog/product');
			
			$json['text_more'] = $this->language->get('text_more');
			$json['text_less'] = $this->language->get('text_less');
			$json['text_price'] = $this->language->get('text_price');
			
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}		
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}		
			
			if (isset($this->request->get['filter_price'])) {
				$filter_price = $this->request->get['filter_price'];
			} else {
				$filter_price = array();
			}
			
			$json['filter_price'] =  $filter_price;
			
			$data['data'] = array(
				'filter_price' 			=> $filter_price,
				'filter_filter' 		=> isset($this->request->get['filter']) ? $this->request->get['filter'] : FALSE,
				'tax' 					=> $setting['tax'],
				'filter_category_id' 	=> $category_id
			);
			
			if(!empty($filter_price)) {
				$json['price_range'] = $this->model_revolution_revfilter->getCategoryPriceRange2($data['data']);	
			} else {
				$json['price_range'] = $this->model_revolution_revfilter->getCategoryPriceRange1($data['data']);
			}
			
			if (isset($this->request->get['filter'])) {
				if (substr($this->request->get['filter'], -1) == ',') 
					$this->request->get['filter'] = substr($this->request->get['filter'], 0, -1);
				$filter_category = $this->request->get['filter'];
				$json['filter_category'] =  explode(',', $filter_category);
			} else {
				$filter_category = array();
				$json['filter_category'] =  array();
			}
			
			$json['filter_groups'] = array();
			$data['data'] = array(
				'filter_category_id' 	=> $category_id, 
				'filter_price' 			=> $filter_price,
				'filter_filter' 		=> isset($this->request->get['filter']) ? $this->request->get['filter'] : FALSE 
			);
			
			$filter_groups = $this->model_module_revfilter->getCategoryFilters($data['data']);
			
			if ($filter_groups) {
				
				foreach ($filter_groups as $filter_group) {
					
					$filter_data = array();
					
					foreach ($filter_group['filter'] as $filter) {		
						$data['data'] = array(
							'filter_category_id' 	=> $category_id,
							'filter_price' 			=> $filter_price,
							'filter_filter' 		=> $filter['filter_id'] . (isset($this->request->get['filter']) ? ','.$this->request->get['filter'] : FALSE )
						);
						
						if ($setting('metod') == 'and') { 
							$total = $this->model_revolution_revfilter->getTotalProductsFiltered($data['data']); // AND Version
						} else {
							$total = $this->model_catalog_product->getTotalProducts($data['data']); // AND/OR Version
						}
						
						if($total > 0) {
							$filter_data[] = array(
								'filter_id' => $filter['filter_id'],
								'checked' 	=> in_array($filter['filter_id'], $json['filter_category']) ? TRUE : FALSE,
								'name'      => $filter['name'] . '<i class="total_count">' .$total. '</i>'
							);
						}
					}
					
					if(count($filter_data) > 0) {
						$json['filter_groups'][] = array(
							'filter_group_id' => $filter_group['filter_group_id'],
							'name'            => $filter_group['name'],
							'filter'          => $filter_data
						);
					}
				}
			}
			
			$json['action'] = str_replace('&amp;', '&', $this->url->link('product/category', 'path=' . $this->request->get['path']));
			
			$this->response->setOutput(json_encode($json));	
		}
  	}
	
	public function ajaxCategory() {
		
		$this->load->language('product/category');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('tool/image'); 
		
		if (!empty($this->request->get['filter'])) {
			if (substr($this->request->get['filter'], -1) == ',') 
				$this->request->get['filter'] = substr($this->request->get['filter'], 0, -1);
			$filter = $this->request->get['filter'];
		} else {
			$filter = NULL;
		}
		
		if (!empty($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = NULL;
		}
		
		if (!empty($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}
		if (!empty($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (!empty($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else { 
			$page = 1;
		}	
							
		if (!empty($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}
		
		if (!empty($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);		
			$category_id = array_pop($parts);
		} else {
			$category_id = 0;
		}
		
		$url = '';
		
		if (!empty($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}
		if (!empty($this->request->get['filter'])) {
			$url .= '&filter=' . $this->request->get['filter'];
		}
	
		if (!empty($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}	
		
		if (!empty($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (!empty($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}
			
		$category_info = $this->model_catalog_category->getCategory($category_id);
	
		if ($category_info) {
			
			$data['products'] = array();
			
			$data['data'] = array(
				'filter_category_id' 	=> $category_id,
				'filter_price' 			=> $filter_price,
				'filter_filter'      	=> $filter,  
				'sort'               	=> $sort,
				'order'              	=> $order,
				'start'              	=> ($page - 1) * $limit,
				'limit'              	=> $limit
			);
					
			$product_total = $this->model_catalog_product->getTotalProducts($data['data']);
			$results = $this->model_catalog_product->getProducts($data['data']);
			
			if($results) {
				foreach ($results as $result) {
					if ($result['image']) {
						$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
					} else {
						$image = false;
					}
					
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}
					
					if ((float)$result['special']) {
						$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}	
					
					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
					} else {
						$tax = false;
					}				
					
					if ($this->config->get('config_review_status')) {
						$rating = (int)$result['rating'];
					} else {
						$rating = false;
					}
									
					$data['products'][] = array(
						'product_id'  => $result['product_id'],
						'thumb'       => $image,
						'name'        => $result['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $result['rating'],
						'reviews'     => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
						'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
					);
				}
			}
					
			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');
			
			$data['pagination'] = $pagination->render();
			$data['url_new'] = str_replace('&amp;', '&', $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url));
			
			$this->response->setOutput(json_encode($data));
    	} 
	}
}
?>