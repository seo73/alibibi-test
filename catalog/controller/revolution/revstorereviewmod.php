<?php
class ControllerRevolutionRevstorereviewmod extends Controller
{
    public function index() {
        
		$setting = $this->config->get('revtheme_home_storereview');
		
		if (!$setting['status']) {
			return false;
		}
		
        $data['heading_title'] = html_entity_decode($setting['title'], ENT_QUOTES, 'UTF-8');
		$data['button_all'] = (int)$setting['button_all'];
        $data['button_all_text'] = html_entity_decode($setting['button_all_text'], ENT_QUOTES, 'UTF-8');
        
        $data['keyword'] = $this->url->link('revolution/revstorereview');

        $this->load->model('revolution/revolution');

        $results = $this->model_revolution_revolution->getModuleReviews(0, $setting['limit'], $setting['order']);

        if ($results) {
            foreach ($results as $result) {
                $data['reviews'][] = array(
                    'review_id'  => $result['review_id'],
                    'text' 		 => utf8_substr(strip_tags(html_entity_decode($result['text'], ENT_QUOTES, 'UTF-8')), 0, $setting['limit_text']) . '..',
                    'rating' 	 => (int)$result['rating'],
                    'author' 	 => $result['author'],
                    'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
                );
            }

            return $this->load->view('revolution/template/revolution/revstorereview_mod.tpl', $data);
        }
    }
}
?>