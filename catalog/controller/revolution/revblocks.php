<?php
class ControllerRevolutionRevblocks extends Controller {

    public function index() {
		$setting = $this->config->get('revtheme_blocks_home');
		
		if (!$setting['status']) {
			return false;
		}

		$results = $this->config->get('revtheme_blocks_home_item');
		if (!empty($results)){
			foreach ($results as $result) {
				$data['blocks'][] = array(
					'title' => $result['title'],
					'image' => $result['image'],
					'description' => $result['description'],
					'link'  => $result['link'],
					'sort'  => $result['sort']
				);
			}
		} else {			
			$data['blocks'] = false;
		}
		if (!empty($data['blocks'])){
			foreach ($data['blocks'] as $key => $value) {
				$sort[$key] = $value['sort'];
			}
			array_multisort($sort, SORT_ASC, $data['blocks']);
		}
		
		$setting_header_menu = $this->config->get('revtheme_header_menu');
		if ($setting_header_menu['type']) {
			$data['amazon'] = true;
		} else {
			$data['amazon'] = false;
		}
		if ($setting_header_menu['inhome']) {
			$data['module_class'] = true;
		} else {
			$data['module_class'] = false;
		}

        return $this->load->view('revolution/template/revolution/revblocks.tpl', $data);
    }

}
?>