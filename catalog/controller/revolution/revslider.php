<?php
class ControllerRevolutionRevslider extends Controller {
    protected $path = array();
	
    public function slider1($setting) {
       
		$setting = $this->config->get('revtheme_slider_1');
		
		if (!$setting['status']) {
			return false;
		}
		
        $this->language->load('revolution/revslider');
		$this->load->language('revolution/revolution');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
   
		$revpopuporder_settings = $this->config->get('revtheme_catalog_popuporder');
		$data['revpopuporder'] = $revpopuporder_settings['status'];
		$product_settings = $this->config->get('revtheme_product_all');
		$data['zakaz'] = $product_settings['zakaz'];
		$setting_catalog_all = $this->config->get('revtheme_catalog_all');
		$data['popup_view'] = $setting_catalog_all['popup_view'];
		$data['img_slider'] = $setting_catalog_all['img_slider'];
		$data['rev_srav_prod'] = $setting_catalog_all['rev_srav_prod'];
		$data['rev_wish_prod'] = $setting_catalog_all['rev_wish_prod'];
		$data['currency_code'] = $this->currency->getCode();
		$data['text_catalog_stiker_netu'] = $this->language->get('text_catalog_stiker_netu');
		$data['text_catalog_stiker_last'] = $this->language->get('text_catalog_stiker_last');
		$data['text_catalog_stiker_best'] = $this->language->get('text_catalog_stiker_best');
		$data['text_catalog_revpopup_purchase'] = $this->language->get('text_catalog_revpopup_purchase');
		$data['text_catalog_revpopup_view'] = $this->language->get('text_catalog_revpopup_view');
		$data['text_catalog_price_na_zakaz'] = $this->language->get('text_catalog_price_na_zakaz');
		
		$settings_stikers = $this->config->get('revtheme_catalog_stiker');
		if ($settings_stikers['status']) {
			$data['stikers_status'] = true;
		} else {
			$data['stikers_status'] = false;
		}
   
		if ($setting['title']) {
			$data['heading_title'] = ('<i class="'.$setting['icon'].'"></i>' . $setting['title']);
		} else {
			$data['heading_title'] = '';
		}

		$data['slider_id'] = '1';
		$data['sort'] = $setting['sort'];
		$data['icon'] = $setting['icon'];

		if ($setting['autoscroll'] > 0) {
			$data['autoscroll'] = $setting['autoscroll'];
		} else {
			$data['autoscroll'] = '0';
		}

		$this->load->model('revolution/revolution');
		$this->load->model('tool/image');

		if (isset($this->request->get['path'])) {
			$this->path = explode('_', $this->request->get['path']);
			$this->category_id = end($this->path);
		}

		$url = '';

		$data['products'] = array();

		if ($setting['category_id'] == 'featured') {
			$data['products'] = $this->getFeaturedProducts($setting);
		} else {
			$data['products'] = $this->getCategoryProducts($setting);
		}

        $sort_order = array();

        $data['button_cart']        = $this->language->get('button_cart');
        $data['button_wishlist']    = $this->language->get('button_wishlist');
        $data['button_compare']     = $this->language->get('button_compare');
        $data['text_tax']           = $this->language->get('text_tax');

        return $this->load->view('revolution/template/revolution/revslider.tpl', $data);
    }

	public function slider2($setting) {
       
		$setting = $this->config->get('revtheme_slider_2');
		
		if (!$setting['status']) {
			return false;
		}
		
        $this->language->load('revolution/revslider');
		$this->load->language('revolution/revolution');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
		
		$revpopuporder_settings = $this->config->get('revtheme_catalog_popuporder');
		$data['revpopuporder'] = $revpopuporder_settings['status'];
		$product_settings = $this->config->get('revtheme_product_all');
		$data['zakaz'] = $product_settings['zakaz'];
		$setting_catalog_all = $this->config->get('revtheme_catalog_all');
		$data['popup_view'] = $setting_catalog_all['popup_view'];
		$data['img_slider'] = $setting_catalog_all['img_slider'];
		$data['rev_srav_prod'] = $setting_catalog_all['rev_srav_prod'];
		$data['rev_wish_prod'] = $setting_catalog_all['rev_wish_prod'];
		$data['currency_code'] = $this->currency->getCode();
		$data['text_catalog_stiker_netu'] = $this->language->get('text_catalog_stiker_netu');
		$data['text_catalog_stiker_last'] = $this->language->get('text_catalog_stiker_last');
		$data['text_catalog_stiker_best'] = $this->language->get('text_catalog_stiker_best');
		$data['text_catalog_revpopup_purchase'] = $this->language->get('text_catalog_revpopup_purchase');
		$data['text_catalog_revpopup_view'] = $this->language->get('text_catalog_revpopup_view');
		$data['text_catalog_price_na_zakaz'] = $this->language->get('text_catalog_price_na_zakaz');
		
		$settings_stikers = $this->config->get('revtheme_catalog_stiker');
		if ($settings_stikers['status']) {
			$data['stikers_status'] = true;
		} else {
			$data['stikers_status'] = false;
		}
       
		if ($setting['title']) {
			$data['heading_title'] = ('<i class="'.$setting['icon'].'"></i>' . $setting['title']);
		} else {
			$data['heading_title'] = '';
		}

		$data['slider_id'] = '2';
		$data['sort'] = $setting['sort'];
		$data['icon'] = $setting['icon'];

		if ($setting['autoscroll'] > 0) {
			$data['autoscroll'] = $setting['autoscroll'];}
			else {$data['autoscroll'] = '0';
		}

		$this->load->model('revolution/revolution');
		$this->load->model('tool/image');

		if (isset($this->request->get['path'])) {
			$this->path = explode('_', $this->request->get['path']);
			$this->category_id = end($this->path);
		}

		$url = '';

		$data['products'] = array();

		if ($setting['category_id'] == 'featured') {
			$data['products'] = $this->getFeaturedProducts($setting);
		} else {
			$data['products'] = $this->getCategoryProducts($setting);
		}

        $sort_order = array();

        $data['button_cart']        = $this->language->get('button_cart');
        $data['button_wishlist']    = $this->language->get('button_wishlist');
        $data['button_compare']     = $this->language->get('button_compare');
        $data['text_tax']           = $this->language->get('text_tax');

        return $this->load->view('revolution/template/revolution/revslider.tpl', $data);
    }
	
	public function slider3($setting) {
       
		$setting = $this->config->get('revtheme_slider_3');
		
		if (!$setting['status']) {
			return false;
		}
		
        $this->language->load('revolution/revslider');
		$this->load->language('revolution/revolution');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
		
		$revpopuporder_settings = $this->config->get('revtheme_catalog_popuporder');
		$data['revpopuporder'] = $revpopuporder_settings['status'];
		$product_settings = $this->config->get('revtheme_product_all');
		$data['zakaz'] = $product_settings['zakaz'];
		$setting_catalog_all = $this->config->get('revtheme_catalog_all');
		$data['popup_view'] = $setting_catalog_all['popup_view'];
		$data['img_slider'] = $setting_catalog_all['img_slider'];
		$data['rev_srav_prod'] = $setting_catalog_all['rev_srav_prod'];
		$data['rev_wish_prod'] = $setting_catalog_all['rev_wish_prod'];
		$data['currency_code'] = $this->currency->getCode();
		$data['text_catalog_stiker_netu'] = $this->language->get('text_catalog_stiker_netu');
		$data['text_catalog_stiker_last'] = $this->language->get('text_catalog_stiker_last');
		$data['text_catalog_stiker_best'] = $this->language->get('text_catalog_stiker_best');
		$data['text_catalog_revpopup_purchase'] = $this->language->get('text_catalog_revpopup_purchase');
		$data['text_catalog_revpopup_view'] = $this->language->get('text_catalog_revpopup_view');
		$data['text_catalog_price_na_zakaz'] = $this->language->get('text_catalog_price_na_zakaz');
		
		$settings_stikers = $this->config->get('revtheme_catalog_stiker');
		if ($settings_stikers['status']) {
			$data['stikers_status'] = true;
		} else {
			$data['stikers_status'] = false;
		}
		
		if ($setting['title']) {
			$data['heading_title'] = ('<i class="'.$setting['icon'].'"></i>' . $setting['title']);
		} else {
			$data['heading_title'] = '';
		}

		$data['slider_id'] = '3';
		$data['sort'] = $setting['sort'];

		if ($setting['autoscroll'] > 0) {
			$data['autoscroll'] = $setting['autoscroll'];}
			else {$data['autoscroll'] = '0';
		}

		$this->load->model('revolution/revolution');
		$this->load->model('tool/image');

		if (isset($this->request->get['path'])) {
			$this->path = explode('_', $this->request->get['path']);
			$this->category_id = end($this->path);
		}

		$url = '';

		$data['products'] = array();

		if ($setting['category_id'] == 'featured') {
			$data['products'] = $this->getFeaturedProducts($setting);
		} else {
			$data['products'] = $this->getCategoryProducts($setting);
		}

        $sort_order = array();

        $data['button_cart']        = $this->language->get('button_cart');
        $data['button_wishlist']    = $this->language->get('button_wishlist');
        $data['button_compare']     = $this->language->get('button_compare');
        $data['text_tax']           = $this->language->get('text_tax');

        return $this->load->view('revolution/template/revolution/revslider.tpl', $data);
    }
	
	public function slider4($setting) {
       
		$setting = $this->config->get('revtheme_slider_4');
		
		if (!$setting['status']) {
			return false;
		}
		
        $this->language->load('revolution/revslider');
		$this->load->language('revolution/revolution');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
		
		$revpopuporder_settings = $this->config->get('revtheme_catalog_popuporder');
		$data['revpopuporder'] = $revpopuporder_settings['status'];
		$product_settings = $this->config->get('revtheme_product_all');
		$data['zakaz'] = $product_settings['zakaz'];
		$setting_catalog_all = $this->config->get('revtheme_catalog_all');
		$data['popup_view'] = $setting_catalog_all['popup_view'];
		$data['img_slider'] = $setting_catalog_all['img_slider'];
		$data['rev_srav_prod'] = $setting_catalog_all['rev_srav_prod'];
		$data['rev_wish_prod'] = $setting_catalog_all['rev_wish_prod'];
		$data['currency_code'] = $this->currency->getCode();
		$data['text_catalog_stiker_netu'] = $this->language->get('text_catalog_stiker_netu');
		$data['text_catalog_stiker_last'] = $this->language->get('text_catalog_stiker_last');
		$data['text_catalog_stiker_best'] = $this->language->get('text_catalog_stiker_best');
		$data['text_catalog_revpopup_purchase'] = $this->language->get('text_catalog_revpopup_purchase');
		$data['text_catalog_revpopup_view'] = $this->language->get('text_catalog_revpopup_view');
		$data['text_catalog_price_na_zakaz'] = $this->language->get('text_catalog_price_na_zakaz');
		
		$settings_stikers = $this->config->get('revtheme_catalog_stiker');
		if ($settings_stikers['status']) {
			$data['stikers_status'] = true;
		} else {
			$data['stikers_status'] = false;
		}
		
		if ($setting['title']) {
			$data['heading_title'] = ('<i class="'.$setting['icon'].'"></i>' . $setting['title']);
		} else {
			$data['heading_title'] = '';
		}

		$data['slider_id'] = '4';
		$data['sort'] = $setting['sort'];

		if ($setting['autoscroll'] > 0) {
			$data['autoscroll'] = $setting['autoscroll'];}
			else {$data['autoscroll'] = '0';
		}

		$this->load->model('revolution/revolution');
		$this->load->model('tool/image');

		if (isset($this->request->get['path'])) {
			$this->path = explode('_', $this->request->get['path']);
			$this->category_id = end($this->path);
		}

		$url = '';

		$data['products'] = array();

		if ($setting['category_id'] == 'featured') {
			$data['products'] = $this->getFeaturedProducts($setting);
		} else {
			$data['products'] = $this->getCategoryProducts($setting);
		}

        $sort_order = array();

        $data['button_cart']        = $this->language->get('button_cart');
        $data['button_wishlist']    = $this->language->get('button_wishlist');
        $data['button_compare']     = $this->language->get('button_compare');
        $data['text_tax']           = $this->language->get('text_tax');

        return $this->load->view('revolution/template/revolution/revslider.tpl', $data);
    }
	
	public function slider5($setting) {
       
		$setting = $this->config->get('revtheme_slider_5');
		
		if (!$setting['status']) {
			return false;
		}
		
        $this->language->load('revolution/revslider');
		$this->load->language('revolution/revolution');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
		
		$revpopuporder_settings = $this->config->get('revtheme_catalog_popuporder');
		$data['revpopuporder'] = $revpopuporder_settings['status'];
		$product_settings = $this->config->get('revtheme_product_all');
		$data['zakaz'] = $product_settings['zakaz'];
		$setting_catalog_all = $this->config->get('revtheme_catalog_all');
		$data['popup_view'] = $setting_catalog_all['popup_view'];
		$data['img_slider'] = $setting_catalog_all['img_slider'];
		$data['rev_srav_prod'] = $setting_catalog_all['rev_srav_prod'];
		$data['rev_wish_prod'] = $setting_catalog_all['rev_wish_prod'];
		$data['currency_code'] = $this->currency->getCode();
		$data['text_catalog_stiker_netu'] = $this->language->get('text_catalog_stiker_netu');
		$data['text_catalog_stiker_last'] = $this->language->get('text_catalog_stiker_last');
		$data['text_catalog_stiker_best'] = $this->language->get('text_catalog_stiker_best');
		$data['text_catalog_revpopup_purchase'] = $this->language->get('text_catalog_revpopup_purchase');
		$data['text_catalog_revpopup_view'] = $this->language->get('text_catalog_revpopup_view');
		$data['text_catalog_price_na_zakaz'] = $this->language->get('text_catalog_price_na_zakaz');
		
		$settings_stikers = $this->config->get('revtheme_catalog_stiker');
		if ($settings_stikers['status']) {
			$data['stikers_status'] = true;
		} else {
			$data['stikers_status'] = false;
		}
		
		if ($setting['title']) {
			$data['heading_title'] = ('<i class="'.$setting['icon'].'"></i>' . $setting['title']);
		} else {
			$data['heading_title'] = '';
		}

		$data['slider_id'] = '5';
		$data['sort'] = $setting['sort'];

		if ($setting['autoscroll'] > 0) {
			$data['autoscroll'] = $setting['autoscroll'];}
			else {$data['autoscroll'] = '0';
		}

		$this->load->model('revolution/revolution');
		$this->load->model('tool/image');

		if (isset($this->request->get['path'])) {
			$this->path = explode('_', $this->request->get['path']);
			$this->category_id = end($this->path);
		}

		$url = '';

		$data['products'] = array();

		if ($setting['category_id'] == 'featured') {
			$data['products'] = $this->getFeaturedProducts($setting);
		} else {
			$data['products'] = $this->getCategoryProducts($setting);
		}

        $sort_order = array();

        $data['button_cart']        = $this->language->get('button_cart');
        $data['button_wishlist']    = $this->language->get('button_wishlist');
        $data['button_compare']     = $this->language->get('button_compare');
        $data['text_tax']           = $this->language->get('text_tax');

        return $this->load->view('revolution/template/revolution/revslider.tpl', $data);
    }
	
    public function getCategoryProducts($setting) {
        $result = array();

        $data = array(
            'filter_category_id'     => $setting['category_id'],
            'filter_manufacturer_id' => $setting['manufacturer_id'],
            'filter_sub_category'    => true,
            'sort'                   => $setting['sort'],
            'order'                  => 'DESC',
            'start'                  => '0',
            'limit'                  => $setting['count']
        );

		$this->load->model('revolution/revolution');
		$settings_stikers = $this->config->get('revtheme_catalog_stiker');
		if ($settings_stikers['status']) {
			$data['stikers_status'] = true;

			if ($settings_stikers['new_status']) {
				$settings_last = $this->config->get('revtheme_catalog_last');
				$data_last = array(
					'sort'  => 'p.date_added',
					'order' => 'DESC',
					'start' => 0,
					'limit' => $settings_last['limit']
					);
				$this->load->model('catalog/product');	
				$date_added = $this->model_catalog_product->getProducts($data_last);
			}

			if ($settings_stikers['best_status']) {
				$settings_best = $this->config->get('revtheme_catalog_best');
				$data_best = array(
					'sort'  => 'p.sales',
					'order' => 'DESC',
					'start' => 0,
					'limit' => $settings_best['limit']
				);
				$best_seller = $this->model_revolution_revolution->getBestProducts($data_best);
			}
		} else {
			$data['stikers_status'] = false;
		}
		
        $products = $this->model_revolution_revolution->getProducts($data);

        foreach ($products as $product) {
			
			if (isset($this->session->data['compare'])) {
				if (in_array($product['product_id'], $this->session->data['compare'])) {
					$compare_class = 'in_compare';
					$button_compare = $this->language->get('button_compare_out');
				} else {
					$compare_class = '';
					$button_compare = $this->language->get('button_compare');
				}
			} else {
				$compare_class = '';
				$button_compare = $this->language->get('button_compare');
			}
			if (isset($this->session->data['wishlist'])) {
				if (in_array($product['product_id'], $this->session->data['wishlist'])) {
					$wishlist_class = 'in_wishlist';
					$button_wishlist = $this->language->get('button_wishlist_out');
				} else {
					$wishlist_class = '';
					$button_wishlist = $this->language->get('button_wishlist');
				}
			} else {
				$wishlist_class = '';
				$button_wishlist = $this->language->get('button_wishlist');
			}
			if ($this->customer->isLogged()) {
				$this->load->model('account/wishlist');
				$wishlist_register = $this->model_account_wishlist->getWishlist();
				if ($wishlist_register) {
					$wishlist_register2 = array();
					foreach ($wishlist_register as $result_wishlist_register_id) {
						$wishlist_register_id[] = $result_wishlist_register_id['product_id'];
					}
					if (in_array($product['product_id'], $wishlist_register_id)) {
						$wishlist_class = 'in_wishlist';
						$button_wishlist = $this->language->get('button_wishlist_out');
					} else {
						$wishlist_class = '';
						$button_wishlist = $this->language->get('button_wishlist');
					}
				}
			}			
			
			if ($settings_stikers['new_status']) {
				if (isset($date_added[$product['product_id']])) {
					$stiker_last = true;
				} else {
					$stiker_last = false;
				}
			} else {
				$stiker_last = false;
			}
			
			if ($settings_stikers['best_status']) {
				if (isset($best_seller[$product['product_id']])) {
					$stiker_best = true;	
				} else {
					$stiker_best = false;
				}
			} else {
				$stiker_best = false;
			}
			
			if ($settings_stikers['spec_status']) {
				$stiker_spec = true;
			} else {
				$stiker_spec = false;
			}
			
			if ($settings_stikers['stock_status']) {
				$stiker_stock = true;
			} else {
				$stiker_stock = false;
			}
			
			if ($settings_stikers['upc']) {
				$stiker_upc = $product['upc'];
			} else {
				$stiker_upc = false;
			}
			if ($settings_stikers['ean']) {
				$stiker_ean = $product['ean'];
			} else {
				$stiker_ean = false;
			}
			if ($settings_stikers['jan']) {
				$stiker_jan = $product['jan'];
			} else {
				$stiker_jan = false;
			}
			if ($settings_stikers['isbn']) {
				$stiker_isbn = $product['isbn'];
			} else {
				$stiker_isbn = false;
			}
			if ($settings_stikers['mpn']) {
				$stiker_mpn = $product['mpn'];
			} else {
				$stiker_mpn = false;
			}
			
            if ($product['image']) {
				$images = array();
				$add_images = $this->model_catalog_product->getProductImages($product['product_id']);
				foreach ($add_images as $add_image) {
					$images[] = array(
						'thumb' => $this->model_tool_image->resize($add_image['image'], $setting['image_width'], $setting['image_height'])
					);
				}
                $image = $this->model_tool_image->resize($product['image'], $setting['image_width'], $setting['image_height']);
            } else {
				$images = false;
                $image = $this->model_tool_image->resize('placeholder.png', $setting['image_width'], $setting['image_height']);
            }

            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
				$price_number = $product['price'];
            } else {
                $price = false;
				$price_number = false;
            }

            if ((float)$product['special']) {
                $special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')));
				$special_number = $product['special'];
            } else {
                $special = false;
				$special_number = false;
            }

            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float)$product['special'] ? $product['special'] : $product['price']);
            } else {
                $tax = false;
            }

            $options = $this->model_catalog_product->getProductOptions($product['product_id']);

            if ($this->config->get('config_review_status')) {
                $rating = (int)$product['rating'];
            } else {
                $rating = false;
            }

			$settings = $this->config->get('revtheme_cat_compare');
			if ($settings['main_cat']) {
				$query_brand_main_cat_true = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product['product_id'] . "' AND main_category = 1");
				if ($query_brand_main_cat_true->num_rows) {
					$query_brand = $this->db->query("SELECT cd.name FROM " . DB_PREFIX . "product_to_category ptc LEFT JOIN " . DB_PREFIX . "category_description cd ON (cd.category_id = ptc.category_id) WHERE ptc.product_id = '" . (int)$product['product_id'] . "' AND main_category = 1");
					$brand = $query_brand->row['name'];
				} else {					
					$brand = 'Все товары';
				}
			} else {
				$query_brand = $this->db->query("SELECT cd.name FROM " . DB_PREFIX . "product_to_category ptc LEFT JOIN " . DB_PREFIX . "category_description cd ON (cd.category_id = ptc.category_id) WHERE ptc.product_id = '" . (int)$product['product_id'] . "'");
				if ($query_brand->num_rows) {
					$brand = $query_brand->row['name'];
				} else {					
					$brand = 'Все товары';
				}
			}
			
            $result[] = array(
                'product_id'  => $product['product_id'],
                'thumb'       => $image,
                'name'        => $product['name'],
                'description' => utf8_substr(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                'href'        => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                'model'       => $product['model'],
                'price'       => $price,
                'special'     => $special,
                'tax'         => $tax,
                'rating'      => $rating,
                'reviews'     => sprintf($this->language->get('text_reviews'), (int)$product['reviews']),
				'price_number' => $price_number,
				'special_number' => $special_number,
				'stiker_last' => $stiker_last,
				'stiker_best' => $stiker_best,
				'stiker_spec' => $stiker_spec,
				'stiker_stock' => $stiker_stock,
				'stiker_upc' => $stiker_upc,
				'stiker_ean' => $stiker_ean,
				'stiker_jan' => $stiker_jan,
				'stiker_isbn' => $stiker_isbn,
				'stiker_mpn' => $stiker_mpn,
				'quantity' => $product['quantity'],	
				'compare_class' => $compare_class,
				'wishlist_class' => $wishlist_class,
				'button_compare' => $button_compare,
				'button_wishlist' => $button_wishlist,
				'brand' => $brand,
				'images' => $images,
				'minimum' => $product['minimum'] > 0 ? $product['minimum'] : 1
            );
        }
        return $result;
    }

    public function getFeaturedProducts($setting){
        $result = array();

        $products = explode(',', $setting['featured']);

        if (empty($setting['count'])) {
            $setting['count'] = 5;
        }

		$this->load->model('revolution/revolution');
		$settings_stikers = $this->config->get('revtheme_catalog_stiker');
		if ($settings_stikers['status']) {
			$data['stikers_status'] = true;

			if ($settings_stikers['new_status']) {
				$settings_last = $this->config->get('revtheme_catalog_last');
				$data_last = array(
					'sort'  => 'p.date_added',
					'order' => 'DESC',
					'start' => 0,
					'limit' => $settings_last['limit']
					);
				$this->load->model('catalog/product');
				$date_added = $this->model_catalog_product->getProducts($data_last);
			}

			if ($settings_stikers['best_status']) {
				$settings_best = $this->config->get('revtheme_catalog_best');
				$data_best = array(
					'sort'  => 'p.sales',
					'order' => 'DESC',
					'start' => 0,
					'limit' => $settings_best['limit']
				);
				$best_seller = $this->model_revolution_revolution->getBestProducts($data_best);
			}
		} else {
			$data['stikers_status'] = false;
		}
		
        $products = array_slice($products, 0, (int)$setting['count']);

        foreach ($products as $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
				
				if (isset($this->session->data['compare'])) {
				if (in_array($product_info['product_id'], $this->session->data['compare'])) {
					$compare_class = 'in_compare';
					$button_compare = $this->language->get('button_compare_out');
				} else {
					$compare_class = '';
					$button_compare = $this->language->get('button_compare');
				}
				} else {
					$compare_class = '';
					$button_compare = $this->language->get('button_compare');
				}
				if (isset($this->session->data['wishlist'])) {
					if (in_array($product_info['product_id'], $this->session->data['wishlist'])) {
						$wishlist_class = 'in_wishlist';
						$button_wishlist = $this->language->get('button_wishlist_out');
					} else {
						$wishlist_class = '';
						$button_wishlist = $this->language->get('button_wishlist');
					}
				} else {
					$wishlist_class = '';
					$button_wishlist = $this->language->get('button_wishlist');
				}
				if ($this->customer->isLogged()) {
					$this->load->model('account/wishlist');
					$wishlist_register = $this->model_account_wishlist->getWishlist();
					if ($wishlist_register) {
						$wishlist_register2 = array();
						foreach ($wishlist_register as $result_wishlist_register_id) {
							$wishlist_register_id[] = $result_wishlist_register_id['product_id'];
						}
						if (in_array($product_info['product_id'], $wishlist_register_id)) {
							$wishlist_class = 'in_wishlist';
							$button_wishlist = $this->language->get('button_wishlist_out');
						} else {
							$wishlist_class = '';
							$button_wishlist = $this->language->get('button_wishlist');
						}
					}
				}				
				
				if ($settings_stikers['new_status']) {
					if (isset($date_added[$product_info['product_id']])) {
						$stiker_last = true;
					} else {
						$stiker_last = false;
					}
				} else {
					$stiker_last = false;
				}
				
				if ($settings_stikers['best_status']) {
					if (isset($best_seller[$product_info['product_id']])) {
						$stiker_best = true;	
					} else {
						$stiker_best = false;
					}
				} else {
					$stiker_best = false;
				}
				
				if ($settings_stikers['spec_status']) {
					$stiker_spec = true;
				} else {
					$stiker_spec = false;
				}
				
				if ($settings_stikers['stock_status']) {
					$stiker_stock = true;
				} else {
					$stiker_stock = false;
				}
				
				if ($settings_stikers['upc']) {
					$stiker_upc = $product_info['upc'];
				} else {
					$stiker_upc = false;
				}
				if ($settings_stikers['ean']) {
					$stiker_ean = $product_info['ean'];
				} else {
					$stiker_ean = false;
				}
				if ($settings_stikers['jan']) {
					$stiker_jan = $product_info['jan'];
				} else {
					$stiker_jan = false;
				}
				if ($settings_stikers['isbn']) {
					$stiker_isbn = $product_info['isbn'];
				} else {
					$stiker_isbn = false;
				}
				if ($settings_stikers['mpn']) {
					$stiker_mpn = $product_info['mpn'];
				} else {
					$stiker_mpn = false;
				}
				
                if ($product_info['image']) {
					$images = array();
					$add_images = $this->model_catalog_product->getProductImages($product_info['product_id']);
					foreach ($add_images as $add_image) {
						$images[] = array(
							'thumb' => $this->model_tool_image->resize($add_image['image'], $setting['image_width'], $setting['image_height'])
						);
					}
                    $image = $this->model_tool_image->resize($product_info['image'], $setting['image_width'], $setting['image_height']);
                } else {
					$images = false;
                    $image = $this->model_tool_image->resize('placeholder.png', $setting['image_width'], $setting['image_height']);
                }
			
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					$price_number = $product_info['price'];
                } else {
                    $price = false;
					$price_number = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					$special_number = $product_info['special'];
                } else {
                    $special = false;
					$special_number = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $product_info['rating'];
                } else {
                    $rating = false;
                }
				
				$settings = $this->config->get('revtheme_cat_compare');
				if ($settings['main_cat']) {
					$query_brand_main_cat_true = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_info['product_id'] . "' AND main_category = 1");
					if ($query_brand_main_cat_true->num_rows) {
						$query_brand = $this->db->query("SELECT cd.name FROM " . DB_PREFIX . "product_to_category ptc LEFT JOIN " . DB_PREFIX . "category_description cd ON (cd.category_id = ptc.category_id) WHERE ptc.product_id = '" . (int)$product_info['product_id'] . "' AND main_category = 1");
						$brand = $query_brand->row['name'];
					} else {					
						$brand = 'Все товары';
					}
				} else {
					$query_brand = $this->db->query("SELECT cd.name FROM " . DB_PREFIX . "product_to_category ptc LEFT JOIN " . DB_PREFIX . "category_description cd ON (cd.category_id = ptc.category_id) WHERE ptc.product_id = '" . (int)$product_info['product_id'] . "'");
					if ($query_brand->num_rows) {
						$brand = $query_brand->row['name'];
					} else {					
						$brand = 'Все товары';
					}
				}
				
                $result[] = array(
                    'product_id'  => $product_info['product_id'],
                    'thumb'       => $image,
                    'name'        => $product_info['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                    'price'       => $price,
                    'special'     => $special,
                    'tax'         => $tax,
                    'rating'      => $rating,
                    'reviews'     => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
					'price_number' => $price_number,
					'special_number' => $special_number,
					'stiker_last' => $stiker_last,
					'stiker_best' => $stiker_best,
					'stiker_spec' => $stiker_spec,
					'stiker_stock' => $stiker_stock,
					'stiker_upc' => $stiker_upc,
					'stiker_ean' => $stiker_ean,
					'stiker_jan' => $stiker_jan,
					'stiker_isbn' => $stiker_isbn,
					'stiker_mpn' => $stiker_mpn,
					'quantity' => $product_info['quantity'],	
					'compare_class' => $compare_class,
					'wishlist_class' => $wishlist_class,
					'button_compare' => $button_compare,
					'button_wishlist' => $button_wishlist,
					'brand' => $brand,
					'images' => $images,
					'minimum' => $product_info['minimum'] > 0 ? $product_info['minimum'] : 1
                );
            }
        }
        return $result;
    }
}
?>