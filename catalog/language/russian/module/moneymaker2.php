<?php
// Text
$_['text_optional']                         = 'Опционально';
$_['text_menu']                             = 'Меню';
$_['text_compare']                          = 'Сравнение товаров';
$_['text_downloads']                        = 'Загрузки';
$_['text_quickorder_phone']                 = 'Телефон';
$_['text_quickorder_comment']               = 'Комментарий';
$_['text_quickorder_help']                  = 'Нажмите Отправить чтобы сделать заказ, и мы вам скоро перезвоним';
$_['text_quickorder_success_message']       = 'Ваш заказ #%s сформирован, мы скоро вам перезвоним';

// Buttons
$_['button_quickorder_success_message']     = 'Заказ отправлен';

// Error
$_['error_quickorder_product_undefined']    = 'ID товара не указан';
$_['error_quickorder_product_quantity']     = 'Товар не доступен в желаемом количестве или отсутстует на складе';
$_['error_quickorder_product_options']      = 'Пожалуйста выберите требуемые опции на странице товара и попробуйте снова';
$_['error_quickorder_phone']                = 'Пожалуйста укажите ваш номер телефона';
