<?php
// Heading 
$_['revsubscribe_title'] 		 = 'Подписка на новости';
$_['revsubscribe_subscribe_now'] = 'Подписаться';
$_['revsubscribe_error1'] 		 = 'Введите ваш e-mail адрес.';
$_['revsubscribe_error2'] 		 = 'Не верно введен e-mail адрес.';
$_['revsubscribe_success'] 		 = 'Поздравляем! Вы подписались на рассылку.';
$_['revsubscribe_duplicate'] 	 = 'Введенная почта уже используется.';
?>