<?php
// Text
$_['text_optional']                         = 'Optional';
$_['text_menu']                             = 'Menu';
$_['text_compare']                          = 'Product Compare';
$_['text_downloads']                        = 'Downloads';
$_['text_quickorder_phone']                 = 'Phone';
$_['text_quickorder_comment']               = 'Comment';
$_['text_quickorder_help']                  = 'Press Submit to send your order and we will call you shortly';
$_['text_quickorder_success_message']       = 'Your order #%s has been placed, we will call you shortly';

// Buttons
$_['button_quickorder_success_message']     = 'Order Sent';

// Error
$_['error_quickorder_product_undefined']    = 'Product ID is undefined';
$_['error_quickorder_product_quantity']     = 'Product is not available in the desired quantity or not in stock';
$_['error_quickorder_product_options']      = 'Please select required options on product page and try again';
$_['error_quickorder_phone']                = 'Please fill in your telephone number';
