<?php
$_['text_all_news']		            = 'All news';

$_['heading_title']                 = 'News';
$_['heading_products_title'] 		= 'Feautered products';
$_['text_blog']              		= 'News';
$_['text_empty']             		= 'No news yet.';
$_['text_sort']             		= 'Sort by:';
$_['text_name_asc']          		= 'Name (A - Z)';
$_['text_name_desc']        		= 'Name (Z - A)';
$_['text_date_desc']	    		= 'New publications';
$_['text_date_asc']		   		    = 'Old publications';
$_['text_limit']           		    = 'Display:';
$_['text_data_added']      		    = 'Date added';
$_['text_tax']      	     		= 'Tax free:';
?>