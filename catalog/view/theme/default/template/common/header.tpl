<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/moneymaker2/stylesheet/fonts.css" rel="stylesheet">
<!-- mmr2 1.0.3 ocs -->
<link href="catalog/view/theme/moneymaker2/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/theme/moneymaker2/stylesheet/stylesheet.custom.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.moneymaker2.js" type="text/javascript"></script>
<?php if ($moneymaker2_header_search_ajax) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/moneymaker2/livesearch.js"></script>
<?php } ?>
<?php if ($moneymaker2_common_browser_warned) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/moneymaker2/browser.update.js"></script>
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<header>
  <nav id="top" class="navbar navbar-default navbar-full" >
    <div class="container">
      <div class="navbar-header">
        <?php if (!$moneymaker2_header_strip_expanded) { ?>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-top-collapse, .navbar-menu-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <?php } ?>
        <div id="logo">
          <?php if ($logo) { ?>
            <?php if ($home == $og_url) { ?>
            <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />
            <?php } else { ?>
            <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
            <?php } ?>
          <?php } else { ?>
          <h2><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h2>
          <?php } ?>
        </div>
      </div>
      <div class="hidden-xs"></div>
      <div class="collapse navbar-collapse navbar-top-collapse">
        <ul class="nav navbar-nav">
          <li class="dropdown" id="contacts">
            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-<?php echo $moneymaker2_header_contacts_icon ? $moneymaker2_header_contacts_icon : 'mobile'; ?>"></i> <span class="phone"><?php echo $moneymaker2_header_contacts_phone ? $moneymaker2_header_contacts_phone : $telephone; ?></span> <?php if ($moneymaker2_header_contacts) { ?><span class="hidden-sm"><?php echo $moneymaker2_header_contacts_title; ?> </span><i class="fa fa-angle-down"></i><?php } ?></a>
            <?php if ($moneymaker2_header_contacts) { ?>
            <ul class="dropdown-menu keep-open">
              <?php foreach ($moneymaker2_header_contacts as $key => $value) { ?>
              <?php if ($value['mode']) { ?>
              <?php if ($value['mode']==1) { ?>
              <li class="dropdown-header"><?php echo $value['text']; ?></li>
              <?php } else if ($value['mode']==2) { ?>
              <?php if ($value['link']) { ?>
              <li><a href="<?php echo $value['link']; ?>" target="_blank"><?php if ($value['image']) { ?><span class="fa fa-fw fa-lg"><img src="<?php echo $value['image']; ?>" alt="<?php echo $value['text']; ?>" /></span><?php } ?> <?php echo $value['text']; ?></a></li>
              <?php } else { ?>
              <li><span><?php if ($value['image']) { ?><span class="fa fa-fw fa-lg"><img src="<?php echo $value['image']; ?>" alt="<?php echo $value['text']; ?>" /></span><?php } ?> <?php echo $value['text']; ?></span></li>
              <?php } ?>
              <?php } else if ($value['mode']==3) { ?>
              <?php if ($value['link']) { ?>
              <li><a href="<?php echo $value['link']; ?>" target="_blank"><i class="fa fa-lg fa-fw fa-<?php echo $value['icon']; ?>"></i> <?php echo $value['text']; ?></a></li>
              <?php } else { ?>
              <li><span><i class="fa fa-lg fa-fw fa-<?php echo $value['icon']; ?>"></i> <?php echo $value['text']; ?></span></li>
              <?php } ?>
              <?php } else if ($value['mode']==4) { ?>
              <li class="divider"></li>
              <?php } ?>
              <?php } ?>
              <?php } ?>
            </ul>
            <?php } ?>
          </li>
          <?php if (!$moneymaker2_header_categories_hide) { ?>
          <li class="dropdown<?php if ($moneymaker2_header_categories_mode) { ?> navbar-full-fw<?php } ?>" id="menu">
            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-th-large"></i> <?php echo $moneymaker2_header_categories_caption ? $moneymaker2_header_categories_caption : $text_category; ?> <i class="fa fa-angle-down"></i></a>
            <?php if ($categories||$moneymaker2_header_banners) { ?>
            <?php if (!$moneymaker2_header_categories_mode) { ?>
            <ul class="dropdown-menu keep-open">
              <?php foreach ($categories as $key => $category) { ?>
              <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
              <?php if (!$moneymaker2_header_categories_hidechilds) { ?>
              <?php if ($category['children']) { ?>
              <?php foreach ($category['children'] as $children) { ?>
              <li><a href="<?php echo $children['href']; ?>"><small>- <?php echo $children['name']; ?></small></a></li>
              <?php } ?>
              <?php } ?>
              <?php } ?>
              <?php if (!$moneymaker2_header_categories_hidechilds) { ?>
              <?php if ($key+1 < count($categories)) { ?>
              <li role="separator" class="divider"></li>
              <?php } ?>
              <?php } ?>
              <?php } ?>
            </ul>
            <?php } else { ?>
            <ul class="dropdown-menu keep-open">
              <li>
                <div>
                  <div class="row">
                    <?php if ($moneymaker2_header_banners) { ?>
                    <?php foreach ($moneymaker2_header_banners as $key => $value) { ?>
                    <?php $categories[] = array('image' => $value['image'], 'icon' => $value['icon'], 'name' => $value['name'], 'children' => '', 'href' => $value['link'], 'text' => $value['text'], 'style' => $value['style']); ?>
                    <?php } ?>
                    <?php } ?>
                    <?php foreach ($categories as $key => $category) { ?>
                    <ul class="col-sm-<?php echo $moneymaker2_header_categories_columns['sm'][0]; ?> col-md-<?php echo $moneymaker2_header_categories_columns['md'][0]; ?> col-lg-<?php echo $moneymaker2_header_categories_columns['lg'][0]; ?> list-unstyled">
                      <li class="text-center"><a href="<?php echo $category['href']; ?>"><?php if (!$moneymaker2_header_categories_hidethumbs) { ?><div class="hidden-xs"><img class="img-thumbnail" src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>" /></div><?php } ?><div class="btn btn-<?php if(isset($category['style'])&&$category['style']) { ?><?php echo $category['style']; ?><?php } else { ?>default<?php } ?> btn-block"><?php if ($moneymaker2_common_categories_icons_enabled&&$category['icon']) { ?><i class="fa fa-fw fa-<?php echo $category['icon']; ?>"></i><?php } ?> <?php echo $category['name']; ?></div></a></li>
                      <?php if (!$moneymaker2_header_categories_hidechilds) { ?>
                      <?php if ($category['children']) { ?>
                      <?php foreach ($category['children'] as $children) { ?>
                      <li><a class="text-muted" href="<?php echo $children['href']; ?>"><small>&ndash; <?php echo $children['name']; ?></small></a></li>
                      <?php } ?>
                      <?php } ?>
                      <?php } ?>
                      <?php if(isset($category['text'])&&$category['text']) { ?>
                      <li class="text-muted"><p><small><?php echo $category['text']; ?></small></p></li>
                      <?php } ?>
                    </ul>
                    <?php if (($key+1)%$moneymaker2_header_categories_columns['sm'][1]==0) { ?><div class="clearfix visible-sm"></div><?php } ?>
                    <?php if (($key+1)%$moneymaker2_header_categories_columns['md'][1]==0) { ?><div class="clearfix visible-md"></div><?php } ?>
                    <?php if (($key+1)%$moneymaker2_header_categories_columns['lg'][1]==0) { ?><div class="clearfix visible-lg"></div><?php } ?>
                    <?php } ?>
                  </div>
                </div>
              </li>
            </ul>
            <?php } ?>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <?php if (!$moneymaker2_common_buy_hide) { ?>
          <?php echo $cart; ?>
          <?php } ?>
          <?php echo $search; ?>
          <li class="dropdown" id="top-links">
            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-bars"></i> <span class="<?php echo $moneymaker2_menu_class; ?>"><?php echo $text_menu; ?> <i class="fa fa-angle-down"></i></span></a>
            <ul class="dropdown-menu keep-open">
              <?php if ($moneymaker2_header_menu_links_enabled) { ?>
              <li class="dropdown-header"><?php echo $moneymaker2_header_menu_links_caption; ?></li>
              <?php if ($moneymaker2_header_links) { ?>
              <?php foreach ($moneymaker2_header_links as $value) { ?>
              <li><a href="<?php echo $value['link']; ?>"><i class="fa fa-fw fa-<?php echo $value['icon']; ?>"></i> <?php echo $value['caption']; ?></a></li>
              <?php } ?>
              <li class="divider"></li>
              <?php } ?>
              <?php } ?>
              <?php echo $language; ?>
              <?php echo $currency; ?>
              <?php if (!$logged) { ?>
              <li class="dropdown-header"><?php echo $text_account; ?></li>
              <li><a href="<?php echo $login; ?>" rel="nofollow"><i class="fa fa-fw fa-sign-in"></i> <?php echo $text_login; ?> / <?php echo $text_register; ?></a></li>
              <?php } else { ?>
              <li class="dropdown-header"><?php echo $moneymaker2_text_customer_name; ?></li>
              <li><a href="<?php echo $account; ?>" rel="nofollow"><i class="fa fa-fw fa-user"></i> <?php echo $text_account; ?></a></li>
              <?php } ?>
              <?php if (!$moneymaker2_common_wishlist_hide) { ?>
              <li><a href="<?php echo $wishlist; ?>" rel="nofollow"><i class="fa fa-fw fa-heart"></i> <span id="wishlist-total"><span><?php echo $text_wishlist; ?></span></span></a></li>
              <?php } ?>
              <?php if (!$moneymaker2_common_compare_hide) { ?>
              <li><a href="<?php echo $compare; ?>" rel="nofollow"><i class="fa fa-fw fa-exchange"></i> <span id="compare-total"><?php echo $text_compare; ?></span></a></li>
              <?php } ?>
              <?php if ($logged) { ?>
              <li class="divider"></li>
              <li><a href="<?php echo $logout; ?>" rel="nofollow"><i class="fa fa-fw fa-sign-out"></i> <?php echo $text_logout; ?></a></li>
              <?php } ?>
            </ul>
          </li>
          <?php if ($moneymaker2_header_categories_menu&&$moneymaker2_header_strip_expanded) { ?>
          <li class="dropdown visible-xs">
            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="collapse" data-target=".navbar-menu-collapse"><i class="fa fa-fw fa-th-large"></i> <?php echo $moneymaker2_header_categories_caption ? $moneymaker2_header_categories_caption : $text_category; ?> <i class="fa fa-angle-down"></i></a>
          </li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </nav>
  <?php if ($moneymaker2_header_categories_menu) { ?>
  <nav id="menu" class="navbar navbar-default navbar-full">
Тест 
	<div class ="sotr2">Для вас<br> работают:</div>
	<div class ="picts"><a href ="http://egtong.ru/about_us"><img src="http://egtong.ru/image/tanya-small.JPG"></div>  <div class ="sotr">Татьяна Булагина<br>
Тел. 8-937-034-56-78 (Мегафон)</a></div>
	<div class ="picts"><a href ="http://egtong.ru/about_us"><img src="http://egtong.ru/image/yura-small.JPG"></div>  <div class ="sotr"> Юрий Булагин<br>
Тел. 8-986-736-26-74 (МТС)</a></div>
	<div class ="picts"><a href ="http://egtong.ru/about_us"><img src="http://egtong.ru/image/irina-small.jpg"></div>  <div class ="sotr"> Ирина Аксёнова<br>
Тел. 8-902-217-57-25 (Теле2)</a></div>

  </nav>
  <?php } ?>
</header>
