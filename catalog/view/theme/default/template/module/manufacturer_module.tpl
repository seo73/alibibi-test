<?php if(isset($categories)) { ?>
<div id="manufacturer_module">
  <div class="box-heading">Бренды</div>
<?php foreach ($categories as $category) { ?>
<ul>
   <li><a onclick="location.href='<?php echo $href; ?>#<?php echo $category['name']; ?>'"><?php echo $category['name']; ?></a>
   <?php if ($category['manufacturer']) { ?>
   <div>
   <ul>
      <?php for ($i = 0; $i < count($category['manufacturer']);) { ?>
		<?php $j = $i + ceil(count($category['manufacturer']) / 4); ?>
        <?php for (; $i < $j; $i++) { ?>
        <?php if (isset($category['manufacturer'][$i])) { ?>
        <li><a onclick="location.href='<?php echo $category['manufacturer'][$i]['href']; ?>'"><?php echo $category['manufacturer'][$i]['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      <?php } ?>
	  </ul>
	   </div>
      <?php } ?>
	  </li>
  </ul> 
<?php } ?>
</div>
<?php } ?>