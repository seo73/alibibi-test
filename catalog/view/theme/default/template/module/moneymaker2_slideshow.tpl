<div id="moneymaker2_slideshow<?php echo $module; ?>" class="owl-carousel owl-moneymaker2 <?php echo $navigation_style; ?>-buttons <?php echo $navigation_style; ?>-pagination" >
  <?php foreach ($banners_settings as $key => $banners_setting) { ?>
  <div class="item">
    <div class="item-text <?php echo $banners_setting['position']; ?> <?php echo $banners_setting['text_style']; ?> /*col-sm-12*/ col-md-<?php echo $banners_setting['text_width']; ?> text-center">
      <div><?php echo $banners_setting['title']; ?></div>
      <p class="hidden-xs"><?php echo $banners_setting['text']; ?></p>
      <?php if ($banners_setting['link']) { ?><p><a href="<?php echo $banners_setting['link']; ?>" class="btn btn-lg btn-<?php echo $banners_setting['btn_style']; ?>"><?php echo $banners_setting['btn_title']; ?></a></p><?php } ?>
    </div>
    <img src="<?php echo $banners_setting['image']; ?>" alt="<?php echo $banners_setting['title']; ?>" class="img-responsive" />
  </div>
  <?php } ?>
</div>
<?php if ($parallax) { ?>
<span class="hidden-xs"></span>
<div>
<style scoped>
#moneymaker2_slideshow<?php echo $module; ?> { overflow: hidden; }
@media (min-width: 320px) { #moneymaker2_slideshow<?php echo $module; ?>, #moneymaker2_slideshow<?php echo $module; ?> .owl-item .item { height: <?php echo $parallax_heights['xxxsm']; ?>px; } }
@media (min-width: 450px) { #moneymaker2_slideshow<?php echo $module; ?>, #moneymaker2_slideshow<?php echo $module; ?> .owl-item .item { height: <?php echo $parallax_heights['xxsm']; ?>px; } }
@media (min-width: 560px) { #moneymaker2_slideshow<?php echo $module; ?>, #moneymaker2_slideshow<?php echo $module; ?> .owl-item .item { height: <?php echo $parallax_heights['xsm']; ?>px; } }
@media (min-width: 768px) { #moneymaker2_slideshow<?php echo $module; ?>, #moneymaker2_slideshow<?php echo $module; ?> .owl-item .item { height: <?php echo $parallax_heights['sm']; ?>px; } }
@media (min-width: 992px) { #moneymaker2_slideshow<?php echo $module; ?>, #moneymaker2_slideshow<?php echo $module; ?> .owl-item .item { height: <?php echo $parallax_heights['md']; ?>px; } }
@media (min-width: 1200px) { #moneymaker2_slideshow<?php echo $module; ?>, #moneymaker2_slideshow<?php echo $module; ?> .owl-item .item { height: <?php echo $parallax_heights['lg']; ?>px; } }
</style>
</div>
<?php } ?>
<script type="text/javascript"><!--
  <?php if ($fullwidth) { ?>
    $('#content > #moneymaker2_slideshow<?php echo $module; ?>').detach().insertAfter( $('body header') );

    $('body').addClass('owl-moneymaker2-fullscreen owl-moneymaker2-fullscreen-<?php echo $navigation_style; ?>');

  <?php } ?>
  $('#moneymaker2_slideshow<?php echo $module; ?>').owlCarousel({
  itemsCustom : [[0, 1], [768, <?php echo $items['sm']; ?>], [992, <?php echo $items['md']; ?>], [1200, <?php echo $items['lg']; ?>]],
  <?php if ($animation) { ?>
  transitionStyle : '<?php echo $animation_in; ?>',
  <?php } ?>
  <?php if ($navigation) { ?>
  navigation: true,
  navigationText: ['<i class="fa fa-angle-left fa-2x"></i>', '<i class="fa fa-angle-right fa-2x"></i>'],
  <?php } else { ?>
  navigation: false,
  <?php } ?>
  <?php if ($pagination) { ?>
  pagination: true,
  <?php } else { ?>
  pagination: false,
  <?php } ?>
  <?php if ($autoplay) { ?>
  autoPlay: true,
  stopOnHover: true,
    <?php if ($autoplay_timeout&&$autoplay_timeout>0) { ?>
    autoPlay: <?php echo $autoplay_timeout*1000; ?>,
    <?php } ?>
  <?php } ?>
});
<?php if ($parallax) { ?>
  $(window).scroll(function () {
    if ($('.hidden-xs').is(":visible")) {
      var wScroll = $(this).scrollTop();
      $('#moneymaker2_slideshow<?php echo $module; ?> .owl-wrapper-outer img').css('-webkit-transform', 'translate(0px, -' + wScroll / 15 + '%)');
      $('#moneymaker2_slideshow<?php echo $module; ?> .owl-wrapper-outer img').css('-moz-transform', 'translate(0px, -' + wScroll / 15 + '%)');
      $('#moneymaker2_slideshow<?php echo $module; ?> .owl-wrapper-outer img').css('-ms-transform', 'translate(0px, -' + wScroll / 15 + '%)');
      $('#moneymaker2_slideshow<?php echo $module; ?> .owl-wrapper-outer img').css('-o-transform', 'translate(0px, -' + wScroll / 15 + '%)');
      $('#moneymaker2_slideshow<?php echo $module; ?> .owl-wrapper-outer img').css('transform', 'translate(0px, -' + wScroll / 15 + '%)');
    }
  });
<?php } ?>
//--></script>