<div class="rev_slider">
  <?php if($heading_title) { ?>
    <h3><i class="fa fa-pencil-square-o" aria-hidden="true"></i><?php echo $heading_title; ?></h3>
  <?php } ?>
  <?php echo $html; ?>
</div>
