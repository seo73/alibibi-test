<?php if ($viewed_products) { ?>
<div class="rev_slider">
    <h3><?php echo $heading_title; ?></h3>
    <div class="row product-layout">
        <div id="slider-viewed_products" class="owl-carousel">
        <?php foreach ($viewed_products as $product) { ?>
        <div class="col-lg-12 item">
          <div class="product-thumb product_<?php echo $product['product_id']; ?>">
			<div class="image">
				<?php if ($img_slider) { ?>		
					<div class="image owl-carousel owlproduct">
						<div class="item text-center">
							<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive <?php if ($product['quantity'] < 1 && !$zakaz) { ?>zatemnenie_img<?php } ?>" /></a>
						</div>			
						<?php if ($product['images']) { ?>
							<?php foreach ($product['images'] as $image) { ?>
								<!--noindex-->
								<div class="item text-center">
									<a href="<?php echo $product['href']; ?>"><img src="<?php echo $image['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive <?php if ($product['quantity'] < 1 && !$zakaz) { ?>zatemnenie_img<?php } ?>" rel="nofollow" /></a>
								</div>
								<!--/noindex-->
							<?php } ?>
						<?php } ?>
					</div>
				<?php } else { ?>
					<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive <?php if ($product['quantity'] < 1 && !$zakaz) { ?>zatemnenie_img<?php } ?>" /></a>
				<?php } ?>
				<?php if ($stikers_status) { ?>
					<div class="stiker_panel">
						<?php if ($product['quantity'] < 1) { ?>
							<?php if ($product['stiker_stock']) { ?>
								<span class="stiker stiker_netu"><?php echo $text_catalog_stiker_netu; ?></span>
							<?php } ?>
						<?php } ?>
						<?php if ($product['quantity'] > 0 || $zakaz) { ?>
							<?php if ($product['stiker_spec']) { ?>
								<?php if ($product['special']) { ?>
									<span class="stiker stiker_spec"><span class="price-old"><?php echo $product['price']; ?></span></span>
								<?php } ?>
							<?php } ?>	
							<?php if ($product['stiker_last']) { ?>
								<span class="stiker stiker_last"><?php echo $text_catalog_stiker_last; ?></span>
							<?php } ?>
							<?php if ($product['stiker_best']) { ?>
								<span class="stiker stiker_best"><?php echo $text_catalog_stiker_best; ?></span>
							<?php } ?>
							<?php if ($product['stiker_upc']) { ?>
								<span class="stiker stiker_user"><?php echo $product['stiker_upc']; ?></span>
							<?php } ?>
							<?php if ($product['stiker_ean']) { ?>
								<span class="stiker stiker_user"><?php echo $product['stiker_ean']; ?></span>
							<?php } ?>
							<?php if ($product['stiker_jan']) { ?>
								<span class="stiker stiker_user"><?php echo $product['stiker_jan']; ?></span>
							<?php } ?>
							<?php if ($product['stiker_isbn']) { ?>
								<span class="stiker stiker_user"><?php echo $product['stiker_isbn']; ?></span>
							<?php } ?>
							<?php if ($product['stiker_mpn']) { ?>
								<span class="stiker stiker_user"><?php echo $product['stiker_mpn']; ?></span>
							<?php } ?>
						<?php } ?>
					</div>
				<?php } ?>
				<?php if ($revpopuporder || $popup_view) { ?>
					<div class="fapanel">
						<?php if ($product['quantity'] > 0 || $zakaz) { ?>
							<?php if ($revpopuporder) { ?>
								<?php if ($product['price_number'] > 0) { ?>
									<div class="zakaz">
										<a onclick="get_revpopup_purchase('<?php echo $product['product_id']; ?>');"><i data-toggle="tooltip" data-placement="left" title="<?php echo $text_catalog_revpopup_purchase; ?>" class='fa fa-border fa-gavel'></i></a>
									</div>
								<?php } ?>
							<?php } ?>
						<?php } ?>
						<?php if ($popup_view) { ?>
							<div class="lupa">
								<a onclick="get_revpopup_view('<?php echo $product['product_id']; ?>');"><i data-toggle="tooltip" data-placement="left" title="<?php echo $text_catalog_revpopup_view; ?>" class='fa fa-border fa-eye'></i></a>
							</div>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
			<div class="caption clearfix">
				<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
				<?php if ($product['rating']) { ?>
				<div class="rating">
				  <?php for ($i = 1; $i <= 5; $i++) { ?>
				  <?php if ($product['rating'] < $i) { ?>
				  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
				  <?php } else { ?>
				  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
				  <?php } ?>
				  <?php } ?>
				</div>
				<?php } ?>
				<p class="description"><?php echo $product['description']; ?></p>
				<div class="product_buttons">
					<?php if ($product['price']) { ?>
						<?php if ($product['price_number'] > 0) { ?>
							<?php if ($product['quantity'] > 0 || $zakaz) { ?>
								<div class="price">
								<?php if ($product['price_number'] > 0) { ?>
									<?php if (!$product['special']) { ?>
										<?php echo $product['price']; ?>
									<?php } else { ?>
										<span class="price-new"><?php echo $product['special']; ?></span>
									<?php } ?>
								<?php } ?>
								</div>
							<?php } ?>
							<?php } else { ?>
								<?php if ($zakaz) { ?>
									<p class="price na_zakaz"><?php echo $text_catalog_price_na_zakaz; ?></p>
								<?php } ?>
						<?php } ?>
					<?php } ?>
					<?php if ($rev_srav_prod || $rev_wish_prod) { ?>
						<?php if ($rev_srav_prod) { ?>
							<div class="compare">
								<a class="<?php echo $product['compare_class'] ?>" data-toggle="tooltip" onclick="compare.add('<?php echo $product['product_id']; ?>', '<?php echo $product['brand']; ?>');" title="<?php echo $product['button_compare']; ?>"><i class="fa fa-border fa-bar-chart-o"></i></a>
							</div>
						<?php } ?>
						<?php if ($rev_wish_prod) { ?>
							<div class="wishlist">
								<a class="<?php echo $product['wishlist_class'] ?>" data-toggle="tooltip" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" title="<?php echo $product['button_wishlist']; ?>"><i class="fa fa-border fa-heart"></i></a>
							</div>
						<?php } ?>
					<?php $button_cart_class = 'prlistb'; } else { $button_cart_class = 'prlistb active'; }?>
					<?php if ($product['quantity'] > 0 || $zakaz) { ?>
						<?php if ($product['price_number'] > 0) { ?>
							<div class="cart">
								<a onclick="get_revpopup_cart('<?php echo $product['product_id']; ?>', 'catalog', '<?php echo $product['minimum']; ?>');" <?php if ($button_cart_class != 'prlistb active') { ?>data-toggle="tooltip" title="<?php echo $button_cart; ?>"<?php } ?>><i class="fa fa-border fa-shopping-basket"><span class="<?php echo $button_cart_class; ?>"><?php echo $button_cart; ?></span></i></a>
							</div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
				
		  </div>
        </div>
        <?php } ?>
        </div>
    </div>
<script type="text/javascript"><!--
$("#slider-viewed_products").owlCarousel({
	responsiveBaseWidth: '#slider-viewed_products',
	itemsCustom: [[0, 1], [375, 2], [750, 3], [970, 4], [1170, 4]],
	mouseDrag: true,
	touchDrag: true,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
	pagination: true
});

<?php if ($img_slider) { ?>
	$('#slider-viewed_products .owlproduct').owlCarousel({
		items: 1,
		singleItem: true,
		mouseDrag: false,
		touchDrag: false,
		autoPlay: false,
		navigation: true,
		navigationText: ['<i class="fa fa-chevron-left fa-3x"></i>', '<i class="fa fa-chevron-right fa-3x"></i>'],
		pagination: false
	});
<?php } else { ?>
	$('.owl-carousel.owlproduct').remove();
<?php } ?>
//--></script>
</div>
<?php } ?>