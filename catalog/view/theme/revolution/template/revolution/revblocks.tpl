<?php if ($blocks) { ?>
	<?php foreach ($blocks as $block) { ?>
		<div class="home_block hidden-xs col-sm-4 col-md-3 col-lg-3">
			<a href="<?php echo $block['link']; ?>">
				<div class="image"><i class="<?php echo $block['image']; ?>"></i></div>
				<div class="text">
					<span class="title"><?php echo $block['title']; ?></span>
					<p><?php echo $block['description']; ?></p>
				</div>
			</a>
		</div>
		<?php } ?>
		<?php if ($amazon && $module_class) { ?>
		<script><!--
		$('.home_block').filter(function(i){if (!(i%3)) $(this).removeClass().addClass('home_block hidden-xs col-sm-4 col-md-3 col-lg-3 col-md-offset-3 col-lg-offset-3')})
		--></script>
		<?php } ?>
<?php } ?>