<?php echo $header; ?>
<div class="container" itemtype="http://schema.org/ItemList" itemscope>
<div itemscope itemtype="http://schema.org/BreadcrumbList" style="display:none;">
<?php $position = 1; foreach ($breadcrumbs as $breadcrumb) { ?>
<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
<link itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
<meta itemprop="name" content="<?php echo $breadcrumb['text']; ?>" />
<meta itemprop="position" content="<?php echo $position; ?>" />
</div>
<?php $position++; } ?>
</div>
<ul class="breadcrumb">
<?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
<?php if($i+1<count($breadcrumbs)) { ?><li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li><?php } else { ?><?php } ?>
<?php } ?>
<li><h1 class="inbreadcrumb" itemprop="name"><?php echo $heading_title; ?></h1></li>
</ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
	<?php if ($thumb || $description) { ?>
		<div class="row category_description dnone">
		<?php if ($thumb) { ?>
		<div class="fl-l"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
		<?php } ?>
		</div>
	<?php } ?>
      <?php if ($products) { ?>
	  <meta itemprop="numberOfItems" content="<?php echo count($products); ?>" />
<div class="well well-sm">
	<div class="row">
		<div class="col-lg-3 col-md-4 col-sm-3 hidden-xs">
			<div class="btn-group btn-group-justified">
			<div class="btn-group">
				<button type="button" id="grid-view" class="btn btn-default"><i class="fa fa-th"></i></button>
			</div>
			<div class="btn-group">
				<button type="button" id="list-view" class="btn btn-default"><i class="fa fa-th-list"></i></button>
			</div>
			<div class="btn-group">
				<button type="button" id="price-view" class="btn btn-default"><i class="fa fa-align-justify"></i></button>
			</div>
			</div>
		</div>
		<br class="visible-xs">
		<div class="col-lg-6 col-md-5 col-sm-5 ">
			<div class="input-group">
				<span class="input-group-addon" ><i class="fa fa-sort-amount-asc"></i><span class="hidden-xs hidden-sm hidden-md"> <?php echo $text_sort; ?></span></span>
				<select id="input-sort" class="form-control" onchange="location = this.value;">
					<?php foreach ($sorts as $sorts) { ?>
					<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
					<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
					<?php } ?>
					<?php } ?>
				</select>
			</div>
		</div>
		<br class="visible-xs">
		<div class="col-lg-3 col-md-3 col-sm-4 ">
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-eye"></i><span class="hidden-xs hidden-sm hidden-md"> <?php echo $text_limit; ?></span></span>
				<select id="input-limit" class="form-control" onchange="location = this.value;">
					<?php foreach ($limits as $limits) { ?>
					<?php if ($limits['value'] == $limit) { ?>
					<option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
					<?php } ?>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
</div>
      <br />
<div class="row">
<?php foreach ($products as $product) { ?>
<div class="product-layout product-list col-xs-12">
  <div class="product-thumb product_<?php echo $product['product_id']; ?>" itemprop="itemListElement" itemscope itemtype="http://schema.org/Product">
	<div class="image">
		<?php if ($img_slider) { ?>		
			<div class="image owl-carousel owlproduct">
				<div class="item text-center">
					<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive <?php if ($product['quantity'] < 1 && !$zakaz) { ?>zatemnenie_img<?php } ?>" itemprop="image" /></a>
				</div>			
				<?php if ($product['images']) { ?>
					<?php foreach ($product['images'] as $image) { ?>
						<!--noindex-->
						<div class="item text-center">
							<a href="<?php echo $product['href']; ?>"><img src="<?php echo $image['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive <?php if ($product['quantity'] < 1 && !$zakaz) { ?>zatemnenie_img<?php } ?>" rel="nofollow" /></a>
						</div>
						<!--/noindex-->
					<?php } ?>
				<?php } ?>
			</div>
		<?php } else { ?>
			<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive <?php if ($product['quantity'] < 1 && !$zakaz) { ?>zatemnenie_img<?php } ?>" itemprop="image" /></a>
		<?php } ?>
		<?php if ($stikers_status) { ?>
			<div class="stiker_panel">
				<?php if ($product['quantity'] < 1) { ?>
					<?php if ($product['stiker_stock']) { ?>
						<span class="stiker stiker_netu"><?php echo $text_catalog_stiker_netu; ?></span>
					<?php } ?>
				<?php } ?>
				<?php if ($product['quantity'] > 0 || $zakaz) { ?>
					<?php if ($product['stiker_spec']) { ?>
						<?php if ($product['special']) { ?>
							<span class="stiker stiker_spec"><span class="price-old"><?php echo $product['price']; ?></span></span>
						<?php } ?>
					<?php } ?>	
					<?php if ($product['stiker_last']) { ?>
						<span class="stiker stiker_last"><?php echo $text_catalog_stiker_last; ?></span>
					<?php } ?>
					<?php if ($product['stiker_best']) { ?>
						<span class="stiker stiker_best"><?php echo $text_catalog_stiker_best; ?></span>
					<?php } ?>
					<?php if ($product['stiker_upc']) { ?>
						<span class="stiker stiker_user"><?php echo $product['stiker_upc']; ?></span>
					<?php } ?>
					<?php if ($product['stiker_ean']) { ?>
						<span class="stiker stiker_user"><?php echo $product['stiker_ean']; ?></span>
					<?php } ?>
					<?php if ($product['stiker_jan']) { ?>
						<span class="stiker stiker_user"><?php echo $product['stiker_jan']; ?></span>
					<?php } ?>
					<?php if ($product['stiker_isbn']) { ?>
						<span class="stiker stiker_user"><?php echo $product['stiker_isbn']; ?></span>
					<?php } ?>
					<?php if ($product['stiker_mpn']) { ?>
						<span class="stiker stiker_user"><?php echo $product['stiker_mpn']; ?></span>
					<?php } ?>
				<?php } ?>
			</div>
		<?php } ?>
		<?php if ($revpopuporder || $popup_view) { ?>
			<div class="fapanel">
				<?php if ($product['quantity'] > 0 || $zakaz) { ?>
					<?php if ($revpopuporder) { ?>
						<?php if ($product['price_number'] > 0) { ?>
							<div class="zakaz">
								<a onclick="get_revpopup_purchase('<?php echo $product['product_id']; ?>');"><i data-toggle="tooltip" data-placement="left" title="<?php echo $text_catalog_revpopup_purchase; ?>" class='fa fa-border fa-gavel'></i></a>
							</div>
						<?php } ?>
					<?php } ?>
				<?php } ?>
				<?php if ($popup_view) { ?>
					<div class="lupa">
						<a onclick="get_revpopup_view('<?php echo $product['product_id']; ?>');"><i data-toggle="tooltip" data-placement="left" title="<?php echo $text_catalog_revpopup_view; ?>" class='fa fa-border fa-eye'></i></a>
					</div>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
	<div class="caption clearfix">
		<h4><a href="<?php echo $product['href']; ?>" itemprop="name"><?php echo $product['name']; ?></a></h4>
		<link itemprop="url" href="<?php echo $product['href']; ?>" />
		<?php if ($product['rating']) { ?>
		<div class="rating">
		  <?php for ($i = 1; $i <= 5; $i++) { ?>
		  <?php if ($product['rating'] < $i) { ?>
		  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
		  <?php } else { ?>
		  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
		  <?php } ?>
		  <?php } ?>
		</div>
		<?php } ?>
		<p class="description" itemprop="description"><?php echo $product['description']; ?></p>
		<?php if ($revpopuporder || $popup_view) { ?>
			<div class="fapanel-price">
				<?php if ($popup_view) { ?>
					<div class="lupa">
						<a onclick="get_revpopup_view('<?php echo $product['product_id']; ?>');"><i data-toggle="tooltip" data-placement="top" title="<?php echo $text_catalog_revpopup_view; ?>" class='fa fa-border fa-eye'></i></a>
					</div>
				<?php } ?>
				<?php if ($product['quantity'] > 0 || $zakaz) { ?>
					<?php if ($revpopuporder) { ?>
						<?php if ($product['price_number'] > 0) { ?>
							<div class="zakaz">
								<a onclick="get_revpopup_purchase('<?php echo $product['product_id']; ?>');"><i data-toggle="tooltip" data-placement="left" title="<?php echo $text_catalog_revpopup_purchase; ?>" class='fa fa-border fa-gavel'></i></a>
							</div>
						<?php } ?>
					<?php } ?>
				<?php } ?>
			</div>
		<?php } ?>
		<div class="product_buttons">
			<?php if ($product['price']) { ?>
				<?php if ($product['price_number'] > 0) { ?>
					<?php if ($product['quantity'] > 0 || $zakaz) { ?>
						<div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
						<?php if ($product['price_number'] > 0) { ?>
							<?php if (!$product['special']) { ?>
								<?php echo $product['price']; ?>
								<meta itemprop="price" content="<?php echo $product['price_number']; ?>">
							<?php } else { ?>
								<span class="price-new"><?php echo $product['special']; ?></span>
								<meta itemprop="price" content="<?php echo $product['special_number']; ?>">
							<?php } ?>
							<meta itemprop="priceCurrency" content="<?php echo $currency_code; ?>">
						<?php } ?>
						</div>
					<?php } ?>
					<?php } else { ?>
						<?php if ($zakaz) { ?>
							<p class="price na_zakaz"><?php echo $text_catalog_price_na_zakaz; ?></p>
						<?php } ?>
				<?php } ?>
			<?php } ?>
			<?php if ($rev_srav_prod || $rev_wish_prod) { ?>
				<?php if ($rev_srav_prod) { ?>
					<div class="compare">
						<a class="<?php echo $product['compare_class'] ?>" data-toggle="tooltip" onclick="compare.add('<?php echo $product['product_id']; ?>', '<?php echo $product['brand']; ?>');" title="<?php echo $product['button_compare']; ?>"><i class="fa fa-border fa-bar-chart-o"></i></a>
					</div>
				<?php } ?>
				<?php if ($rev_wish_prod) { ?>
					<div class="wishlist">
						<a class="<?php echo $product['wishlist_class'] ?>" data-toggle="tooltip" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" title="<?php echo $product['button_wishlist']; ?>"><i class="fa fa-border fa-heart"></i></a>
					</div>
				<?php } ?>
			<?php $button_cart_class = 'prlistb'; } else { $button_cart_class = 'prlistb active'; }?>
			<?php if ($product['quantity'] > 0 || $zakaz) { ?>
				<?php if ($product['price_number'] > 0) { ?>
					<div class="cart">
						<a onclick="get_revpopup_cart('<?php echo $product['product_id']; ?>', 'catalog', '<?php echo $product['minimum']; ?>');" <?php if ($button_cart_class != 'prlistb active') { ?>data-toggle="tooltip" title="<?php echo $button_cart; ?>"<?php } ?>><i class="fa fa-border fa-shopping-basket"><span class="<?php echo $button_cart_class; ?>"><?php echo $button_cart; ?></span></i></a>
					</div>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
		
  </div>
</div>
<?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
	  <div class="footer-category"></div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
<script type="text/javascript"><!--
<?php if ($img_slider) { ?>
	$('.product_related .owlproduct').owlCarousel({
		items: 1,
		singleItem: true,
		mouseDrag: false,
		autoPlay: false,
		navigation: true,
		navigationText: ['<i class="fa fa-chevron-left fa-3x"></i>', '<i class="fa fa-chevron-right fa-3x"></i>'],
		pagination: false
	});
	$(document).ready(function() {
		if (localStorage.getItem('display') == 'list') {
			$('.product-thumb > .image').css('width', <?php echo $catalog_img_width; ?>);
		}
		$('.owlproduct').owlCarousel({
			beforeInit: true, 
			items: 1,
			singleItem: true,
			mouseDrag: false,
			autoPlay: false,
			navigation: true,
			navigationText: ['<i class="fa fa-chevron-left fa-3x"></i>', '<i class="fa fa-chevron-right fa-3x"></i>'],
			pagination: false
		});
		if (localStorage.getItem('display') == 'price') {
			$('.product-thumb > .image').css('width', 79);
			<?php foreach ($products as $product) { ?>
			$('.product_<?php echo $product['product_id']; ?> .owlproduct').data('owlCarousel').reinit({navigation: false});
			<?php } ?>
		}
		function podgon_img(){
			<?php foreach ($products as $product) { ?>
			$('.product_<?php echo $product['product_id']; ?> .owlproduct').data('owlCarousel').reinit({navigation: true});
			<?php } ?>
		}
		$('#grid-view').click(function() {
			$('.product-thumb > .image').css('width', 'initial');
			podgon_img()
		});
		$('#list-view').click(function() {
			$('.product-thumb > .image').css('width', <?php echo $catalog_img_width; ?>);
			podgon_img()
		});
		$('#price-view').click(function() {
			$('.product-thumb > .image').css('width', 79);
			podgon_img()
		});
		if($(window).width() < 767) {
			$('.product-thumb > .image').css('width', 'initial');
			<?php foreach ($products as $product) { ?>
			$('.product_<?php echo $product['product_id']; ?> .owlproduct').data('owlCarousel').reinit({navigation: false});
			<?php } ?>
		}
	});
<?php } else { ?>
	$('.owl-carousel.owlproduct').remove();
<?php } ?>
--></script>