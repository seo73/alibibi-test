<?php echo $header; ?>
<div class="container">
<div itemscope itemtype="http://schema.org/BreadcrumbList" style="display:none;">
<?php $position = 1; foreach ($breadcrumbs as $breadcrumb) { ?>
<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
<link itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
<meta itemprop="name" content="<?php echo $breadcrumb['text']; ?>" />
<meta itemprop="position" content="<?php echo $position; ?>" />
</div>
<?php $position++; } ?>
</div>
<ul class="breadcrumb">
<?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
<?php if($i+1<count($breadcrumbs)) { ?><li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li><?php } else { ?><?php } ?>
<?php } ?>
<li><h1 class="inbreadcrumb"><?php echo $heading_title; ?></h1></li>
</ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <label class="control-label" for="input-search"><?php echo $entry_search; ?>:</label>
      <div class="row">
	    <div class="col-sm-12">
		<div class="row">
        <div class="col-sm-5">
          <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control" />
        </div>
        <div class="col-sm-5">
          <select name="category_id" class="form-control">
            <option value="0"><?php echo $text_category; ?></option>
            <?php foreach ($categories as $category_1) { ?>
            <?php if ($category_1['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_1['children'] as $category_2) { ?>
            <?php if ($category_2['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_2['children'] as $category_3) { ?>
            <?php if ($category_3['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
		<div class="col-sm-2">
		<input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary" />
		</div>
		</div>
		</div>
		<div class="col-sm-12">
		<div class="row">
		<div class="col-sm-5">
			<label class="checkbox-inline" style="padding-top: 10px;">
			<?php if ($description) { ?>
			<input type="checkbox" name="description" value="1" id="description" checked="checked" style="margin-top: 2px;" />
			<?php } else { ?>
			<input type="checkbox" name="description" value="1" id="description" style="margin-top: 2px;" />
			<?php } ?>
			<?php echo $entry_description; ?></label>
		</div>
        <div class="col-sm-5">
			<label class="checkbox-inline" style="padding-top: 10px;">
			<?php if ($sub_category) { ?>
			<input type="checkbox" name="sub_category" value="1" checked="checked" style="margin-top: 2px;" />
			<?php } else { ?>
			<input type="checkbox" name="sub_category" value="1" style="margin-top: 2px;" />
			<?php } ?>
			<?php echo $text_sub_category; ?></label>
        </div>
		</div>
		</div>
	  </div>
      <h2><?php echo $text_search; ?></h2>
      <?php if ($products) { ?>
<div class="well well-sm">
	<div class="row">
		<div class="col-lg-3 col-md-4 col-sm-3 hidden-xs">
			<div class="btn-group btn-group-justified">
			<div class="btn-group">
				<button type="button" id="grid-view" class="btn btn-default"><i class="fa fa-th"></i></button>
			</div>
			<div class="btn-group">
				<button type="button" id="list-view" class="btn btn-default"><i class="fa fa-th-list"></i></button>
			</div>
			<div class="btn-group">
				<button type="button" id="price-view" class="btn btn-default"><i class="fa fa-align-justify"></i></button>
			</div>
			</div>
		</div>
		<br class="visible-xs">
		<div class="col-lg-6 col-md-5 col-sm-5 ">
			<div class="input-group">
				<span class="input-group-addon" ><i class="fa fa-sort-amount-asc"></i><span class="hidden-xs hidden-sm hidden-md"> <?php echo $text_sort; ?></span></span>
				<select id="input-sort" class="form-control" onchange="location = this.value;">
					<?php foreach ($sorts as $sorts) { ?>
					<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
					<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
					<?php } ?>
					<?php } ?>
				</select>
			</div>
		</div>
		<br class="visible-xs">
		<div class="col-lg-3 col-md-3 col-sm-4 ">
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-eye"></i><span class="hidden-xs hidden-sm hidden-md"> <?php echo $text_limit; ?></span></span>
				<select id="input-limit" class="form-control" onchange="location = this.value;">
					<?php foreach ($limits as $limits) { ?>
					<?php if ($limits['value'] == $limit) { ?>
					<option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
					<?php } ?>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
</div>
      <br />
      <div class="row">
<?php foreach ($products as $product) { ?>
<div class="product-layout product-list col-xs-12">
  <div class="product-thumb product_<?php echo $product['product_id']; ?>">
	<div class="image">
		<?php if ($img_slider) { ?>		
			<div class="image owl-carousel owlproduct">
				<div class="item text-center">
					<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive <?php if ($product['quantity'] < 1 && !$zakaz) { ?>zatemnenie_img<?php } ?>" /></a>
				</div>			
				<?php if ($product['images']) { ?>
					<?php foreach ($product['images'] as $image) { ?>
						<!--noindex-->
						<div class="item text-center">
							<a href="<?php echo $product['href']; ?>"><img src="<?php echo $image['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive <?php if ($product['quantity'] < 1 && !$zakaz) { ?>zatemnenie_img<?php } ?>" rel="nofollow" /></a>
						</div>
						<!--/noindex-->
					<?php } ?>
				<?php } ?>
			</div>
		<?php } else { ?>
			<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive <?php if ($product['quantity'] < 1 && !$zakaz) { ?>zatemnenie_img<?php } ?>" /></a>
		<?php } ?>
		<?php if ($stikers_status) { ?>
			<div class="stiker_panel">
				<?php if ($product['quantity'] < 1) { ?>
					<?php if ($product['stiker_stock']) { ?>
						<span class="stiker stiker_netu"><?php echo $text_catalog_stiker_netu; ?></span>
					<?php } ?>
				<?php } ?>
				<?php if ($product['quantity'] > 0 || $zakaz) { ?>
					<?php if ($product['stiker_spec']) { ?>
						<?php if ($product['special']) { ?>
							<span class="stiker stiker_spec"><span class="price-old"><?php echo $product['price']; ?></span></span>
						<?php } ?>
					<?php } ?>	
					<?php if ($product['stiker_last']) { ?>
						<span class="stiker stiker_last"><?php echo $text_catalog_stiker_last; ?></span>
					<?php } ?>
					<?php if ($product['stiker_best']) { ?>
						<span class="stiker stiker_best"><?php echo $text_catalog_stiker_best; ?></span>
					<?php } ?>
					<?php if ($product['stiker_upc']) { ?>
						<span class="stiker stiker_user"><?php echo $product['stiker_upc']; ?></span>
					<?php } ?>
					<?php if ($product['stiker_ean']) { ?>
						<span class="stiker stiker_user"><?php echo $product['stiker_ean']; ?></span>
					<?php } ?>
					<?php if ($product['stiker_jan']) { ?>
						<span class="stiker stiker_user"><?php echo $product['stiker_jan']; ?></span>
					<?php } ?>
					<?php if ($product['stiker_isbn']) { ?>
						<span class="stiker stiker_user"><?php echo $product['stiker_isbn']; ?></span>
					<?php } ?>
					<?php if ($product['stiker_mpn']) { ?>
						<span class="stiker stiker_user"><?php echo $product['stiker_mpn']; ?></span>
					<?php } ?>
				<?php } ?>
			</div>
		<?php } ?>
		<?php if ($revpopuporder || $popup_view) { ?>
			<div class="fapanel">
				<?php if ($product['quantity'] > 0 || $zakaz) { ?>
					<?php if ($revpopuporder) { ?>
						<?php if ($product['price_number'] > 0) { ?>
							<div class="zakaz">
								<a onclick="get_revpopup_purchase('<?php echo $product['product_id']; ?>');"><i data-toggle="tooltip" data-placement="left" title="<?php echo $text_catalog_revpopup_purchase; ?>" class='fa fa-border fa-gavel'></i></a>
							</div>
						<?php } ?>
					<?php } ?>
				<?php } ?>
				<?php if ($popup_view) { ?>
					<div class="lupa">
						<a onclick="get_revpopup_view('<?php echo $product['product_id']; ?>');"><i data-toggle="tooltip" data-placement="left" title="<?php echo $text_catalog_revpopup_view; ?>" class='fa fa-border fa-eye'></i></a>
					</div>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
	<div class="caption clearfix">
		<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
		<?php if ($product['rating']) { ?>
		<div class="rating">
		  <?php for ($i = 1; $i <= 5; $i++) { ?>
		  <?php if ($product['rating'] < $i) { ?>
		  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
		  <?php } else { ?>
		  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
		  <?php } ?>
		  <?php } ?>
		</div>
		<?php } ?>
		<p class="description"><?php echo $product['description']; ?></p>
		<?php if ($revpopuporder || $popup_view) { ?>
			<div class="fapanel-price">
				<?php if ($popup_view) { ?>
					<div class="lupa">
						<a onclick="get_revpopup_view('<?php echo $product['product_id']; ?>');"><i data-toggle="tooltip" data-placement="top" title="<?php echo $text_catalog_revpopup_view; ?>" class='fa fa-border fa-eye'></i></a>
					</div>
				<?php } ?>
				<?php if ($product['quantity'] > 0 || $zakaz) { ?>
					<?php if ($revpopuporder) { ?>
						<?php if ($product['price_number'] > 0) { ?>
							<div class="zakaz">
								<a onclick="get_revpopup_purchase('<?php echo $product['product_id']; ?>');"><i data-toggle="tooltip" data-placement="left" title="<?php echo $text_catalog_revpopup_purchase; ?>" class='fa fa-border fa-gavel'></i></a>
							</div>
						<?php } ?>
					<?php } ?>
				<?php } ?>
			</div>
		<?php } ?>
		<div class="product_buttons">
			<?php if ($product['price']) { ?>
				<?php if ($product['price_number'] > 0) { ?>
					<?php if ($product['quantity'] > 0 || $zakaz) { ?>
						<div class="price">
						<?php if ($product['price_number'] > 0) { ?>
							<?php if (!$product['special']) { ?>
								<?php echo $product['price']; ?>
							<?php } else { ?>
								<span class="price-new"><?php echo $product['special']; ?></span>
							<?php } ?>
						<?php } ?>
						</div>
					<?php } ?>
					<?php } else { ?>
						<?php if ($zakaz) { ?>
							<p class="price na_zakaz"><?php echo $text_catalog_price_na_zakaz; ?></p>
						<?php } ?>
				<?php } ?>
			<?php } ?>
			<?php if ($rev_srav_prod || $rev_wish_prod) { ?>
				<?php if ($rev_srav_prod) { ?>
					<div class="compare">
						<a class="<?php echo $product['compare_class'] ?>" data-toggle="tooltip" onclick="compare.add('<?php echo $product['product_id']; ?>', '<?php echo $product['brand']; ?>');" title="<?php echo $product['button_compare']; ?>"><i class="fa fa-border fa-bar-chart-o"></i></a>
					</div>
				<?php } ?>
				<?php if ($rev_wish_prod) { ?>
					<div class="wishlist">
						<a class="<?php echo $product['wishlist_class'] ?>" data-toggle="tooltip" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" title="<?php echo $product['button_wishlist']; ?>"><i class="fa fa-border fa-heart"></i></a>
					</div>
				<?php } ?>
			<?php $button_cart_class = 'prlistb'; } else { $button_cart_class = 'prlistb active'; }?>
			<?php if ($product['quantity'] > 0 || $zakaz) { ?>
				<?php if ($product['price_number'] > 0) { ?>
					<div class="cart">
						<a onclick="get_revpopup_cart('<?php echo $product['product_id']; ?>', 'catalog', '<?php echo $product['minimum']; ?>');" <?php if ($button_cart_class != 'prlistb active') { ?>data-toggle="tooltip" title="<?php echo $button_cart; ?>"<?php } ?>><i class="fa fa-border fa-shopping-basket"><span class="<?php echo $button_cart_class; ?>"><?php echo $button_cart; ?></span></i></a>
					</div>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
		
  </div>
</div>
<?php } ?>
      </div>
      <div class="row">
		<div class="pagpages clearfix">
			<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
			<div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>
<script type="text/javascript"><!--
<?php if ($img_slider) { ?>
	$('.product_related .owlproduct').owlCarousel({
		items: 1,
		singleItem: true,
		mouseDrag: false,
		autoPlay: false,
		navigation: true,
		navigationText: ['<i class="fa fa-chevron-left fa-3x"></i>', '<i class="fa fa-chevron-right fa-3x"></i>'],
		pagination: false
	});
	$(document).ready(function() {
		if (localStorage.getItem('display') == 'list') {
			$('.product-thumb > .image').css('width', <?php echo $catalog_img_width; ?>);
		}
		$('.owlproduct').owlCarousel({
			beforeInit: true, 
			items: 1,
			singleItem: true,
			mouseDrag: false,
			autoPlay: false,
			navigation: true,
			navigationText: ['<i class="fa fa-chevron-left fa-3x"></i>', '<i class="fa fa-chevron-right fa-3x"></i>'],
			pagination: false
		});
		if (localStorage.getItem('display') == 'price') {
			$('.product-thumb > .image').css('width', 79);
			<?php foreach ($products as $product) { ?>
			$('.product_<?php echo $product['product_id']; ?> .owlproduct').data('owlCarousel').reinit({navigation: false});
			<?php } ?>
		}
		function podgon_img(){
			<?php foreach ($products as $product) { ?>
			$('.product_<?php echo $product['product_id']; ?> .owlproduct').data('owlCarousel').reinit({navigation: true});
			<?php } ?>
		}
		$('#grid-view').click(function() {
			$('.product-thumb > .image').css('width', 'initial');
			podgon_img()
		});
		$('#list-view').click(function() {
			$('.product-thumb > .image').css('width', <?php echo $catalog_img_width; ?>);
			podgon_img()
		});
		$('#price-view').click(function() {
			$('.product-thumb > .image').css('width', 79);
			podgon_img()
		});
		if($(window).width() < 767) {
			$('.product-thumb > .image').css('width', 'initial');
			<?php foreach ($products as $product) { ?>
			$('.product_<?php echo $product['product_id']; ?> .owlproduct').data('owlCarousel').reinit({navigation: false});
			<?php } ?>
		}
	});
<?php } else { ?>
	$('.owl-carousel.owlproduct').remove();
<?php } ?>
--></script>