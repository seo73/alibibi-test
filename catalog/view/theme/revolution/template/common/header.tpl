<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="iexpl"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="iexpl"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="<?php echo $class; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<meta property="og:image" content="<?php echo $logo; ?>" />
<meta property="og:site_name" content="<?php echo $name; ?>" />
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<link href="catalog/view/javascript/revolution/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/revolution/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/javascript/revolution/nprogress.css" rel="stylesheet">
<link href="catalog/view/javascript/revolution/jasny-bootstrap.min.css" rel="stylesheet">
<link href="catalog/view/javascript/revolution/magnific-popup.css" rel="stylesheet" media="screen" />
<link href="catalog/view/javascript/revolution/owl-carousel/owl.carousel.css" type="text/css" rel="stylesheet" media="screen">
<style>
<?php if ($stikers_settings['last_color']) { ?>
.image .stiker_last {color: #<?php echo $stikers_settings['last_color_text']; ?>; background-color: #<?php echo $stikers_settings['last_color']; ?>; }
<?php } ?>
<?php if ($stikers_settings['best_color']) { ?>
.image .stiker_best {color: #<?php echo $stikers_settings['best_color_text']; ?>; background-color: #<?php echo $stikers_settings['best_color']; ?>; }
<?php } ?>
<?php if ($stikers_settings['spec_color']) { ?>
.image .stiker_spec {color: #<?php echo $stikers_settings['spec_color_text']; ?>; background-color: #<?php echo $stikers_settings['spec_color']; ?>; }
<?php } ?>
<?php if ($stikers_settings['user_color']) { ?>
.image .stiker_user {color: #<?php echo $stikers_settings['user_color_text']; ?>; background-color: #<?php echo $stikers_settings['user_color']; ?>; }
<?php } ?>
#top7 {background-color: #<?php echo $b_color_home_blog; ?>;}
.rev_slider.rev_blog_mod h3 {background-color: #<?php echo $b_color_home_blog; ?>;}
.rev_slider.rev_blog_mod .owl-pagination {background-color: #<?php echo $b_color_home_blog; ?>;}
.rev_slider.rev_blog_mod .vertical-sreview {margin-bottom: 2px;}
<?php if ($b_color_home_blog != 'FFFFFF') { ?>
.rev_slider.rev_blog_mod .vertical-sreview {border: none;}
#top7 {padding: 30px 0 15px 0; margin-bottom: 20px;}
<?php } ?>
<?php if ($b_color_home_blog == 'FFFFFF') { ?>
.rev_slider.rev_blog_mod {margin-top: 0;}
<?php } ?>
<?php if ($b_color_home_slideshow) { ?>
#top4 {background-color: #<?php echo $b_color_home_slideshow; ?>;}
<?php } ?>
<?php if ($color_selecta) { ?>
::selection {background-color: #<?php echo $color_selecta; ?>;}
<?php } ?>
<?php if ($user_styles) { ?>
<?php echo $user_styles; ?>
<?php } ?>
</style>
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body>
<?php if ($microdata_status) { ?>
	<div itemscope itemtype="http://schema.org/Organization" style="display:none;">
		<meta itemprop="name" content="<?php echo $name; ?>" />
		<link itemprop="url" href="<?php echo $og_url; ?>" />
		<link itemprop="logo" href="<?php echo $logo; ?>" />
		<?php if($description) { ?>
			<meta itemprop="description" content="<?php echo $description; ?>" />
		<?php } ?>
		<?php if ($microdata_postcode && $microdata_city && $microdata_adress) { ?>
			<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<meta itemprop="postalCode" content="<?php echo $microdata_postcode; ?>" />
				<meta itemprop="addressLocality" content="<?php echo $microdata_city; ?>" />
				<meta itemprop="streetAddress" content="<?php echo $microdata_adress; ?>" />
			</div>
		<?php } ?>
		<?php if ($microdata_phones){ ?>
			<?php foreach($microdata_phones as $microdata_phone){ ?>
				<meta itemprop="telephone" content="<?php echo $microdata_phone; ?>" />
			<?php } ?>
		<?php } ?>
		<meta itemprop="email" content="<?php echo $microdata_email; ?>" />	
		<?php if ($microdata_social){ ?>
		<?php foreach($microdata_social as $microdata_soc){ ?>
		<link itemprop="sameAs" href="<?php echo $microdata_soc; ?>" />
		<?php } ?>
		<?php } ?>
	</div>
<?php } ?>
<div id="pagefader"></div>
<div id="pagefader2"></div>
<!--noindex-->
<div class="hidden-md hidden-lg">
	<nav class="mobilemenu navmenu mobcats navmenu-default navmenu-fixed-left offcanvas">
		<div class="collapse navbar-collapse navbar-ex1-collapse">
		  <ul class="nav navbar-nav">
			<li><button data-toggle="offcanvas" data-target=".mobcats" data-canvas="body"><?php echo $text_header_back; ?><i class="fa fa-chevron-right"></i></button></li>
			<?php foreach ($categories as $category) { ?>
			<?php if ($category['children']) { ?>
			<li class="dropdown">
				<a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown" rel="nofollow"><?php echo $category['name']; ?><i class="dropdown-toggle fa fa-chevron-down"></i></a>
				<div class="dropdown-menu">
					<div class="dropdown-inner">
						<?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
							<ul class="list-unstyled">
								<?php foreach ($children as $child) { ?>
									<li><a href="<?php echo $child['href']; ?>" rel="nofollow"><?php echo $child['name']; ?></a></li>
								<?php } ?>
							</ul>
						<?php } ?>
					</div>
				</div>
			</li>
			<?php } else { ?>
			<li><a href="<?php echo $category['href']; ?>" rel="nofollow"><?php echo $category['name']; ?></a></li>
			<?php } ?>
			<?php } ?>
		  </ul>
		</div>
	</nav>
	<nav class="mobilemenu navmenu moblinks navmenu-default navmenu-fixed-left offcanvas">
		<div class="collapse navbar-collapse navbar-ex1-collapse">
		  <ul class="nav navbar-nav">
			<li><button data-toggle="offcanvas" data-target=".moblinks" data-canvas="body"><?php echo $text_header_back; ?><i class="fa fa-chevron-right"></i></button></li>
			<?php foreach ($informations as $information) { ?>
				<li><a href="<?php echo $information['href']; ?>" rel="nofollow"><?php echo $information['title']; ?></a></li>
			<?php } ?>
			<?php if ($revtheme_header_links) { ?>
				<?php foreach ($revtheme_header_links as $revtheme_header_link) { ?>
					<li><a href="<?php echo $revtheme_header_link['link']; ?>" rel="nofollow"><?php echo $revtheme_header_link['title']; ?></a></li>
				<?php } ?>
			<?php } ?>
			<li class="foroppro_mob" style="display:none;"></li>
		  </ul>
		</div>
	</nav>
</div>
<!--/noindex-->
<header>
	<?php if ($informations || $revtheme_header_links || $rev_lang || $rev_curr || $rev_srav || $rev_wish || $rev_acc) { ?>
	<div id="top">
		<div class="container">
			<?php if ($informations || $revtheme_header_links) { ?>
			<div class="hidden-sm hidden-md hidden-lg mobile_info">
				<div class="navbar navbar-default pull-left">
					<button type="button" class="navbar-toggle" data-toggle="offcanvas"
					data-target=".navmenu.moblinks" data-canvas="body">
					<i class="fa fa-info" aria-hidden="true"></i>
					<span class="hidden-xs"><?php echo $text_header_information; ?></span>
					<i class="fa fa-chevron-left"></i>
					</button>
				</div>
			</div>
			<div class="top-links hidden-xs verh nav pull-left text-center">			
				<?php foreach ($informations as $information) { ?>
					<a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
				<?php } ?>
				<?php if ($revtheme_header_links) { ?>
					<?php foreach ($revtheme_header_links as $revtheme_header_link) { ?>
						<a href="<?php echo $revtheme_header_link['link']; ?>"><?php echo $revtheme_header_link['title']; ?></a>
					<?php } ?>
				<?php } ?>
				<span class="foroppro" style="display:none;"></span>
			</div>
			<?php } ?>
			<?php if ($rev_lang || $rev_curr || $rev_srav || $rev_wish || $rev_acc) { ?>
			<div class="top-links verh nav pull-right <?php if ((count($informations) + count($revtheme_header_links)) > 4) { ?>text_ico_skrit<?php } ?>">
				<ul class="list-inline">
					<?php if ($rev_lang) { ?>
					<?php echo $language; ?>
					<?php } ?>
					<?php if ($rev_curr) { ?>
					<?php echo $currency; ?>
					<?php } ?>
					<?php if ($rev_srav) { ?>
					<li>
					<a href="<?php echo $compare; ?>" class="dropdown-toggle"><span id="compare-total"><?php echo $text_compare; ?></span></a>
					</li>
					<?php } ?>
					<?php if ($rev_wish) { ?>
					<li>
					<a href="<?php echo $wishlist; ?>" class="dropdown-toggle"><span id="wishlist-total"><?php echo $text_wishlist; ?></span></a>
					</li>
					<?php } ?>
					<?php if ($rev_acc) { ?>
					<li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm"><?php echo $text_account; ?></span><i class="fa fa-chevron-down strdown"></i></a>
						<ul class="dropdown-menu dropdown-menu-right">
						<?php if ($logged) { ?>
							<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
							<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
							<li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
							<li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
							<li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
						<?php } else { ?>
							<?php if ($popup_login) { ?>
								<li><a onclick="get_revpopup_login();"><?php echo $text_login; ?></a></li>
							<?php } else { ?>
								<li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
							<?php } ?>
							<li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
						<?php } ?>
						</ul>
					</li>
					<?php } ?>
				</ul>
			</div>
			<?php } ?>
		</div>
	</div>
	<?php } ?>
	<div id="top2">
	  <div class="container">
		<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-3">
			<div id="logo">
			  <?php if ($logo) { ?>
				<?php if ($home == $og_url) { ?>
				  <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
				<?php } else { ?>
				  <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
				<?php } ?>
			  <?php } else { ?>
				<h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
			  <?php } ?>
			</div>
		</div>
		<?php if ($header_phone_number != '') { ?>
			<div class="hidden-xs hidden-sm col-xs-12 col-sm-6 col-md-6"><?php echo $search; ?></div>
			<div class="col-xs-12 col-sm-6 col-md-3 t-ac">	
				<?php 
				if ($header_phone_text == '') {
					$class_tel = 'only_tel';
				} else if ($header_phone_text2 != '') {
					$class_tel = 'dve_stroky';
				} else {
					$class_tel = '';
				}
				?>
				<div class="tel <?php echo $class_tel; ?>">
					<?php if ($header_phone_text != '') { ?>
						<span class="s11"><?php echo $header_phone_text; ?></span>
						<?php if ($header_phone_text2) { ?>
							<span class="s12"><?php echo $header_phone_text2; ?></span>
						<?php } ?>
					<?php } ?>
					<button type="button" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
					<span class="s22">
						<?php if ($header_phone_cod) { ?>
							<span class="telefon"><?php echo $header_phone_cod; ?></span> 
						<?php } ?>
						<?php echo $header_phone_number; ?>
						<?php if ($revtheme_header_popupphone || $dop_contacts) { ?>
						<i class="fa fa-chevron-down"></i>
						<?php } ?>
					</span>
					</button>
					<?php if ($revtheme_header_popupphone || $dop_contacts) { ?>
					<ul class="dropdown-menu dropdown-menu-right dop_contss">
						<?php if ($revtheme_header_popupphone) { ?>
						<li>
							<a onclick="get_revpopup_phone();"><i class="fa fa-reply-all" aria-hidden="true"></i><?php echo $text_header_revpopup_phone; ?></a>
						</li>
						<?php } ?>
						<?php if ($revtheme_header_popupphone && $dop_contacts) { ?>
						<li class="divider"></li>
						<?php } ?>
						<?php if ($dop_contacts) { ?>
							<?php foreach ($dop_contacts as $dop_contact) { ?>
								<li><span><i class="fa <?php echo $dop_contact['icon']; ?>" aria-hidden="true"></i><span><?php echo $dop_contact['number']; ?></span></span></li>
							<?php } ?>
						<?php } ?>
					</ul>	
					<?php } ?>
				</div>
			</div>
		<?php } else { ?>
			<div class="hidden-xs hidden-sm col-xs-12 col-sm-6 col-md-9"><?php echo $search; ?></div>
		<?php } ?>
		<div class="hidden-md hidden-lg col-xs-12 col-sm-12"><?php echo $search; ?></div>
		</div>
	  </div>
	</div>  
	<!--noindex-->
	<div id="top3_links" class="clearfix hidden-md hidden-lg">
		<div class="container">
			<div class="row">
				<div class="top-links col-xs-12">
						<ul class="list-inline">
							<?php foreach ($informations2 as $information) { ?>
								<li><a href="<?php echo $information['href']; ?>" rel="nofollow"><?php echo $information['title']; ?></a></li>
							<?php } ?>
							<?php if ($revtheme_header_links2) { ?>
								<?php foreach ($revtheme_header_links2 as $revtheme_header_link2) { ?>
									<li><a href="<?php echo $revtheme_header_link2['link']; ?>" rel="nofollow"><?php echo $revtheme_header_link2['title']; ?></a></li>
								<?php } ?>
							<?php } ?>
						</ul>
				</div>
			</div>
		</div>
	</div>
	<!--/noindex-->
	<div id="top3" class="clearfix">
		<div class="container">
			<div class="row">
				<?php if ($amazon) { ?>
				<div class="col-xs-6 col-md-3">
					<?php echo $revmenu; ?>
				</div>
				<div class="top-links nav razmivcont hidden-xs hidden-sm col-md-6">
					<ul class="list-inline">
						<?php foreach ($informations2 as $information) { ?>
							<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
						<?php } ?>
						<?php if ($revtheme_header_links2) { ?>
							<?php foreach ($revtheme_header_links2 as $revtheme_header_link2) { ?>
								<li><a href="<?php echo $revtheme_header_link2['link']; ?>"><?php echo $revtheme_header_link2['title']; ?></a></li>
							<?php } ?>
						<?php } ?>
					</ul>	
				</div>
				<?php } else { ?>
					<div class="col-xs-6 hidden-md hidden-lg">
						<div id="menu2_button" class="page-fader inhome">
							<div class="box-heading" data-toggle="offcanvas" data-target=".navmenu.mobcats" data-canvas="body"><i class="fa fa-bars"></i><?php echo $text_header_menu2_heading; ?><span class="icorightmenu"><i class="fa fa-chevron-down"></i></span></div>
						</div>
					</div>
					<div class="hidden-xs hidden-sm col-md-9">
						<nav id="menu">
							<div class="collapse navbar-collapse navbar-ex1-collapse">
							  <ul class="nav navbar-nav">
								<?php foreach ($categories as $category) { ?>
								<?php if ($category['children']) { ?>
								<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?><span class="icorightmenu"><i class="fa fa-chevron-down"></i></span></a>
								<span class="dropdown-toggle visible-xs visible-sm"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
								  <div class="mmmenu">
									  <div class="dropdown-menu">
										<div class="dropdown-inner">
										  <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
										  <ul class="list-unstyled <?php if ($category['column']) { echo 'column';} ?>">
											<?php foreach ($children as $child) { ?>
											<li><a href="<?php echo $child['href']; ?>"><i class="fa fa-level-up visible-xs visible-sm"></i><?php echo $child['name']; ?></a>
												<?php if ($child['children']) { ?>
												<span class="visible-xs visible-sm"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
													<div class="dropdown-menu">
													<div class="dropdown-inner">
													<ul class="list-unstyled">
													<?php foreach ($child['children'] as $child) { ?>
														<li><a href="<?php echo $child['href']; ?>"><i class="fa fa-minus"></i><?php echo $child['name']; ?></a></li>
													<?php } ?>
													</ul>
													</div>
													</div>
												<?php } ?>
											</li>
											<?php } ?>
										  </ul>
										  <?php } ?>
										  <?php if ($category['thumb2']) { ?>
											<img class="img_sub" src="<?php echo $category['thumb2']; ?>" alt="<?php echo $category['name']; ?>" />	
										  <?php } ?>
										</div>
										</div>
									</div>
								</li>
								<?php } else { ?>
								<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
								<?php } ?>
								<?php } ?>
								<?php if ($revtheme_header_links2) { ?>
									<?php foreach ($revtheme_header_links2 as $revtheme_header_link2) { ?>
										<li><a href="<?php echo $revtheme_header_link2['link']; ?>"><?php echo $revtheme_header_link2['title']; ?></a></li>
									<?php } ?>
								<?php } ?>		
							  </ul>
							</div>
						</nav>
						<script type="text/javascript"><!--
						$(function () {
						  $("#menu .nav > li .mmmenu").mouseenter(function(){
								$('#pagefader').fadeIn(70);
								$('body').addClass('razmiv');
						   });
							$("#menu .nav > li .mmmenu").mouseleave(function(){
								$('#pagefader').fadeOut(70);
								$('body').removeClass('razmiv');
						   });
						});
						//--></script>
					</div>
				<?php } ?>
				<div class="razmivcont col-xs-6 col-md-3">
					<?php echo $cart; ?>
				</div>		
			</div>
		</div>
	</div>
</header>
<section class="main-content">