<?php echo $header; ?>
<div id="top5" class="clearfix">
<?php if ($slideshow) { ?>
<div id="top4" class="clearfix">
	<div class="container">
		<div class="row">
			<?php if ($allwide && $amazon) { ?>
				<?php if ($h1_home) { ?>
					<div class="hidden-xs hidden-sm col-md-offset-3"><h1 class="home_h1"><?php echo $h1_home; ?></h1></div>
				<?php } ?>
				<div class="hidden-xs hidden-sm col-md-3"></div>
				<div class="hidden-xs col-sm-12 col-md-9"><?php echo $slideshow; ?></div>
			<?php } else { ?>
				<?php if ($h1_home) { ?>
					<div class="hidden-xs hidden-sm"><h1 class="home_h1"><?php echo $h1_home; ?></h1></div>
				<?php } ?>	
				<div class="hidden-xs col-sm-12 col-md-12"><?php echo $slideshow; ?></div>
			<?php } ?>
		</div>
	</div>
</div>
<?php } else { ?>
	<?php if ($h1_home) { ?>
		<div class="container">
			<div class="row">
				<?php if ($allwide && $amazon) { ?>
				<div class="hidden-xs hidden-sm col-md-offset-3"><h1 class="home2_h1"><?php echo $h1_home; ?></h1></div>
				<?php } else { ?>
				<div class="hidden-xs hidden-sm"><h1 class="home2_h1"><?php echo $h1_home; ?></h1></div>
				<?php } ?>
			</div>
		</div>
	<?php } ?>
<?php } ?>
<?php if ($blocks) { ?>
<div id="top6" class="clearfix">
	<div class="container">
		<div class="row">
			<?php echo $blocks; ?>
		</div>
	</div>
</div>	
<?php } else { ?>
<?php if ($allwide && $amazon) { ?>
<div style="height:70px; width:100%; clear:both"></div>
<?php } else { ?>
<div style="height:30px; width:100%; clear:both"></div>
<?php } ?>
<?php } ?>
</div>

<div class="container">

<div class="ob3" style="">
<div class="sotr2">Для Вас <br>работают:</div>
<div class="sotr"><a href="/about_us"><img src="/image/tanya-small.JPG">Татьяна Булагина<br>Тел. 8-937-034-56-78 (Мегафон)</a></div>
<div class="sotr" style="display:none;"><a href="/about_us"><img src="/image/sveta-small.JPG">Светлана Новикова<br>Тел. 8-960-372-12-96 (Билайн)</a></div>
<div class="sotr"><a href="/about_us"><img src="/image/yura-small.JPG">Юрий Булагин<br>Тел. 8-986-736-26-74 (МТС)</a></div>
<div class="sotr"><a href="/about_us"><img src="/image/irina-small.jpg">Ирина Аксёнова<br>Тел. 8-902-217-57-25 (Теле2)</a></div>
<div style="clear:both;"></div>
</div>

	<div class="row">
	<?php echo $column_left; ?>
	
	<?php if ($column_left && $column_right) { ?>
	<?php $class = 'col-sm-6'; ?>
	<?php } elseif ($column_left || $column_right) { ?>
	<?php $class = 'col-sm-9'; ?>
	<?php } else { ?>
	<?php $class = 'col-sm-12'; ?>
	<?php } ?>
	
	<div id="content" class="<?php echo $class; ?>">
	
	<?php echo $content_top; ?>
	
	<?php echo $pbest; ?>
	<?php echo $pspec; ?>
	<?php echo $plast; ?>
	<?php echo $slider_1; ?>
	<?php echo $slider_2; ?>
	<?php echo $slider_3; ?>
	<?php echo $slider_4; ?>
	<?php echo $slider_5; ?>
	
	<?php if ($blog) { ?>
	</div>
	</div>
	</div>
	<div id="top7" class="clearfix">
	<div class="container">
	<?php echo $blog; ?>
	</div>
	</div>
	<div class="container">
	<div class="row">
	<div class="col-sm-12">
	<?php } ?>

	<?php if ($aboutstore) { ?>
	<?php if ($socv) {$aboutclass1="col-sm-12 col-md-8 col-lg-9";$aboutclass2="col-sm-12 col-md-4 col-lg-3";} else {$aboutclass1="col-sm-12";$aboutclass2="";} ?>
	<div class="row">
	<div class="<?php echo $aboutclass1; ?>">
	<?php echo $aboutstore; ?>
	</div>
	<div class="<?php echo $aboutclass2; ?>">
	<?php echo $socv; ?>
	</div>
	</div>
	<?php } ?>
	
	<?php echo $storereview; ?>
	<?php echo $viewed_products; ?>
	
	<?php echo $content_bottom; ?>
	
	</div>
	<?php echo $column_right; ?>
	</div>
</div>

<?php echo $footer; ?>