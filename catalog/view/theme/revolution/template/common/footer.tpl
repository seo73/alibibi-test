</section>
<footer>
<?php echo $revsubscribe; ?>
  <div class="container">
    <div class="row">
	<?php if ($informations || $revtheme_footer_links) { ?>
	<?php $class_informations = 'col-sm-12' ?>
	<?php if (($informations || $revtheme_footer_links) && $revtheme_footer_socs) { $class_informations = 'col-sm-8'; } ?>
	<div class="footer_links <?php echo $class_informations; ?> col-xs-12">
		<?php if ($informations) { ?>
			<?php foreach ($informations as $information) { ?>
				<a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
			<?php } ?>
		<?php } ?>
		<?php if ($revtheme_footer_links) { ?>
			<?php foreach ($revtheme_footer_links as $revtheme_footer_link) { ?>
				<a href="<?php echo $revtheme_footer_link['link']; ?>"><?php echo $revtheme_footer_link['title']; ?></a>
			<?php } ?>
		<?php } ?>
	</div>
	<?php } ?>
	<?php if ($revtheme_footer_socs) { ?>
	<?php $class_footer_socs = 'col-sm-12' ?>
	<?php if (($informations || $revtheme_footer_links) && $revtheme_footer_socs) { $class_footer_socs = 'col-sm-4'; } ?>
	<div class="soc_s <?php echo $class_footer_socs; ?> col-xs-12">
		<?php foreach ($revtheme_footer_socs as $revtheme_footer_soc) { ?>
			<a href="<?php echo $revtheme_footer_soc['link']; ?>"><i class="<?php echo $revtheme_footer_soc['image']; ?>" data-toggle="tooltip" title="<?php echo $revtheme_footer_soc['title']; ?>"></i></a>
		<?php } ?>
	</div>
	<?php } ?>
	<?php if ($revtheme_footer_icons) { ?>
	<?php $style_powered = 'text-align:right'; $class_powered = 'col-sm-6' ?>
	<div class="ficons col-sm-6 col-xs-12">
		<?php foreach ($revtheme_footer_icons as $revtheme_footer_icon) { ?>
			<span class="revtheme_footer_icon">
				<img src="<?php echo $revtheme_footer_icon['image']; ?>" alt=""/>
			</span>
		<?php } ?>
	</div>
	<?php } else { ?>
	<?php $style_powered = 'text-align:left'; $class_powered = 'col-sm-12' ?>
	<?php } ?>
	<div style="<?php echo $style_powered; ?>" class="powered <?php echo $class_powered; ?> col-xs-12"><?php echo $powered; ?></div>	
    </div>
  </div>
</footer>
<?php if ($popup_phone) { ?>
<div class="popup-phone-wrapper" data-toggle="tooltip" data-placement="left"  title="<?php echo $text_footer_popup_phone_tooltip; ?>">
	<span class="scroll-top-inner">
		<i class="fa fa-2x fa-phone"></i>
	</span>
</div>
<?php } ?>
<?php if ($in_top) { ?>
<div class="scroll-top-wrapper ">
	<span class="scroll-top-inner">
		<i class="fa fa-2x fa-arrow-circle-up"></i>
	</span>
</div>
<?php } ?>
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/revolution/common.js" type="text/javascript"></script>
<script type="text/javascript"><!--
podgon_fona();
<?php if ($n_progres) { ?>
NProgress.start();
$(window).load(function() {
	NProgress.done();
});
<?php } ?>

<?php if ($sticky) { ?>
if($(window).width() > 768) {
	$('#top3').affix({
		offset: {
			top: $('#top').outerHeight()+$('#top2').outerHeight()+$('html.common-home #menu2.inhome').outerHeight()
		}
	});
}
<?php } ?>

$(function() {
	podgon_fona();
	$(window).resize(podgon_fona);
});
function podgon_fona() {
	var h_top5 = $('.inhome #menu2').outerHeight();
	if (h_top5) {
		$('#top5').css('min-height', h_top5+20);
	}
	<?php if ($up_menu_height) { ?>
	var h_top4 = $('#top4').outerHeight();
	$('html.common-home #menu2.inhome').css('min-height', h_top4+50);
	<?php } ?>
	var m2inh = $('html.common-home #menu2.inhome').outerHeight();
	$('html.common-home #menu2.inhome .podmenu2').css('height', m2inh);
	var m2inhw = $('html.common-home #menu2_button').outerWidth();
	$('html.common-home #menu2.inhome .podmenu2').css('min-width', m2inhw-0.5);
	<?php if ($sticky) { ?>
	var h_top3 = $('#top3').outerHeight();
	$('.main-content').css('margin-top', h_top3+25);
	<?php } ?>
}

$(document).on( 'scroll', function(){
	if ($(window).scrollTop() > 100) {
		$('.scroll-top-wrapper').addClass('show');
	} else {
		$('.scroll-top-wrapper').removeClass('show');
	}
});

$('.scroll-top-wrapper').on('click', scrollToTop);
$('.popup-phone-wrapper').on('click', get_revpopup_phone);
function scrollToTop() {
	verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
	element = $('body');
	offset = element.offset();
	offsetTop = offset.top;
	$('html, body').animate({scrollTop: offsetTop}, 200, 'linear');
};
function get_revpopup_notification(m_class, m_header, message) {
	if (document.body.scrollHeight > document.body.offsetHeight) {
		$('#top3').css('right', '8.5px');
	}
	$.magnificPopup.open({
		removalDelay: 300,
		callbacks: {
			beforeOpen: function() {
			   this.st.mainClass = 'mfp-zoom-in';
			},
			open: function() {
				$('body').addClass('razmiv2');
				$('#pagefader2').fadeIn(70);
			}, 
			close: function() {
				$('body').removeClass('razmiv2');
				$('#pagefader2').fadeOut(70);
				$('#top3').css('right', 'initial');
			}
		},
		tLoading: '',
		items: {
			src: $('<div class="popup_notification"><div class="popup_notification_heading '+m_class+'">'+m_header+'</div><div class="popup_notification_message">'+message+'</div></div>'),
			type: 'inline'
		}
	});
}
function get_revpopup_phone() {
	if (document.body.scrollHeight > document.body.offsetHeight) {
		$('#top3').css('right', '8.5px');
	}
	$.magnificPopup.open({
		removalDelay: 300,
		callbacks: {
			beforeOpen: function() {
				this.st.mainClass = 'mfp-zoom-in';
				$('.dropdown-menu.dop_contss').fadeOut(70);
			},
			open: function() {
				$('body').addClass('razmiv2');
				$('#pagefader2').fadeIn(70);
			}, 
			close: function() {
				$('body').removeClass('razmiv2');
				$('#pagefader2').fadeOut(70);
				$('#top3').css('right', 'initial');
				$('.dropdown-menu.dop_contss').css('display', '');
			}
		},
		tLoading: '',
		items: {
			src: 'index.php?route=revolution/revpopupphone',
			type: 'ajax'
		}
	});
}
function get_revpopup_view(product_id) {
	if (document.body.scrollHeight > document.body.offsetHeight) {
		$('#top3').css('right', '8.5px');
	}
	$.magnificPopup.open({
		removalDelay: 300,
		callbacks: {
			beforeOpen: function() {
			   this.st.mainClass = 'mfp-zoom-in';
			},
			open: function() {
				$('body').addClass('razmiv2');
				$('#pagefader2').fadeIn(70);
			},
			close: function() {
				$('body').removeClass('razmiv2');
				$('#pagefader2').fadeOut(70);
				$('#top3').css('right', 'initial');
			}
		},
		tLoading: '',
		items: {
			src: 'index.php?route=revolution/revpopupview&product_id='+product_id,
			type: 'ajax'
		}
	});
}
function get_revpopup_purchase(product_id) {
	if (document.body.scrollHeight > document.body.offsetHeight) {
		$('#top3').css('right', '8.5px');
	}
	$.magnificPopup.open({
		removalDelay: 300,
		callbacks: {
			beforeOpen: function() {
			   this.st.mainClass = 'mfp-zoom-in';
			},
			open: function() {
				$('body').addClass('razmiv2');
				$('#pagefader2').fadeIn(70);
			}, 
			close: function() {
				$('body').removeClass('razmiv2');
				$('#pagefader2').fadeOut(70);
				$('#top3').css('right', 'initial');
			}
		},
		tLoading: '',
		items: {
			src: 'index.php?route=revolution/revpopuporder&product_id='+product_id,
			type: 'ajax'
		}
	});
}

function get_revpopup_cart( product_id, action, quantity ) {
	quantity = typeof(quantity) != 'undefined' ? quantity : 1;
	if ( action == "catalog" ) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: 'product_id=' + product_id + '&quantity=' + quantity,
			dataType: 'json',
			beforeSend: function(){
				$('body').addClass('razmiv2');
				$('#pagefader2').fadeIn(70);
			},
			success: function( json ) {
				$('.alert, .text-danger').remove();
				if ( json['redirect'] ) {
					location = json['redirect'];
				}
				if ( json['success'] ) {
					if (document.body.scrollHeight > document.body.offsetHeight) {
						$('#top3').css('right', '8.5px');
					}
					$.magnificPopup.open({
					removalDelay: 300,
					callbacks: {
						beforeOpen: function() {
						   this.st.mainClass = 'mfp-zoom-in';
						},
						close: function() {
							$('body').removeClass('razmiv2');
							$('#pagefader2').fadeOut(70);
							$('#top3').css('right', 'initial');
						}
					},
					tLoading: '',
					items: {
						src: 'index.php?route=revolution/revpopupcart',
						type: 'ajax'
					}
					});
					$('#cart-total' ).html(json['total']);
					$('#cart-total-popup').html(json['total']);
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			}
		});
	}
	if ( action == "product" ) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
			dataType: 'json',
			beforeSend: function(){
				$('body').addClass('razmiv2');
				$('#pagefader2').fadeIn(70);
			},
			success: function( json ) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			$('.success, .warning, .attention, information, .error').remove();
				
				if (json['error']) {
					$('body').removeClass('razmiv2');
					$('#pagefader2').fadeOut(70);
					if (json['error']['option']) {
						for (i in json['error']['option']) {
							$('#input-option' + i).before('<span class="error bg-danger">' + json['error']['option'][i] + '</span>');
						}
					}
				}
				if ( json['success'] ) {
					if (document.body.scrollHeight > document.body.offsetHeight) {
						$('#top3').css('right', '8.5px');
					}
					$.magnificPopup.open({
						removalDelay: 300,
						callbacks: {
							beforeOpen: function() {
							   this.st.mainClass = 'mfp-zoom-in';
							},
							close: function() {
								$('body').removeClass('razmiv2');
								$('#pagefader2').fadeOut(70);
								$('#top3').css('right', 'initial');
							}
						},
						tLoading: '',
						items: {
							src: 'index.php?route=revolution/revpopupcart',
							type: 'ajax'
						}
					});
					$('#cart-total' ).html(json['total']);
					$('#cart-total-popup').html(json['total']);
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			}
		});
	}
	if ( action == "popup_product" ) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: $('#popup-view-wrapper .product-info input[type=\'text\'], #popup-view-wrapper .product-info input[type=\'hidden\'], #popup-view-wrapper .product-info input[type=\'radio\']:checked, #popup-view-wrapper .product-info input[type=\'checkbox\']:checked, #popup-view-wrapper .product-info select, #popup-view-wrapper .product-info textarea'),
			dataType: 'json',
			beforeSend: function(){
				$('body').addClass('razmiv2');
				$('#pagefader2').fadeIn(70);
			},
			success: function( json ) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			$('.success, .warning, .attention, information, .error').remove();
				
				if (json['error']) {
					$('body').removeClass('razmiv2');
					$('#pagefader2').fadeOut(70);
					if (json['error']['option']) {
						for (i in json['error']['option']) {
							$('#input-option' + i).before('<span class="error bg-danger">' + json['error']['option'][i] + '</span>');
						}
					}
				}
				if ( json['success'] ) {
					if (document.body.scrollHeight > document.body.offsetHeight) {
						$('#top3').css('right', '8.5px');
					}
					$.magnificPopup.open({
						removalDelay: 300,
						callbacks: {
							beforeOpen: function() {
							   this.st.mainClass = 'mfp-zoom-in';
							},
							close: function() {
								$('body').removeClass('razmiv2');
								$('#pagefader2').fadeOut(70);
								$('#top3').css('right', 'initial');
							}
						},
						tLoading: '',
						items: {
							src: 'index.php?route=revolution/revpopupcart',
							type: 'ajax'
						}
					});
					$('#cart-total' ).html(json['total']);
					$('#cart-total-popup').html(json['total']);
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			}
		});
	}
	if ( action == "show_cart" ) {
		if (document.body.scrollHeight > document.body.offsetHeight) {
			$('#top3').css('right', '8.5px');
		}
		$.magnificPopup.open({
			removalDelay: 300,
			callbacks: {
				beforeOpen: function() {
				   this.st.mainClass = 'mfp-zoom-in';
				},
				open: function() {
					$('body').addClass('razmiv2');
					$('#pagefader2').fadeIn(70);
				}, 
				close: function() {
					$('body').removeClass('razmiv2');
					$('#pagefader2').fadeOut(70);
					$('#top3').css('right', 'initial');
				}
			},
			tLoading: '',
			items: {
				src: 'index.php?route=revolution/revpopupcart',
				type: 'ajax'
			}
		});
	}
}
function get_revpopup_login() {
	if (document.body.scrollHeight > document.body.offsetHeight) {
		$('#top3').css('right', '8.5px');
	}
	$.magnificPopup.open({
		removalDelay: 300,
		callbacks: {
			beforeOpen: function() {
			   this.st.mainClass = 'mfp-zoom-in';
			},
			open: function() {
				$('body').addClass('razmiv2');
				$('#pagefader2').fadeIn(70);
			}, 
			close: function() {
				$('body').removeClass('razmiv2');
				$('#pagefader2').fadeOut(70);
				$('#top3').css('right', 'initial');
			}
		},
		tLoading: '',
		items: {
			src: 'index.php?route=revolution/revpopuplogin',
			type: 'ajax'
		}
	});
}
$(document).on('click', '.tel .dropdown-menu', function (e) {
	$(this).hasClass('dropdown-menu-right') && e.stopPropagation(); // This replace if conditional.
});

<?php if ($modal_status) { ?>
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

if (!getCookie('revmodal')) {
	$(document).ready(setTimeout(function() {
		if (document.body.scrollHeight > document.body.offsetHeight) {
			$('#top3').css('right', '8.5px');
		}
		$.magnificPopup.open({
			removalDelay: 300,
			callbacks: {
				beforeOpen: function() {
					this.st.mainClass = 'mfp-zoom-in';
					$('.dropdown-menu.dop_contss').fadeOut(70);
				},
				open: function() {
					$('body').addClass('razmiv2');
					$('#pagefader2').fadeIn(70);
				}, 
				close: function() {
					$('body').removeClass('razmiv2');
					$('#pagefader2').fadeOut(70);
					$('#top3').css('right', 'initial');
					$('.dropdown-menu.dop_contss').css('display', '');
				}
			},
			tLoading: '',
			items: {
				src: 'index.php?route=revolution/revmodal',
				type: 'ajax'
			}
		});
	}, 1000));
}
<?php } ?>
//--></script>
<?php if ($user_scripts) { ?>
	<?php echo $user_scripts; ?>
<?php } ?>
</body></html>