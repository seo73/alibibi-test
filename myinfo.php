<?php 
if ( PHP_VERSION_ID >= 50300 )  {
  $status_php = '<b><font color="green">Все ОК</font></b>';
} else {
  $status_php = '<b><font color="red">Необходимо обновить версию PHP</font></b>';
}
if (ioncube_loader_iversion() >= 50000) {
  $status_ion = '<b><font color="green">Все ОК</font></b>';
} else {
  $status_ion = '<b><font color="red">Необходимо обновить Ioncube Loader</font></b>';
}
?>
<b>Необходима версия PHP:</b> 5.3 и выше
<br />
<b>Ваша версия PHP:</b> <?php echo phpversion() ?>
<br />
<b>Статус:</b> <?php echo $status_php; ?>
<br />
<br />
<b>Необходима версия Ioncube Loader:</b> 5.0 и выше
<br />
<b>Ваша версия:</b> <?php echo ioncube_loader_iversion(); ?>
<br />
<b>Статус:</b> <?php echo $status_ion; ?>