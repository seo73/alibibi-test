<?php
	echo '<meta http-equiv="content-type" content="text/html; charset=utf-8" />';

	require_once 'config.php';
	
	$dbhost = DB_HOSTNAME;
	$dbuser = DB_USERNAME;
	$dbpass = DB_PASSWORD;
	$dbdatabase = DB_DATABASE;
	$dbprefix = DB_PREFIX;
	
	$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbdatabase);
	if(!$conn ) die('Could not connect: ' . mysql_error());

	$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbdatabase);
		
	$table = $dbprefix . "suppler";		
	if (!getColumnName($conn, $table, "bonus")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `bonus` VARCHAR(64) NOT NULL";
		$retval = $conn->query($query);
	}	
	if (!getColumnName($conn, $table, "bprice")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `bprice` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "ddesc")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `ddesc` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "ffile")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `ffile` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "idcat")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `idcat` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "jopt")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `jopt` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "kmenu")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `kmenu` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "main")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `main` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "metka")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `metka` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "newproduct")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `newproduct` VARCHAR(5) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "onoff")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `onoff` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "opt_fotos")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `opt_fotos` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "opt_prices")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `opt_prices` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "optsku")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `optsku` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "parsq")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `parsq` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "placeq")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `placeq` VARCHAR(5) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "plusopt")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `plusopt` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "pointq")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `pointq` VARCHAR(64) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "qu_discount")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `qu_discount` VARCHAR(128) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "ratek")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `ratek` DECIMAL(12,4) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "ratep")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `ratep` DECIMAL(12,4) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "ref1")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `ref1` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "serie")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `serie` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "sleep")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `sleep` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "t_ref")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `t_ref` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "t_ref1")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `t_ref1` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "t_status")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `t_status` VARCHAR(255) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "termin")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `termin` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "usd")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `usd` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "zero")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `zero` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "cheap")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `cheap` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "spec")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `spec` VARCHAR(128) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "subfolder")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `subfolder` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "rprice")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `rprice` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "ddata")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `ddata` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "mpn")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `mpn` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "ean")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `ean` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "upc")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `upc` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "newurl")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `newurl` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "disc")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `disc` VARCHAR(12) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "refer")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `refer` VARCHAR(3) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "onn")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `onn` VARCHAR(12) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "umanuf")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `umanuf` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "off")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `off` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "stay")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `stay` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "catcreate")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `catcreate` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	if (!getColumnName($conn, $table, "pic_ext")) {
		$query = "ALTER TABLE `".$dbprefix."suppler` ADD `pic_ext` VARCHAR(1) NOT NULL";
		$retval = $conn->query($query);
	}
	
	$query = "ALTER TABLE `".$dbprefix."suppler` MODIFY `ad` VARCHAR(2) NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler` MODIFY `cheap` VARCHAR(3) NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler` MODIFY `pic_ext` VARCHAR(128) NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler` MODIFY `spec` VARCHAR(128) NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler` MODIFY `warranty` VARCHAR(512) NOT NULL";
	$retval = $conn->query($query);
	
	$query = "ALTER TABLE `".$dbprefix."suppler_data` MODIFY `cat_ext` VARCHAR(255) NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler_data` MODIFY `pic_int` VARCHAR(255) NOT NULL";
	$retval = $conn->query($query);
	
	$table = $dbprefix . "suppler_attributes";
	if (!getColumnName($conn, $table, "filter_group_id")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_attributes` ADD `filter_group_id` INT(11) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "attr_point")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_attributes` ADD `attr_point` INT(11) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "attr_point")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_attributes` ADD `attr_point` INT(11) NOT NULL";
		$retval = $conn->query($query);		
	}
	$query = "ALTER TABLE `".$dbprefix."suppler_attributes` MODIFY `attr_ext` VARCHAR(255) NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler_attributes` MODIFY `attr_point` VARCHAR(255) NOT NULL";
	$retval = $conn->query($query);
	
	$table = $dbprefix . "suppler_base_price";
	if (!getColumnName($conn, $table, "bav")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_base_price` ADD `bav` DECIMAL(12,4) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "bmax")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_base_price` ADD `bmax` DECIMAL(12,4) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "bmin")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_base_price` ADD `bmin` DECIMAL(12,4) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "market_percent_to_bdprice")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_base_price` ADD `market_percent_to_bdprice` DECIMAL(6,3) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "market_percent_to_bprice")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_base_price` ADD `market_percent_to_bprice` DECIMAL(6,3) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "market_percent_to_price")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_base_price` ADD `market_percent_to_price` DECIMAL(6,3) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "optimal")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_base_price` ADD `optimal` DECIMAL(12,4) NOT NULL";
		$retval = $conn->query($query);		
	}
	
	$table = $dbprefix . "relatedoptions";
	if (!getColumnName($conn, $table, "defaultselect")) {
		$query = "ALTER TABLE `".$dbprefix."relatedoptions` ADD `defaultselect` tinyint(1) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "defaultselectpriority")) {
		$query = "ALTER TABLE `".$dbprefix."relatedoptions` ADD `defaultselectpriority` int(11) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "model")) {
		$query = "ALTER TABLE `".$dbprefix."relatedoptions` ADD `model` VARCHAR(64) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "price")) {
		$query = "ALTER TABLE `".$dbprefix."relatedoptions` ADD `price` DECIMAL(15,4) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "weight_prefix")) {
		$query = "ALTER TABLE `".$dbprefix."relatedoptions` ADD `weight_prefix` varchar(1) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "weight")) {
		$query = "ALTER TABLE `".$dbprefix."relatedoptions` ADD `weight` decimal(15,8) NOT NULL";
		$retval = $conn->query($query);		
	}
	
	$table = $dbprefix . "suppler_options";
	if (!getColumnName($conn, $table, "art")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_options` ADD `art` varchar(3) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "foto")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_options` ADD `foto` varchar(3) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "opt_point")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_options` ADD `opt_point` varchar(128) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "opt_pref")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_options` ADD `opt_pref` varchar(1) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "opt_margin")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_options` ADD `opt_margin` varchar(1) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "opt")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_options` ADD `opt` varchar(1) NOT NULL";
		$retval = $conn->query($query);		
	}
	$query = "ALTER TABLE `".$dbprefix."suppler_options` MODIFY `opt` VARCHAR(128) NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler_options` MODIFY `pr` VARCHAR(128) NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler_options` MODIFY `opt_point` VARCHAR(128) NOT NULL";
	$retval = $conn->query($query);
	
	$table = $dbprefix . "suppler_price";
	if (!getColumnName($conn, $table, "baseprice")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_price` ADD `baseprice` INT(1) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "noprice")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_price` ADD `noprice` VARCHAR(128) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "paramnp")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_price` ADD `paramnp` VARCHAR(128) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "pointnp")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_price` ADD `pointnp` VARCHAR(128) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "mratek")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_price` ADD `mratek` DECIMAL(15,4) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "point")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_price` ADD `point` VARCHAR(128) NOT NULL";
		$retval = $conn->query($query);		
	}
	$query = "ALTER TABLE `".$dbprefix."suppler_price` MODIFY `noprice` VARCHAR(128) NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler_price` MODIFY `pointnp` VARCHAR(128) NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler_price` MODIFY `point` VARCHAR(128) NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler_price` MODIFY `ident` VARCHAR(128) NOT NULL";
	$retval = $conn->query($query);
	
	$table = $dbprefix . "suppler_ref";
	if (!getColumnName($conn, $table, "price")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_ref` ADD `price` DECIMAL(15,4) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "ident")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_ref` ADD `ident` VARCHAR(128) NOT NULL";
		$retval = $conn->query($query);		
	}
	$query = "ALTER TABLE `".$dbprefix."suppler_ref` MODIFY `ident` VARCHAR(128) NOT NULL";
	$retval = $conn->query($query);
	
	$table = $dbprefix . "suppler_seo";
	if (!getColumnName($conn, $table, "prod_h1")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_seo` ADD `prod_h1` TEXT NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "prod_keyword")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_seo` ADD `prod_keyword` VARCHAR(1000) NOT NULL";
		$retval = $conn->query($query);		
	}
	if (!getColumnName($conn, $table, "prod_photo")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_seo` ADD `prod_photo` TEXT NOT NULL";
		$retval = $conn->query($query);		
	}
	
	$query = "ALTER TABLE `".$dbprefix."suppler_seo` MODIFY `cat_meta_desc` TEXT NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler_seo` MODIFY `cat_title` TEXT NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler_seo` MODIFY `manuf_meta_desc` TEXT NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler_seo` MODIFY `manuf_title` TEXT NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler_seo` MODIFY `prod_meta_desc` TEXT NOT NULL";
	$retval = $conn->query($query);
	$query = "ALTER TABLE `".$dbprefix."suppler_seo` MODIFY `prod_title` TEXT NOT NULL";
	$retval = $conn->query($query);
	
	$table = $dbprefix . "suppler_sku_description";
	if (!getColumnName($conn, $table, "store_id")) {
		$query = "ALTER TABLE `".$dbprefix."suppler_sku_description` ADD `store_id` INT(2) NOT NULL";
		$retval = $conn->query($query);		
	}
	$query = "ALTER TABLE `".$dbprefix."suppler_sku_description` MODIFY `sku` VARCHAR(64) NOT NULL";
	$retval = $conn->query($query);
	
	$table = $dbprefix . "product_option_value";
	if (!getColumnName($conn, $table, "optsku")) {
		$query = "ALTER TABLE `".$dbprefix."product_option_value` ADD `optsku` VARCHAR(64) NOT NULL";
		$retval = $conn->query($query);		
	}	
	
	$conn->close();
	echo " The database is ready";
		
  /*******************************************************/
	function getColumnName($conn, $table, $name) {
		$query = "SELECT COLUMN_NAME FROM information_schema.columns WHERE table_name='" .$table."' AND  column_name = '". $name ."'";
		
		$retval = $conn->query($query);
		$rows = $retval->fetch_assoc();
		
		$ok = 0;
		if (isset($rows['COLUMN_NAME'])) $ok = 1;
		
		return $ok;
	}
?>	