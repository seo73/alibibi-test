<?php 
class ModelRevolutionRevsubscribe extends Model {

	public function viewsubscribers($data = array()) {
		$query =  $this->db->query("SELECT * FROM `" . DB_PREFIX . "revsubscribe`
			ORDER BY `date_created` DESC
			LIMIT ".$data['start'].", ".$data['limit']);
		return $query->rows; 
	}
	
	public function getTotalSubscriptions(){
		$query = $this->db->query("SELECT COUNT(*) as `count`  FROM `" . DB_PREFIX . "revsubscribe`");
		return $query->row['count']; 
	}

}
?>