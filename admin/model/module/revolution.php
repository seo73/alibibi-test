<?php
class ModelModuleRevolution extends Model {

	public function install() {
		// revmenu

		$this->db->query("
		ALTER TABLE `" . DB_PREFIX . "category` 
		ADD COLUMN `image2` VARCHAR(255) NULL DEFAULT NULL AFTER `parent_id`;
		");
		
		$this->db->query("
		ALTER TABLE `" . DB_PREFIX . "information` 
		ADD COLUMN `top` INT(1) NOT NULL DEFAULT '0' AFTER `bottom`, 
		ADD COLUMN `top2` INT(1) NOT NULL DEFAULT '0' AFTER `top`;
		");

		// revblog
		
		$this->db->query("
		CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "revblog` ( 
		`blog_id` int(11) NOT NULL AUTO_INCREMENT, 
		`image` varchar(255) DEFAULT NULL, 
		`date_added` datetime NOT NULL, 
		`date_modified` datetime NOT NULL, 
		`date_available` date NOT NULL DEFAULT '0000-00-00', 
		`sort_order` int(3) NOT NULL DEFAULT '0', 
		`status` tinyint(1) NOT NULL DEFAULT '1', 
		PRIMARY KEY (`blog_id`) 
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7;
		");
		
		$this->db->query("
		CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "revblog_description` ( 
		`blog_id` int(11) NOT NULL, 
		`language_id` int(11) NOT NULL, 
		`title` varchar(64) NOT NULL, 
		`title_pr` varchar(64) NOT NULL, 
		`title_products` varchar(64) NOT NULL, 
		`description` text NOT NULL, 
		`meta_title` varchar(255) NOT NULL, 
		`meta_description` varchar(255) NOT NULL, 
		`meta_keyword` varchar(255) NOT NULL, 
		PRIMARY KEY (`blog_id`,`language_id`) 
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;
		");

		$this->db->query("
		CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "revblog_to_layout` ( 
		`blog_id` int(11) NOT NULL, 
		`store_id` int(11) NOT NULL, 
		`layout_id` int(11) NOT NULL, 
		PRIMARY KEY (`blog_id`,`store_id`) 
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;
		");

		$this->db->query("
		CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "revblog_to_store` ( 
		`blog_id` int(11) NOT NULL, 
		`store_id` int(11) NOT NULL, 
		PRIMARY KEY (`blog_id`,`store_id`) 
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;
		");
		
		$this->db->query("
		CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "revblog_product` ( 
		`blog_id` int(11) NOT NULL, 
		`product_id` int(11) NOT NULL, 
		PRIMARY KEY (`blog_id`,`product_id`) 
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;
		");
		
		// revsubscribe
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "revsubscribe` (
		`subscribe_id` INT(11) NOT NULL AUTO_INCREMENT, 
		`email` VARCHAR(200) NULL DEFAULT NULL, 
		`store_id` int(11) DEFAULT NULL, 
		`date_created` DATETIME  NOT NULL DEFAULT '0000-00-00 00:00:00', 
		`language_id` VARCHAR(100) NULL DEFAULT '".$this->config->get('config_language_id')."', 
		PRIMARY KEY (`subscribe_id`));");
		
		// дополнительные вкладки в товаре
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX ."product_tab` (
		`product_tab_id` int(11) NOT NULL AUTO_INCREMENT, 
		`product_id` INT(11) NOT NULL, 
		`sort_order` TINYINT(4) NOT NULL, 
		`status` TINYINT(4) NOT NULL, 
		PRIMARY KEY(`product_tab_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8;
		");
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "product_tab_desc` (
		`product_tab_id` int(11) NOT NULL, 
		`heading` varchar(255) NOT NULL, 
		`description` text NOT NULL, 
		`product_id` int(11) NOT NULL, 
		`language_id` int(11) NOT NULL ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
		");
		
		// дополнительные вкладки для всех товаров
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX ."tabs` (
		`tab_id` int(11) NOT NULL AUTO_INCREMENT, 
		`products` text NOT NULL, 
		`categories` text NOT NULL, 
		`manufactures` text NOT NULL, 
		`ingore_products` text NOT NULL, 
		`stores` text NOT NULL, 
		`allproducts` tinyint(4) NOT NULL, 
		`status` int(11) NOT NULL, 
		`sort_order` int(11) NOT NULL, 
		`date_added` datetime NOT NULL,PRIMARY KEY (`tab_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8;
		");
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `". DB_PREFIX ."tabs_description` (
		`tab_id` int(11) NOT NULL, 
		`language_id` int(11) NOT NULL, 
		`title` varchar(255) NOT NULL, 
		`description` text NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=utf8;
		");
		
		// review_admin_ansver
		
		$this->db->query("ALTER TABLE `" . DB_PREFIX ."review` ADD COLUMN `answer` TEXT NOT NULL AFTER `text`");
		
		// video_in_product
		
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "product_image` 
		ADD COLUMN `video` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL");
		
		// popupphone_in_admin
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "revpopupphones` (
		`pphone_id` int(11) NOT NULL AUTO_INCREMENT,
		`name` varchar(64) NOT NULL,
		`telephone` varchar(64) NOT NULL,
		`email` varchar(64) NOT NULL,
		`comment` text NOT NULL,
		`status` int(1) NOT NULL DEFAULT '0',
		`date_added` datetime NOT NULL,
		PRIMARY KEY (`pphone_id`)
		) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
		");
		
		// seo_urls

		$this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` (`query`, `keyword`) VALUES ('revolution/revpbest', 'best')");
		$this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` (`query`, `keyword`) VALUES ('revolution/revplast', 'latest')");
		$this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` (`query`, `keyword`) VALUES ('revolution/revblog', 'news')");
		$this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` (`query`, `keyword`) VALUES ('revolution/revstorereview', 'otzivy')");

	}

	public function uninstall() {

		// revmenu

		$this->db->query("
		ALTER TABLE `" . DB_PREFIX . "category` 
		DROP image2
		");
		
		$this->db->query("
		ALTER TABLE `" . DB_PREFIX . "information` 
		DROP top, 
		DROP top2
		");
		
		// revblog
		
		$this->db->query("
		DROP TABLE IF EXISTS 
		`" . DB_PREFIX . "revblog`, 
		`" . DB_PREFIX . "revblog_description`, 
		`" . DB_PREFIX . "revblog_to_layout`, 
		`" . DB_PREFIX . "revblog_to_store`, 
		`" . DB_PREFIX . "revblog_product`
		");
		
		// revsubscribe
		
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "revsubscribe`");
		
		// дополнительные вкладки в товаре
		
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "product_tab`");
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "product_tab_desc`");
		
		// дополнительные вкладки для всех товаров
		
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "tabs`");
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "tabs_description`");
				
		// review_admin_ansver

		$this->db->query("ALTER TABLE `" . DB_PREFIX . "review` DROP `answer`");
		
		// video_in_product
		
		$this->db->query("
		ALTER TABLE `" . DB_PREFIX . "product_image` DROP video");
		
		// popupphone_in_admin
		
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "revpopupphones`");
		
		// seo_urls
		$this->db->query("DELETE FROM `" . DB_PREFIX . "url_alias` WHERE `query` = 'revolution/revpbest'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "url_alias` WHERE `query` = 'revolution/revplast'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "url_alias` WHERE `query` = 'revolution/revblog'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "url_alias` WHERE `query` = 'revolution/revstorereview'");
		
	}

// дополнительные вкладки для всех товаров -->

	public function addtabs($data){
		$this->event->trigger('pre.admin.tabs.add', $data);
		
		if(!empty($data['tabs_product'])){
			$data['tabs_product'] = json_encode($data['tabs_product']);
		}else{
			$data['tabs_product'] = '';
		}
		
		if(!empty($data['tabs_category'])){
			$data['tabs_category'] = json_encode($data['tabs_category']);
		}else{
			$data['tabs_category'] = '';
		}
		
		if(!empty($data['tabs_manufacturer'])){
			$data['tabs_manufacturer'] = json_encode($data['tabs_manufacturer']);
		}else{
			$data['tabs_manufacturer'] = '';
		}
		
		if(!empty($data['tabs_ingore_product'])){
			$data['tabs_ingore_product'] = json_encode($data['tabs_ingore_product']);
		}else{
			$data['tabs_ingore_product'] = '';
		}
		
		if(!empty($data['tabs_store'])){
			$data['tabs_store'] = json_encode($data['tabs_store']);
		}else{
			$data['tabs_store'] = '';
		}
		
		if(!isset($data['allproducts'])){
			$data['allproducts'] = 0;
		}
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "tabs SET sort_order = '" . (int)$data['sort_order'] . "', status = '".(int)$data['status']."', products = '".$data['tabs_product']."', categories = '".$data['tabs_category']."', manufactures = '".$data['tabs_manufacturer']."', ingore_products = '".$data['tabs_ingore_product']."',stores = '".$data['tabs_store']."', allproducts = '".(int)$data['allproducts']."', date_added = NOW()");
		
		$tab_id = $this->db->getLastId();
		
		foreach($data['tabs_description'] as $language_id => $value){
			$this->db->query("INSERT INTO " . DB_PREFIX . "tabs_description SET tab_id = '" . (int)$tab_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}

		$this->event->trigger('post.admin.tabs.add', $tab_id);

		return $tab_id;
	}
	
	public function edittabs($tab_id, $data){
		$this->event->trigger('pre.admin.tabs.edit', $data);

		if(!empty($data['tabs_product'])){
			$data['tabs_product'] = json_encode($data['tabs_product']);
		}else{
			$data['tabs_product'] = '';
		}
		
		if(!empty($data['tabs_category'])){
			$data['tabs_category'] = json_encode($data['tabs_category']);
		}else{
			$data['tabs_category'] = '';
		}
		
		if(!empty($data['tabs_manufacturer'])){
			$data['tabs_manufacturer'] = json_encode($data['tabs_manufacturer']);
		}else{
			$data['tabs_manufacturer'] = '';
		}
		
		if(!empty($data['tabs_ingore_product'])){
			$data['tabs_ingore_product'] = json_encode($data['tabs_ingore_product']);
		}else{
			$data['tabs_ingore_product'] = '';
		}
		
		if(!empty($data['tabs_store'])){
			$data['tabs_store'] = json_encode($data['tabs_store']);
		}else{
			$data['tabs_store'] = '';
		}
		
		if(!isset($data['allproducts'])){
			$data['allproducts'] = 0;
		}
		
		$this->db->query("UPDATE " . DB_PREFIX . "tabs SET sort_order = '" . (int)$data['sort_order'] . "', status = '".(int)$data['status']."', products = '".$data['tabs_product']."', categories = '".$data['tabs_category']."', manufactures = '".$data['tabs_manufacturer']."', ingore_products = '".$data['tabs_ingore_product']."',stores = '".$data['tabs_store']."',allproducts = '".(int)$data['allproducts']."' WHERE tab_id = '".(int)$tab_id."'");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "tabs_description WHERE tab_id = '" . (int)$tab_id . "'");
		
		foreach($data['tabs_description'] as $language_id => $value){
			$this->db->query("INSERT INTO " . DB_PREFIX . "tabs_description SET tab_id = '" . (int)$tab_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}

		$this->cache->delete('tabs');

		$this->event->trigger('post.admin.tabs.edit', $tabs_id);
	}
	
	public function deletetabs($tab_id) {
		$this->event->trigger('pre.admin.tabs.delete', $tab_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "tabs WHERE tab_id = '" . (int)$tab_id . "'");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "tabs_description WHERE tab_id = '" . (int)$tab_id . "'");
		
		$this->event->trigger('post.admin.tabs.delete', $tab_id);
	}
	
	public function gettabs($tab_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "tabs WHERE tab_id = '" . (int)$tab_id . "'");

		return $query->row;
	}
	
	public function gettabss($data = array()){

		$sql = "SELECT * FROM " . DB_PREFIX . "tabs t LEFT JOIN ".DB_PREFIX."tabs_description td ON(t.tab_id = td.tab_id) WHERE td.language_id = '".$this->config->get('config_language_id')."'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND td.title LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND t.status = '" . (int)$data['filter_status'] . "'";
		}

		$sort_data = array(
			'td.title',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY td.title";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getTabsDescriptions($tab_id) {
		$category_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "tabs_description WHERE tab_id = '" . (int)$tab_id . "'");

		foreach ($query->rows as $result) {
			$category_description_data[$result['language_id']] = array(
				'name'             => $result['title'],
				'description'      => $result['description']
			);
		}

		return $category_description_data;
	}
	
	public function getTotaltabss() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "tabs");

		return $query->row['total'];
	}
	
// дополнительные вкладки для всех товаров <--
	
	public function install_demo_autotovars() {
		$db_prefix = DB_PREFIX;
		$file = DIR_APPLICATION . 'model/module/demo_autotovars.sql';
		
		if (!file_exists($file)) {
			exit('Нет файла настроек: ' . $file);
		}
		
		$lines = file($file);

		if ($lines) {
			$sql = '';

			foreach($lines as $line) {
				if ($line && (substr($line, 0, 2) != '--') && (substr($line, 0, 1) != '#')) {
					$sql .= $line;

					if (preg_match('/;\s*$/', $line)) {
						$sql = str_replace("TRUNCATE TABLE `oc_", "TRUNCATE TABLE `" . $db_prefix, $sql);
						$sql = str_replace("DELETE FROM `oc_", "DELETE FROM `" . $db_prefix, $sql);
						$sql = str_replace("UPDATE `oc_", "UPDATE `" . $db_prefix, $sql);
						$sql = str_replace("INSERT INTO `oc_", "INSERT INTO `" . $db_prefix, $sql);

						$this->db->query($sql);

						$sql = '';
					}
				}
			}

			$this->db->query("SET CHARACTER SET utf8");

			$this->db->query("SET @@session.sql_mode = 'MYSQL40'");

		}
	}
	public function install_demo_odegda() {
		$db_prefix = DB_PREFIX;
		$file = DIR_APPLICATION . 'model/module/demo_odegda.sql';
		
		if (!file_exists($file)) {
			exit('Нет файла настроек: ' . $file);
		}

		$lines = file($file);

		if ($lines) {
			$sql = '';

			foreach($lines as $line) {
				if ($line && (substr($line, 0, 2) != '--') && (substr($line, 0, 1) != '#')) {
					$sql .= $line;

					if (preg_match('/;\s*$/', $line)) {
						$sql = str_replace("TRUNCATE TABLE `oc_", "TRUNCATE TABLE `" . $db_prefix, $sql);
						$sql = str_replace("DELETE FROM `oc_", "DELETE FROM `" . $db_prefix, $sql);
						$sql = str_replace("UPDATE `oc_", "UPDATE `" . $db_prefix, $sql);
						$sql = str_replace("INSERT INTO `oc_", "INSERT INTO `" . $db_prefix, $sql);

						$this->db->query($sql);

						$sql = '';
					}
				}
			}

			$this->db->query("SET CHARACTER SET utf8");

			$this->db->query("SET @@session.sql_mode = 'MYSQL40'");

		}
	}

} 
?>