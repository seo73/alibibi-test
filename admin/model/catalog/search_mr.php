<?php

class ModelCatalogSearchMr extends Model {

	public function getDefaultOptions() {

		return array(
			'key' => '45b845adfd23d6fdad94373bc5511532',
			'min_word_length' => 2,
			'cache_results' => 1,
			'fix_keyboard_layout' => 0,
			'sort_order_stock' => 1,
			'fields' => array(
				'name' => array(
					'search' => 'contains',
					'phrase' => 'cut',
					'use_morphology' => 1,
					'use_relevance' => 1,
					'logic' => 'OR',
					'relevance' => array(
						'start' => 80,
						'phrase' => 60,
						'word' => 40
					)
				),
				'description' => array(
					'search' => 'contains',
					'phrase' => 'cut',
					'use_morphology' => 1,
					'use_relevance' => 1,
					'logic' => 'OR',
					'relevance' => array(
						'start' => 0,
						'phrase' => 20,
						'word' => 10
					)
				),
				'tags' => array(
					'search' => 'contains',
					'phrase' => 'cut',
					'use_morphology' => 1,
					'use_relevance' => 1,
					'logic' => 'OR',
					'relevance' => array(
						'start' => 0,
						'phrase' => 0,
						'word' => 45
					)
				),
				'attributes' => array(
					'search' => 0,
					'phrase' => 'cut',
					'use_morphology' => 1,
					'use_relevance' => 1,
					'logic' => 'OR',
					'relevance' => array(
						'start' => 0,
						'phrase' => 0,
						'word' => 0
					)
				),
				'model' => array(
					'search' => 0,
					'phrase' => 'cut',
					'use_morphology' => 1,
					'use_relevance' => 1,
					'logic' => 'OR',
					'exclude_characters' => '-/_',
					'relevance' => array(
						'start' => 0,
						'phrase' => 0,
						'word' => 0
					)
				),
				'sku' => array(
					'search' => 0,
					'phrase' => 'cut',
					'use_morphology' => 1,
					'use_relevance' => 1,
					'logic' => 'OR',
					'relevance' => array(
						'start' => 0,
						'phrase' => 0,
						'word' => 0
					)
				),
				'upc' => array(
					'search' => 0,
					'phrase' => 'cut',
					'use_morphology' => 1,
					'use_relevance' => 1,
					'logic' => 'OR',
					'relevance' => array(
						'start' => 0,
						'phrase' => 0,
						'word' => 0
					)
				),
				'ean' => array(
					'search' => 0,
					'phrase' => 'cut',
					'use_morphology' => 1,
					'use_relevance' => 1,
					'logic' => 'OR',
					'relevance' => array(
						'start' => 0,
						'phrase' => 0,
						'word' => 0
					)
				),
				'jan' => array(
					'search' => 0,
					'phrase' => 'cut',
					'use_morphology' => 1,
					'use_relevance' => 1,
					'logic' => 'OR',
					'relevance' => array(
						'start' => 0,
						'phrase' => 0,
						'word' => 0
					)
				),
				'isbn' => array(
					'search' => 0,
					'phrase' => 'cut',
					'use_morphology' => 1,
					'use_relevance' => 1,
					'logic' => 'OR',
					'relevance' => array(
						'start' => 0,
						'phrase' => 0,
						'word' => 0
					)
				),
				'mpn' => array(
					'search' => 0,
					'phrase' => 'cut',
					'use_morphology' => 1,
					'use_relevance' => 1,
					'logic' => 'OR',
					'relevance' => array(
						'start' => 0,
						'phrase' => 0,
						'word' => 0
					)
				),
			),
		);
	}

	public function getFields() {

		return array(
			'name',
			'description',
			'tags',
			'attributes',
			'model',
			'sku',
			'upc',
			'ean',
			'jan',
			'isbn',
			'mpn');
	}

	public function install() {
		
				
		$a4c7b9dca4e809ba66b8055fc7b286f12 = array (
						'model' => true,
			'sku' => true,
			'upc' => true,
			'ean' => true,
			'jan' => true,
			'isbn' => true,
			'mpn' => true
		);
		
		$a0143b4459e134f89d8f98f8fd2180c16 = $this->db->query("SHOW INDEX FROM " . DB_PREFIX . "product");
		
		foreach ($a0143b4459e134f89d8f98f8fd2180c16->rows as $a8484134b479da66538daea6b7b70ce2b) {
						if (isset($a8484134b479da66538daea6b7b70ce2b['Column_name']) && array_key_exists($a8484134b479da66538daea6b7b70ce2b['Column_name'], $a4c7b9dca4e809ba66b8055fc7b286f12)) {
				$a4c7b9dca4e809ba66b8055fc7b286f12[$a8484134b479da66538daea6b7b70ce2b['Column_name']] = false;
			}
		}
		
		foreach ($a4c7b9dca4e809ba66b8055fc7b286f12 as $ab55f0a8f1810ab666bf15d09d32d89d1 => $aca9df789ee0dfb8be113bf150aa979d9) {
			if ($aca9df789ee0dfb8be113bf150aa979d9) {
				$this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD INDEX ( " . $ab55f0a8f1810ab666bf15d09d32d89d1 . " )");
			}
		}				
	}
}
//author sv2109 (sv2109@gmail.com) license for 1 product copy granted for zuber (nefelium@mail.ru alibibi.ru)
