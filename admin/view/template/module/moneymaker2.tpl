<?php echo $header; ?>
<?php
$moneymaker2_contacts_modes = array(1=>$entry_caption, $tab_image.' + '.$entry_text.' ('.$button_link.')',  $entry_icon.' + '.$entry_text.' ('.$button_link.')', $entry_divider);
$moneymaker2_title_overflow_modes = array(1=>$text_disabled, $entry_common_products_title_overflow_gradient, $entry_common_products_title_overflow_2lines, $entry_common_products_title_overflow_3lines, $entry_common_products_title_overflow_4lines);
$moneymaker2_header_modes = array($text_disabled, $entry_header." + ".$entry_title, $entry_title." + ".$entry_header, $entry_header." + ".$entry_text." + ".$entry_title, $entry_title." + ".$entry_text." + ".$entry_header);
$moneymaker2_stickers_sizes = array('lg', '2x', '3x', '4x', '5x');
$moneymaker2_fa_icons  = array(
1=>'adjust', 'anchor', 'archive', 'area-chart', 'arrows', 'arrows-h', 'arrows-v', 'asterisk', 'at', 'balance-scale', 'ban', 'bar-chart', 'barcode', 'bars', 'battery-empty', 'battery-full', 'battery-half', 'battery-quarter', 'battery-three-quarters', 'bed', 'beer', 'bell', 'bell-o', 'bell-slash', 'bell-slash-o', 'bicycle', 'binoculars', 'birthday-cake', 'bolt', 'bomb', 'book', 'bookmark', 'bookmark-o', 'briefcase', 'bug', 'building', 'building-o', 'bullhorn', 'bullseye', 'bus', 'calculator', 'calendar', 'calendar-check-o', 'calendar-minus-o', 'calendar-o', 'calendar-plus-o', 'calendar-times-o', 'camera', 'camera-retro', 'car', 'caret-square-o-down', 'caret-square-o-left', 'caret-square-o-right', 'caret-square-o-up', 'cart-arrow-down', 'cart-plus', 'cc', 'certificate', 'check', 'check-circle', 'check-circle-o', 'check-square', 'check-square-o', 'child', 'circle', 'circle-o', 'circle-o-notch', 'circle-thin', 'clock-o', 'clone', 'cloud', 'cloud-download', 'cloud-upload', 'code', 'code-fork', 'coffee', 'cog', 'cogs', 'comment', 'comment-o', 'commenting', 'commenting-o', 'comments', 'comments-o', 'compass', 'copyright', 'creative-commons', 'credit-card', 'crop', 'crosshairs', 'cube', 'cubes', 'cutlery', 'database', 'desktop', 'diamond', 'dot-circle-o', 'download', 'ellipsis-h', 'ellipsis-v', 'envelope', 'envelope-o', 'envelope-square', 'eraser', 'exchange', 'exclamation', 'exclamation-circle', 'exclamation-triangle', 'external-link', 'external-link-square', 'eye', 'eye-slash', 'eyedropper', 'fax', 'female', 'fighter-jet', 'file-archive-o', 'file-audio-o', 'file-code-o', 'file-excel-o', 'file-image-o', 'file-pdf-o', 'file-powerpoint-o', 'file-video-o', 'file-word-o', 'film', 'filter', 'fire', 'fire-extinguisher', 'flag', 'flag-checkered', 'flag-o', 'flask', 'folder', 'folder-o', 'folder-open', 'folder-open-o', 'frown-o', 'futbol-o', 'gamepad', 'gavel', 'gift', 'glass', 'globe', 'graduation-cap', 'hand-lizard-o', 'hand-paper-o', 'hand-peace-o', 'hand-pointer-o', 'hand-rock-o', 'hand-scissors-o', 'hand-spock-o', 'hdd-o', 'headphones', 'heart', 'heart-o', 'heartbeat', 'history', 'home', 'hourglass', 'hourglass-end', 'hourglass-half', 'hourglass-o', 'hourglass-start', 'i-cursor', 'inbox', 'industry', 'info', 'info-circle', 'key', 'keyboard-o', 'language', 'laptop', 'leaf', 'lemon-o', 'level-down', 'level-up', 'life-ring', 'lightbulb-o', 'line-chart', 'location-arrow', 'lock', 'magic', 'magnet', 'male', 'map', 'map-marker', 'map-o', 'map-pin', 'map-signs', 'meh-o', 'microphone', 'microphone-slash', 'minus', 'minus-circle', 'minus-square', 'minus-square-o', 'mobile', 'money', 'moon-o', 'motorcycle', 'mouse-pointer', 'music', 'newspaper-o', 'object-group', 'object-ungroup', 'paint-brush', 'paper-plane', 'paper-plane-o', 'paw', 'pencil', 'pencil-square', 'pencil-square-o', 'phone', 'phone-square', 'picture-o', 'pie-chart', 'plane', 'plug', 'plus', 'plus-circle', 'plus-square', 'plus-square-o', 'power-off', 'print', 'puzzle-piece', 'qrcode', 'question', 'question-circle', 'quote-left', 'quote-right', 'random', 'recycle', 'refresh', 'registered', 'reply', 'reply-all', 'retweet', 'road', 'rocket', 'rss', 'rss-square', 'search', 'search-minus', 'search-plus', 'server', 'share', 'share-alt', 'share-alt-square', 'share-square', 'share-square-o', 'shield', 'ship', 'shopping-cart', 'sign-in', 'sign-out', 'signal', 'sitemap', 'sliders', 'smile-o', 'sort', 'sort-alpha-asc', 'sort-alpha-desc', 'sort-amount-asc', 'sort-amount-desc', 'sort-asc', 'sort-desc', 'sort-numeric-asc', 'sort-numeric-desc', 'space-shuttle', 'spinner', 'spoon', 'square', 'square-o', 'star', 'star-half', 'star-half-o', 'star-o', 'sticky-note', 'sticky-note-o', 'street-view', 'suitcase', 'sun-o', 'tablet', 'tachometer', 'tag', 'tags', 'tasks', 'taxi', 'television', 'terminal', 'thumb-tack', 'thumbs-down', 'thumbs-o-down', 'thumbs-o-up', 'thumbs-up', 'ticket', 'times', 'times-circle', 'times-circle-o', 'tint', 'toggle-off', 'toggle-on', 'trademark', 'trash', 'trash-o', 'tree', 'trophy', 'truck', 'tty', 'umbrella', 'university', 'unlock', 'unlock-alt', 'upload', 'user', 'user-plus', 'user-secret', 'user-times', 'users', 'video-camera', 'volume-down', 'volume-off', 'volume-up', 'wheelchair', 'wifi', 'wrench', 'hand-lizard-o', 'hand-o-down', 'hand-o-left', 'hand-o-right', 'hand-o-up', 'hand-paper-o', 'hand-peace-o', 'hand-pointer-o', 'hand-rock-o', 'hand-scissors-o', 'hand-spock-o', 'thumbs-down', 'thumbs-o-down', 'thumbs-o-up', 'thumbs-up', 'ambulance', 'bicycle', 'bus', 'car', 'fighter-jet', 'motorcycle', 'plane', 'rocket', 'ship', 'space-shuttle', 'subway', 'taxi', 'train', 'truck', 'wheelchair', 'genderless', 'mars', 'mars-double', 'mars-stroke', 'mars-stroke-h', 'mars-stroke-v', 'mercury', 'neuter', 'transgender', 'transgender-alt', 'venus', 'venus-double', 'venus-mars', 'file', 'file-archive-o', 'file-audio-o', 'file-code-o', 'file-excel-o', 'file-image-o', 'file-o', 'file-pdf-o', 'file-powerpoint-o', 'file-text', 'file-text-o', 'file-video-o', 'file-word-o', 'circle-o-notch', 'cog', 'refresh', 'spinner', 'check-square', 'check-square-o', 'circle', 'circle-o', 'dot-circle-o', 'minus-square', 'minus-square-o', 'plus-square', 'plus-square-o', 'square', 'square-o', 'cc-amex', 'cc-diners-club', 'cc-discover', 'cc-jcb', 'cc-mastercard', 'cc-paypal', 'cc-stripe', 'cc-visa', 'credit-card', 'google-wallet', 'paypal', 'area-chart', 'bar-chart', 'line-chart', 'pie-chart', 'btc', 'eur', 'gbp', 'gg', 'gg-circle', 'ils', 'inr', 'jpy', 'krw', 'money', 'rub', 'try', 'usd', 'align-center', 'align-justify', 'align-left', 'align-right', 'bold', 'chain-broken', 'clipboard', 'columns', 'eraser', 'file', 'file-o', 'file-text', 'file-text-o', 'files-o', 'floppy-o', 'font', 'header', 'indent', 'italic', 'link', 'list', 'list-alt', 'list-ol', 'list-ul', 'outdent', 'paperclip', 'paragraph', 'repeat', 'scissors', 'strikethrough', 'subscript', 'superscript', 'table', 'text-height', 'text-width', 'th', 'th-large', 'th-list', 'underline', 'undo', 'angle-double-down', 'angle-double-left', 'angle-double-right', 'angle-double-up', 'angle-down', 'angle-left', 'angle-right', 'angle-up', 'arrow-circle-down', 'arrow-circle-left', 'arrow-circle-o-down', 'arrow-circle-o-left', 'arrow-circle-o-right', 'arrow-circle-o-up', 'arrow-circle-right', 'arrow-circle-up', 'arrow-down', 'arrow-left', 'arrow-right', 'arrow-up', 'arrows', 'arrows-alt', 'arrows-h', 'arrows-v', 'caret-down', 'caret-left', 'caret-right', 'caret-square-o-down', 'caret-square-o-left', 'caret-square-o-right', 'caret-square-o-up', 'caret-up', 'chevron-circle-down', 'chevron-circle-left', 'chevron-circle-right', 'chevron-circle-up', 'chevron-down', 'chevron-left', 'chevron-right', 'chevron-up', 'exchange', 'hand-o-down', 'hand-o-left', 'hand-o-right', 'hand-o-up', 'long-arrow-down', 'long-arrow-left', 'long-arrow-right', 'long-arrow-up', 'arrows-alt', 'backward', 'compress', 'eject', 'expand', 'fast-backward', 'fast-forward', 'forward', 'pause', 'play', 'play-circle', 'play-circle-o', 'random', 'step-backward', 'step-forward', 'stop', 'youtube-play', '500px', 'adn', 'amazon', 'android', 'angellist', 'apple', 'behance', 'behance-square', 'bitbucket', 'bitbucket-square', 'black-tie', 'btc', 'buysellads', 'cc-amex', 'cc-diners-club', 'cc-discover', 'cc-jcb', 'cc-mastercard', 'cc-paypal', 'cc-stripe', 'cc-visa', 'chrome', 'codepen', 'connectdevelop', 'contao', 'css3', 'dashcube', 'delicious', 'deviantart', 'digg', 'dribbble', 'dropbox', 'drupal', 'empire', 'expeditedssl', 'facebook', 'facebook-official', 'facebook-square', 'firefox', 'flickr', 'fonticons', 'forumbee', 'foursquare', 'get-pocket', 'gg', 'gg-circle', 'git', 'git-square', 'github', 'github-alt', 'github-square', 'google', 'google-plus', 'google-plus-square', 'google-wallet', 'gratipay', 'hacker-news', 'houzz', 'html5', 'instagram', 'internet-explorer', 'ioxhost', 'joomla', 'jsfiddle', 'lastfm', 'lastfm-square', 'leanpub', 'linkedin', 'linkedin-square', 'linux', 'maxcdn', 'meanpath', 'medium', 'odnoklassniki', 'odnoklassniki-square', 'opencart', 'openid', 'opera', 'optin-monster', 'pagelines', 'paypal', 'pied-piper', 'pied-piper-alt', 'pinterest', 'pinterest-p', 'pinterest-square', 'qq', 'rebel', 'reddit', 'reddit-square', 'renren', 'safari', 'sellsy', 'share-alt', 'share-alt-square', 'shirtsinbulk', 'simplybuilt', 'skyatlas', 'skype', 'slack', 'slideshare', 'soundcloud', 'spotify', 'stack-exchange', 'stack-overflow', 'steam', 'steam-square', 'stumbleupon', 'stumbleupon-circle', 'tencent-weibo', 'trello', 'tripadvisor', 'tumblr', 'tumblr-square', 'twitch', 'twitter', 'twitter-square', 'viacoin', 'vimeo', 'vimeo-square', 'vine', 'vk', 'weibo', 'weixin', 'whatsapp', 'wikipedia-w', 'windows', 'wordpress', 'xing', 'xing-square', 'y-combinator', 'yahoo', 'yelp', 'youtube', 'youtube-play', 'youtube-square', 'ambulance', 'h-square', 'heart', 'heart-o', 'heartbeat', 'hospital-o', 'medkit', 'plus-square', 'stethoscope', 'user-md', 'wheelchair', 'bluetooth', 'bluetooth-b', 'codiepie', 'credit-card-alt', 'edge', 'fort-awesome', 'hashtag', 'mixcloud', 'modx', 'pause-circle', 'pause-circle-o', 'percent', 'product-hunt', 'reddit-alien', 'scribd', 'shopping-bag', 'shopping-basket', 'stop-circle', 'stop-circle-o', 'usb'
);
$moneymaker2_animation_in_styles = array('bounceIn', 'bounceInDown', 'bounceInLeft', 'bounceInRight', 'bounceInUp', 'fadeIn', 'fadeInDown', 'fadeInDownBig', 'fadeInLeft', 'fadeInLeftBig', 'fadeInRight', 'fadeInRightBig', 'fadeInUp', 'fadeInUpBig', 'flipInX', 'flipInY', 'lightSpeedIn', 'rotateIn', 'rotateInDownLeft', 'rotateInDownRight', 'rotateInUpLeft', 'rotateInUpRight', 'rollIn', 'slideInDown', 'slideInLeft', 'slideInRight', 'slideInUp', 'zoomIn', 'zoomInDown', 'zoomInLeft', 'zoomInRight', 'zoomInUp', 'bounce', 'flash', 'flip', 'hinge', 'pulse', 'rubberBand', 'shake', 'swing', 'tada', 'wobble', 'jello');
$moneymaker2_animation_out_styles = array('bounceOut', 'bounceOutDown', 'bounceOutLeft', 'bounceOutRight', 'bounceOutUp', 'fadeOut', 'fadeOutDown', 'fadeOutDownBig', 'fadeOutLeft', 'fadeOutLeftBig', 'fadeOutRight', 'fadeOutRightBig', 'fadeOutUp', 'fadeOutUpBig', 'flipOutX', 'flipOutY', 'lightSpeedOut', 'rotateOut', 'rotateOutDownLeft', 'rotateOutDownRight', 'rotateOutUpLeft', 'rotateOutUpRight', 'rollOut', 'slideOutDown', 'slideOutLeft', 'slideOutRight', 'slideOutUp', 'zoomOut', 'zoomOutDown', 'zoomOutLeft', 'zoomOutRight', 'zoomOutUp', 'bounce', 'flash', 'flip', 'hinge', 'pulse', 'rubberBand', 'shake', 'swing', 'tada', 'wobble', 'jello');
$moneymaker2_gallery_animation_styles = array('3d-unfold', 'move-from-top', 'move-horizontal', 'newspaper', 'zoom-in', 'zoom-out', 'slide-bottom');
$moneymaker2_delimiters = array('/',',',':','|','(',')');

// XY, X=bootstrap class symbol (col-sm-X), Y=columns number
$moneymaker2_columns  = array(2=>'62', '43', '34', 'f5', '26', 's7', 'e8');
$moneymaker2_columns_bs  = array('62', '43', '34', '26');
$moneymaker2_banners_styles = array('default','primary', 'success', 'info', 'warning', 'danger');
$moneymaker2_bw_styles  = array('inverse','default');
$moneymaker2_custom_fields  = array('model', 'sku', 'upc', 'ean', 'jan', 'isbn', 'mpn');

if(empty($moneymaker2_common_products_title_overflow)) $moneymaker2_common_products_title_overflow                      = "2";

if(empty($moneymaker2_header_transparency)) $moneymaker2_header_transparency                                            = "80";
if(empty($moneymaker2_header_strip_bg_color)) $moneymaker2_header_strip_bg_color                                        = "#111111";
if(empty($moneymaker2_header_strip_font_color[0])) $moneymaker2_header_strip_font_color[0]                              = "#9d9d9d"; //static
if(empty($moneymaker2_header_strip_font_color[1])) $moneymaker2_header_strip_font_color[1]                              = "#ffffff"; //hover
if(empty($moneymaker2_header_strip_font_color[2])) $moneymaker2_header_strip_font_color[2]                              = "#ffffff"; //active
if(empty($moneymaker2_header_glowing_color)) $moneymaker2_header_glowing_color                                          = "#ffffff";
if(empty($moneymaker2_header_contacts_phone_highlight_color)) $moneymaker2_header_contacts_phone_highlight_color        = "#ffffff";
if(empty($moneymaker2_header_contacts_icon)) $moneymaker2_header_contacts_icon                                          = "mobile";
if(empty($moneymaker2_header_categories_thumbs_width)) $moneymaker2_header_categories_thumbs_width                      = '90';
if(empty($moneymaker2_header_categories_thumbs_height)) $moneymaker2_header_categories_thumbs_height                    = '90';
if(empty($moneymaker2_header_categories_columns['lg'])) $moneymaker2_header_categories_columns['lg']                    = 's7';
if(empty($moneymaker2_header_categories_columns['md'])) $moneymaker2_header_categories_columns['md']                    = 'f5';
if(empty($moneymaker2_header_categories_columns['sm'])) $moneymaker2_header_categories_columns['sm']                    = '34';
if(empty($moneymaker2_header_cart_effect_color)) $moneymaker2_header_cart_effect_color                                  = "#FFFFFF";
if(empty($moneymaker2_header_cart_link_icon)) $moneymaker2_header_cart_link_icon                                        = "gift";
if(empty($moneymaker2_header_search_ajax_image_width)) $moneymaker2_header_search_ajax_image_width                      = "35";
if(empty($moneymaker2_header_search_ajax_image_height)) $moneymaker2_header_search_ajax_image_height                    = "35";

if(empty($moneymaker2_catalog_compare_charts_columns['lg'])) $moneymaker2_catalog_compare_charts_columns['lg']          = '43';
if(empty($moneymaker2_catalog_compare_charts_columns['md'])) $moneymaker2_catalog_compare_charts_columns['md']          = '43';
if(empty($moneymaker2_catalog_compare_charts_columns['sm'])) $moneymaker2_catalog_compare_charts_columns['sm']          = '43';
if(empty($moneymaker2_catalog_products_stock_color[0])) $moneymaker2_catalog_products_stock_color[0]                    = "#449d44";
if(empty($moneymaker2_catalog_products_stock_color[1])) $moneymaker2_catalog_products_stock_color[1]                    = "#999999";
if(empty($moneymaker2_catalog_products_code_field)) $moneymaker2_catalog_products_code_field                            = "sku";

if(empty($moneymaker2_product_code_field)) $moneymaker2_product_code_field                                              = "model";
if(empty($moneymaker2_product_thumbs_limit['lg'])) $moneymaker2_product_thumbs_limit['lg']                              = '5';
if(empty($moneymaker2_product_thumbs_limit['md'])) $moneymaker2_product_thumbs_limit['md']                              = '4';
if(empty($moneymaker2_product_thumbs_limit['sm'])) $moneymaker2_product_thumbs_limit['sm']                              = '6';
if(empty($moneymaker2_product_thumbs_limit['xsm'])) $moneymaker2_product_thumbs_limit['xsm']                            = '6';
if(empty($moneymaker2_product_thumbs_limit['xxsm'])) $moneymaker2_product_thumbs_limit['xxsm']                          = '5';
if(empty($moneymaker2_product_thumbs_limit['xxxsm'])) $moneymaker2_product_thumbs_limit['xxxsm']                        = '3';
if(empty($moneymaker2_product_points_size)) $moneymaker2_product_points_size                                            = 'lg';
foreach ($stock_statuses as $stock_status) {
if(empty($moneymaker2_product_points_stock_color[$stock_status['stock_status_id']])) $moneymaker2_product_points_stock_color[$stock_status['stock_status_id']] = "#aaaaaa";
}
if(empty($moneymaker2_product_points_manufacturer_image_width)) $moneymaker2_product_points_manufacturer_image_width    = '28';
if(empty($moneymaker2_product_points_manufacturer_image_height)) $moneymaker2_product_points_manufacturer_image_height  = '28';
if(empty($moneymaker2_product_options_hide_limit)) $moneymaker2_product_options_hide_limit                              = "2";

if(empty($moneymaker2_footer_contacts_icon)) $moneymaker2_footer_contacts_icon                                          = 'phone';
if(empty($moneymaker2_footer_links_icon)) $moneymaker2_footer_links_icon                                                = 'external-link';
if(empty($moneymaker2_footer_text1_icon)) $moneymaker2_footer_text1_icon                                                = 'file-text';
if(empty($moneymaker2_footer_text2_icon)) $moneymaker2_footer_text2_icon                                                = 'file-text';

if(empty($moneymaker2_modules_stickers_transparency)) $moneymaker2_modules_stickers_transparency                        = "40";
if(empty($moneymaker2_modules_stickers_size_catalog)) $moneymaker2_modules_stickers_size_catalog                        = 'lg';
if(empty($moneymaker2_modules_stickers_size_product)) $moneymaker2_modules_stickers_size_product                        = '2x';
if(empty($moneymaker2_modules_stickers_specials_icon)) $moneymaker2_modules_stickers_specials_icon                      = "percent";
if(empty($moneymaker2_modules_stickers_specials_color)) $moneymaker2_modules_stickers_specials_color                    = "#D9534F";
if(empty($moneymaker2_modules_stickers_popular_icon)) $moneymaker2_modules_stickers_popular_icon                        = "thumbs-up";
if(empty($moneymaker2_modules_stickers_popular_color)) $moneymaker2_modules_stickers_popular_color                      = "#337AB7";
if(empty($moneymaker2_modules_stickers_popular_limit)) $moneymaker2_modules_stickers_popular_limit                      = "1000";
if(empty($moneymaker2_modules_stickers_rated_icon)) $moneymaker2_modules_stickers_rated_icon                            = "star";
if(empty($moneymaker2_modules_stickers_rated_color)) $moneymaker2_modules_stickers_rated_color                          = "#F7941D";
if(empty($moneymaker2_modules_stickers_rated_limit)) $moneymaker2_modules_stickers_rated_limit                          = "5";
if(empty($moneymaker2_modules_stickers_new_icon)) $moneymaker2_modules_stickers_new_icon                                = "bolt";
if(empty($moneymaker2_modules_stickers_new_color)) $moneymaker2_modules_stickers_new_color                              = "#5CB85C";
if(empty($moneymaker2_modules_stickers_new_limit)) $moneymaker2_modules_stickers_new_limit                              = "30";
if(empty($moneymaker2_modules_stickers_custom1_icon)) $moneymaker2_modules_stickers_custom1_icon                        = "tag";
if(empty($moneymaker2_modules_stickers_custom1_color)) $moneymaker2_modules_stickers_custom1_color                      = "#999999";
if(empty($moneymaker2_modules_stickers_custom1_field)) $moneymaker2_modules_stickers_custom1_field                      = "upc";
if(empty($moneymaker2_modules_stickers_custom2_icon)) $moneymaker2_modules_stickers_custom2_icon                        = "tags";
if(empty($moneymaker2_modules_stickers_custom2_color)) $moneymaker2_modules_stickers_custom2_color                      = "#BBBBBB";
if(empty($moneymaker2_modules_stickers_custom2_field)) $moneymaker2_modules_stickers_custom2_field                      = "ean";
if(empty($moneymaker2_modules_stickers_promo_icon)) $moneymaker2_modules_stickers_promo_icon                            = "bolt";
if(empty($moneymaker2_modules_promo_style)) $moneymaker2_modules_promo_style                                            = "danger";
if(empty($moneymaker2_modules_quickorder_image_width)) $moneymaker2_modules_quickorder_image_width                      = "228";
if(empty($moneymaker2_modules_quickorder_image_height)) $moneymaker2_modules_quickorder_image_height                    = "228";
if(empty($moneymaker2_modules_quickorder_phone_mask)) $moneymaker2_modules_quickorder_phone_mask                        = "(000) 000-00-00";
?>

<style type="text/css">
#vtab-header_contacts .img-thumbnail img {
  max-width: 25px;
}
fieldset {
  padding-bottom: 45px;
}
fieldset.successively {
  padding-bottom: 5px;
}
fieldset.successively legend small.text-muted {
  font-size: 80%;
}
.form-group {
  padding-bottom: 22px;
}
.form-group .well.well-sm {
  height: 215px;
  overflow: auto;
}
.form-group p.text-center {
  margin-bottom: -15px;
  margin-top: -1px;

  font-size: 11px;
  color: #999;
}
.form-group .alert {
  margin-bottom: 0px;
}
.margintop {
  margin-top: 30px;
}
.margin-top {
  margin-top: -20px;
}
.paddingtop {
  padding-top: 9px;
}
.strong {
  font-weight: bold;
}
.btn.btn-success.form-control {
  text-align: left;
}
.colorpicker {
  color: #fff;
  font-size: 10px;
  text-align: center;
  text-transform: uppercase;
  text-shadow: 0px 0px 6px #000;
}
#animationSandbox {
  font-size: 42px;
  padding: 80px 0;
  margin: 35px 0;
  border: 1px solid #ccc;
  background-color: #fff;
}
</style>
<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a onclick="apply()" data-toggle="tooltip" title="<?php echo $button_apply; ?>" class="btn btn-success"><span><i class="fa fa-check-square"></i> &nbsp;<?php echo $button_apply; ?></span></a><script language="javascript">function apply(){$('#form-moneymaker2').append('<input type="hidden" id="apply" name="apply" value="1"  />'); $('#form-moneymaker2').submit();}</script>
        <button type="submit" form="form-moneymaker2" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" form="form-backup" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-moneymaker2" class="form-horizontal">

          <div class="tab-pane">
            <ul class="nav nav-tabs" id="tabs">
              <li><a href="#tab_common" data-toggle="tab"><?php echo $entry_tab_common; ?></a></li>
              <li><a href="#tab_header" data-toggle="tab"><?php echo $entry_tab_header; ?></a></li>
              <li><a href="#tab_catalog" data-toggle="tab"><?php echo $entry_tab_catalog; ?></a></li>
              <li><a href="#tab_product" data-toggle="tab"><?php echo $entry_tab_product; ?></a></li>
              <li><a href="#tab_footer" data-toggle="tab"><?php echo $entry_tab_footer; ?></a></li>
              <li><a href="#tab_modules" data-toggle="tab"><?php echo $entry_tab_modules; ?></a></li>
              <li><a href="#tab_help" data-toggle="tab"><?php echo $entry_tab_help; ?></a></li>
            </ul>
            <div class="tab-content">

              <div class="tab-pane" id="tab_common">
                <fieldset>
                  <legend><?php echo $entry_tab_common; ?></legend>
                  <div class="form-group margin-top">
                    <p class="col-sm-12"><?php echo $entry_tab_common_help; ?><br /></p>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_common_buy; ?></label>
                    <div class="col-sm-4">
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_buy_hide" value="0" <?php if (!$moneymaker2_common_buy_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_buy_hide" value="1" <?php if ($moneymaker2_common_buy_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                    </div>
                    <label class="col-sm-2 control-label"><?php echo $entry_common_cart_outofstock_disabled; ?></label>
                    <div class="col-sm-4">
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_cart_outofstock_disabled" value="1" <?php if ($moneymaker2_common_cart_outofstock_disabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_cart_outofstock_disabled" value="0" <?php if (!$moneymaker2_common_cart_outofstock_disabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_common_price_detached; ?></label>
                    <div class="col-sm-10">
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_price_detached" value="1" <?php if ($moneymaker2_common_price_detached) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_price_detached" value="0" <?php if (!$moneymaker2_common_price_detached) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_common_wishlist; ?></label>
                    <div class="col-sm-4">
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_wishlist_hide" value="0" <?php if (!$moneymaker2_common_wishlist_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_wishlist_hide" value="1" <?php if ($moneymaker2_common_wishlist_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                    </div>
                    <label class="col-sm-2 control-label"><?php echo $entry_display_caption; ?></label>
                    <div class="col-sm-4">
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_wishlist_caption" value="1" <?php if ($moneymaker2_common_wishlist_caption) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_wishlist_caption" value="0" <?php if (!$moneymaker2_common_wishlist_caption) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_common_compare; ?></label>
                    <div class="col-sm-4">
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_compare_hide" value="0" <?php if (!$moneymaker2_common_compare_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_compare_hide" value="1" <?php if ($moneymaker2_common_compare_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                    </div>
                    <label class="col-sm-2 control-label"><?php echo $entry_display_caption; ?></label>
                    <div class="col-sm-4">
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_compare_caption" value="1" <?php if ($moneymaker2_common_compare_caption) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_compare_caption" value="0" <?php if (!$moneymaker2_common_compare_caption) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_common_browser_warned; ?></label>
                    <div class="col-sm-10">
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_browser_warned" value="1" <?php if ($moneymaker2_common_browser_warned) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_browser_warned" value="0" <?php if (!$moneymaker2_common_browser_warned) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                    </div>
                  </div>
                </fieldset>
                <fieldset>
                  <legend><?php echo $entry_custom_css; ?></legend>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_status; ?></label>
                    <div class="col-sm-10">
                      <?php $filename = '../catalog/view/theme/moneymaker2/stylesheet/stylesheet.custom.css';
                      $moneymaker2_styles = "@media(min-width:768px){ ";
                      $moneymaker2_styles.= "#top.navbar-default .navbar-nav>li>a {color: ".$moneymaker2_header_strip_font_color[0].";}";
                      $moneymaker2_styles.= "#top.navbar-default .navbar-nav>li>a:focus, #top.navbar-default .navbar-nav>li>a:hover {color: ".$moneymaker2_header_strip_font_color[1].";}";
                      $moneymaker2_styles.= "#top.navbar-default .navbar-nav>.open>a, #top.navbar-default .navbar-nav>.open>a:focus, #top.navbar-default .navbar-nav>.open>a:hover {color: ".$moneymaker2_header_strip_font_color[2].";}";

                      list($moneymaker2_r, $moneymaker2_g, $moneymaker2_b) = sscanf($moneymaker2_header_strip_bg_color, "#%02x%02x%02x");
                      $moneymaker2_styles.= "#top.navbar-default {background-color: rgb(".$moneymaker2_r.", ".$moneymaker2_g.", ".$moneymaker2_b.");background-color: rgba(".$moneymaker2_r.", ".$moneymaker2_g.", ".$moneymaker2_b.", ".($moneymaker2_header_transparency/100).");}";
                      if ($moneymaker2_header_glowing) {
                      $moneymaker2_styles.= "#top.navbar-default .navbar-nav>li>a {text-shadow: 0 0 25px ".$moneymaker2_header_glowing_color.";}";
                      $moneymaker2_styles.= "#top.navbar-default .navbar-nav>li>a:focus, #top.navbar-default .navbar-nav>li>a:hover {text-shadow: 0 0 20px ".$moneymaker2_header_glowing_color.";}";
                      $moneymaker2_styles.= "#top.navbar-default .navbar-nav>.open>a, #top.navbar-default .navbar-nav>.open>a:focus, #top.navbar-default .navbar-nav>.open>a:hover {text-shadow: 0 0 15px ".$moneymaker2_header_glowing_color.";}";
                      $moneymaker2_styles.= "}";
                      };
                      if ($moneymaker2_header_contacts_phone_highlight) {
                      $moneymaker2_styles.= "#contacts span.phone {color: ".$moneymaker2_header_contacts_phone_highlight_color.";}";
                      }
                      if ($moneymaker2_header_cart_effect) {
                      $moneymaker2_styles.= "@-moz-keyframes cart-effect-blink { 50% { color: ".$moneymaker2_header_cart_effect_color."; text-shadow: 0 0 20px ".$moneymaker2_header_cart_effect_color."; } }";
                      $moneymaker2_styles.= "@-webkit-keyframes cart-effect-blink { 50% { color: ".$moneymaker2_header_cart_effect_color."; text-shadow: 0 0 20px ".$moneymaker2_header_cart_effect_color."; } }";
                      $moneymaker2_styles.= "@keyframes cart-effect-blink { 50% { color: ".$moneymaker2_header_cart_effect_color."; text-shadow: 0 0 20px ".$moneymaker2_header_cart_effect_color."; } }";
                      }
                      $moneymaker2_styles.= "}/*@media*/";

                      $moneymaker2_styles.= "@media(max-width:767px){ ";
                      $moneymaker2_styles.= "#top .navbar-header {background-color: rgb(".$moneymaker2_r.", ".$moneymaker2_g.", ".$moneymaker2_b.");background-color: rgba(".$moneymaker2_r.", ".$moneymaker2_g.", ".$moneymaker2_b.", ".($moneymaker2_header_transparency/100).");}";
                      if (!$moneymaker2_header_categories_menu) {
                      $moneymaker2_styles.= "body.owl-moneymaker2-fullscreen header > #top.navbar-default { margin-bottom: 0;}";
                      }
                      $moneymaker2_styles.= "}/*@media*/";
                      if ($moneymaker2_header_strip_expanded) {
                      $moneymaker2_styles.= "#top .navbar-collapse.collapse { display: block!important; }";
                      }
                      $moneymaker2_styles_modules_stickers_background = $moneymaker2_modules_stickers_background ? "255, 255, 255" : "0, 0, 0";
                      $moneymaker2_styles.= ".sticker > div {background: -webkit-linear-gradient(right, rgba(".$moneymaker2_styles_modules_stickers_background.", 0.".$moneymaker2_modules_stickers_transparency."),rgba(255, 255, 255, 0));background: -o-linear-gradient(right, rgba(".$moneymaker2_styles_modules_stickers_background.", 0.".$moneymaker2_modules_stickers_transparency."),rgba(255, 255, 255, 0));background: -moz-linear-gradient(right, rgba(".$moneymaker2_styles_modules_stickers_background.", 0.".$moneymaker2_modules_stickers_transparency."),rgba(255, 255, 255, 0));background: -ms-linear-gradient(right, rgba(".$moneymaker2_styles_modules_stickers_background.", 0.".$moneymaker2_modules_stickers_transparency."),rgba(255, 255, 255, 0));background: linear-gradient(to right, rgba(".$moneymaker2_styles_modules_stickers_background.", 0.".$moneymaker2_modules_stickers_transparency."),rgba(255, 255, 255, 0));}";
                      $moneymaker2_styles.= ".sticker.sticker-special {color: ".$moneymaker2_modules_stickers_specials_color.";}";
                      $moneymaker2_styles.= ".sticker.sticker-popular {color: ".$moneymaker2_modules_stickers_popular_color.";}";
                      $moneymaker2_styles.= ".sticker.sticker-rated {color: ".$moneymaker2_modules_stickers_rated_color.";}";
                      $moneymaker2_styles.= ".sticker.sticker-new {color: ".$moneymaker2_modules_stickers_new_color.";}";
                      $moneymaker2_styles.= ".sticker.sticker-custom1 {color: ".$moneymaker2_modules_stickers_custom1_color.";}";
                      $moneymaker2_styles.= ".sticker.sticker-custom2 {color: ".$moneymaker2_modules_stickers_custom2_color.";}";

                      if ($moneymaker2_catalog_products_list_stock_hide) {
                      $moneymaker2_styles.= ".product-list .product-thumb .additional .stock {display: none;}";
                      }
                      if ($moneymaker2_catalog_products_grid_stock_hide) {
                      $moneymaker2_styles.= ".product-grid .product-thumb .additional .stock {display: none;}";
                      }
                      if ($moneymaker2_catalog_products_list_code_hide) {
                      $moneymaker2_styles.= ".product-list .product-thumb .additional .code {display: none;}";
                      }
                      if ($moneymaker2_catalog_products_grid_code_hide) {
                      $moneymaker2_styles.= ".product-grid .product-thumb .additional .code {display: none;}";
                      }
                      $moneymaker2_styles.= ".product-thumb .additional .stock.instock {border-color: ".$moneymaker2_catalog_products_stock_color[0].";color: ".$moneymaker2_catalog_products_stock_color[0].";}";
                      $moneymaker2_styles.= ".product-thumb .additional .stock {border-color: ".$moneymaker2_catalog_products_stock_color[1].";color: ".$moneymaker2_catalog_products_stock_color[1].";}";
                      if ($moneymaker2_common_products_title_overflow==2) {
                      $moneymaker2_styles.= "#content .product-thumb .caption {white-space: nowrap; overflow: hidden;}#content .product-thumb .caption > a::after, #content .product-thumb .caption > span::after {content: '';position: absolute;right: 15px;width: 80px;height: 22px;background: -moz-linear-gradient(left, rgba(255,204,0, 0.1), rgba(255,255,255, 1));background: -webkit-linear-gradient(left, rgba(255, 255, 255, 0.1), rgba(255,255,255, 1));background: -o-linear-gradient(left, rgba(255,204,0, 0.1), rgba(255,255,255, 1));background: -ms-linear-gradient(left, rgba(255,204,0, 0.1), rgba(255,255,255, 1));background: linear-gradient(to right, rgba(255, 255, 255, 0.1), rgba(255,255,255, 1));}";
                      } else if ($moneymaker2_common_products_title_overflow==3) {
                      $moneymaker2_styles.= "#content .product-grid .product-thumb .caption {min-height: 53px;}";
                      } else if ($moneymaker2_common_products_title_overflow==4) {
                      $moneymaker2_styles.= "#content .product-grid .product-thumb .caption {min-height: 73px;}";
                      } else if ($moneymaker2_common_products_title_overflow==5) {
                      $moneymaker2_styles.= "#content .product-grid .product-thumb .caption {min-height: 93px;}";
                      }

                      foreach ($stock_statuses as $stock_status) {
                      $moneymaker2_styles.= ".product-points .stock-".$stock_status['stock_status_id']." .fa-stack { color: ".$moneymaker2_product_points_stock_color[$stock_status['stock_status_id']]." ; } ";
                      }
                      $moneymaker2_styles.= ($moneymaker2_common_css_enabled&&isset($moneymaker2_common_css)) ? $moneymaker2_common_css : '';
                      $styles = html_entity_decode($moneymaker2_styles, ENT_QUOTES, 'UTF-8');

                      if (is_writable($filename)) {
                      if ($styles) {
                      if (!$handle = fopen($filename, 'w')) { echo '<div class="alert alert-danger">File open error</div>'; exit; }
                      if (fwrite($handle, $styles) === FALSE) { echo '<div class="alert alert-danger">Write error</div>'; exit; }
                      echo '<div class="alert alert-success">stylesheet.custom.css updated</div>'; fclose($handle);
                      } else {
                      echo '<div class="alert alert-info">Nothing to save</div>';
                      }
                      } else {
                      echo '<div class="alert alert-danger">Writing error or file stylesheet.custom.css not found</div>';
                      }
                      ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                    <div class="col-sm-10">
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_css_enabled" value="1" <?php if ($moneymaker2_common_css_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                      <label class="radio-inline"><input type="radio" name="moneymaker2_common_css_enabled" value="0" <?php if (!$moneymaker2_common_css_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_code; ?></label>
                    <div class="col-sm-10">
                      <textarea name="moneymaker2_common_css" rows="2" class="form-control"><?php echo isset($moneymaker2_common_css) ? $moneymaker2_common_css : ''; ?></textarea>
                    </div>
                  </div>
                </fieldset>
                <fieldset>
                  <legend><?php echo $entry_categories; ?></legend>
                  <div class="form-group margin-top">
                    <p class="col-sm-12"><?php echo $entry_common_categories_help; ?><br /></p>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $entry_common_categories_icons_help; ?>"><?php echo $entry_common_categories_icons; ?></span></label>
                    <div class="col-sm-4">
                      <div class="input-group">
                        <div class="input-group-btn">
                          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                        </div>
                        <select name="moneymaker2_common_categories_icons_enabled" id="moneymaker2_common_categories_icons_enabled" class="form-control">
                          <?php if ($moneymaker2_common_categories_icons_enabled) { ?>
                          <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                          <option value="0"><?php echo $text_disabled; ?></option>
                          <?php } else { ?>
                          <option value="1"><?php echo $text_enabled; ?></option>
                          <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <a href="#moneymaker2_categories" class="btn btn-default" data-toggle="collapse"><?php echo $entry_categories; ?> <i class="fa fa-angle-down"></i></a>
                    </div>
                  </div>
                  <div class="collapse" id="moneymaker2_categories">
                  <?php foreach ($categories as $key => $row) {$name[$key]  = $row['name']; $category_id[$key] = $row['category_id']; } ?>
                  <?php array_multisort($name, SORT_ASC, $category_id, SORT_ASC, $categories); ?>
                  <?php foreach ($categories as $category) { ?>
                    <div class="row form-inline">
                      <div class="col-sm-6 text-right">
                        <select name="moneymaker2_common_categories_icons[<?php echo $category['category_id']; ?>]" class="form-control">
                          <option value="0"><?php echo $text_select ?></option>
                          <?php $first = false; ?>
                          <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                            <?php $moneymaker2_common_categories_icons[$category['category_id']] == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                            <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-6">
                        <p class="paddingtop <?php if ($category['parent_id'] == 0) { ?>strong<?php } ?>"><?php echo $category['name'] ?></p>
                      </div>
                    </div>
                  <?php } ?>
                  </div>
                </fieldset>
                <fieldset>
                  <legend><?php echo $entry_products; ?></legend>
                  <div class="form-group margin-top">
                    <p class="col-sm-12"><?php echo $entry_common_products_help; ?><br /></p>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $entry_common_products_title_overflow_help; ?>"><?php echo $entry_common_products_title_overflow; ?></span></label>
                    <div class="col-sm-10">
                      <select name="moneymaker2_common_products_title_overflow" class="form-control">
                        <?php foreach ($moneymaker2_title_overflow_modes as $key => $moneymaker2_title_overflow_mode) { ?>
                        <option value="<?php echo $key; ?>" <?php if ($moneymaker2_common_products_title_overflow == $key) { ?>selected="selected"<?php } ?>><?php echo $moneymaker2_title_overflow_mode; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </fieldset>
              </div>

              <div class="tab-pane" id="tab_header">
              <fieldset>
                <legend><?php echo $entry_tab_header; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_header_help; ?><br /></p>
                </div>
              </fieldset>
              <fieldset class="margin-top">
                <legend><?php echo $entry_top_menu; ?></legend>


                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_header_strip_expanded; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_strip_expanded" value="1" <?php if ($moneymaker2_header_strip_expanded) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_strip_expanded" value="0" <?php if (!$moneymaker2_header_strip_expanded) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_background; ?></label>
                  <div class="col-sm-3">
                    <input type="text" name="moneymaker2_header_strip_bg_color" value="<?php echo $moneymaker2_header_strip_bg_color; ?>"  class="form-control colorpicker" />
                  </div>
                  <label class="col-sm-2 control-label" for="moneymaker2_header_transparency"><?php echo $entry_transparency; ?></label>
                  <div class="col-sm-3">
                    <select name="moneymaker2_header_transparency" id="moneymaker2_header_transparency" class="form-control">
                      <?php for ($j = 5; $j <= 100; $j=$j+5) {
                        ($moneymaker2_header_transparency == $j) ? $active = 'selected' : $active = ''; ?>
                      <option value="<?php echo $j; ?>" <?php echo $active; ?>><?php echo $j; ?>%</option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="moneymaker2_header_glowing"><?php echo $entry_glowing; ?></label>
                  <div class="col-sm-3">
                    <select name="moneymaker2_header_glowing" id="input-moneymaker2_header_glowing" class="form-control">
                      <?php if ($moneymaker2_header_glowing) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_color; ?></label>
                  <div class="col-sm-3">
                    <input type="text" name="moneymaker2_header_glowing_color" value="<?php echo $moneymaker2_header_glowing_color; ?>"  class="form-control colorpicker" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_header_strip_font_color; ?></label>
                  <div class="col-sm-3">
                    <input type="text" name="moneymaker2_header_strip_font_color[0]" value="<?php echo $moneymaker2_header_strip_font_color[0]; ?>"  class="form-control colorpicker" /><p class="text-center"><small><?php echo $entry_static; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <input type="text" name="moneymaker2_header_strip_font_color[1]" value="<?php echo $moneymaker2_header_strip_font_color[1]; ?>"  class="form-control colorpicker" /><p class="text-center"><small><?php echo $entry_hover; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <input type="text" name="moneymaker2_header_strip_font_color[2]" value="<?php echo $moneymaker2_header_strip_font_color[2]; ?>"  class="form-control colorpicker" /><p class="text-center"><small><?php echo $entry_active; ?></small></p>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_contacts; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_header_contacts_help; ?><br /></p>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                      </div>
                      <select name="moneymaker2_header_contacts_icon" class="form-control">
                        <?php $first = false; ?>
                        <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                        <?php $moneymaker2_header_contacts_icon == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                        <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="moneymaker2_header_contacts_phone"><span data-toggle="tooltip" title="<?php echo $entry_header_contacts_phone_help; ?>"><?php echo $entry_telephone; ?></span></label>
                  <div class="col-sm-10">
                    <input type="text" name="moneymaker2_header_contacts_phone" value="<?php echo isset($moneymaker2_header_contacts_phone) ? $moneymaker2_header_contacts_phone : ''; ?>" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_header_contacts_phone" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_header_contacts_phone_highlight; ?></label>
                  <div class="col-sm-3">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_contacts_phone_highlight" value="1" <?php if ($moneymaker2_header_contacts_phone_highlight) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_contacts_phone_highlight" value="0" <?php if (!$moneymaker2_header_contacts_phone_highlight) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_color; ?></label>
                  <div class="col-sm-3">
                    <input type="text" name="moneymaker2_header_contacts_phone_highlight_color" value="<?php echo $moneymaker2_header_contacts_phone_highlight_color; ?>"  class="form-control colorpicker" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $entry_header_contacts_caption_help; ?>"><?php echo $entry_caption; ?></span></label>
                  <div class="col-sm-10">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_header_contacts_title[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_header_contacts_title[$language['language_id']]) ? $moneymaker2_header_contacts_title[$language['language_id']] : ''; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><span class="text-muted"><?php echo $entry_contacts; ?> ></span> <?php echo $button_add; ?></legend>
                <div class="row" id="vtab-header_contacts">
                  <div class="col-sm-2">
                    <ul class="nav nav-pills nav-stacked" id="header_contacts">
                      <?php $header_contact_row = 1; ?>
                      <?php foreach ($moneymaker2_header_contacts as $value) { ?>
                      <li><a href="#tab-contact<?php echo $header_contact_row; ?>" data-toggle="tab" id="link-contact<?php echo $header_contact_row; ?>"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-contact<?php echo $header_contact_row; ?>\']').parent().remove(); $('#tab-contact<?php echo $header_contact_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#<?php echo $header_contact_row; ?></a></li>
                      <?php $header_contact_row++; ?>
                      <?php } ?>
                      <li>
                        <button type="button" onclick="addHeaderContact();" class="btn btn-success form-control"><i class="fa fa-plus-circle"></i> <?php echo $button_add; ?></button>
                      </li>
                    </ul>
                  </div>
                  <div class="col-sm-10">
                    <div class="tab-content">
                      <?php $header_contact_row = 1; ?>
                      <?php foreach ($moneymaker2_header_contacts as $value) { ?>
                      <div class="tab-pane" id="tab-contact<?php echo $header_contact_row; ?>">
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <p><h4><?php echo $tab_data; ?> #<?php echo $header_contact_row; ?></h4></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_mode; ?></label>
                          <div class="col-sm-10">
                            <select name="moneymaker2_header_contacts[<?php echo $header_contact_row; ?>][mode]" class="form-control">
                              <option value="0"><?php echo $text_select ?></option>
                              <?php foreach ($moneymaker2_contacts_modes as $key => $moneymaker2_contacts_mode) { ?>
                              <option value="<?php echo $key; ?>" <?php if ($value['mode'] == $key) { ?>selected="selected"<?php } ?>><?php echo $moneymaker2_contacts_mode; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $tab_image; ?></label>
                          <div class="col-sm-3">
                            <a href="" id="moneymaker2_header_contacts_image_thumb<?php echo $header_contact_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $value['thumb']; ?>" alt="" data-placeholder="<?php echo $placeholder; ?>"  /></a><input type="hidden" name="moneymaker2_header_contacts[<?php echo $header_contact_row; ?>][image]" value="<?php echo $value['image']; ?>" id="moneymaker2_header_contacts_image_input<?php echo $header_contact_row; ?>" />
                          </div>
                          <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                          <div class="col-sm-5">
                            <div class="input-group">
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                              </div>
                              <select name="moneymaker2_header_contacts[<?php echo $header_contact_row; ?>][icon]" class="form-control">
                                <?php $first = false; ?>
                                <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                                <?php $value['icon'] == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                                <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_text; ?></label>
                          <div class="col-sm-10">
                            <?php foreach ($languages as $language) { ?>
                            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input type="text" name="moneymaker2_header_contacts[<?php echo $header_contact_row; ?>][text][<?php echo $language['language_id']; ?>]" value="<?php echo isset($value['text'][$language['language_id']]) ? $value['text'][$language['language_id']] : ''; ?>" class="form-control">
                            </div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="moneymaker2_header_contacts_link<?php echo $header_contact_row; ?>"><?php echo $button_link; ?></label>
                          <div class="col-sm-10">
                            <input type="text" name="moneymaker2_header_contacts[<?php echo $header_contact_row; ?>][link]" value="<?php echo isset($value['link']) ? $value['link'] : ''; ?>" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_header_contacts_link<?php echo $header_contact_row; ?>" class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="moneymaker2_header_contacts_sort_order<?php echo $header_contact_row; ?>"><?php echo $entry_sort_order; ?></label>
                          <div class="col-sm-10">
                            <input type="number" min="0" name="moneymaker2_header_contacts[<?php echo $header_contact_row; ?>][sort_order]" value="<?php echo isset($value['sort_order']) ? $value['sort_order'] : ''; ?>" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_header_contacts_sort_order<?php echo $header_contact_row; ?>" class="form-control">
                          </div>
                        </div>
                      </div>
                      <?php $header_contact_row++; ?>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_categories; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_header_categories_help; ?><br /></p>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_categories_hide" value="0" <?php if (!$moneymaker2_header_categories_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_categories_hide" value="1" <?php if ($moneymaker2_header_categories_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_header_categories_showchilds; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_categories_hidechilds" value="0" <?php if (!$moneymaker2_header_categories_hidechilds) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_categories_hidechilds" value="1" <?php if ($moneymaker2_header_categories_hidechilds) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $entry_header_categories_caption_help; ?>"><?php echo $entry_caption; ?></span></label>
                  <div class="col-sm-10">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_header_categories_caption[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_header_categories_caption[$language['language_id']]) ? $moneymaker2_header_categories_caption[$language['language_id']] : ''; ?>" placeholder="<?php echo $entry_optional; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><span class="text-muted"><?php echo $entry_categories; ?> ></span> <?php echo $entry_advanced; ?></legend>
                <div class="form-group margin-top">
                  <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $entry_header_categories_mode_help; ?>"><?php echo $text_enabled; ?></span></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_categories_mode" value="1" <?php if ($moneymaker2_header_categories_mode) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_categories_mode" value="0" <?php if (!$moneymaker2_header_categories_mode) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_images; ?></label>
                  <div class="col-sm-3">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_categories_hidethumbs" value="0" <?php if (!$moneymaker2_header_categories_hidethumbs) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_categories_hidethumbs" value="1" <?php if ($moneymaker2_header_categories_hidethumbs) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_image_size; ?></label>
                  <div class="col-sm-3">
                    <div class="input-group">
                      <input type="number" min="1" name="moneymaker2_header_categories_thumbs_width" value="<?php echo $moneymaker2_header_categories_thumbs_width; ?>" class="form-control">
                      <span class="input-group-addon"><?php echo $entry_px; ?></span>
                    </div><p class="text-center"><small><?php echo $entry_width; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group">
                      <input type="number" min="1" name="moneymaker2_header_categories_thumbs_height" value="<?php echo $moneymaker2_header_categories_thumbs_height; ?>" class="form-control">
                      <span class="input-group-addon"><?php echo $entry_px; ?></span>
                    </div><p class="text-center"><small><?php echo $entry_height; ?></small></p>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_columns; ?></label>
                  <div class="col-sm-3">
                    <select name="moneymaker2_header_categories_columns[lg]" class="form-control">
                      <?php for ($j = 2; $j <= count($moneymaker2_columns)+1; $j++) {
                        ($moneymaker2_header_categories_columns['lg'] == $moneymaker2_columns[$j]) ? $active = 'selected' : $active = ''; ?>
                      <option value="<?php echo $moneymaker2_columns[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_large; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <select name="moneymaker2_header_categories_columns[md]" class="form-control">
                      <?php for ($j = 2; $j <= count($moneymaker2_columns)+1; $j++) {
                        ($moneymaker2_header_categories_columns['md'] == $moneymaker2_columns[$j]) ? $active = 'selected' : $active = ''; ?>
                      <option value="<?php echo $moneymaker2_columns[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_medium; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <select name="moneymaker2_header_categories_columns[sm]" class="form-control">
                      <?php for ($j = 2; $j <= count($moneymaker2_columns)+1; $j++) {
                        ($moneymaker2_header_categories_columns['sm'] == $moneymaker2_columns[$j]) ? $active = 'selected' : $active = ''; ?>
                      <option value="<?php echo $moneymaker2_columns[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_small; ?></small></p>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><span class="text-muted"><?php echo $entry_categories; ?> > <?php echo $entry_advanced; ?> ></span> <?php echo $button_add; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_header_categories_add_banner_help; ?><br /></p>
                </div>
                <div class="row" id="vtab-header_banners">
                  <div class="col-sm-2">
                    <ul class="nav nav-pills nav-stacked" id="header_banners">
                      <?php $header_banner_row = 1; ?>
                      <?php foreach ($moneymaker2_header_banners as $value) { ?>
                      <li><a href="#tab-banner<?php echo $header_banner_row; ?>" data-toggle="tab" id="link-banner<?php echo $header_banner_row; ?>"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-banner<?php echo $header_banner_row; ?>\']').parent().remove(); $('#tab-banner<?php echo $header_banner_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#<?php echo $header_banner_row; ?></a></li>
                      <?php $header_banner_row++; ?>
                      <?php } ?>
                      <li>
                        <button type="button" onclick="addHeaderBanner();" class="btn btn-success form-control"><i class="fa fa-plus-circle"></i> <?php echo $button_add; ?></button>
                      </li>
                    </ul>
                  </div>
                  <div class="col-sm-10">
                    <div class="tab-content">
                      <?php $header_banner_row = 1; ?>
                      <?php foreach ($moneymaker2_header_banners as $value) { ?>
                      <div class="tab-pane" id="tab-banner<?php echo $header_banner_row; ?>">
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <p><h4><?php echo $tab_data; ?> #<?php echo $header_banner_row; ?></h4></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $tab_image; ?></label>
                          <div class="col-sm-10">
                            <a href="" id="moneymaker2_header_banners_image_thumb<?php echo $header_banner_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $value['thumb']; ?>" alt="" data-placeholder="<?php echo $placeholder; ?>"  /></a><input type="hidden" name="moneymaker2_header_banners[<?php echo $header_banner_row; ?>][image]" value="<?php echo $value['image']; ?>" id="moneymaker2_header_banners_image_input<?php echo $header_banner_row; ?>" />
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                          <div class="col-sm-5">
                            <div class="input-group">
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                              </div>
                              <select name="moneymaker2_header_banners[<?php echo $header_banner_row; ?>][icon]" class="form-control">
                                <?php $first = false; ?>
                                <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                                <?php $value['icon'] == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                                <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_style; ?></label>
                          <div class="col-sm-10">
                            <div class="input-group">
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#stylesModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                              </div>
                              <select name="moneymaker2_header_banners[<?php echo $header_banner_row; ?>][style]" class="form-control">
                                <?php foreach ($moneymaker2_banners_styles as $moneymaker2_banners_style) { ?>
                                <?php ($moneymaker2_banners_style ==  $value['style']) ? $active = 'selected' : $active=''; ?>
                                <option value="<?php echo $moneymaker2_banners_style; ?>" <?php echo $active; ?>><?php echo $moneymaker2_banners_style; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_title; ?></label>
                          <div class="col-sm-10">
                            <?php foreach ($languages as $language) { ?>
                            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input type="text" name="moneymaker2_header_banners[<?php echo $header_banner_row; ?>][name][<?php echo $language['language_id']; ?>]" value="<?php echo isset($value['name'][$language['language_id']]) ? $value['name'][$language['language_id']] : ''; ?>" class="form-control">
                            </div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_text; ?></label>
                          <div class="col-sm-10">
                            <?php foreach ($languages as $language) { ?>
                            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <textarea name="moneymaker2_header_banners[<?php echo $header_banner_row; ?>][text][<?php echo $language['language_id']; ?>]" rows="2" placeholder="<?php echo $entry_optional; ?>" class="form-control"><?php echo isset($value['text'][$language['language_id']]) ? $value['text'][$language['language_id']] : ''; ?></textarea>
                            </div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="moneymaker2_header_banners_link<?php echo $header_banner_row; ?>"><?php echo $button_link; ?></label>
                          <div class="col-sm-10">
                            <input type="text" name="moneymaker2_header_banners[<?php echo $header_banner_row; ?>][link]" value="<?php echo isset($value['link']) ? $value['link'] : ''; ?>" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_header_banners_link<?php echo $header_banner_row; ?>" class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="moneymaker2_header_banners_sort_order<?php echo $header_banner_row; ?>"><?php echo $entry_sort_order; ?></label>
                          <div class="col-sm-10">
                            <input type="number" min="0" name="moneymaker2_header_banners[<?php echo $header_banner_row; ?>][sort_order]" value="<?php echo isset($value['sort_order']) ? $value['sort_order'] : ''; ?>" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_header_banners_sort_order<?php echo $header_banner_row; ?>" class="form-control">
                          </div>
                        </div>
                      </div>
                      <?php $header_banner_row++; ?>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_categories_menu; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_header_categories_menu_help; ?><br /></p>
                </div>
                <div class="form-group margin-top">
                  <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_categories_menu" value="1" <?php if ($moneymaker2_header_categories_menu) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_categories_menu" value="0" <?php if (!$moneymaker2_header_categories_menu) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group margin-top">
                  <label class="col-sm-2 control-label"><?php echo $entry_common_categories_icons; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_categories_menu_icons" value="1" <?php if ($moneymaker2_header_categories_menu_icons) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_categories_menu_icons" value="0" <?php if (!$moneymaker2_header_categories_menu_icons) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_cart; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_header_cart_help; ?><br /></p>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_caption; ?></label>
                  <div class="col-sm-3">
                    <select name="moneymaker2_header_cart_caption_hide[lg]" class="form-control">
                      <?php if (!$moneymaker2_header_cart_caption_hide['lg']) { ?>
                      <option value="0" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="1"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="0"><?php echo $text_enabled; ?></option>
                      <option value="1" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_large; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <select name="moneymaker2_header_cart_caption_hide[md]" class="form-control">
                      <?php if (!$moneymaker2_header_cart_caption_hide['md']) { ?>
                      <option value="0" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="1"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="0"><?php echo $text_enabled; ?></option>
                      <option value="1" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_medium; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <select name="moneymaker2_header_cart_caption_hide[sm]" class="form-control">
                      <?php if (!$moneymaker2_header_cart_caption_hide['sm']) { ?>
                      <option value="0" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="1"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="0"><?php echo $text_enabled; ?></option>
                      <option value="1" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_small; ?></small></p>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_header_cart_custom; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_cart_custom" value="1" <?php if ($moneymaker2_header_cart_custom) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_cart_custom" value="0" <?php if (!$moneymaker2_header_cart_custom) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                  <div class="col-sm-10">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_header_cart_caption[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_header_cart_caption[$language['language_id']]) ? $moneymaker2_header_cart_caption[$language['language_id']] : ''; ?>" placeholder="<?php echo $entry_optional; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_animation; ?></label>
                  <div class="col-sm-3">

                    <select name="moneymaker2_header_cart_effect" class="form-control">
                      <?php if ($moneymaker2_header_cart_effect) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>


                  </div>

                  <div class="col-sm-3">
                    <input type="text" name="moneymaker2_header_cart_effect_color" value="<?php echo $moneymaker2_header_cart_effect_color; ?>"  class="form-control colorpicker" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_text; ?></label>
                  <div class="col-sm-10">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <textarea name="moneymaker2_header_cart_text[<?php echo $language['language_id']; ?>]" rows="2" placeholder="<?php echo $entry_optional; ?>" class="form-control"><?php echo isset($moneymaker2_header_cart_text[$language['language_id']]) ? $moneymaker2_header_cart_text[$language['language_id']] : ''; ?></textarea>
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_image; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_cart_image" value="1" <?php if ($moneymaker2_header_cart_image) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_cart_image" value="0" <?php if (!$moneymaker2_header_cart_image) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $tab_image; ?></label>
                  <div class="col-sm-10">
                    <a href="" id="moneymaker2_header_cart_image_src_thumb" data-toggle="image" class="img-thumbnail"><img src="<?php echo $moneymaker2_header_cart_thumb; ?>" alt="" data-placeholder="<?php echo $placeholder; ?>"  /></a><input type="hidden" name="moneymaker2_header_cart_image_src" value="<?php echo $moneymaker2_header_cart_image_src; ?>" id="moneymaker2_header_cart_image_src_input" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_button; ?></label>
                  <div class="col-sm-3">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_cart_link_enabled" value="1" <?php if ($moneymaker2_header_cart_link_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_cart_link_enabled" value="0" <?php if (!$moneymaker2_header_cart_link_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                  <div class="col-sm-5">
                    <div class="input-group">
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                      </div>
                      <select name="moneymaker2_header_cart_link_icon" class="form-control">
                        <?php $first = false; ?>
                        <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                        <?php $moneymaker2_header_cart_link_icon == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                        <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_button_title; ?></label>
                  <div class="col-sm-10">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_header_cart_link_title[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_header_cart_link_title[$language['language_id']]) ? $moneymaker2_header_cart_link_title[$language['language_id']] : ''; ?>" placeholder="<?php echo $entry_optional; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $button_link; ?></label>
                  <div class="col-sm-10">
                    <select name="moneymaker2_header_cart_link" class="form-control">
                      <option value="0"><?php echo $text_select ?></option>
                      <?php foreach ($informations as $information) { ?>
                      <option value="<?php echo $information['information_id']; ?>" <?php if ($information['information_id'] == $moneymaker2_header_cart_link) { ?>selected="selected"<?php } ?>><?php echo $information['title'] ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_search; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_header_search_help; ?><br /></p>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_caption; ?></label>
                  <div class="col-sm-3">
                    <select name="moneymaker2_header_search_caption_hide[lg]" class="form-control">
                      <?php if (!$moneymaker2_header_search_caption_hide['lg']) { ?>
                      <option value="0" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="1"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="0"><?php echo $text_enabled; ?></option>
                      <option value="1" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_large; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <select name="moneymaker2_header_search_caption_hide[md]" class="form-control">
                      <?php if (!$moneymaker2_header_search_caption_hide['md']) { ?>
                      <option value="0" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="1"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="0"><?php echo $text_enabled; ?></option>
                      <option value="1" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_medium; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <select name="moneymaker2_header_search_caption_hide[sm]" class="form-control">
                      <?php if (!$moneymaker2_header_search_caption_hide['sm']) { ?>
                      <option value="0" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="1"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="0"><?php echo $text_enabled; ?></option>
                      <option value="1" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_small; ?></small></p>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_header_search_ajax; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_search_ajax" value="1" <?php if ($moneymaker2_header_search_ajax) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_search_ajax" value="0" <?php if (!$moneymaker2_header_search_ajax) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_image; ?></label>
                  <div class="col-sm-3">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_search_ajax_image" value="1" <?php if ($moneymaker2_header_search_ajax_image) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_search_ajax_image" value="0" <?php if (!$moneymaker2_header_search_ajax_image) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_size; ?></label>
                  <div class="col-sm-2">
                    <input type="number" min="1" name="moneymaker2_header_search_ajax_image_width" value="<?php echo $moneymaker2_header_search_ajax_image_width; ?>"  id="moneymaker2_header_search_ajax_image_width" class="form-control"><p class="text-center"><small><?php echo $entry_width; ?></small></p>
                  </div>
                  <div class="col-sm-2">
                    <input type="number" min="1" name="moneymaker2_header_search_ajax_image_height" value="<?php echo $moneymaker2_header_search_ajax_image_height; ?>"  id="moneymaker2_header_search_ajax_image_height" class="form-control"><p class="text-center"><small><?php echo $entry_height; ?></small></p>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_header_search_ajax_price; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_search_ajax_price" value="1" <?php if ($moneymaker2_header_search_ajax_price) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_search_ajax_price" value="0" <?php if (!$moneymaker2_header_search_ajax_price) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_product_code; ?></label>
                  <div class="col-sm-3">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_search_ajax_code" value="1" <?php if ($moneymaker2_header_search_ajax_code) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_search_ajax_code" value="0" <?php if (!$moneymaker2_header_search_ajax_code) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                  <label class="col-sm-2 control-label" for="moneymaker2_header_search_ajax_code_field"><?php echo $entry_product_code_field; ?></label>
                  <div class="col-sm-4">
                    <select name="moneymaker2_header_search_ajax_code_field" id="moneymaker2_header_search_ajax_code_field" class="form-control">
                      <?php foreach ($moneymaker2_custom_fields as $moneymaker2_custom_field) { ?>
                      <?php ($moneymaker2_custom_field ==  $moneymaker2_header_search_ajax_code_field) ? $active = 'selected' : $active=''; ?>
                      <option value="<?php echo $moneymaker2_custom_field; ?>" <?php echo $active; ?>><?php echo $moneymaker2_custom_field; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_menu; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_header_menu_help; ?><br /></p>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_caption; ?></label>
                  <div class="col-sm-3">
                    <select name="moneymaker2_header_menu_caption_hide[lg]" class="form-control">
                      <?php if (!$moneymaker2_header_menu_caption_hide['lg']) { ?>
                      <option value="0" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="1"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="0"><?php echo $text_enabled; ?></option>
                      <option value="1" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_large; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <select name="moneymaker2_header_menu_caption_hide[md]" class="form-control">
                      <?php if (!$moneymaker2_header_menu_caption_hide['md']) { ?>
                      <option value="0" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="1"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="0"><?php echo $text_enabled; ?></option>
                      <option value="1" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_medium; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <select name="moneymaker2_header_menu_caption_hide[sm]" class="form-control">
                      <?php if (!$moneymaker2_header_menu_caption_hide['sm']) { ?>
                      <option value="0" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="1"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="0"><?php echo $text_enabled; ?></option>
                      <option value="1" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_small; ?></small></p>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><span class="text-muted"><?php echo $entry_menu; ?> ></span> <?php echo $entry_links; ?></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_menu_links_enabled" value="1" <?php if ($moneymaker2_header_menu_links_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_header_menu_links_enabled" value="0" <?php if (!$moneymaker2_header_menu_links_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                  <div class="col-sm-10">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_header_menu_links_caption[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_header_menu_links_caption[$language['language_id']]) ? $moneymaker2_header_menu_links_caption[$language['language_id']] : ''; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><span class="text-muted"><?php echo $entry_menu; ?> > <?php echo $entry_links; ?> ></span> <?php echo $button_add; ?></legend>
                <div class="row" id="vtab-header_links">
                  <div class="col-sm-2">
                    <ul class="nav nav-pills nav-stacked" id="header_links">
                      <?php $header_link_row = 1; ?>
                      <?php foreach ($moneymaker2_header_links as $value) { ?>
                      <li><a href="#tab-hlink<?php echo $header_link_row; ?>" data-toggle="tab" id="link-hlink<?php echo $header_link_row; ?>"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-hlink<?php echo $header_link_row; ?>\']').parent().remove(); $('#tab-hlink<?php echo $header_link_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#<?php echo $header_link_row; ?></a></li>
                      <?php $header_link_row++; ?>
                      <?php } ?>
                      <li>
                        <button type="button" onclick="addHeaderLink();" class="btn btn-success form-control"><i class="fa fa-plus-circle"></i> <?php echo $button_add; ?></button>
                      </li>
                    </ul>
                  </div>
                  <div class="col-sm-10">
                    <div class="tab-content">
                      <?php $header_link_row = 1; ?>
                      <?php foreach ($moneymaker2_header_links as $value) { ?>
                      <div class="tab-pane" id="tab-hlink<?php echo $header_link_row; ?>">
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <p><h4><?php echo $tab_data; ?> #<?php echo $header_link_row; ?></h4></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                          <div class="col-sm-5">
                            <div class="input-group">
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                              </div>
                              <select name="moneymaker2_header_links[<?php echo $header_link_row; ?>][icon]" class="form-control">
                                <?php $first = false; ?>
                                <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                                <?php $value['icon'] == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                                <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                          <div class="col-sm-10">
                            <?php foreach ($languages as $language) { ?>
                            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input type="text" name="moneymaker2_header_links[<?php echo $header_link_row; ?>][caption][<?php echo $language['language_id']; ?>]" value="<?php echo isset($value['caption'][$language['language_id']]) ? $value['caption'][$language['language_id']] : ''; ?>" class="form-control">
                            </div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $button_link; ?></label>
                          <div class="col-sm-10">
                            <input type="text" name="moneymaker2_header_links[<?php echo $header_link_row; ?>][link]" value="<?php echo isset($value['link']) ? $value['link'] : ''; ?>" class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_sort_order; ?></label>
                          <div class="col-sm-10">
                            <input type="number" min="0" name="moneymaker2_header_links[<?php echo $header_link_row; ?>][sort_order]" value="<?php echo isset($value['sort_order']) ? $value['sort_order'] : ''; ?>" placeholder="<?php echo $entry_optional; ?>" class="form-control">
                          </div>
                        </div>
                      </div>
                      <?php $header_link_row++; ?>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </fieldset>
              </div>

              <div class="tab-pane" id="tab_catalog">
              <fieldset>
                <legend><?php echo $entry_tab_catalog; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_tab_catalog_help; ?><br /></p>
                </div>
              </fieldset>
              <fieldset class="margin-top">
                <legend><?php echo $entry_layout; ?> (<?php echo $entry_list; ?> / <?php echo $entry_grid; ?>)</legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_catalog_layout_switcher; ?></label>
                  <div class="col-sm-4">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_layout_switcher_hide" value="0" <?php if (!$moneymaker2_catalog_layout_switcher_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_layout_switcher_hide" value="1" <?php if ($moneymaker2_catalog_layout_switcher_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_catalog_layout_default; ?></label>
                  <div class="col-sm-4">
                    <select name="moneymaker2_catalog_layout_default" class="form-control">
                      <?php if ($moneymaker2_catalog_layout_default) { ?>
                      <option value="1" selected="selected"><?php echo $entry_list; ?></option>
                      <option value="0"><?php echo $entry_grid; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $entry_list; ?></option>
                      <option value="0" selected="selected"><?php echo $entry_grid; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_categories_options; ?></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_catalog_categories_metatitles_enabled; ?></label>
                  <div class="col-sm-4">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_categories_metatitles_enabled" value="1" <?php if ($moneymaker2_catalog_categories_metatitles_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_categories_metatitles_enabled" value="0" <?php if (!$moneymaker2_catalog_categories_metatitles_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_images; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_categories_images_hide" value="0" <?php if (!$moneymaker2_catalog_categories_images_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_categories_images_hide" value="1" <?php if ($moneymaker2_catalog_categories_images_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_catalog_categories_move_description; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_categories_move_description" value="1" <?php if ($moneymaker2_catalog_categories_move_description) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_categories_move_description" value="0" <?php if (!$moneymaker2_catalog_categories_move_description) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_products_options; ?></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_stock_status; ?></label>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <span class="input-group-addon"><?php echo $entry_list; ?>:</span>
                      <select name="moneymaker2_catalog_products_list_stock_hide" class="form-control">
                        <?php if (!$moneymaker2_catalog_products_list_stock_hide) { ?>
                        <option value="0" selected="selected"><?php echo $text_enabled; ?></option>
                        <option value="1"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                        <option value="0"><?php echo $text_enabled; ?></option>
                        <option value="1" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <span class="input-group-addon"><?php echo $entry_grid; ?>:</span>
                      <select name="moneymaker2_catalog_products_grid_stock_hide" class="form-control">
                        <?php if (!$moneymaker2_catalog_products_grid_stock_hide) { ?>
                        <option value="0" selected="selected"><?php echo $text_enabled; ?></option>
                        <option value="1"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                        <option value="0"><?php echo $text_enabled; ?></option>
                        <option value="1" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_stock_status_color; ?></label>
                  <div class="col-sm-4">
                    <input type="text" name="moneymaker2_catalog_products_stock_color[0]" value="<?php echo $moneymaker2_catalog_products_stock_color[0]; ?>"  class="form-control colorpicker" /><p class="text-center"><small><?php echo $entry_instock; ?></small></p>
                  </div>
                  <div class="col-sm-4">
                    <input type="text" name="moneymaker2_catalog_products_stock_color[1]" value="<?php echo $moneymaker2_catalog_products_stock_color[1]; ?>"  class="form-control colorpicker" /><p class="text-center"><small><?php echo $entry_other; ?></small></p>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_product_code; ?></label>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <span class="input-group-addon"><?php echo $entry_list; ?>:</span>
                      <select name="moneymaker2_catalog_products_list_code_hide" class="form-control">
                        <?php if (!$moneymaker2_catalog_products_list_code_hide) { ?>
                        <option value="0" selected="selected"><?php echo $text_enabled; ?></option>
                        <option value="1"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                        <option value="0"><?php echo $text_enabled; ?></option>
                        <option value="1" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <span class="input-group-addon"><?php echo $entry_grid; ?>:</span>
                      <select name="moneymaker2_catalog_products_grid_code_hide" class="form-control">
                        <?php if (!$moneymaker2_catalog_products_grid_code_hide) { ?>
                        <option value="0" selected="selected"><?php echo $text_enabled; ?></option>
                        <option value="1"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                        <option value="0"><?php echo $text_enabled; ?></option>
                        <option value="1" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="moneymaker2_catalog_products_code_field"><?php echo $entry_product_code_field; ?></label>
                  <div class="col-sm-4">
                    <select name="moneymaker2_catalog_products_code_field" id="moneymaker2_catalog_products_code_field" class="form-control">
                      <?php foreach ($moneymaker2_custom_fields as $moneymaker2_custom_field) { ?>
                      <?php ($moneymaker2_custom_field ==  $moneymaker2_catalog_products_code_field) ? $active = 'selected' : $active=''; ?>
                      <option value="<?php echo $moneymaker2_custom_field; ?>" <?php echo $active; ?>><?php echo $moneymaker2_custom_field; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_contacts; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_catalog_contacts_help; ?><br /></p>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_quicklinks; ?></label>
                  <div class="col-sm-4">
                    <select name="moneymaker2_catalog_contacts_quicklinks" class="form-control">
                      <?php if ($moneymaker2_catalog_contacts_quicklinks) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_catalog_contacts_call_title; ?></label>
                  <div class="col-sm-4">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_catalog_contacts_call_title[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_catalog_contacts_call_title[$language['language_id']]) ? $moneymaker2_catalog_contacts_call_title[$language['language_id']] : ''; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_catalog_contacts_mail_title; ?></label>
                  <div class="col-sm-4">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_catalog_contacts_mail_title[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_catalog_contacts_mail_title[$language['language_id']]) ? $moneymaker2_catalog_contacts_mail_title[$language['language_id']] : ''; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_map; ?></label>
                  <div class="col-sm-4">
                    <select name="moneymaker2_catalog_contacts_map" class="form-control">
                      <?php if ($moneymaker2_catalog_contacts_map) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_address; ?></label>
                  <div class="col-sm-4">
                    <p class="paddingtop"><?php echo $entry_catalog_contacts_map_help; ?></p>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_compare; ?></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_charts; ?></label>
                  <div class="col-sm-4">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_charts_enabled" value="1" <?php if ($moneymaker2_catalog_compare_charts_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_charts_enabled" value="0" <?php if (!$moneymaker2_catalog_compare_charts_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_catalog_compare_charts_price_display; ?></label>
                  <div class="col-sm-4">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_charts_price_enabled" value="1" <?php if ($moneymaker2_catalog_compare_charts_price_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_charts_price_enabled" value="0" <?php if (!$moneymaker2_catalog_compare_charts_price_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_columns; ?></label>
                  <div class="col-sm-3">
                    <select name="moneymaker2_catalog_compare_charts_columns[lg]" class="form-control">
                      <?php for ($j = 0; $j <= count($moneymaker2_columns_bs)-1; $j++) {
                        ($moneymaker2_catalog_compare_charts_columns['lg'] == $moneymaker2_columns_bs[$j]) ? $active = 'selected' : $active = ''; ?>
                      <option value="<?php echo $moneymaker2_columns_bs[$j]; ?>" <?php echo $active; ?>><?php echo $moneymaker2_columns_bs[$j][1]; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_large; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <select name="moneymaker2_catalog_compare_charts_columns[md]" class="form-control">
                      <?php for ($j = 0; $j <= count($moneymaker2_columns_bs)-1; $j++) {
                        ($moneymaker2_catalog_compare_charts_columns['md'] == $moneymaker2_columns_bs[$j]) ? $active = 'selected' : $active = ''; ?>
                      <option value="<?php echo $moneymaker2_columns_bs[$j]; ?>" <?php echo $active; ?>><?php echo $moneymaker2_columns_bs[$j][1]; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_medium; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <select name="moneymaker2_catalog_compare_charts_columns[sm]" class="form-control">
                      <?php for ($j = 0; $j <= count($moneymaker2_columns_bs)-1; $j++) {
                        ($moneymaker2_catalog_compare_charts_columns['sm'] == $moneymaker2_columns_bs[$j]) ? $active = 'selected' : $active = ''; ?>
                      <option value="<?php echo $moneymaker2_columns_bs[$j]; ?>" <?php echo $active; ?>><?php echo $moneymaker2_columns_bs[$j][1]; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_small; ?></small></p>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_images; ?></label>
                  <div class="col-sm-4">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_images_hide" value="0" <?php if (!$moneymaker2_catalog_compare_images_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_images_hide" value="1" <?php if ($moneymaker2_catalog_compare_images_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_display_model; ?></label>
                  <div class="col-sm-4">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_model_hide" value="0" <?php if (!$moneymaker2_catalog_compare_model_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_model_hide" value="1" <?php if ($moneymaker2_catalog_compare_model_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_brand; ?></label>
                  <div class="col-sm-4">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_brand_hide" value="0" <?php if (!$moneymaker2_catalog_compare_brand_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_brand_hide" value="1" <?php if ($moneymaker2_catalog_compare_brand_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_display_availability; ?></label>
                  <div class="col-sm-4">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_availability_hide" value="0" <?php if (!$moneymaker2_catalog_compare_availability_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_availability_hide" value="1" <?php if ($moneymaker2_catalog_compare_availability_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_rating; ?></label>
                  <div class="col-sm-4">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_rating_hide" value="0" <?php if (!$moneymaker2_catalog_compare_rating_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_rating_hide" value="1" <?php if ($moneymaker2_catalog_compare_rating_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_display_description; ?></label>
                  <div class="col-sm-4">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_description_hide" value="0" <?php if (!$moneymaker2_catalog_compare_description_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_description_hide" value="1" <?php if ($moneymaker2_catalog_compare_description_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_weight; ?></label>
                  <div class="col-sm-4">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_weight_hide" value="0" <?php if (!$moneymaker2_catalog_compare_weight_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_weight_hide" value="1" <?php if ($moneymaker2_catalog_compare_weight_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_display_dimensions; ?></label>
                  <div class="col-sm-4">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_dimensions_hide" value="0" <?php if (!$moneymaker2_catalog_compare_dimensions_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_dimensions_hide" value="1" <?php if ($moneymaker2_catalog_compare_dimensions_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><span class="text-muted"><?php echo $entry_compare; ?> ></span> <?php echo $entry_catalog_compare_chart_add; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_catalog_compare_chart_add_help; ?><br /></p>
                </div>
                <div class="row" id="vtab-catalog_compare_charts">
                  <div class="col-sm-2">
                    <ul class="nav nav-pills nav-stacked" id="catalog_compare_charts">
                      <?php $catalog_compare_chart_row = 1; ?>
                      <?php foreach ($moneymaker2_catalog_compare_charts as $value) { ?>
                      <li><a href="#tab-catalog_compare_chart<?php echo $catalog_compare_chart_row; ?>" data-toggle="tab" id="link-contact<?php echo $catalog_compare_chart_row; ?>"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-catalog_compare_chart<?php echo $catalog_compare_chart_row; ?>\']').parent().remove(); $('#tab-catalog_compare_chart<?php echo $catalog_compare_chart_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#<?php echo $catalog_compare_chart_row; ?></a></li>
                      <?php $catalog_compare_chart_row++; ?>
                      <?php } ?>
                      <li>
                        <button type="button" onclick="addCatalogCompareChart();" class="btn btn-success form-control"><i class="fa fa-plus-circle"></i> <?php echo $button_add; ?></button>
                      </li>
                    </ul>
                  </div>
                  <div class="col-sm-10">
                    <div class="tab-content">
                      <?php $catalog_compare_chart_row = 1; ?>
                      <?php foreach ($moneymaker2_catalog_compare_charts as $value) { ?>
                      <div class="tab-pane" id="tab-catalog_compare_chart<?php echo $catalog_compare_chart_row; ?>">
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <p><h4><?php echo $tab_data; ?> #<?php echo $catalog_compare_chart_row; ?></h4></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_mode; ?></label>
                          <div class="col-sm-10">
                            <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_charts[<?php echo $catalog_compare_chart_row; ?>][mode]" value="0" <?php if (!$value['mode']) { ?>checked="checked"<?php } ?> /> <?php echo $entry_chart_line; ?></label>
                            <label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_charts[<?php echo $catalog_compare_chart_row; ?>][mode]" value="1" <?php if ($value['mode']) { ?>checked="checked"<?php } ?> /> <?php echo $entry_chart_bar; ?></label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_catalog_compare_chart_functions; ?></label>
                          <div class="col-sm-4">
                            <select name="moneymaker2_catalog_compare_charts[<?php echo $catalog_compare_chart_row; ?>][fns]" class="form-control">
                              <?php for ($j = 1; $j <= 5; $j++) {
                        ($value['fns'] == $j) ? $active = 'selected' : $active = ''; ?>
                              <option value="<?php echo $j; ?>" <?php echo $active; ?>><?php echo $j; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <label class="col-sm-2 control-label"><?php echo $entry_delimiter; ?></label>
                          <div class="col-sm-4">
                            <select name="moneymaker2_catalog_compare_charts[<?php echo $catalog_compare_chart_row; ?>][delimiter]" class="form-control">
                              <option value="0"><?php echo $text_select ?></option>
                              <?php foreach ($moneymaker2_delimiters as $moneymaker2_delimiter) { ?>
                              <?php ($moneymaker2_delimiter ==  $value['delimiter']) ? $active = 'selected' : $active=''; ?>
                              <option value="<?php echo $moneymaker2_delimiter; ?>" <?php echo $active; ?>><?php echo $moneymaker2_delimiter; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_attribute; ?></label>
                          <div class="col-sm-10">
                            <select name="moneymaker2_catalog_compare_charts[<?php echo $catalog_compare_chart_row; ?>][attribute]" class="form-control">
                              <option value="0"><?php echo $text_select ?></option>
                              <?php foreach ($attribute_groups as $attribute_group) { ?>
                              <?php foreach ($attributes as $attribute) { ?>
                              <?php if ($attribute['attribute_group_id'] == $attribute_group['attribute_group_id']) { ?>
                              <?php ($attribute['attribute_id'] == $value['attribute']) ? $active = 'selected' : $active=''; ?>
                              <option value="<?php echo $attribute['attribute_id']; ?>" <?php echo $active; ?>> <?php echo $attribute_group['name']; ?> > <?php echo $attribute['name']; ?></option>
                              <?php } ?>
                              <?php } ?>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_unit; ?></label>
                          <div class="col-sm-10">
                            <?php foreach ($languages as $language) { ?>
                            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input type="text" name="moneymaker2_catalog_compare_charts[<?php echo $catalog_compare_chart_row; ?>][unit][<?php echo $language['language_id']; ?>]" value="<?php echo isset($value['unit'][$language['language_id']]) ? $value['unit'][$language['language_id']] : ''; ?>" placeholder="<?php echo $entry_optional; ?>" class="form-control">
                            </div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_sort_order; ?></label>
                          <div class="col-sm-10">
                            <p class="paddingtop"><?php echo $entry_catalog_compare_chart_sort_help; ?></p>
                          </div>
                        </div>
                      </div>
                      <?php $catalog_compare_chart_row++; ?>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </fieldset>
              </div>

              <div class="tab-pane" id="tab_product">
              <fieldset>
                <legend><?php echo $entry_tab_product; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_tab_product_help; ?><br /></p>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_product_metatitles_enabled; ?></label>
                  <div class="col-sm-4">
                    <select name="moneymaker2_product_metatitles_enabled" class="form-control">
                      <?php if ($moneymaker2_product_metatitles_enabled) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_socials; ?></label>
                  <div class="col-sm-4">
                    <select name="moneymaker2_product_socials_enabled" class="form-control">
                      <?php if ($moneymaker2_product_socials_enabled) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_product_code; ?></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_display_product_code; ?></label>
                  <div class="col-sm-3">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_code" value="1" <?php if ($moneymaker2_product_code) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_code" value="0" <?php if (!$moneymaker2_product_code) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_product_code_field; ?></label>
                  <div class="col-sm-4">
                    <select name="moneymaker2_product_code_field" class="form-control">
                      <?php foreach ($moneymaker2_custom_fields as $moneymaker2_custom_field) { ?>
                      <?php ($moneymaker2_custom_field ==  $moneymaker2_product_code_field) ? $active = 'selected' : $active=''; ?>
                      <option value="<?php echo $moneymaker2_custom_field; ?>" <?php echo $active; ?>><?php echo $moneymaker2_custom_field; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                  <div class="col-sm-4">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_product_code_caption[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_product_code_caption[$language['language_id']]) ? $moneymaker2_product_code_caption[$language['language_id']] : ''; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_photogallery; ?></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_product_gallery_moved; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_gallery_moved" value="1" <?php if ($moneymaker2_product_gallery_moved) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_gallery_moved" value="0" <?php if (!$moneymaker2_product_gallery_moved) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_product_gallery_round; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_gallery_round" value="1" <?php if ($moneymaker2_product_gallery_round) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_gallery_round" value="0" <?php if (!$moneymaker2_product_gallery_round) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_animation; ?></label>
                  <div class="col-sm-3">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_gallery_animation" value="1" <?php if ($moneymaker2_product_gallery_animation) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_gallery_animation" value="0" <?php if (!$moneymaker2_product_gallery_animation) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_animation_in; ?></label>
                  <div class="col-sm-4">
                    <select name="moneymaker2_product_gallery_animation_in" class="form-control">
                      <?php foreach ($moneymaker2_gallery_animation_styles as $moneymaker2_gallery_animation_style) { ?>
                      <?php ($moneymaker2_gallery_animation_style ==  $moneymaker2_product_gallery_animation_in) ? $active = 'selected' : $active=''; ?>
                      <option value="<?php echo $moneymaker2_gallery_animation_style; ?>" <?php echo $active; ?>><?php echo $moneymaker2_gallery_animation_style; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_product_thumbs; ?></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_limit; ?></label>
                  <div class="col-sm-3">
                    <select name="moneymaker2_product_thumbs_limit[lg]" class="form-control">
                      <?php for ($j = 1; $j <= 10; $j++) {
                          ($moneymaker2_product_thumbs_limit['lg'] == $j) ? $active = 'selected' : $active = ''; ?>
                      <option value="<?php echo $j; ?>" <?php echo $active; ?>><?php echo $j; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_large; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <select name="moneymaker2_product_thumbs_limit[md]" class="form-control">
                      <?php for ($j = 1; $j <= 10; $j++) {
                          ($moneymaker2_product_thumbs_limit['md'] == $j) ? $active = 'selected' : $active = ''; ?>
                      <option value="<?php echo $j; ?>" <?php echo $active; ?>><?php echo $j; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_medium; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <select name="moneymaker2_product_thumbs_limit[sm]" class="form-control">
                      <?php for ($j = 1; $j <= 10; $j++) {
                          ($moneymaker2_product_thumbs_limit['sm'] == $j) ? $active = 'selected' : $active = ''; ?>
                      <option value="<?php echo $j; ?>" <?php echo $active; ?>><?php echo $j; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_small; ?></small></p>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"></label>
                  <div class="col-sm-3">
                    <select name="moneymaker2_product_thumbs_limit[xsm]" class="form-control">
                      <?php for ($j = 1; $j <= 10; $j++) {
                          ($moneymaker2_product_thumbs_limit['xsm'] == $j) ? $active = 'selected' : $active = ''; ?>
                      <option value="<?php echo $j; ?>" <?php echo $active; ?>><?php echo $j; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_xsmall; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <select name="moneymaker2_product_thumbs_limit[xxsm]" class="form-control">
                      <?php for ($j = 1; $j <= 10; $j++) {
                          ($moneymaker2_product_thumbs_limit['xxsm'] == $j) ? $active = 'selected' : $active = ''; ?>
                      <option value="<?php echo $j; ?>" <?php echo $active; ?>><?php echo $j; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_xxsmall; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <select name="moneymaker2_product_thumbs_limit[xxxsm]" class="form-control">
                      <?php for ($j = 1; $j <= 10; $j++) {
                          ($moneymaker2_product_thumbs_limit['xxxsm'] == $j) ? $active = 'selected' : $active = ''; ?>
                      <option value="<?php echo $j; ?>" <?php echo $active; ?>><?php echo $j; ?></option>
                      <?php } ?>
                    </select><p class="text-center"><small><?php echo $entry_screen_xxxsmall; ?></small></p>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_main_points; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_product_main_points_help; ?><br /></p>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_size; ?></label>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#sizesModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                      </div>
                      <select name="moneymaker2_product_points_size" class="form-control">
                        <?php foreach ($moneymaker2_stickers_sizes as $moneymaker2_stickers_size) { ?>
                        <option value="<?php echo $moneymaker2_stickers_size; ?>" <?php if ($moneymaker2_product_points_size == $moneymaker2_stickers_size) { ?>selected="selected"<?php } ?>><?php echo $moneymaker2_stickers_size; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><span class="text-muted"><?php echo $entry_main_points; ?> ></span> <?php echo $entry_stock_status; ?></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_points_stock" value="1" <?php if ($moneymaker2_product_points_stock) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_points_stock" value="0" <?php if (!$moneymaker2_product_points_stock) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-2 control-label"><?php echo $entry_product_points_stock_default; ?></label>
                  <div class="col-sm-4">
                    <select name="moneymaker2_product_points_stock_default" class="form-control">
                      <option value="0"><?php echo $text_select ?></option>
                      <?php foreach ($stock_statuses as $stock_status) { ?>
                      <?php ($stock_status['stock_status_id'] == $moneymaker2_product_points_stock_default) ? $active = 'selected' : $active=''; ?>
                      <option value="<?php echo $stock_status['stock_status_id']; ?>" <?php echo $active; ?>><?php echo $stock_status['name'] ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </fieldset>
              <?php foreach ($stock_statuses as $stock_status) { ?>
              <fieldset>
                <legend class="text-center"><small><?php echo $stock_status['name']; ?></small></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                      </div>
                      <select name="moneymaker2_product_points_stock_icon[<?php echo $stock_status['stock_status_id']; ?>]" class="form-control">
                        <?php $first = false; ?>
                        <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                        <?php $moneymaker2_product_points_stock_icon[$stock_status['stock_status_id']] == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                        <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_color; ?></label>
                  <div class="col-sm-3">
                    <input type="text" name="moneymaker2_product_points_stock_color[<?php echo $stock_status['stock_status_id']; ?>]" value="<?php echo $moneymaker2_product_points_stock_color[$stock_status['stock_status_id']]; ?>"  class="form-control colorpicker" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                  <div class="col-sm-9">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_product_points_stock_caption[<?php echo $stock_status['stock_status_id']; ?>][<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_product_points_stock_caption[$stock_status['stock_status_id']][$language['language_id']]) ? $moneymaker2_product_points_stock_caption[$stock_status['stock_status_id']][$language['language_id']] : ''; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </fieldset>
              <?php } ?>
              <fieldset>
                <legend><span class="text-muted"><?php echo $entry_main_points; ?> ></span> <?php echo $entry_manufacturer; ?></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_points_manufacturer" value="1" <?php if ($moneymaker2_product_points_manufacturer) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_points_manufacturer" value="0" <?php if (!$moneymaker2_product_points_manufacturer) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_image_size; ?></label>
                  <div class="col-sm-3">
                    <div class="input-group">
                      <input type="number" min="1" name="moneymaker2_product_points_manufacturer_image_width" value="<?php echo $moneymaker2_product_points_manufacturer_image_width; ?>" class="form-control">
                      <span class="input-group-addon"><?php echo $entry_px; ?></span>
                    </div><p class="text-center"><small><?php echo $entry_width; ?></small></p>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group">
                      <input type="number" min="1" name="moneymaker2_product_points_manufacturer_image_height" value="<?php echo $moneymaker2_product_points_manufacturer_image_height; ?>" class="form-control">
                      <span class="input-group-addon"><?php echo $entry_px; ?></span>
                    </div><p class="text-center"><small><?php echo $entry_height; ?></small></p>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_caption; ?> (<?php echo $button_link; ?>)</label>
                  <div class="col-sm-9">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_product_points_manufacturer_caption[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_product_points_manufacturer_caption[$language['language_id']]) ? $moneymaker2_product_points_manufacturer_caption[$language['language_id']] : ''; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><span class="text-muted"><?php echo $entry_main_points; ?> ></span> <?php echo $button_add; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_product_points_add_point_help; ?><br /></p>
                </div>
                <div class="row" id="vtab-product_points">
                  <div class="col-sm-2">
                    <ul class="nav nav-pills nav-stacked" id="product_points">
                      <?php $product_point_row = 1; ?>
                      <?php foreach ($moneymaker2_product_points as $value) { ?>
                      <li><a href="#tab-point<?php echo $product_point_row; ?>" data-toggle="tab" id="link-point<?php echo $product_point_row; ?>"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-point<?php echo $product_point_row; ?>\']').parent().remove(); $('#tab-point<?php echo $product_point_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#<?php echo $product_point_row; ?></a></li>
                      <?php $product_point_row++; ?>
                      <?php } ?>
                      <li>
                        <button type="button" onclick="addProductPoint();" class="btn btn-success form-control"><i class="fa fa-plus-circle"></i> <?php echo $button_add; ?></button>
                      </li>
                    </ul>
                  </div>
                  <div class="col-sm-10">
                    <div class="tab-content">
                      <?php $product_point_row = 1; ?>
                      <?php foreach ($moneymaker2_product_points as $value) { ?>
                      <div class="tab-pane" id="tab-point<?php echo $product_point_row; ?>">
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <p><h4><?php echo $tab_data; ?> #<?php echo $product_point_row; ?></h4></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                          <div class="col-sm-5">
                            <div class="input-group">
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                              </div>
                              <select name="moneymaker2_product_points[<?php echo $product_point_row; ?>][icon]" class="form-control">
                                <?php $first = false; ?>
                                <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                                <?php $value['icon'] == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                                <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_categories; ?></label>
                          <div class="col-sm-10">
                            <div class="well well-sm">
                              <?php foreach ($categories as $category) { ?>
                              <div class="checkbox">
                                <label>
                                  <?php if (in_array($category['category_id'], $value['categories'])) { ?>
                                  <input type="checkbox" name="moneymaker2_product_points[<?php echo $product_point_row; ?>][categories][]" value="<?php echo $category['category_id']; ?>" checked="checked" /> <?php echo $category['name']; ?>
                                  <?php } else { ?>
                                  <input type="checkbox" name="moneymaker2_product_points[<?php echo $product_point_row; ?>][categories][]" value="<?php echo $category['category_id']; ?>" /> <?php echo $category['name']; ?>
                                  <?php } ?>
                                </label>
                              </div>
                              <?php } ?>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_title; ?></label>
                          <div class="col-sm-10">
                            <?php foreach ($languages as $language) { ?>
                            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input type="text" name="moneymaker2_product_points[<?php echo $product_point_row; ?>][name][<?php echo $language['language_id']; ?>]" value="<?php echo isset($value['name'][$language['language_id']]) ? $value['name'][$language['language_id']] : ''; ?>" class="form-control">
                            </div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_text; ?></label>
                          <div class="col-sm-10">
                            <?php foreach ($languages as $language) { ?>
                            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <textarea name="moneymaker2_product_points[<?php echo $product_point_row; ?>][text][<?php echo $language['language_id']; ?>]" rows="2" class="form-control"><?php echo isset($value['text'][$language['language_id']]) ? $value['text'][$language['language_id']] : ''; ?></textarea>
                            </div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="moneymaker2_product_points_sort_order<?php echo $product_point_row; ?>"><?php echo $entry_sort_order; ?></label>
                          <div class="col-sm-10">
                            <input type="number" min="0" name="moneymaker2_product_points[<?php echo $product_point_row; ?>][sort_order]" value="<?php echo isset($value['sort_order']) ? $value['sort_order'] : ''; ?>" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_product_points_sort_order<?php echo $product_point_row; ?>" class="form-control">
                          </div>
                        </div>
                      </div>
                      <?php $product_point_row++; ?>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </fieldset>

              <fieldset>
                <legend><?php echo $entry_options; ?></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_product_options_hide; ?></label>
                  <div class="col-sm-4">
                    <select name="moneymaker2_product_options_hide" class="form-control">
                      <?php if ($moneymaker2_product_options_hide) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_limit; ?></label>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <input type="number" min="1" name="moneymaker2_product_options_hide_limit" value="<?php echo $moneymaker2_product_options_hide_limit; ?>"  class="form-control">
                      <span class="input-group-addon"><?php echo $entry_options; ?></span>
                    </div>
                  </div>
                </div>
              </fieldset>

              <fieldset>
                <legend><?php echo $entry_tabs; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_product_tabs_help; ?><br /></p>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $entry_product_tabs_display_help; ?>"><?php echo $entry_product_tabs_display; ?></span></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_tabs_hide" value="0" <?php if (!$moneymaker2_product_tabs_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_tabs_hide" value="1" <?php if ($moneymaker2_product_tabs_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_product_tabs_headers; ?></label>
                  <div class="col-sm-4">
                    <select name="moneymaker2_product_tabs_headers" class="form-control">
                      <?php foreach ($moneymaker2_header_modes as $key => $value) { ?>
                      <option value="<?php echo $key; ?>" <?php if ($moneymaker2_product_tabs_headers == $key) { ?>selected="selected"<?php } ?>><?php echo $value; ?></option>
                      <?php } ?>
                    </select>
                    <p class="paddingtop"><?php echo $entry_product_tabs_headers_help; ?></p>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_text; ?></label>
                  <div class="col-sm-4">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_product_tabs_headers_text[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_product_tabs_headers_text[$language['language_id']]) ? $moneymaker2_product_tabs_headers_text[$language['language_id']] : ''; ?>" class="form-control">
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_product_tabs_attributes_responsive; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_tabs_attributes_responsive" value="1" <?php if ($moneymaker2_product_tabs_attributes_responsive) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_product_tabs_attributes_responsive" value="0" <?php if (!$moneymaker2_product_tabs_attributes_responsive) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
              </fieldset>
              </div>

              <div class="tab-pane" id="tab_footer">
              <fieldset>
                <legend><?php echo $entry_tab_footer; ?></legend>
                <div class="form-group margin-top">
                  <p class="col-sm-12"><?php echo $entry_footer_help; ?><br /></p>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_footer_display_information; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_information_hide" value="0" <?php if (!$moneymaker2_footer_information_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_information_hide" value="1" <?php if ($moneymaker2_footer_information_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_footer_display_customer; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_customer_hide" value="0" <?php if (!$moneymaker2_footer_customer_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_customer_hide" value="1" <?php if ($moneymaker2_footer_customer_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_footer_display_extras; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_extras_hide" value="0" <?php if (!$moneymaker2_footer_extras_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_extras_hide" value="1" <?php if ($moneymaker2_footer_extras_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_footer_display_account; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_account_hide" value="0" <?php if (!$moneymaker2_footer_account_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_account_hide" value="1" <?php if ($moneymaker2_footer_account_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_contacts; ?></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_contacts_enabled" value="1" <?php if ($moneymaker2_footer_contacts_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_contacts_enabled" value="0" <?php if (!$moneymaker2_footer_contacts_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_title; ?></label>
                  <div class="col-sm-4">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_footer_contacts_title[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_footer_contacts_title[$language['language_id']]) ? $moneymaker2_footer_contacts_title[$language['language_id']] : ''; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                      </div>
                      <select name="moneymaker2_footer_contacts_icon" class="form-control">
                        <?php $first = false; ?>
                        <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                        <?php $moneymaker2_footer_contacts_icon == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                        <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><span class="text-muted"><?php echo $entry_contacts; ?> ></span> <?php echo $button_add; ?></legend>
                <div class="row" id="vtab-footer_contacts">
                  <div class="col-sm-2">
                    <ul class="nav nav-pills nav-stacked" id="footer_contacts">
                      <?php $footer_contact_row = 1; ?>
                      <?php foreach ($moneymaker2_footer_contacts as $value) { ?>
                      <li><a href="#tab-fcontact<?php echo $footer_contact_row; ?>" data-toggle="tab" id="link-fcontact<?php echo $footer_contact_row; ?>"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-fcontact<?php echo $footer_contact_row; ?>\']').parent().remove(); $('#tab-fcontact<?php echo $footer_contact_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#<?php echo $footer_contact_row; ?></a></li>
                      <?php $footer_contact_row++; ?>
                      <?php } ?>
                      <li>
                        <button type="button" onclick="addFooterContact();" class="btn btn-success form-control"><i class="fa fa-plus-circle"></i> <?php echo $button_add; ?></button>
                      </li>
                    </ul>
                  </div>
                  <div class="col-sm-10">
                    <div class="tab-content">
                      <?php $footer_contact_row = 1; ?>
                      <?php foreach ($moneymaker2_footer_contacts as $value) { ?>
                      <div class="tab-pane" id="tab-fcontact<?php echo $footer_contact_row; ?>">
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <p><h4><?php echo $tab_data; ?> #<?php echo $footer_contact_row; ?></h4></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                          <div class="col-sm-10">
                            <?php foreach ($languages as $language) { ?>
                            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input type="text" name="moneymaker2_footer_contacts[<?php echo $footer_contact_row; ?>][caption][<?php echo $language['language_id']; ?>]" value="<?php echo isset($value['caption'][$language['language_id']]) ? $value['caption'][$language['language_id']] : ''; ?>" class="form-control">
                            </div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="moneymaker2_footer_contacts_link<?php echo $footer_contact_row; ?>"><?php echo $button_link; ?></label>
                          <div class="col-sm-10">
                            <input type="text" name="moneymaker2_footer_contacts[<?php echo $footer_contact_row; ?>][link]" value="<?php echo isset($value['link']) ? $value['link'] : ''; ?>" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_footer_contacts_link<?php echo $footer_contact_row; ?>" class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="moneymaker2_footer_contacts_sort_order<?php echo $footer_contact_row; ?>"><?php echo $entry_sort_order; ?></label>
                          <div class="col-sm-10">
                            <input type="number" min="0" name="moneymaker2_footer_contacts[<?php echo $footer_contact_row; ?>][sort_order]" value="<?php echo isset($value['sort_order']) ? $value['sort_order'] : ''; ?>" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_footer_contacts_sort_order<?php echo $footer_contact_row; ?>" class="form-control">
                          </div>
                        </div>
                      </div>
                      <?php $footer_contact_row++; ?>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_links; ?></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_links_enabled" value="1" <?php if ($moneymaker2_footer_links_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_links_enabled" value="0" <?php if (!$moneymaker2_footer_links_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_title; ?></label>
                  <div class="col-sm-4">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_footer_links_title[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_footer_links_title[$language['language_id']]) ? $moneymaker2_footer_links_title[$language['language_id']] : ''; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                      </div>
                      <select name="moneymaker2_footer_links_icon" class="form-control">
                        <?php $first = false; ?>
                        <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                        <?php $moneymaker2_footer_links_icon == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                        <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><span class="text-muted"><?php echo $entry_links; ?> ></span> <?php echo $button_add; ?></legend>
                <div class="row" id="vtab-footer_links">
                  <div class="col-sm-2">
                    <ul class="nav nav-pills nav-stacked" id="footer_links">
                      <?php $footer_link_row = 1; ?>
                      <?php foreach ($moneymaker2_footer_links as $value) { ?>
                      <li><a href="#tab-flink<?php echo $footer_link_row; ?>" data-toggle="tab" id="link-flink<?php echo $footer_link_row; ?>"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-flink<?php echo $footer_link_row; ?>\']').parent().remove(); $('#tab-flink<?php echo $footer_link_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#<?php echo $footer_link_row; ?></a></li>
                      <?php $footer_link_row++; ?>
                      <?php } ?>
                      <li>
                        <button type="button" onclick="addFooterLink();" class="btn btn-success form-control"><i class="fa fa-plus-circle"></i> <?php echo $button_add; ?></button>
                      </li>
                    </ul>
                  </div>
                  <div class="col-sm-10">
                    <div class="tab-content">
                      <?php $footer_link_row = 1; ?>
                      <?php foreach ($moneymaker2_footer_links as $value) { ?>
                      <div class="tab-pane" id="tab-flink<?php echo $footer_link_row; ?>">
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <p><h4><?php echo $tab_data; ?> #<?php echo $footer_link_row; ?></h4></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                          <div class="col-sm-10">
                            <?php foreach ($languages as $language) { ?>
                            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input type="text" name="moneymaker2_footer_links[<?php echo $footer_link_row; ?>][caption][<?php echo $language['language_id']; ?>]" value="<?php echo isset($value['caption'][$language['language_id']]) ? $value['caption'][$language['language_id']] : ''; ?>" class="form-control">
                            </div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="moneymaker2_footer_links_link<?php echo $footer_link_row; ?>"><?php echo $button_link; ?></label>
                          <div class="col-sm-10">
                            <input type="text" name="moneymaker2_footer_links[<?php echo $footer_link_row; ?>][link]" value="<?php echo isset($value['link']) ? $value['link'] : ''; ?>" id="moneymaker2_footer_links_link<?php echo $footer_link_row; ?>" class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="moneymaker2_footer_links_sort_order<?php echo $footer_link_row; ?>"><?php echo $entry_sort_order; ?></label>
                          <div class="col-sm-10">
                            <input type="number" min="0" name="moneymaker2_footer_links[<?php echo $footer_link_row; ?>][sort_order]" value="<?php echo isset($value['sort_order']) ? $value['sort_order'] : ''; ?>" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_footer_links_sort_order<?php echo $footer_link_row; ?>" class="form-control">
                          </div>
                        </div>
                      </div>
                      <?php $footer_link_row++; ?>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_text; ?> I</legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_text1_enabled" value="1" <?php if ($moneymaker2_footer_text1_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_text1_enabled" value="0" <?php if (!$moneymaker2_footer_text1_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_title; ?></label>
                  <div class="col-sm-4">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_footer_text1_title[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_footer_text1_title[$language['language_id']]) ? $moneymaker2_footer_text1_title[$language['language_id']] : ''; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                      </div>
                      <select name="moneymaker2_footer_text1_icon" class="form-control">
                        <?php $first = false; ?>
                        <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                        <?php $moneymaker2_footer_text1_icon == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                        <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_text; ?></label>
                  <div class="col-sm-10">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <textarea name="moneymaker2_footer_text1[<?php echo $language['language_id']; ?>]" rows="2" class="form-control"><?php echo isset($moneymaker2_footer_text1[$language['language_id']]) ? $moneymaker2_footer_text1[$language['language_id']] : ''; ?></textarea>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_text; ?> II</legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_text2_enabled" value="1" <?php if ($moneymaker2_footer_text2_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_text2_enabled" value="0" <?php if (!$moneymaker2_footer_text2_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_title; ?></label>
                  <div class="col-sm-4">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_footer_text2_title[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_footer_text2_title[$language['language_id']]) ? $moneymaker2_footer_text2_title[$language['language_id']] : ''; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                      </div>
                      <select name="moneymaker2_footer_text2_icon" class="form-control">
                        <?php $first = false; ?>
                        <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                        <?php $moneymaker2_footer_text2_icon == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                        <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_text; ?></label>
                  <div class="col-sm-10">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <textarea name="moneymaker2_footer_text2[<?php echo $language['language_id']; ?>]" rows="2" class="form-control"><?php echo isset($moneymaker2_footer_text2[$language['language_id']]) ? $moneymaker2_footer_text2[$language['language_id']] : ''; ?></textarea>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_copyrights; ?></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_footer_display_copyrights; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_copyrights_hide" value="0" <?php if (!$moneymaker2_footer_copyrights_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_copyrights_hide" value="1" <?php if ($moneymaker2_footer_copyrights_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_footer_display_powered; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_powered_hide" value="0" <?php if (!$moneymaker2_footer_powered_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_powered_hide" value="1" <?php if ($moneymaker2_footer_powered_hide) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><span class="text-muted"><?php echo $entry_copyrights; ?> ></span> <?php echo $entry_custom; ?></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_powered_custom_enabled" value="1" <?php if ($moneymaker2_footer_powered_custom_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_powered_custom_enabled" value="0" <?php if (!$moneymaker2_footer_powered_custom_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_text; ?></label>
                  <div class="col-sm-10">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <textarea name="moneymaker2_footer_powered_custom_text[<?php echo $language['language_id']; ?>]" rows="2" class="form-control"><?php echo isset($moneymaker2_footer_powered_custom_text[$language['language_id']]) ? $moneymaker2_footer_powered_custom_text[$language['language_id']] : ''; ?></textarea>
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_title; ?></label>
                  <div class="col-sm-4">
                    <?php foreach ($languages as $language) { ?>
                    <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                      <input type="text" name="moneymaker2_footer_powered_custom_title[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_footer_powered_custom_title[$language['language_id']]) ? $moneymaker2_footer_powered_custom_title[$language['language_id']] : ''; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </div>
                  <label class="col-sm-2 control-label"><?php echo $entry_year_foundation; ?></label>
                  <div class="col-sm-4">
                    <input type="number" name="moneymaker2_footer_powered_custom_year" value="<?php echo isset($moneymaker2_footer_powered_custom_year) ? $moneymaker2_footer_powered_custom_year : ''; ?>" placeholder="<?php echo $entry_optional; ?>" class="form-control" />
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><?php echo $entry_socials; ?></legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_socials_enabled" value="1" <?php if ($moneymaker2_footer_socials_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_socials_enabled" value="0" <?php if (!$moneymaker2_footer_socials_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo $entry_target_blank; ?></label>
                  <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_socials_target_blank" value="1" <?php if ($moneymaker2_footer_socials_target_blank) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                    <label class="radio-inline"><input type="radio" name="moneymaker2_footer_socials_target_blank" value="0" <?php if (!$moneymaker2_footer_socials_target_blank) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend><span class="text-muted"><?php echo $entry_socials; ?> ></span> <?php echo $button_add; ?></legend>
                <div class="row" id="vtab-footer_socials">
                  <div class="col-sm-2">
                    <ul class="nav nav-pills nav-stacked" id="footer_socials">
                      <?php $footer_social_row = 1; ?>
                      <?php foreach ($moneymaker2_footer_socials as $value) { ?>
                      <li><a href="#tab-social<?php echo $footer_social_row; ?>" data-toggle="tab" id="link-social<?php echo $footer_social_row; ?>"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-social<?php echo $footer_social_row; ?>\']').parent().remove(); $('#tab-social<?php echo $footer_social_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#<?php echo $footer_social_row; ?></a></li>
                      <?php $footer_social_row++; ?>
                      <?php } ?>
                      <li>
                        <button type="button" onclick="addFooterSocial();" class="btn btn-success form-control"><i class="fa fa-plus-circle"></i> <?php echo $button_add; ?></button>
                      </li>
                    </ul>
                  </div>
                  <div class="col-sm-10">
                    <div class="tab-content">
                      <?php $footer_social_row = 1; ?>
                      <?php foreach ($moneymaker2_footer_socials as $value) { ?>
                      <div class="tab-pane" id="tab-social<?php echo $footer_social_row; ?>">
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <p><h4><?php echo $tab_data; ?> #<?php echo $footer_social_row; ?></h4></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                          <div class="col-sm-5">
                            <div class="input-group">
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                              </div>
                              <select name="moneymaker2_footer_socials[<?php echo $footer_social_row; ?>][icon]" class="form-control">
                                <?php $first = false; ?>
                                <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                                <?php $value['icon'] == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                                <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                          <div class="col-sm-10">
                            <?php foreach ($languages as $language) { ?>
                            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input type="text" name="moneymaker2_footer_socials[<?php echo $footer_social_row; ?>][caption][<?php echo $language['language_id']; ?>]" value="<?php echo isset($value['caption'][$language['language_id']]) ? $value['caption'][$language['language_id']] : ''; ?>" class="form-control">
                            </div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="moneymaker2_footer_socials_link<?php echo $footer_social_row; ?>"><?php echo $button_link; ?></label>
                          <div class="col-sm-10">
                            <input type="text" name="moneymaker2_footer_socials[<?php echo $footer_social_row; ?>][link]" value="<?php echo isset($value['link']) ? $value['link'] : ''; ?>" id="moneymaker2_footer_socials_link<?php echo $footer_social_row; ?>" class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="moneymaker2_footer_socials_sort_order<?php echo $footer_social_row; ?>"><?php echo $entry_sort_order; ?></label>
                          <div class="col-sm-10">
                            <input type="number" min="0" name="moneymaker2_footer_socials[<?php echo $footer_social_row; ?>][sort_order]" value="<?php echo isset($value['sort_order']) ? $value['sort_order'] : ''; ?>" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_footer_socials_sort_order<?php echo $footer_social_row; ?>" class="form-control">
                          </div>
                        </div>
                      </div>
                      <?php $footer_social_row++; ?>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </fieldset>
              </div>

              <div class="tab-pane" id="tab_modules">
                <fieldset>
                  <legend><?php echo $entry_tab_modules; ?></legend>
                  <div class="form-group margin-top">
                    <p class="col-sm-12"><?php echo $entry_tab_modules_help; ?><br /></p>
                  </div>
                </fieldset>
                <div class="row" id="vtab-modules">
                  <div class="col-sm-2">
                    <ul class="nav nav-pills nav-stacked" id="modules">
                      <li><a href="#tab-modules_stickers" data-toggle="tab"> <?php echo $entry_tab_stickers; ?></a></li>
                      <li><a href="#tab-modules_promo" data-toggle="tab"> <?php echo $entry_tab_promo; ?></a></li>
                      <li><a href="#tab-modules_quickorder" data-toggle="tab"> <?php echo $entry_tab_quickorder; ?></a></li>
                      <li><a href="#tab-modules_callback" data-toggle="tab"> <?php echo $entry_tab_callback; ?></a></li>
                    </ul>
                  </div>
                  <div class="col-sm-10">
                    <div class="tab-content">
                      <div class="tab-pane" id="tab-modules_stickers">
                        <fieldset>
                          <legend><?php echo $entry_tab_stickers; ?></legend>
                          <div class="form-group margin-top">
                            <p class="col-sm-12"><?php echo $entry_modules_stickers_help; ?><br /></p>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                            <div class="col-sm-10">
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_mode" value="1" <?php if ($moneymaker2_modules_stickers_mode) { ?>checked="checked"<?php } ?> /> <?php echo $entry_text; ?></label>
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_mode" value="0" <?php if (!$moneymaker2_modules_stickers_mode) { ?>checked="checked"<?php } ?> /> <?php echo $entry_tooltip; ?> (<?php echo $entry_hover; ?>)</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_background; ?> (<?php echo $entry_text; ?>)</label>
                            <div class="col-sm-4">
                              <select name="moneymaker2_modules_stickers_background" id="moneymaker2_modules_stickers_background" class="form-control">
                                <?php if ($moneymaker2_modules_stickers_background) { ?>
                                <option value="1" selected="selected"><?php echo $moneymaker2_bw_styles[1]; ?></option>
                                <option value="0"><?php echo $moneymaker2_bw_styles[0]; ?></option>
                                <?php } else { ?>
                                <option value="1"><?php echo $moneymaker2_bw_styles[1]; ?></option>
                                <option value="0" selected="selected"><?php echo $moneymaker2_bw_styles[0]; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                            <label class="col-sm-2 control-label" for="moneymaker2_modules_stickers_transparency"><?php echo $entry_transparency; ?></label>
                            <div class="col-sm-4">
                              <select name="moneymaker2_modules_stickers_transparency" id="moneymaker2_modules_stickers_transparency" class="form-control">
                                <?php for ($j = 10; $j <= 95; $j=$j+5) {
                          ($moneymaker2_modules_stickers_transparency == $j) ? $active = 'selected' : $active = ''; ?>
                                <option value="<?php echo $j; ?>" <?php echo $active; ?>><?php echo $j; ?>%</option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="moneymaker2_modules_stickers_size_catalog"><?php echo $entry_size; ?> (<?php echo $entry_tab_catalog; ?>)</label>
                            <div class="col-sm-10">
                              <div class="input-group">
                                <div class="input-group-btn">
                                  <button type="button" class="btn btn-default" data-toggle="modal" data-target="#sizesModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                                </div>
                                <select id="moneymaker2_modules_stickers_size_catalog" name="moneymaker2_modules_stickers_size_catalog" class="form-control">
                                  <?php foreach ($moneymaker2_stickers_sizes as $moneymaker2_stickers_size) { ?>
                                  <option value="<?php echo $moneymaker2_stickers_size; ?>" <?php if ($moneymaker2_modules_stickers_size_catalog == $moneymaker2_stickers_size) { ?>selected="selected"<?php } ?>><?php echo $moneymaker2_stickers_size; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="moneymaker2_modules_stickers_size_product"><?php echo $entry_size; ?> (<?php echo $entry_tab_product; ?>)</label>
                            <div class="col-sm-10">
                              <div class="input-group">
                                <div class="input-group-btn">
                                  <button type="button" class="btn btn-default" data-toggle="modal" data-target="#sizesModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                                </div>
                                <select id="moneymaker2_modules_stickers_size_product" name="moneymaker2_modules_stickers_size_product" class="form-control">
                                  <?php foreach ($moneymaker2_stickers_sizes as $moneymaker2_stickers_size) { ?>
                                  <option value="<?php echo $moneymaker2_stickers_size; ?>" <?php if ($moneymaker2_modules_stickers_size_product == $moneymaker2_stickers_size) { ?>selected="selected"<?php } ?>><?php echo $moneymaker2_stickers_size; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                        </fieldset>
                        <fieldset>
                          <legend><span class="text-muted"><?php echo $entry_tab_stickers; ?> ></span> <?php echo $entry_specials; ?></legend>
                          <div class="form-group margin-top">
                            <p class="col-sm-12"><?php echo $entry_modules_stickers_specials_help; ?></p>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                            <div class="col-sm-10">
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_specials_enabled" value="1" <?php if ($moneymaker2_modules_stickers_specials_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_specials_enabled" value="0" <?php if (!$moneymaker2_modules_stickers_specials_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                            <div class="col-sm-5">
                              <div class="input-group">
                                <div class="input-group-btn">
                                  <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                                </div>
                                <select name="moneymaker2_modules_stickers_specials_icon" class="form-control">
                                  <?php $first = false; ?>
                                  <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                                    <?php $moneymaker2_modules_stickers_specials_icon == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                                    <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                            <label class="col-sm-2 control-label"><?php echo $entry_color; ?></label>
                            <div class="col-sm-3">
                              <input type="text" name="moneymaker2_modules_stickers_specials_color" value="<?php echo $moneymaker2_modules_stickers_specials_color; ?>"  class="form-control colorpicker" />
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                            <div class="col-sm-10">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                <input type="text" name="moneymaker2_modules_stickers_specials_caption[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_modules_stickers_specials_caption[$language['language_id']]) ? $moneymaker2_modules_stickers_specials_caption[$language['language_id']] : ''; ?>" class="form-control" />
                              </div>
                              <?php } ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="moneymaker2_modules_stickers_specials_discount"><?php echo $entry_discount; ?></label>
                            <div class="col-sm-5">
                              <select id="moneymaker2_modules_stickers_specials_discount" name="moneymaker2_modules_stickers_specials_discount" class="form-control">
                                <?php if ($moneymaker2_modules_stickers_specials_discount) { ?>
                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                <option value="1"><?php echo $text_enabled; ?></option>
                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                            <label class="col-sm-2 control-label" for="moneymaker2_modules_stickers_specials_discount_mode"><?php echo $entry_mode; ?></label>
                            <div class="col-sm-3">
                              <select id="moneymaker2_modules_stickers_specials_discount_mode" name="moneymaker2_modules_stickers_specials_discount_mode" class="form-control">
                                <?php if ($moneymaker2_modules_stickers_specials_discount_mode) { ?>
                                <option value="1" selected="selected"><?php echo $entry_percentage; ?></option>
                                <option value="0"><?php echo $entry_difference; ?></option>
                                <?php } else { ?>
                                <option value="1"><?php echo $entry_percentage; ?></option>
                                <option value="0" selected="selected"><?php echo $entry_difference; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </fieldset>
                        <fieldset>
                          <legend><span class="text-muted"><?php echo $entry_tab_stickers; ?> ></span> <?php echo $entry_popular; ?></legend>
                          <div class="form-group margin-top">
                            <p class="col-sm-12"><?php echo $entry_modules_stickers_popular_help; ?></p>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                            <div class="col-sm-10">
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_popular_enabled" value="1" <?php if ($moneymaker2_modules_stickers_popular_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_popular_enabled" value="0" <?php if (!$moneymaker2_modules_stickers_popular_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                            <div class="col-sm-5">
                              <div class="input-group">
                                <div class="input-group-btn">
                                  <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                                </div>
                                <select name="moneymaker2_modules_stickers_popular_icon" class="form-control">
                                  <?php $first = false; ?>
                                  <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                                    <?php $moneymaker2_modules_stickers_popular_icon == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                                    <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                            <label class="col-sm-2 control-label"><?php echo $entry_color; ?></label>
                            <div class="col-sm-3">
                              <input type="text" name="moneymaker2_modules_stickers_popular_color" value="<?php echo $moneymaker2_modules_stickers_popular_color; ?>"  class="form-control colorpicker" />
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                            <div class="col-sm-10">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                <input type="text" name="moneymaker2_modules_stickers_popular_caption[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_modules_stickers_popular_caption[$language['language_id']]) ? $moneymaker2_modules_stickers_popular_caption[$language['language_id']] : ''; ?>" class="form-control" />
                              </div>
                              <?php } ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="moneymaker2_modules_stickers_popular_limit"><?php echo $entry_limit; ?></label>
                            <div class="col-sm-5">
                              <div class="input-group">
                                <input type="number" min="1" name="moneymaker2_modules_stickers_popular_limit" value="<?php echo $moneymaker2_modules_stickers_popular_limit; ?>"  id="moneymaker2_modules_stickers_popular_limit" class="form-control">
                                <span class="input-group-addon"><?php echo $entry_views; ?></span>
                              </div>
                            </div>
                          </div>
                        </fieldset>
                        <fieldset>
                          <legend><span class="text-muted"><?php echo $entry_tab_stickers; ?> ></span> <?php echo $entry_rated; ?></legend>
                          <div class="form-group margin-top">
                            <p class="col-sm-12"><?php echo $entry_modules_stickers_rated_help; ?></p>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                            <div class="col-sm-10">
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_rated_enabled" value="1" <?php if ($moneymaker2_modules_stickers_rated_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_rated_enabled" value="0" <?php if (!$moneymaker2_modules_stickers_rated_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                            <div class="col-sm-5">
                              <div class="input-group">
                                <div class="input-group-btn">
                                  <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                                </div>
                                <select name="moneymaker2_modules_stickers_rated_icon" class="form-control">
                                  <?php $first = false; ?>
                                  <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                                    <?php $moneymaker2_modules_stickers_rated_icon == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                                    <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                            <label class="col-sm-2 control-label"><?php echo $entry_color; ?></label>
                            <div class="col-sm-3">
                              <input type="text" name="moneymaker2_modules_stickers_rated_color" value="<?php echo $moneymaker2_modules_stickers_rated_color; ?>"  class="form-control colorpicker" />
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                            <div class="col-sm-10">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                <input type="text" name="moneymaker2_modules_stickers_rated_caption[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_modules_stickers_rated_caption[$language['language_id']]) ? $moneymaker2_modules_stickers_rated_caption[$language['language_id']] : ''; ?>" class="form-control" />
                              </div>
                              <?php } ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="moneymaker2_modules_stickers_rated_limit"><?php echo $entry_limit; ?></label>
                            <div class="col-sm-5">
                              <div class="input-group">
                                <input type="number" min="1" max="5" name="moneymaker2_modules_stickers_rated_limit" value="<?php echo $moneymaker2_modules_stickers_rated_limit; ?>"  id="moneymaker2_modules_stickers_rated_limit" class="form-control">
                                <span class="input-group-addon"><?php echo $entry_rating; ?></span>
                              </div>
                            </div>
                          </div>
                        </fieldset>
                        <fieldset>
                          <legend><span class="text-muted"><?php echo $entry_tab_stickers; ?> ></span> <?php echo $entry_new; ?></legend>
                          <div class="form-group margin-top">
                            <p class="col-sm-12"><?php echo $entry_modules_stickers_new_help; ?></p>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                            <div class="col-sm-10">
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_new_enabled" value="1" <?php if ($moneymaker2_modules_stickers_new_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_new_enabled" value="0" <?php if (!$moneymaker2_modules_stickers_new_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                            <div class="col-sm-5">
                              <div class="input-group">
                                <div class="input-group-btn">
                                  <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                                </div>
                                <select name="moneymaker2_modules_stickers_new_icon" class="form-control">
                                  <?php $first = false; ?>
                                  <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                                    <?php $moneymaker2_modules_stickers_new_icon == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                                    <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                            <label class="col-sm-2 control-label"><?php echo $entry_color; ?></label>
                            <div class="col-sm-3">
                              <input type="text" name="moneymaker2_modules_stickers_new_color" value="<?php echo $moneymaker2_modules_stickers_new_color; ?>"  class="form-control colorpicker" />
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                            <div class="col-sm-10">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                <input type="text" name="moneymaker2_modules_stickers_new_caption[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_modules_stickers_new_caption[$language['language_id']]) ? $moneymaker2_modules_stickers_new_caption[$language['language_id']] : ''; ?>" class="form-control" />
                              </div>
                              <?php } ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="moneymaker2_modules_stickers_new_limit"><?php echo $entry_limit; ?></label>
                            <div class="col-sm-5">
                              <div class="input-group">
                                <input type="number" min="1" name="moneymaker2_modules_stickers_new_limit" value="<?php echo $moneymaker2_modules_stickers_new_limit; ?>"  id="moneymaker2_modules_stickers_new_limit" class="form-control">
                                <span class="input-group-addon"><?php echo $entry_days; ?></span>
                              </div>
                            </div>
                          </div>
                        </fieldset>
                        <fieldset>
                          <legend><span class="text-muted"><?php echo $entry_tab_stickers; ?> ></span> <?php echo $entry_custom; ?> I</legend>
                          <div class="form-group margin-top">
                            <p class="col-sm-12"><?php echo $entry_modules_customstickers_help; ?></p>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                            <div class="col-sm-10">
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_custom1_enabled" value="1" <?php if ($moneymaker2_modules_stickers_custom1_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_custom1_enabled" value="0" <?php if (!$moneymaker2_modules_stickers_custom1_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                            <div class="col-sm-5">
                              <div class="input-group">
                                <div class="input-group-btn">
                                  <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                                </div>
                                <select name="moneymaker2_modules_stickers_custom1_icon" class="form-control">
                                  <?php $first = false; ?>
                                  <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                                    <?php $moneymaker2_modules_stickers_custom1_icon == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                                    <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                            <label class="col-sm-2 control-label"><?php echo $entry_color; ?></label>
                            <div class="col-sm-3">
                              <input type="text" name="moneymaker2_modules_stickers_custom1_color" value="<?php echo $moneymaker2_modules_stickers_custom1_color; ?>"  class="form-control colorpicker" />
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                            <div class="col-sm-10">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                <input type="text" name="moneymaker2_modules_stickers_custom1_caption[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_modules_stickers_custom1_caption[$language['language_id']]) ? $moneymaker2_modules_stickers_custom1_caption[$language['language_id']] : ''; ?>" class="form-control" />
                              </div>
                              <?php } ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="moneymaker2_modules_stickers_custom1_field"><?php echo $entry_field; ?></label>
                            <div class="col-sm-5">
                              <select name="moneymaker2_modules_stickers_custom1_field" id="moneymaker2_modules_stickers_custom1_field" class="form-control">
                                <?php foreach ($moneymaker2_custom_fields as $moneymaker2_custom_field) { ?>
                                <?php ($moneymaker2_custom_field ==  $moneymaker2_modules_stickers_custom1_field) ? $active = 'selected' : $active=''; ?>
                                <option value="<?php echo $moneymaker2_custom_field; ?>" <?php echo $active; ?>><?php echo $moneymaker2_custom_field; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </fieldset>
                        <fieldset>
                          <legend><span class="text-muted"><?php echo $entry_tab_stickers; ?> ></span> <?php echo $entry_custom; ?> II</legend>
                          <div class="form-group margin-top">
                            <p class="col-sm-12"><?php echo $entry_modules_customstickers_help; ?></p>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                            <div class="col-sm-10">
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_custom2_enabled" value="1" <?php if ($moneymaker2_modules_stickers_custom2_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_custom2_enabled" value="0" <?php if (!$moneymaker2_modules_stickers_custom2_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                            <div class="col-sm-5">
                              <div class="input-group">
                                <div class="input-group-btn">
                                  <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                                </div>
                                <select name="moneymaker2_modules_stickers_custom2_icon" class="form-control">
                                  <?php $first = false; ?>
                                  <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                                    <?php $moneymaker2_modules_stickers_custom2_icon == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                                    <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                            <label class="col-sm-2 control-label"><?php echo $entry_color; ?></label>
                            <div class="col-sm-3">
                              <input type="text" name="moneymaker2_modules_stickers_custom2_color" value="<?php echo $moneymaker2_modules_stickers_custom2_color; ?>"  class="form-control colorpicker" />
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                            <div class="col-sm-10">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                <input type="text" name="moneymaker2_modules_stickers_custom2_caption[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_modules_stickers_custom2_caption[$language['language_id']]) ? $moneymaker2_modules_stickers_custom2_caption[$language['language_id']] : ''; ?>" class="form-control" />
                              </div>
                              <?php } ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="moneymaker2_modules_stickers_custom2_field"><?php echo $entry_field; ?></label>
                            <div class="col-sm-5">
                              <select name="moneymaker2_modules_stickers_custom2_field" id="moneymaker2_modules_stickers_custom2_field" class="form-control">
                                <?php foreach ($moneymaker2_custom_fields as $moneymaker2_custom_field) { ?>
                                <?php ($moneymaker2_custom_field ==  $moneymaker2_modules_stickers_custom2_field) ? $active = 'selected' : $active=''; ?>
                                <option value="<?php echo $moneymaker2_custom_field; ?>" <?php echo $active; ?>><?php echo $moneymaker2_custom_field; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </fieldset>
                      </div>

                      <div class="tab-pane" id="tab-modules_promo">
                        <fieldset>
                          <legend><?php echo $entry_tab_promo; ?></legend>
                          <div class="form-group margin-top">
                            <p class="col-sm-12"><?php echo $entry_modules_promo_help; ?><br /></p>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                            <div class="col-sm-6">
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_promo_enabled" value="1" <?php if ($moneymaker2_modules_promo_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_promo_enabled" value="0" <?php if (!$moneymaker2_modules_promo_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_style; ?></label>
                            <div class="col-sm-6">
                              <div class="input-group">
                                <div class="input-group-btn">
                                  <button type="button" class="btn btn-default" data-toggle="modal" data-target="#stylesModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                                </div>
                                <select name="moneymaker2_modules_promo_style" class="form-control">
                                  <?php foreach ($moneymaker2_banners_styles as $moneymaker2_banners_style) { ?>
                                  <?php ($moneymaker2_banners_style ==  $moneymaker2_modules_promo_style) ? $active = 'selected' : $active=''; ?>
                                  <option value="<?php echo $moneymaker2_banners_style; ?>" <?php echo $active; ?>><?php echo $moneymaker2_banners_style; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                        </fieldset>
                        <fieldset>
                          <legend><span class="text-muted"><?php echo $entry_tab_promo; ?> ></span> <?php echo $entry_tab_sticker; ?></legend>
                          <div class="form-group margin-top">
                            <p class="col-sm-12"><?php echo $entry_modules_promostickers_help; ?></p>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                            <div class="col-sm-10">
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_promo_enabled" value="1" <?php if ($moneymaker2_modules_stickers_promo_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_stickers_promo_enabled" value="0" <?php if (!$moneymaker2_modules_stickers_promo_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>
                            <div class="col-sm-6">
                              <div class="input-group">
                                <div class="input-group-btn">
                                  <button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>
                                </div>
                                <select name="moneymaker2_modules_stickers_promo_icon" class="form-control">
                                  <?php $first = false; ?>
                                  <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
                                  <?php $moneymaker2_modules_stickers_promo_icon == $moneymaker2_fa_icons[$j] ? !$first ? $active = $first = 'selected' : '' : $active = ''; ?>
                                  <option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php echo $active; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>
                            <div class="col-sm-7">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                <input type="text" name="moneymaker2_modules_stickers_promo_caption[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_modules_stickers_promo_caption[$language['language_id']]) ? $moneymaker2_modules_stickers_promo_caption[$language['language_id']] : ''; ?>" class="form-control" />
                              </div>
                              <?php } ?>
                            </div>
                          </div>
                        </fieldset>
                        <fieldset>
                          <legend><span class="text-muted"><?php echo $entry_tab_promo; ?> ></span> <?php echo $button_add; ?></legend>
                          <div class="form-group margin-top">
                            <p class="col-sm-12"><?php echo $entry_modules_promo_add_help; ?></p>
                          </div>
                          <div class="row" id="vtab-modules_promos">
                            <div class="col-sm-2">
                              <ul class="nav nav-pills nav-stacked" id="modules_promos">
                                <?php $modules_promo_row = 1; ?>
                                <?php foreach ($moneymaker2_modules_promos as $value) { ?>
                                <li><a href="#tab-promo<?php echo $modules_promo_row; ?>" data-toggle="tab" id="link-promo<?php echo $modules_promo_row; ?>"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-promo<?php echo $modules_promo_row; ?>\']').parent().remove(); $('#tab-promo<?php echo $modules_promo_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#<?php echo $modules_promo_row; ?></a></li>
                                <?php $modules_promo_row++; ?>
                                <?php } ?>
                                <li>
                                  <button type="button" onclick="addModulesPromo();" class="btn btn-success form-control"><i class="fa fa-plus-circle"></i> <?php echo $button_add; ?></button>
                                </li>
                              </ul>
                            </div>
                            <div class="col-sm-10">
                              <div class="tab-content">
                                <?php $modules_promo_row = 1; ?>
                                <?php foreach ($moneymaker2_modules_promos as $value) { ?>
                                <div class="tab-pane" id="tab-promo<?php echo $modules_promo_row; ?>">
                                  <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                      <p><h4><?php echo $tab_data; ?> #<?php echo $modules_promo_row; ?></h4></p>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo $tab_image; ?></label>
                                    <div class="col-sm-10">
                                      <a href="" id="moneymaker2_modules_promos_image_thumb<?php echo $modules_promo_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $value['thumb']; ?>" alt="" data-placeholder="<?php echo $placeholder; ?>"  /></a><input type="hidden" name="moneymaker2_modules_promos[<?php echo $modules_promo_row; ?>][image]" value="<?php echo $value['image']; ?>" id="moneymaker2_modules_promos_image_input<?php echo $modules_promo_row; ?>" />
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo $entry_categories; ?></label>
                                    <div class="col-sm-10">
                                      <div class="well well-sm">
                                        <?php foreach ($categories as $category) { ?>
                                        <div class="checkbox">
                                          <label>
                                            <?php if (in_array($category['category_id'], $value['categories'])) { ?>
                                            <input type="checkbox" name="moneymaker2_modules_promos[<?php echo $modules_promo_row; ?>][categories][]" value="<?php echo $category['category_id']; ?>" checked="checked" /> <?php echo $category['name']; ?>
                                            <?php } else { ?>
                                            <input type="checkbox" name="moneymaker2_modules_promos[<?php echo $modules_promo_row; ?>][categories][]" value="<?php echo $category['category_id']; ?>" /> <?php echo $category['name']; ?>
                                            <?php } ?>
                                          </label>
                                        </div>
                                        <?php } ?>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label" for="moneymaker2_modules_promos_sort_order<?php echo $modules_promo_row; ?>"><?php echo $entry_sort_order; ?></label>
                                    <div class="col-sm-10">
                                      <input type="number" min="0" name="moneymaker2_modules_promos[<?php echo $modules_promo_row; ?>][sort_order]" value="<?php echo isset($value['sort_order']) ? $value['sort_order'] : ''; ?>" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_modules_promos_sort_order<?php echo $modules_promo_row; ?>" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo $entry_text; ?></label>
                                    <div class="col-sm-10">
                                      <?php foreach ($languages as $language) { ?>
                                      <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                        <input type="text" name="moneymaker2_modules_promos[<?php echo $modules_promo_row; ?>][text][<?php echo $language['language_id']; ?>]" value="<?php echo isset($value['text'][$language['language_id']]) ? $value['text'][$language['language_id']] : ''; ?>" class="form-control">
                                      </div>
                                      <?php } ?>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo $entry_button_title; ?></label>
                                    <div class="col-sm-10">
                                      <?php foreach ($languages as $language) { ?>
                                      <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                        <input type="text" name="moneymaker2_modules_promos[<?php echo $modules_promo_row; ?>][title][<?php echo $language['language_id']; ?>]" value="<?php echo isset($value['title'][$language['language_id']]) ? $value['title'][$language['language_id']] : ''; ?>" class="form-control">
                                      </div>
                                      <?php } ?>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label" for="moneymaker2_modules_promos_link<?php echo $modules_promo_row; ?>"><?php echo $button_link; ?></label>
                                    <div class="col-sm-10">
                                      <select name="moneymaker2_modules_promos[<?php echo $modules_promo_row; ?>][link]" class="form-control">
                                        <option value="0"><?php echo $text_select ?></option>
                                        <?php foreach ($informations as $information) { ?>
                                        <option value="<?php echo $information['information_id']; ?>" <?php if ($information['information_id'] == $value['link']) { ?>selected="selected"<?php } ?>><?php echo $information['title'] ?></option>
                                        <?php } ?>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo $entry_description; ?></label>
                                    <div class="col-sm-10">
                                      <?php foreach ($languages as $language) { ?>
                                      <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                        <textarea name="moneymaker2_modules_promos[<?php echo $modules_promo_row; ?>][description][<?php echo $language['language_id']; ?>]" rows="2" placeholder="<?php echo $entry_optional; ?>" class="form-control"><?php echo isset($value['description'][$language['language_id']]) ? $value['description'][$language['language_id']] : ''; ?></textarea>
                                      </div>
                                      <?php } ?>
                                    </div>
                                  </div>
                                </div>
                                <?php $modules_promo_row++; ?>
                                <?php } ?>
                              </div>
                            </div>
                          </div>
                        </fieldset>
                      </div>

                      <div class="tab-pane" id="tab-modules_quickorder">
                        <fieldset>
                          <legend><?php echo $entry_tab_quickorder; ?></legend>
                          <div class="form-group margin-top">
                            <p class="col-sm-12"><?php echo $entry_modules_quickorder_help; ?><br /></p>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $text_enabled; ?></label>
                            <div class="col-sm-6">
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_quickorder_enabled" value="1" <?php if ($moneymaker2_modules_quickorder_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_quickorder_enabled" value="0" <?php if (!$moneymaker2_modules_quickorder_enabled) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_modules_quickorder_clear_cart; ?></label>
                            <div class="col-sm-6">
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_quickorder_clear_cart" value="1" <?php if ($moneymaker2_modules_quickorder_clear_cart) { ?>checked="checked"<?php } ?> /> <?php echo $text_yes; ?></label>
                              <label class="radio-inline"><input type="radio" name="moneymaker2_modules_quickorder_clear_cart" value="0" <?php if (!$moneymaker2_modules_quickorder_clear_cart) { ?>checked="checked"<?php } ?> /> <?php echo $text_no; ?></label>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_image_size; ?></label>
                            <div class="col-sm-4">
                              <div class="input-group">
                                <input type="number" min="1" name="moneymaker2_modules_quickorder_image_width" value="<?php echo $moneymaker2_modules_quickorder_image_width; ?>" class="form-control">
                                <span class="input-group-addon"><?php echo $entry_px; ?></span>
                              </div><p class="text-center"><small><?php echo $entry_width; ?></small></p>
                            </div>
                            <div class="col-sm-4">
                              <div class="input-group">
                                <input type="number" min="1" name="moneymaker2_modules_quickorder_image_height" value="<?php echo $moneymaker2_modules_quickorder_image_height; ?>" class="form-control">
                                <span class="input-group-addon"><?php echo $entry_px; ?></span>
                              </div><p class="text-center"><small><?php echo $entry_height; ?></small></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_button_title; ?></label>
                            <div class="col-sm-8">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                <input type="text" name="moneymaker2_modules_quickorder_button_title[<?php echo $language['language_id']; ?>]" value="<?php echo isset($moneymaker2_modules_quickorder_button_title[$language['language_id']]) ? $moneymaker2_modules_quickorder_button_title[$language['language_id']] : ''; ?>" class="form-control" />
                              </div>
                              <?php } ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_input_mask; ?></label>
                            <div class="col-sm-4">
                              <select name="moneymaker2_modules_quickorder_phone_mask_enabled" class="form-control">
                                <?php if ($moneymaker2_modules_quickorder_phone_mask_enabled) { ?>
                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                <option value="1"><?php echo $text_enabled; ?></option>
                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                            <div class="col-sm-4">
                              <input type="text" name="moneymaker2_modules_quickorder_phone_mask" value="<?php echo $moneymaker2_modules_quickorder_phone_mask; ?>" class="form-control" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-offset-2 col-sm-10">
                              <p class="paddingtop"><?php echo $entry_phone_mask_help; ?></p>
                            </div>
                          </div>
                        </fieldset>
                      </div>

                      <div class="tab-pane" id="tab-modules_callback">
                        <fieldset>
                          <legend><?php echo $entry_tab_callback; ?></legend>
                          <div class="form-group margin-top">
                            <p class="col-sm-12"><?php echo $entry_modules_callback_help; ?><br /></p>
                          </div>
                        </fieldset>
                      </div>

                    </div>
                  </div>
                </div>
              </div>

              <div class="tab-pane" id="tab_help">
                <fieldset>
                  <legend><?php echo $entry_tab_help; ?></legend>
                  <div class="form-group margin-top">
                    <p class="col-sm-12"><?php echo $entry_help_text; ?><br /></p>
                  </div>
                </fieldset>
              </div>

            </div>
          </div>
        </form>

        <div class="modal fade" id="iconsModal" tabindex="-1" role="dialog" aria-labelledby="iconsModalLabel">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
              </div>
              <div class="modal-body">
                <img class="img-responsive" src="../catalog/view/theme/moneymaker2/image/fa-icons.png" />
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_close; ?></button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="stylesModal" tabindex="-1" role="dialog" aria-labelledby="iconsModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
              </div>
              <div class="modal-body">
                <?php foreach ($moneymaker2_banners_styles as $value) { ?>
                <span class="btn {*disabled*} btn-lg btn-<?php echo $value; ?>"><?php echo $value; ?></span>&nbsp;
                <?php } ?>
                <p>&nbsp;</p>
                <p>*<?php echo $entry_style_help; ?></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_close; ?></button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="sizesModal" tabindex="-1" role="dialog" aria-labelledby="sizesModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
              </div>
              <div class="modal-body">
                <?php foreach ($moneymaker2_stickers_sizes as $value) { ?>
                <span class="fa-stack fa-<?php echo $value; ?>">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-stack-1x fa-inverse"><?php echo $value; ?></i>
                </span> &nbsp;
                <?php } ?>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_close; ?></button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="animationsModal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">&nbsp;</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-sm-12 text-center">
                    <div id="animationSandbox"><?php echo $entry_animation; ?></div>
                  </div>
                  <div class="col-sm-12 text-center">
                    <div class="input-group">
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-default  js--triggerAnimation" ><i class="fa fa-fw fa-play-circle"></i></button>
                      </div>
                      <select class="form-control js--animations">
                        <optgroup label="Bouncing Entrances">
                          <option value="bounceIn">bounceIn</option>
                          <option value="bounceInDown">bounceInDown</option>
                          <option value="bounceInLeft">bounceInLeft</option>
                          <option value="bounceInRight">bounceInRight</option>
                          <option value="bounceInUp">bounceInUp</option>
                        </optgroup>
                        <optgroup label="Bouncing Exits">
                          <option value="bounceOut">bounceOut</option>
                          <option value="bounceOutDown">bounceOutDown</option>
                          <option value="bounceOutLeft">bounceOutLeft</option>
                          <option value="bounceOutRight">bounceOutRight</option>
                          <option value="bounceOutUp">bounceOutUp</option>
                        </optgroup>
                        <optgroup label="Fading Entrances">
                          <option value="fadeIn">fadeIn</option>
                          <option value="fadeInDown">fadeInDown</option>
                          <option value="fadeInDownBig">fadeInDownBig</option>
                          <option value="fadeInLeft">fadeInLeft</option>
                          <option value="fadeInLeftBig">fadeInLeftBig</option>
                          <option value="fadeInRight">fadeInRight</option>
                          <option value="fadeInRightBig">fadeInRightBig</option>
                          <option value="fadeInUp">fadeInUp</option>
                          <option value="fadeInUpBig">fadeInUpBig</option>
                        </optgroup>
                        <optgroup label="Fading Exits">
                          <option value="fadeOut">fadeOut</option>
                          <option value="fadeOutDown">fadeOutDown</option>
                          <option value="fadeOutDownBig">fadeOutDownBig</option>
                          <option value="fadeOutLeft">fadeOutLeft</option>
                          <option value="fadeOutLeftBig">fadeOutLeftBig</option>
                          <option value="fadeOutRight">fadeOutRight</option>
                          <option value="fadeOutRightBig">fadeOutRightBig</option>
                          <option value="fadeOutUp">fadeOutUp</option>
                          <option value="fadeOutUpBig">fadeOutUpBig</option>
                        </optgroup>
                        <optgroup label="Flippers">
                          <option value="flip">flip</option>
                          <option value="flipInX">flipInX</option>
                          <option value="flipInY">flipInY</option>
                          <option value="flipOutX">flipOutX</option>
                          <option value="flipOutY">flipOutY</option>
                        </optgroup>
                        <optgroup label="Lightspeed">
                          <option value="lightSpeedIn">lightSpeedIn</option>
                          <option value="lightSpeedOut">lightSpeedOut</option>
                        </optgroup>
                        <optgroup label="Rotating Entrances">
                          <option value="rotateIn">rotateIn</option>
                          <option value="rotateInDownLeft">rotateInDownLeft</option>
                          <option value="rotateInDownRight">rotateInDownRight</option>
                          <option value="rotateInUpLeft">rotateInUpLeft</option>
                          <option value="rotateInUpRight">rotateInUpRight</option>
                        </optgroup>
                        <optgroup label="Rotating Exits">
                          <option value="rotateOut">rotateOut</option>
                          <option value="rotateOutDownLeft">rotateOutDownLeft</option>
                          <option value="rotateOutDownRight">rotateOutDownRight</option>
                          <option value="rotateOutUpLeft">rotateOutUpLeft</option>
                          <option value="rotateOutUpRight">rotateOutUpRight</option>
                        </optgroup>
                        <optgroup label="Sliding Entrances">
                          <option value="slideInUp">slideInUp</option>
                          <option value="slideInDown">slideInDown</option>
                          <option value="slideInLeft">slideInLeft</option>
                          <option value="slideInRight">slideInRight</option>
                        </optgroup>
                        <optgroup label="Sliding Exits">
                          <option value="slideOutUp">slideOutUp</option>
                          <option value="slideOutDown">slideOutDown</option>
                          <option value="slideOutLeft">slideOutLeft</option>
                          <option value="slideOutRight">slideOutRight</option>
                        </optgroup>
                        <optgroup label="Zoom Entrances">
                          <option value="zoomIn">zoomIn</option>
                          <option value="zoomInDown">zoomInDown</option>
                          <option value="zoomInLeft">zoomInLeft</option>
                          <option value="zoomInRight">zoomInRight</option>
                          <option value="zoomInUp">zoomInUp</option>
                        </optgroup>
                        <optgroup label="Zoom Exits">
                          <option value="zoomOut">zoomOut</option>
                          <option value="zoomOutDown">zoomOutDown</option>
                          <option value="zoomOutLeft">zoomOutLeft</option>
                          <option value="zoomOutRight">zoomOutRight</option>
                          <option value="zoomOutUp">zoomOutUp</option>
                        </optgroup>
                        <optgroup label="Attention Seekers">
                          <option value="bounce">bounce</option>
                          <option value="flash">flash</option>
                          <option value="pulse">pulse</option>
                          <option value="rubberBand">rubberBand</option>
                          <option value="shake">shake</option>
                          <option value="swing">swing</option>
                          <option value="tada">tada</option>
                          <option value="wobble">wobble</option>
                          <option value="jello">jello</option>
                        </optgroup>
                        <optgroup label="Specials">
                          <option value="hinge">hinge</option>
                          <option value="rollIn">rollIn</option>
                          <option value="rollOut">rollOut</option>
                        </optgroup>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_close; ?></button>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
    $('.colorpicker').each(function(index) {
      $(this).attr('id', 'colorpicker_'+index );
      var colorpicker = new Array();
      colorpicker[index] = $('#colorpicker_'+index).val();
      $('#colorpicker_'+index).css('background-color', colorpicker[index]);

      $('#colorpicker_'+index).colpick({
        layout:'rgbhex',
        submit:0,
        color: colorpicker[index],
        onChange:function(hsb,hex,rgb,el,bySetColor) {
          if(!bySetColor) {
            $(el).val('#'+hex);
            $('#colorpicker_'+index).val('#'+hex );
          }
          $(el).css('background-color','#'+hex);
          $('.colpick_current_color').css('display', 'visible' );
        }
      }).keyup(function(){
        $(this).colpickSetColor(this.value);
      });

    });

    $('#tabs a:first').tab('show')
    $('#header_contacts a:first').tab('show');
    $('#header_banners a:first').tab('show');
    $('#header_links a:first').tab('show');
    $('#footer_contacts a:first').tab('show');
    $('#footer_links a:first').tab('show');
    $('#footer_socials a:first').tab('show');
    $('#product_points a:first').tab('show');
    $('#modules_promos a:first').tab('show');
    $('#catalog_compare_charts a:first').tab('show');
    $('#modules a:first').tab('show');

    var header_contact_row = <?php echo $header_contact_row; ?>;
    function addHeaderContact() {
      html = '<div class="tab-pane active" id="tab-contact' + header_contact_row + '">';
      html += '<div class="form-group">';
      html += '<div class="col-sm-offset-2 col-sm-10">';
      html += '<p><h4><?php echo $tab_data; ?> #' + header_contact_row + '</h4></p>';
      html += '</div>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_mode; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<select name="moneymaker2_header_contacts[' + header_contact_row + '][mode]" class="form-control">';
      html += '<option value="0" selected="selected"><?php echo $text_select ?></option>';
      <?php foreach ($moneymaker2_contacts_modes as $key => $value) { ?>
      html += '<option value="<?php echo $key; ?>"><?php echo $value; ?></option>';
      <?php } ?>
      html += '</select>';
      html += '</div>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $tab_image; ?></label>';
      html += '<div class="col-sm-3">';
      html += '<a href="" id="moneymaker2_header_contacts_image_thumb' + header_contact_row + '" data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" /></a>';
      html += '<input type="hidden" name="moneymaker2_header_contacts[' + header_contact_row + '][image]" value="" id="moneymaker2_header_contacts_image_input' + header_contact_row + '" />';
      html += '</div>';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>';
      html += '<div class="col-sm-5">';
      html += '<div class="input-group">';
      html += '<div class="input-group-btn">';
      html += '<button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>';
      html += '</div>';
      html += '<select name="moneymaker2_header_contacts[' + header_contact_row + '][icon]" class="form-control">';
      <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
      html += '<option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php if ($j==1) echo "selected"; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>';
      <?php } ?>
      html += '</select>';
      html += '</div>';
      html += '</div>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_text; ?></label>';
      html += '<div class="col-sm-10">';
      <?php foreach ($languages as $language) { ?>
      html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
      html += '<input type="text" name="moneymaker2_header_contacts[' + header_contact_row + '][text][<?php echo $language['language_id']; ?>]" value="" class="form-control">';
      html += '</div>';
      <?php } ?>
      html += '</div>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label" for="moneymaker2_header_contacts_link' + header_contact_row + '"><?php echo $button_link; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<input type="text" name="moneymaker2_header_contacts[' + header_contact_row + '][link]" value="" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_header_contacts_link' + header_contact_row + '" class="form-control">';
      html += '</div>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label" for="moneymaker2_header_contacts_sort_order' + header_contact_row + '"><?php echo $entry_sort_order; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<input type="number" min="0" name="moneymaker2_header_contacts[' + header_contact_row + '][sort_order]" value="' + header_contact_row + '" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_header_contacts_sort_order' + header_contact_row + '" class="form-control">';
      html += '</div>';
      html += '</div>';

      html += '</div>';
      $('#vtab-header_contacts .tab-content').append(html);
      $('#header_contacts li .btn-success').parent().before('<li><a href="#tab-contact' + header_contact_row + '" data-toggle="tab" id="link-contact' + header_contact_row + '"><i class="fa fa-minus-circle" onclick="$(\'#link-contact' + header_contact_row + '\').parent().remove(); $(\'#tab-contact' + header_contact_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#' + header_contact_row + '</a></li>');

      $('#link-contact' + header_contact_row).trigger('click');
      header_contact_row++;
    }

    var header_banner_row = <?php echo $header_banner_row; ?>;
    function addHeaderBanner() {
      html = '<div class="tab-pane active" id="tab-banner' + header_banner_row + '">';
      html += '<div class="form-group">';
      html += '<div class="col-sm-offset-2 col-sm-10">';
      html += '<p><h4><?php echo $tab_data; ?> #' + header_banner_row + '</h4></p>';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $tab_image; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<a href="" id="moneymaker2_header_banners_image_thumb' + header_banner_row + '" data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" /></a>';
      html += '<input type="hidden" name="moneymaker2_header_banners[' + header_banner_row + '][image]" value="" id="moneymaker2_header_banners_image_input' + header_banner_row + '" />';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>';
      html += '<div class="col-sm-5">';
      html += '<div class="input-group">';
      html += '<div class="input-group-btn">';
      html += '<button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>';
      html += '</div>';
      html += '<select name="moneymaker2_header_banners[' + header_banner_row + '][icon]" class="form-control">';
    <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
        html += '<option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php if ($j==1) echo "selected"; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>';
      <?php } ?>
      html += '</select>';
      html += '</div>';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_style; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<div class="input-group">';
      html += '<div class="input-group-btn">';
      html += '<button type="button" class="btn btn-default" data-toggle="modal" data-target="#stylesModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>';
      html += '</div>';
      html += '<select name="moneymaker2_header_banners[' + header_banner_row + '][style]" class="form-control">';
      <?php foreach ($moneymaker2_banners_styles as $key => $value) { ?>
      html += '<option value="<?php echo $value; ?>" <?php if ($key == 0) { ?>selected<?php } ?>><?php echo $value; ?></option>';
      <?php } ?>
      html += '</select>';
      html += '</div>';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_title; ?></label>';
      html += '<div class="col-sm-10">';
      <?php foreach ($languages as $language) { ?>
      html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
      html += '<input type="text" name="moneymaker2_header_banners[' + header_banner_row + '][name][<?php echo $language['language_id']; ?>]" value="" class="form-control">';
      html += '</div>';
      <?php } ?>
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_text; ?></label>';
      html += '<div class="col-sm-10">';
      <?php foreach ($languages as $language) { ?>
      html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
      html += '<textarea name="moneymaker2_header_banners[' + header_banner_row + '][text][<?php echo $language['language_id']; ?>]" rows="2" placeholder="<?php echo $entry_optional; ?>" class="form-control"></textarea>';
      html += '</div>';
      <?php } ?>
      html += '</div>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label" for="moneymaker2_header_banners_link' + header_banner_row + '"><?php echo $button_link; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<input type="text" name="moneymaker2_header_banners[' + header_banner_row + '][link]" value="" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_header_banners_link' + header_banner_row + '" class="form-control">';
      html += '</div>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label" for="moneymaker2_header_banners_sort_order' + header_banner_row + '"><?php echo $entry_sort_order; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<input type="number" min="0" name="moneymaker2_header_banners[' + header_banner_row + '][sort_order]" value="' + header_banner_row + '" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_header_banners_sort_order' + header_banner_row + '" class="form-control">';
      html += '</div>';
      html += '</div>';

      html += '</div>';
      $('#vtab-header_banners .tab-content').append(html);
      $('#header_banners li .btn-success').parent().before('<li><a href="#tab-banner' + header_banner_row + '" data-toggle="tab" id="link-banner' + header_banner_row + '"><i class="fa fa-minus-circle" onclick="$(\'#link-banner' + header_banner_row + '\').parent().remove(); $(\'#tab-banner' + header_banner_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#' + header_banner_row + '</a></li>');

      $('#link-banner' + header_banner_row).trigger('click');

      header_banner_row++;
    }

    var header_link_row = <?php echo $header_link_row; ?>;
    function addHeaderLink() {
      html = '<div class="tab-pane active" id="tab-hlink' + header_link_row + '">';
      html += '<div class="form-group">';
      html += '<div class="col-sm-offset-2 col-sm-10">';
      html += '<p><h4><?php echo $tab_data; ?> #' + header_link_row + '</h4></p>';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>';
      html += '<div class="col-sm-5">';
      html += '<div class="input-group">';
      html += '<div class="input-group-btn">';
      html += '<button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>';
      html += '</div>';
      html += '<select name="moneymaker2_header_links[' + header_link_row + '][icon]" class="form-control">';
    <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
        html += '<option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php if ($j==1) echo "selected"; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>';
      <?php } ?>
      html += '</select>';
      html += '</div>';
      html += '</div>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>';
      html += '<div class="col-sm-10">';
      <?php foreach ($languages as $language) { ?>
      html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
      html += '<input type="text" name="moneymaker2_header_links[' + header_link_row + '][caption][<?php echo $language['language_id']; ?>]" value="" class="form-control">';
      html += '</div>';
      <?php } ?>
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $button_link; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<input type="text" name="moneymaker2_header_links[' + header_link_row + '][link]" value="" class="form-control">';
      html += '</div>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label" for="moneymaker2_header_links_sort_order' + header_link_row + '"><?php echo $entry_sort_order; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<input type="number" min="0" name="moneymaker2_header_links[' + header_link_row + '][sort_order]" value="' + header_link_row + '" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_header_links_sort_order' + header_link_row + '" class="form-control">';
      html += '</div>';
      html += '</div>';

      html += '</div>';
      $('#vtab-header_links .tab-content').append(html);
      $('#header_links li .btn-success').parent().before('<li><a href="#tab-hlink' + header_link_row + '" data-toggle="tab" id="link-hlink' + header_link_row + '"><i class="fa fa-minus-circle" onclick="$(\'#link-hlink' + header_link_row + '\').parent().remove(); $(\'#tab-hlink' + header_link_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#' + header_link_row + '</a></li>');
      $('#link-hlink' + header_link_row).trigger('click');
      header_link_row++;
    }
  
    var footer_contact_row = <?php echo $footer_contact_row; ?>;
    function addFooterContact() {
      html = '<div class="tab-pane active" id="tab-fcontact' + footer_contact_row + '">';
      html += '<div class="form-group">';
      html += '<div class="col-sm-offset-2 col-sm-10">';
      html += '<p><h4><?php echo $tab_data; ?> #' + footer_contact_row + '</h4></p>';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>';
      html += '<div class="col-sm-10">';
      <?php foreach ($languages as $language) { ?>
      html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
      html += '<input type="text" name="moneymaker2_footer_contacts[' + footer_contact_row + '][caption][<?php echo $language['language_id']; ?>]" value="" class="form-control">';
      html += '</div>';
      <?php } ?>
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label" for="moneymaker2_footer_contacts_link' + footer_contact_row + '"><?php echo $button_link; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<input type="text" name="moneymaker2_footer_contacts[' + footer_contact_row + '][link]" value="" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_footer_contacts_link' + footer_contact_row + '" class="form-control">';
      html += '</div>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label" for="moneymaker2_footer_contacts_sort_order' + footer_contact_row + '"><?php echo $entry_sort_order; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<input type="number" min="0" name="moneymaker2_footer_contacts[' + footer_contact_row + '][sort_order]" value="' + footer_contact_row + '" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_footer_contacts_sort_order' + footer_contact_row + '" class="form-control">';
      html += '</div>';
      html += '</div>';

      html += '</div>';
      $('#vtab-footer_contacts .tab-content').append(html);
      $('#footer_contacts li .btn-success').parent().before('<li><a href="#tab-fcontact' + footer_contact_row + '" data-toggle="tab" id="link-fcontact' + footer_contact_row + '"><i class="fa fa-minus-circle" onclick="$(\'#link-fcontact' + footer_contact_row + '\').parent().remove(); $(\'#tab-fcontact' + footer_contact_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#' + footer_contact_row + '</a></li>');

      $('#link-fcontact' + footer_contact_row).trigger('click');

      footer_contact_row++;
    }
  
    var footer_link_row = <?php echo $footer_link_row; ?>;
    function addFooterLink() {
      html = '<div class="tab-pane active" id="tab-flink' + footer_link_row + '">';
      html += '<div class="form-group">';
      html += '<div class="col-sm-offset-2 col-sm-10">';
      html += '<p><h4><?php echo $tab_data; ?> #' + footer_link_row + '</h4></p>';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>';
      html += '<div class="col-sm-10">';
      <?php foreach ($languages as $language) { ?>
      html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
      html += '<input type="text" name="moneymaker2_footer_links[' + footer_link_row + '][caption][<?php echo $language['language_id']; ?>]" value="" class="form-control">';
      html += '</div>';
      <?php } ?>
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label" for="moneymaker2_footer_links_link' + footer_link_row + '"><?php echo $button_link; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<input type="text" name="moneymaker2_footer_links[' + footer_link_row + '][link]" value="http://" id="moneymaker2_footer_links_link' + footer_link_row + '" class="form-control">';
      html += '</div>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label" for="moneymaker2_footer_links_sort_order' + footer_link_row + '"><?php echo $entry_sort_order; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<input type="number" min="0" name="moneymaker2_footer_links[' + footer_link_row + '][sort_order]" value="' + footer_link_row + '" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_footer_links_sort_order' + footer_link_row + '" class="form-control">';
      html += '</div>';
      html += '</div>';

      html += '</div>';
      $('#vtab-footer_links .tab-content').append(html);
      $('#footer_links li .btn-success').parent().before('<li><a href="#tab-flink' + footer_link_row + '" data-toggle="tab" id="link-flink' + footer_link_row + '"><i class="fa fa-minus-circle" onclick="$(\'#link-flink' + footer_link_row + '\').parent().remove(); $(\'#tab-flink' + footer_link_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#' + footer_link_row + '</a></li>');

      $('#link-flink' + footer_link_row).trigger('click');

      footer_link_row++;
    }
  
    var footer_social_row = <?php echo $footer_social_row; ?>;
    function addFooterSocial() {
      html = '<div class="tab-pane active" id="tab-social' + footer_social_row + '">';
      html += '<div class="form-group">';
      html += '<div class="col-sm-offset-2 col-sm-10">';
      html += '<p><h4><?php echo $tab_data; ?> #' + footer_social_row + '</h4></p>';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>';
      html += '<div class="col-sm-5">';
      html += '<div class="input-group">';
      html += '<div class="input-group-btn">';
      html += '<button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>';
      html += '</div>';
      html += '<select name="moneymaker2_footer_socials[' + footer_social_row + '][icon]" class="form-control">';
    <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
        html += '<option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php if ($j==1) echo "selected"; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>';
      <?php } ?>
      html += '</select>';
      html += '</div>';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_caption; ?></label>';
      html += '<div class="col-sm-10">';
      <?php foreach ($languages as $language) { ?>
      html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
      html += '<input type="text" name="moneymaker2_footer_socials[' + footer_social_row + '][caption][<?php echo $language['language_id']; ?>]" value="" class="form-control">';
      html += '</div>';
      <?php } ?>
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label" for="moneymaker2_footer_socials_link' + footer_social_row + '"><?php echo $button_link; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<input type="text" name="moneymaker2_footer_socials[' + footer_social_row + '][link]" value="http://" id="moneymaker2_footer_socials_link' + footer_social_row + '" class="form-control">';
      html += '</div>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label" for="moneymaker2_footer_socials_sort_order' + footer_social_row + '"><?php echo $entry_sort_order; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<input type="number" min="0" name="moneymaker2_footer_socials[' + footer_social_row + '][sort_order]" value="' + footer_social_row + '" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_footer_socials_sort_order' + footer_social_row + '" class="form-control">';
      html += '</div>';
      html += '</div>';

      html += '</div>';
      $('#vtab-footer_socials .tab-content').append(html);
      $('#footer_socials li .btn-success').parent().before('<li><a href="#tab-social' + footer_social_row + '" data-toggle="tab" id="link-social' + footer_social_row + '"><i class="fa fa-minus-circle" onclick="$(\'#link-social' + footer_social_row + '\').parent().remove(); $(\'#tab-social' + footer_social_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#' + footer_social_row + '</a></li>');

      $('#link-social' + footer_social_row).trigger('click');

      footer_social_row++;
    }
  
    var product_point_row = <?php echo $product_point_row; ?>;
    function addProductPoint() {
      html = '<div class="tab-pane active" id="tab-point' + product_point_row + '">';
      html += '<div class="form-group">';
      html += '<div class="col-sm-offset-2 col-sm-10">';
      html += '<p><h4><?php echo $tab_data; ?> #' + product_point_row + '</h4></p>';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_icon; ?></label>';
      html += '<div class="col-sm-5">';
      html += '<div class="input-group">';
      html += '<div class="input-group-btn">';
      html += '<button type="button" class="btn btn-default" data-toggle="modal" data-target="#iconsModal"><i class="fa fa-search"></i> <?php echo $button_view; ?></button>';
      html += '</div>';
      html += '<select name="moneymaker2_product_points[' + product_point_row + '][icon]" class="form-control">';
    <?php for ($j = 1; $j <= count($moneymaker2_fa_icons); $j++) { ?>
        html += '<option value="<?php echo $moneymaker2_fa_icons[$j]; ?>" <?php if ($j==1) echo "selected"; ?>><?php echo $j; ?> - <?php echo $moneymaker2_fa_icons[$j]; ?></option>';
      <?php } ?>
      html += '</select>';
      html += '</div>';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_categories; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<div class="well well-sm">';
      <?php foreach ($categories as $category) { ?>
      html += '<div class="checkbox">';
      html += '<label>';
      html += '<input type="checkbox" name="moneymaker2_product_points[' + product_point_row + '][categories][]" value="<?php echo $category['category_id']; ?>" /> <?php echo addslashes($category['name']); ?>';
      html += '</label>';
      html += '</div>';
      <?php } ?>
      html += '</div>';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_title; ?></label>';
      html += '<div class="col-sm-10">';
      <?php foreach ($languages as $language) { ?>
      html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
      html += '<input type="text" name="moneymaker2_product_points[' + product_point_row + '][name][<?php echo $language['language_id']; ?>]" value="" class="form-control">';
      html += '</div>';
      <?php } ?>
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_text; ?></label>';
      html += '<div class="col-sm-10">';
      <?php foreach ($languages as $language) { ?>
      html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
      html += '<textarea name="moneymaker2_product_points[' + product_point_row + '][text][<?php echo $language['language_id']; ?>]" rows="2" class="form-control"></textarea>';
      html += '</div>';
      <?php } ?>
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label" for="moneymaker2_product_points_sort_order' + product_point_row + '"><?php echo $entry_sort_order; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<input type="number" min="0" name="moneymaker2_product_points[' + product_point_row + '][sort_order]" value="' + product_point_row + '" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_product_points_sort_order' + product_point_row + '" class="form-control">';
      html += '</div>';
      html += '</div>';

      html += '</div>';
      $('#vtab-product_points .tab-content').append(html);
      $('#product_points li .btn-success').parent().before('<li><a href="#tab-point' + product_point_row + '" data-toggle="tab" id="link-point' + product_point_row + '"><i class="fa fa-minus-circle" onclick="$(\'#link-point' + product_point_row + '\').parent().remove(); $(\'#tab-point' + product_point_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#' + product_point_row + '</a></li>');

      $('#link-point' + product_point_row).trigger('click');

      product_point_row++;
    }

    var modules_promo_row = <?php echo $modules_promo_row; ?>;
    function addModulesPromo() {
      html = '<div class="tab-pane active" id="tab-promo' + modules_promo_row + '">';
      html += '<div class="form-group">';
      html += '<div class="col-sm-offset-2 col-sm-10">';
      html += '<p><h4><?php echo $tab_data; ?> #' + modules_promo_row + '</h4></p>';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $tab_image; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<a href="" id="moneymaker2_modules_promos_image_thumb' + modules_promo_row + '" data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" /></a>';
      html += '<input type="hidden" name="moneymaker2_modules_promos[' + modules_promo_row + '][image]" value="" id="moneymaker2_modules_promos_image_input' + modules_promo_row + '" />';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_categories; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<div class="well well-sm">';
    <?php foreach ($categories as $category) { ?>
        html += '<div class="checkbox">';
        html += '<label>';
        html += '<input type="checkbox" name="moneymaker2_modules_promos[' + modules_promo_row + '][categories][]" value="<?php echo $category['category_id']; ?>" /> <?php echo addslashes($category['name']); ?>';
        html += '</label>';
        html += '</div>';
      <?php } ?>
      html += '</div>';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label" for="moneymaker2_modules_promos_sort_order' + modules_promo_row + '"><?php echo $entry_sort_order; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<input type="number" min="0" name="moneymaker2_modules_promos[' + modules_promo_row + '][sort_order]" value="' + modules_promo_row + '" placeholder="<?php echo $entry_optional; ?>" id="moneymaker2_modules_promos_sort_order' + modules_promo_row + '" class="form-control">';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_text; ?></label>';
      html += '<div class="col-sm-10">';
      <?php foreach ($languages as $language) { ?>
      html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
      html += '<input type="text" name="moneymaker2_modules_promos[' + modules_promo_row + '][text][<?php echo $language['language_id']; ?>]" value="" class="form-control">';
      html += '</div>';
      <?php } ?>
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_button_title; ?></label>';
      html += '<div class="col-sm-10">';
      <?php foreach ($languages as $language) { ?>
      html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
      html += '<input type="text" name="moneymaker2_modules_promos[' + modules_promo_row + '][title][<?php echo $language['language_id']; ?>]" value="" class="form-control">';
      html += '</div>';
      <?php } ?>
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label" for="moneymaker2_modules_promos_link' + modules_promo_row + '"><?php echo $button_link; ?></label>';
      html += '<div class="col-sm-10">';
      html += '<select name="moneymaker2_modules_promos[' + modules_promo_row + '][link]" class="form-control">';
      html += '<option value="0" selected><?php echo $text_select ?></option>';
      <?php foreach ($informations as $information) { ?>
      html += '<option value="<?php echo $information['information_id']; ?>"><?php echo addslashes($information['title']); ?></option>';
      <?php } ?>
      html += '</select>';
      html += '</div>';
      html += '</div>';

      html += '<div class="form-group">';
      html += '<label class="col-sm-2 control-label"><?php echo $entry_description; ?></label>';
      html += '<div class="col-sm-10">';
    <?php foreach ($languages as $language) { ?>
        html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
        html += '<textarea name="moneymaker2_modules_promos[' + modules_promo_row + '][description][<?php echo $language['language_id']; ?>]" rows="2" placeholder="<?php echo $entry_optional; ?>" class="form-control"></textarea>';
        html += '</div>';
      <?php } ?>
      html += '</div>';
      html += '</div>';

      html += '</div>';
      $('#vtab-modules_promos .tab-content').append(html);
      $('#modules_promos li .btn-success').parent().before('<li><a href="#tab-promo' + modules_promo_row + '" data-toggle="tab" id="link-promo' + modules_promo_row + '"><i class="fa fa-minus-circle" onclick="$(\'#link-promo' + modules_promo_row + '\').parent().remove(); $(\'#tab-promo' + modules_promo_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#' + modules_promo_row + '</a></li>');

      $('#link-promo' + modules_promo_row).trigger('click');

      modules_promo_row++;
    }

  var catalog_compare_chart_row = <?php echo $catalog_compare_chart_row; ?>;
  function addCatalogCompareChart() {
    html = '<div class="tab-pane active" id="tab-catalog_compare_chart' + catalog_compare_chart_row + '">';
    html += '<div class="form-group">';
    html += '<div class="col-sm-offset-2 col-sm-10">';
    html += '<p><h4><?php echo $tab_data; ?> #' + catalog_compare_chart_row + '</h4></p>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<label class="col-sm-2 control-label"><?php echo $entry_mode; ?></label>';
    html += '<div class="col-sm-10">';
    html += '<label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_charts[' + catalog_compare_chart_row + '][mode]" value="0"  /> <?php echo $entry_chart_line; ?></label>';
    html += '<label class="radio-inline"><input type="radio" name="moneymaker2_catalog_compare_charts[' + catalog_compare_chart_row + '][mode]" value="1" checked="checked" /> <?php echo $entry_chart_bar; ?></label>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<label class="col-sm-2 control-label"><?php echo $entry_catalog_compare_chart_functions; ?></label>';
    html += '<div class="col-sm-4">';
    html += '<select name="moneymaker2_catalog_compare_charts[' + catalog_compare_chart_row + '][fns]" class="form-control">';
  <?php for ($j = 1; $j <= 5; $j++) { ?>
      html += '<option value="<?php echo $j; ?>" <?php if ($j==1) echo "selected"; ?>><?php echo $j; ?></option>';
    <?php } ?>
    html += '</select>';
    html += '</div>';
    html += '<label class="col-sm-2 control-label"><?php echo $entry_delimiter; ?></label>';
    html += '<div class="col-sm-4">';
    html += '<select name="moneymaker2_catalog_compare_charts[' + catalog_compare_chart_row + '][delimiter]" class="form-control">';
    html += '<option value="0" selected><?php echo $text_select ?></option>';
    <?php foreach ($moneymaker2_delimiters as $moneymaker2_delimiter) { ?>
    html += '<option value="<?php echo $moneymaker2_delimiter; ?>"><?php echo $moneymaker2_delimiter; ?></option>';
    <?php } ?>
    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<label class="col-sm-2 control-label"><?php echo $entry_attribute; ?></label>';
    html += '<div class="col-sm-10">';
    html += '<select name="moneymaker2_catalog_compare_charts[' + catalog_compare_chart_row + '][attribute]" class="form-control">';
    html += '<option value="0" selected><?php echo $text_select ?></option>';
  <?php foreach ($attribute_groups as $attribute_group) { ?>
    <?php foreach ($attributes as $attribute) { ?>
      <?php if ($attribute['attribute_group_id'] == $attribute_group['attribute_group_id']) { ?>
          html += '<option value="<?php echo $attribute['attribute_id']; ?>"><?php echo addslashes($attribute_group['name']); ?> > <?php echo addslashes($attribute['name']); ?></option>';
        <?php } ?>
      <?php } ?>
    <?php } ?>
    html += '</select>';
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<label class="col-sm-2 control-label"><?php echo $entry_unit; ?></label>';
    html += '<div class="col-sm-10">';
  <?php foreach ($languages as $language) { ?>
      html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
      html += '<input type="text" name="moneymaker2_catalog_compare_charts[' + catalog_compare_chart_row + '][unit][<?php echo $language['language_id']; ?>]" value="" placeholder="<?php echo $entry_optional; ?>" class="form-control">';
      html += '</div>';
    <?php } ?>
    html += '</div>';
    html += '</div>';

    html += '<div class="form-group">';
    html += '<label class="col-sm-2 control-label"><?php echo $entry_sort_order; ?></label>';
    html += '<div class="col-sm-10">';
    html += '<p class="paddingtop"><?php echo $entry_catalog_compare_chart_sort_help; ?></p>';
    html += '</div>';
    html += '</div>';
    
    html += '</div>';
    $('#vtab-catalog_compare_charts .tab-content').append(html);
    $('#catalog_compare_charts li .btn-success').parent().before('<li><a href="#tab-catalog_compare_chart' + catalog_compare_chart_row + '" data-toggle="tab" id="link-chart' + catalog_compare_chart_row + '"><i class="fa fa-minus-circle" onclick="$(\'#link-chart' + catalog_compare_chart_row + '\').parent().remove(); $(\'#tab-catalog_compare_chart' + catalog_compare_chart_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></i> <?php echo $tab_data; ?>&nbsp;#' + catalog_compare_chart_row + '</a></li>');

    $('#link-chart' + catalog_compare_chart_row).trigger('click');
    catalog_compare_chart_row++;
  }

  function testAnim(x) {
    $('#animationSandbox').removeClass().addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
      $(this).removeClass();
    });
  };
  $(document).ready(function(){
    $('.js--triggerAnimation').click(function(e){
      e.preventDefault();
      var anim = $('.js--animations').val();
      testAnim(anim);
    });

    $('.js--animations').change(function(){
      var anim = $(this).val();
      testAnim(anim);
    });
  });
    //--></script>
</div>
<?php echo $footer; ?>