<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">		
      <div class="pull-right" id="control-buttons">
		<a onclick="apply()" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="Применить"><i class="fa fa-check"></i></a>
        <button type="submit" form="form-revolution" data-toggle="tooltip" data-placement="bottom" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $button_cancel; ?>" class="btn btn-warning"><i class="fa fa-reply"></i></a>
		</div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid"> 
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default alert-helper">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
		<?php if (!in_array('revtheme', $extension)) { ?>
		<a href="<?php echo $install; ?>" data-toggle="tooltip" data-placement="right" title="<?php echo $button_install; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Установить шаблон</a>
		<?php } else { ?>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-revolution" class="form-horizontal">
		<ul class="nav nav-tabs" role="tablist" id="revtabs">
			<li class="active"><a href="#tab_header" role="tab" data-toggle="tab">Шапка сайта</a></li>
			<li><a href="#tab_home" role="tab" data-toggle="tab">Главная страница</a></li>
			<li><a href="#tab_catalog" role="tab" data-toggle="tab">Каталог товаров</a></li>
			<li><a href="#tab_product" role="tab" data-toggle="tab">Товар</a></li>
			<li><a href="#tab_footer" role="tab" data-toggle="tab">Подвал сайта</a></li>
			<li><a href="#tab_all_settings" role="tab" data-toggle="tab">Дополнительные настройки</a></li>
			<li><a href="#tab_user_set" role="tab" data-toggle="tab">Свои стили и скрипты</a></li>
		</ul>		
<div class="tab-content">
<div class="tab-pane active" id="tab_header">
	<div class="col-sm-2">
		<nav class="nav-sidebar">
			<ul class="nav tabs">
				<li class="active"><a href="#tab_header_menu" data-toggle="tab">Каталог товаров</a></li>
				<li><a href="#tab_header_links" data-toggle="tab">Ссылки в верхней линии</a></li>
				<li><a href="#tab_header_links2" data-toggle="tab">Доп. ссылки в линии каталога</a></li>
				<li><a href="#tab_header_contacts" data-toggle="tab">Контакты</a></li>
				<li><a href="#tab_header_popupphone" data-toggle="tab">Обратный звонок</a></li>
				<li><a href="#tab_header_search" data-toggle="tab">Поиск в шапке</a></li>
				<li><a href="#tab_header_cart" data-toggle="tab">Корзина товаров</a></li>
			</ul>
		</nav>
	</div>	
	<div class="tab-content col-sm-10">
		<div class="tab-pane active text-style" id="tab_header_menu">
			<div class="form-group">
				<label class="col-sm-2 control-label">Плавающая полоса с каталогом:</label>
				<div class="col-sm-3">
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_menu[sticky]" value="1" <?php if ($revtheme_header_menu['sticky']) { echo 'checked'; } ?> /> Да
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_menu[sticky]" value="0" <?php if (!$revtheme_header_menu['sticky']) { echo 'checked';} ?> /> Нет
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Тип меню:</label>
				<div class="col-sm-3">
					<label class="radio-inline">
						<input type="radio" id="header_menu_zavisimost_radio" onChange="header_menu_zavisimost(this)" name="revtheme_header_menu[type]" value="1" <?php if ($revtheme_header_menu['type']) { echo 'checked'; } ?> /> Amazon
					</label>
					<label class="radio-inline">
						<input type="radio" id="header_menu_zavisimost_radio" onChange="header_menu_zavisimost(this)" name="revtheme_header_menu[type]" value="0" <?php if (!$revtheme_header_menu['type']) { echo 'checked';} ?> /> Простое
					</label>
				</div>
			</div>
			<hr/>
			<div id="header_menu_zavisimost" style="display:none;">
			<div class="form-group">
				<label class="col-sm-2 control-label">Раскрыто на главной странице:</label>
				<div class="col-sm-3">
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_menu[inhome]" value="1" <?php if ($revtheme_header_menu['inhome']) { echo 'checked'; } ?> /> Да
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_menu[inhome]" value="0" <?php if (!$revtheme_header_menu['inhome']) { echo 'checked';} ?> /> Нет
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Увеличенная высота меню на главной странице:</label>
				<div class="col-sm-3">
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_menu[up_menu_height]" value="1" <?php if ($revtheme_header_menu['up_menu_height']) { echo 'checked'; } ?> /> Да
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_menu[up_menu_height]" value="0" <?php if (!$revtheme_header_menu['up_menu_height']) { echo 'checked';} ?> /> Нет
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Меню производителей:</label>
				<div class="col-sm-2">
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_menu[manuf]" value="1" <?php if ($revtheme_header_menu['manuf']) { echo 'checked'; } ?> /> Вкл.
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_menu[manuf]" value="0" <?php if (!$revtheme_header_menu['manuf']) { echo 'checked';} ?> /> Выкл.
					</label>
				</div>
				<label class="col-sm-3 control-label">Число колонок для производителей в меню:</label>
				<div class="col-sm-1">
					<input type="text" name="revtheme_header_menu[n_column]" value="<?php echo $revtheme_header_menu['n_column']; ?>" placeholder="495" class="form-control" />
				</div>
			</div>
			</div>
		</div>
		<div class="tab-pane text-style" id="tab_header_links">
			<div class="form-group">
				<label class="col-sm-2 control-label">Выбор языка:</label>
				<div class="col-sm-3">
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_standart_links[rev_lang]" value="1" <?php if ($revtheme_header_standart_links['rev_lang']) { echo 'checked'; } ?> /> Вкл.
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_standart_links[rev_lang]" value="0" <?php if (!$revtheme_header_standart_links['rev_lang']) { echo 'checked';} ?> /> Выкл.
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Выбор валюты:</label>
				<div class="col-sm-3">
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_standart_links[rev_curr]" value="1" <?php if ($revtheme_header_standart_links['rev_curr']) { echo 'checked'; } ?> /> Вкл.
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_standart_links[rev_curr]" value="0" <?php if (!$revtheme_header_standart_links['rev_curr']) { echo 'checked';} ?> /> Выкл.
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Сравнение:</label>
				<div class="col-sm-3">
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_standart_links[rev_srav]" value="1" <?php if ($revtheme_header_standart_links['rev_srav']) { echo 'checked'; } ?> /> Вкл.
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_standart_links[rev_srav]" value="0" <?php if (!$revtheme_header_standart_links['rev_srav']) { echo 'checked';} ?> /> Выкл.
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Закладки:</label>
				<div class="col-sm-3">
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_standart_links[rev_wish]" value="1" <?php if ($revtheme_header_standart_links['rev_wish']) { echo 'checked'; } ?> /> Вкл.
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_standart_links[rev_wish]" value="0" <?php if (!$revtheme_header_standart_links['rev_wish']) { echo 'checked';} ?> /> Выкл.
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Личный кабинет:</label>
				<div class="col-sm-3">
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_standart_links[rev_acc]" value="1" <?php if ($revtheme_header_standart_links['rev_acc']) { echo 'checked'; } ?> /> Вкл.
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_standart_links[rev_acc]" value="0" <?php if (!$revtheme_header_standart_links['rev_acc']) { echo 'checked';} ?> /> Выкл.
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Авторизация в всплывающем окне:</label>
				<div class="col-sm-3">
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_standart_links[popup_login]" value="1" <?php if ($revtheme_header_standart_links['popup_login']) { echo 'checked'; } ?> /> Да
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_header_standart_links[popup_login]" value="0" <?php if (!$revtheme_header_standart_links['popup_login']) { echo 'checked';} ?> /> Нет
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-12">Дополнительные ссылки в верхней линии шапки сайта:</label>
				<div class="col-sm-12">
					<table id="t_header_links" class="table table-bordered">
						<thead>
							<tr>
								<td class="nowrap">Ссылка</td>
								<td class="nowrap">Заголовок</td>
								<td class="nowrap">Сортировка</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
						<?php $item_row_header_link = 1; ?>
						<?php foreach ($revtheme_header_links as $revtheme_header_link) { ?>
							<tr id="item-row-main<?php echo $item_row_header_link; ?>" class="item_row_header_link">							
								<td class="text-left">
										<input class="form-control" type="text" name="revtheme_header_link[<?php echo $item_row_header_link; ?>][link]" value="<?php echo $revtheme_header_link['link']; ?>" />
								</td>
								<td class="text-left">
										<input class="form-control" type="text" name="revtheme_header_link[<?php echo $item_row_header_link; ?>][title]" value="<?php echo $revtheme_header_link['title']; ?>" />
								</td>
								<td class="text-left">
									<input class="form-control" type="text" name="revtheme_header_link[<?php echo $item_row_header_link; ?>][sort]" value="<?php echo $revtheme_header_link['sort']; ?>" />
								</td>
								<td class="text-right">
									<a class="btn btn-danger" onclick="$('#t_header_links #item-row-main<?php echo $item_row_header_link; ?>').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
						<?php $item_row_header_link++; ?>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3"></td>
								<td class="text-right"><a class="btn btn-primary" onclick="addBlockHeaderLink();" data-toggle="tooltip" title="Добавить"><i class="fa fa-plus-circle"></i></a></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane text-style" id="tab_header_links2">
			<div class="form-group">
				<div class="col-sm-12">
					<table id="t_header_links2" class="table table-bordered">
						<thead>
							<tr>
								<td class="nowrap">Ссылка</td>
								<td class="nowrap">Заголовок</td>
								<td class="nowrap">Сортировка</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
						<?php $item_row_header_link2 = 1; ?>
						<?php foreach ($revtheme_header_links2 as $revtheme_header_link2) { ?>
							<tr id="item-row-main<?php echo $item_row_header_link2; ?>" class="item_row_header_link2">							
								<td class="text-left">
										<input class="form-control" type="text" name="revtheme_header_link2[<?php echo $item_row_header_link2; ?>][link]" value="<?php echo $revtheme_header_link2['link']; ?>" />
								</td>
								<td class="text-left">
										<input class="form-control" type="text" name="revtheme_header_link2[<?php echo $item_row_header_link2; ?>][title]" value="<?php echo $revtheme_header_link2['title']; ?>" />
								</td>
								<td class="text-left">
									<input class="form-control" type="text" name="revtheme_header_link2[<?php echo $item_row_header_link2; ?>][sort]" value="<?php echo $revtheme_header_link2['sort']; ?>" />
								</td>
								<td class="text-right">
									<a class="btn btn-danger" onclick="$('#t_header_links2 #item-row-main<?php echo $item_row_header_link2; ?>').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
						<?php $item_row_header_link2++; ?>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3"></td>
								<td class="text-right"><a class="btn btn-primary" onclick="addBlockHeaderLink2();" data-toggle="tooltip" title="Добавить"><i class="fa fa-plus-circle"></i></a></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane text-style" id="tab_header_contacts">
			<div class="form-group">
				<label class="col-sm-2 control-label">Режим работы:</label>
				<div class="col-sm-3">
				  <input type="text" name="revtheme_header_phone[text]" value="<?php echo $revtheme_header_phone['text']; ?>" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Режим работы 2 строка:</label>
				<div class="col-sm-3">
				  <input type="text" name="revtheme_header_phone[text2]" value="<?php echo $revtheme_header_phone['text2']; ?>" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Номер телефона:</label>
				<div class="col-sm-2">
				  <input data-toggle="tooltip" data-placement="top" title="Код города" type="text" name="revtheme_header_phone[cod]" value="<?php echo $revtheme_header_phone['cod']; ?>" placeholder="495" class="form-control" />
				</div>
				<div class="col-sm-3">
				  <input type="text" name="revtheme_header_phone[number]" value="<?php echo $revtheme_header_phone['number']; ?>" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Включить доп. контакты:</label>
				<div class="col-sm-3">
					<label class="radio-inline">
						<input type="radio" id="dop_contacts_zavisimost_radio" onChange="dop_contacts_zavisimost(this)" name="revtheme_header_dop_contacts_status" value="1" <?php if ($revtheme_header_dop_contacts_status) { echo 'checked'; } ?> /> Да
					</label>
					<label class="radio-inline">
						<input type="radio" id="dop_contacts_zavisimost_radio" onChange="dop_contacts_zavisimost(this)" name="revtheme_header_dop_contacts_status" value="0" <?php if (!$revtheme_header_dop_contacts_status) { echo 'checked';} ?> /> Нет
					</label>
				</div>
			</div>
			<div id="dop_contacts_zavisimost" style="display:none;">
			<div class="form-group">
				<div class="col-sm-12 dop_contact">
					<table id="t_dop_contacts" class="table table-bordered">
					<thead>
						<tr>
							<td class="nowrap">Иконка</td>
							<td class="nowrap">Контакт</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					<?php $contact_row = 1; ?>
					<?php foreach ($revtheme_header_dop_contacts as $revtheme_header_dop_contact) { ?>
					<tr id="item-row-dop_contact<?php echo $contact_row; ?>" class="contact_row">
						<td class="text-center">
							<span class="fa_icon" id="icon_dop_contact_<?php echo $contact_row; ?>" onclick="fa_icons($(this).attr('id'))"><i class="<?php echo $revtheme_header_dop_contact['icon']; ?>"></i></span>
							<input type="hidden" name="revtheme_header_dop_contact[<?php echo $contact_row; ?>][icon]" value="<?php echo $revtheme_header_dop_contact['icon']; ?>" id="input-icon<?php echo $contact_row; ?>" />
						</td>
						<td class="text-left">
							<input class="form-control" type="text" name="revtheme_header_dop_contact[<?php echo $contact_row; ?>][number]" value="<?php if(isset($revtheme_header_dop_contact['number'])) { ?><?php echo $revtheme_header_dop_contact['number']; ?><?php } ?>" placeholder="Телефон <?php echo $contact_row; ?>" />
						</td>	
						<td class="text-right">
							<a class="btn btn-danger" onclick="$('#t_dop_contacts #item-row-dop_contact<?php echo $contact_row; ?>').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a>
						</td>
					</tr>
					<?php $contact_row++; ?>
					<?php } ?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2"></td>
							<td class="text-right"><a class="btn btn-primary" onclick="addDopContact();" data-toggle="tooltip" title="Добавить"><i class="fa fa-plus-circle"></i></a></td>
						</tr>
					</tfoot>
					</table>
				</div>
			</div>
			</div>
		</div>
		<div class="tab-pane text-style" id="tab_header_popupphone">
		<div class="form-group">
			<label class="col-sm-2 control-label">Включить:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_popupphone[status]" value="1" <?php if ($revtheme_header_popupphone['status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_popupphone[status]" value="0" <?php if (!$revtheme_header_popupphone['status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="input-firstname">Имя:</label>
			<div class="col-sm-3">
			  <select name="revtheme_header_popupphone[firstname]" id="input-firstname" class="form-control">
				<?php if ($revtheme_header_popupphone['firstname'] == 1) { ?>
				<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } elseif ($revtheme_header_popupphone['firstname'] == 2) { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2" selected="selected">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
			  </select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="input-telephone">Телефон:</label>
			<div class="col-sm-3">
			  <select name="revtheme_header_popupphone[telephone]" id="input-telephone" class="form-control">
				<?php if ($revtheme_header_popupphone['telephone'] == 1) { ?>
				<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } elseif ($revtheme_header_popupphone['telephone'] == 2) { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2" selected="selected">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
			  </select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Маска для телефона:</label>
			<div class="col-sm-3">
				<input class="form-control" name="revtheme_header_popupphone[telephone_mask]" value="<?php echo isset($revtheme_header_popupphone['telephone_mask']) ? $revtheme_header_popupphone['telephone_mask'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="input-email">E-mail:</label>
			<div class="col-sm-3">
			  <select name="revtheme_header_popupphone[email]" id="input-email" class="form-control">
				<?php if ($revtheme_header_popupphone['email'] == 1) { ?>
				<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } elseif ($revtheme_header_popupphone['email'] == 2) { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2" selected="selected">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
			  </select>
			</div>
		</div>		  
		<div class="form-group">
			<label class="col-sm-2 control-label" for="input-comment">Комментарий:</label>
			<div class="col-sm-3">
			  <select name="revtheme_header_popupphone[comment]" id="input-comment" class="form-control">
				<?php if ($revtheme_header_popupphone['comment'] == 1) { ?>
				<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } elseif ($revtheme_header_popupphone['comment'] == 2) { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2" selected="selected">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
			  </select>
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_header_search">
		<div class="form-group">
			<label class="col-sm-2 control-label">Выбор категории поиска:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[in_category]" value="1" <?php if ($revtheme_header_search['in_category']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[in_category]" value="0" <?php if (!$revtheme_header_search['in_category']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
			<label class="col-sm-2 control-label">Ajax поиск товаров:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" id="ajax_search_status" onChange="ajax_search_status_zavisimost(this)" name="revtheme_header_search[ajax_search_status]" value="1" <?php if ($revtheme_header_search['ajax_search_status']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" id="ajax_search_status" onChange="ajax_search_status_zavisimost(this)" name="revtheme_header_search[ajax_search_status]" value="0" <?php if (!$revtheme_header_search['ajax_search_status']) {echo 'checked'; } ?> /> Выкл.
				</label>
			</div>
		</div>
		<div id="ajax_search_status_zavisimost" style="display:none;">
		<div class="form-group"><div class="col-sm-12">Настройки Ajax поиска:</div></div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Лимит результатов поиска:</label>
			<div class="col-sm-3">
				<input class="form-control" type="text" name="revtheme_header_search[ajax_search_limit]" value="<?php echo isset($revtheme_header_search['ajax_search_limit']) ? $revtheme_header_search['ajax_search_limit'] : ''; ?>" />
			</div>
			<label class="col-sm-2 control-label">Искать по модели товара:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_model]" value="1" <?php if ($revtheme_header_search['ajax_search_model']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_model]" value="0" <?php if (!$revtheme_header_search['ajax_search_model']) {echo 'checked'; } ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Искать по производителю товара:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_manufacturer]" value="1" <?php if ($revtheme_header_search['ajax_search_manufacturer']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_manufacturer]" value="0" <?php if (!$revtheme_header_search['ajax_search_manufacturer']) {echo 'checked'; } ?> /> Нет
				</label>
			</div>
			<label class="col-sm-2 control-label">Искать по тегам товара:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_tag]" value="1" <?php if ($revtheme_header_search['ajax_search_tag']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_tag]" value="0" <?php if (!$revtheme_header_search['ajax_search_tag']) {echo 'checked'; } ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Искать по артикулу (sku) товара:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_sku]" value="1" <?php if ($revtheme_header_search['ajax_search_sku']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_sku]" value="0" <?php if (!$revtheme_header_search['ajax_search_sku']) {echo 'checked'; } ?> /> Нет
				</label>
			</div>
			<label class="col-sm-2 control-label">Искать по upc товара:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_upc]" value="1" <?php if ($revtheme_header_search['ajax_search_upc']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_upc]" value="0" <?php if (!$revtheme_header_search['ajax_search_upc']) {echo 'checked'; } ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Искать по mpn товара:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_mpn]" value="1" <?php if ($revtheme_header_search['ajax_search_mpn']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_mpn]" value="0" <?php if (!$revtheme_header_search['ajax_search_mpn']) {echo 'checked'; } ?> /> Нет
				</label>
			</div>
			<label class="col-sm-2 control-label">Искать по isbn товара:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_isbn]" value="1" <?php if ($revtheme_header_search['ajax_search_isbn']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_isbn]" value="0" <?php if (!$revtheme_header_search['ajax_search_isbn']) {echo 'checked'; } ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Искать по jan товара:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_jan]" value="1" <?php if ($revtheme_header_search['ajax_search_jan']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_jan]" value="0" <?php if (!$revtheme_header_search['ajax_search_jan']) {echo 'checked'; } ?> /> Нет
				</label>
			</div>
			<label class="col-sm-2 control-label">Искать по ean товара:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_ean]" value="1" <?php if ($revtheme_header_search['ajax_search_ean']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_search[ajax_search_ean]" value="0" <?php if (!$revtheme_header_search['ajax_search_ean']) {echo 'checked'; } ?> /> Нет
				</label>
			</div>
		</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_header_cart">
		<div class="form-group">
			<label class="col-sm-2 control-label">Тип корзины:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_cart[type]" value="1" <?php if ($revtheme_header_cart['type']) { echo 'checked'; } ?> /> Выпадающая
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_header_cart[type]" value="0" <?php if (!$revtheme_header_cart['type']) { echo 'checked';} ?> /> Всплывающая
				</label>
			</div>
		</div>
	</div>
	</div>	
</div>
<div class="tab-pane" id="tab_catalog">
	<div class="col-sm-2">
		<nav class="nav-sidebar">
			<ul class="nav tabs">
				<li class="active"><a href="#tab_cat_all" data-toggle="tab">Общие настройки</a></li>
				<li><a href="#tab_cat_sorts" data-toggle="tab">Сортировки</a></li>
				<li><a href="#tab_cat_stikers" data-toggle="tab">Стикеры</a></li>
				<li><a href="#tab_cat_popuporder" data-toggle="tab">Быстрый заказ</a></li>
				<li><a href="#tab_cat_compare" data-toggle="tab">Сравнение товаров</a></li>
				<li><a href="#tab_cat_mods" data-toggle="tab">Модули</a></li>
				<li><a href="#tab_cat_attributes" data-toggle="tab">Вывод свойств товара</a></li>
				<!--<li><a href="#tab_cat_filter" data-toggle="tab">Фильтр товаров</a></li>-->
			</ul>
		</nav>
	</div>
<div class="tab-content col-sm-10">
	<div class="tab-pane active text-style" id="tab_cat_all">
		<div class="form-group">
			<label class="col-sm-2 control-label">Показывать выбор подкатегорий на странице с товарами:</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_all[podcategory_status]" value="1" <?php if ($revtheme_catalog_all['podcategory_status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_all[podcategory_status]" value="0" <?php if (!$revtheme_catalog_all['podcategory_status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Кнопка "Сравнения":</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_all[rev_srav_prod]" value="1" <?php if ($revtheme_catalog_all['rev_srav_prod']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_all[rev_srav_prod]" value="0" <?php if (!$revtheme_catalog_all['rev_srav_prod']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Кнопка "Закладок":</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_all[rev_wish_prod]" value="1" <?php if ($revtheme_catalog_all['rev_wish_prod']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_all[rev_wish_prod]" value="0" <?php if (!$revtheme_catalog_all['rev_wish_prod']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Быстрый просмотр товара:</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_all[popup_view]" value="1" <?php if ($revtheme_catalog_all['popup_view']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_all[popup_view]" value="0" <?php if (!$revtheme_catalog_all['popup_view']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Слайдер доп. изображений:</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_all[img_slider]" value="1" <?php if ($revtheme_catalog_all['img_slider']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_all[img_slider]" value="0" <?php if (!$revtheme_catalog_all['img_slider']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label pt0">Лимит товаров на странице<br>"Новинки":
			</label>
			<div class="col-sm-2">
				<input class="form-control" type="text" name="revtheme_catalog_last[limit]" value="<?php echo isset($revtheme_catalog_last['limit']) ? $revtheme_catalog_last['limit'] : ''; ?>" />
			</div>
		</div>
		<hr>
		<div class="form-group">
			<label class="col-sm-2 control-label pt0">Лимит товаров на странице"Хиты продаж":
			</label>
			<div class="col-sm-2">
				<input class="form-control" type="text" name="revtheme_catalog_best[limit]" value="<?php echo isset($revtheme_catalog_best['limit']) ? $revtheme_catalog_best['limit'] : ''; ?>" />
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_cat_sorts">
		<div class="form-group">
            <label class="col-sm-2 control-label">Сортировка по умолчанию:</label>
            <div class="col-sm-2">
              <select name="revtheme_catalog_sorts_category[sort_default]" class="form-control">
                <?php if ($revtheme_catalog_sorts_category['sort_default'] == 'p.sort_order') { ?>
                <option value="p.sort_order" selected="selected">По умолчанию</option>
                <?php } else { ?>
                <option value="p.sort_order">По умолчанию</option>
                <?php } ?>
				<?php if ($revtheme_catalog_sorts_category['sort_default'] == 'pd.name') { ?>
                <option value="pd.name" selected="selected">По имени</option>
                <?php } else { ?>
                <option value="pd.name">По имени</option>
                <?php } ?>
				<?php if ($revtheme_catalog_sorts_category['sort_default'] == 'p.price') { ?>
                <option value="p.price" selected="selected">По Цене</option>
                <?php } else { ?>
				<option value="p.price">По Цене</option>
				<?php } ?>
				<?php if ($revtheme_catalog_sorts_category['sort_default'] == 'rating') { ?>
                <option value="rating" selected="selected">По Рейтингу</option>
                <?php } else { ?>
                <option value="rating">По Рейтингу</option>
                <?php } ?>             
				<?php if ($revtheme_catalog_sorts_category['sort_default'] == 'p.model') { ?>
                <option value="p.model" selected="selected">По Модели</option>
                <?php } else { ?>
                <option value="p.model">По Модели</option>
                <?php } ?>
				<?php if ($revtheme_catalog_sorts_category['sort_default'] == 'p.date_added') { ?>
                <option value="p.date_added" selected="selected">По дате поступления </option>
                <?php } else { ?>
                <option value="p.date_added">По дате поступления </option>
                <?php } ?>
              </select>
            </div>
			<label class="col-sm-2 control-label">Направление:</label>
            <div class="col-sm-2">
              <select name="revtheme_catalog_sorts_category[sort_default_adesc]" class="form-control">
				<?php if ($revtheme_catalog_sorts_category['sort_default_adesc'] == 'ASC') { ?>
                <option value="ASC" selected="selected">По возрастанию</option>
                <?php } else { ?>
                <option value="ASC">По возрастанию</option>
                <?php } ?>
                <?php if ($revtheme_catalog_sorts_category['sort_default_adesc'] == 'DESC') { ?>
                <option value="DESC" selected="selected">По убыванию</option>
                <?php } else { ?>
                <option value="DESC">По убыванию</option>
                <?php } ?>		
              </select>
            </div>
        </div>
		<div class="form-group"><div class="col-sm-12">Используемые сортировки и их заголовки:</div></div>
		<div class="form-group">
			<label class="col-sm-2 control-label">По умолчанию:</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[order_ASC]" value="1" <?php if ($revtheme_catalog_sorts_category['order_ASC']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[order_ASC]" value="0" <?php if (!$revtheme_catalog_sorts_category['order_ASC']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-2">
				<input class="form-control" name="revtheme_catalog_sorts_category[order_ASC_text]" value="<?php echo isset($revtheme_catalog_sorts_category['order_ASC_text']) ? $revtheme_catalog_sorts_category['order_ASC_text'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">По имени (А-Я):</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[name_ASC]" value="1" <?php if ($revtheme_catalog_sorts_category['name_ASC']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[name_ASC]" value="0" <?php if (!$revtheme_catalog_sorts_category['name_ASC']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-2">
				<input class="form-control" name="revtheme_catalog_sorts_category[name_ASC_text]" value="<?php echo isset($revtheme_catalog_sorts_category['name_ASC_text']) ? $revtheme_catalog_sorts_category['name_ASC_text'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">По имени (Я-А):</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[name_DESC]" value="1" <?php if ($revtheme_catalog_sorts_category['name_DESC']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[name_DESC]" value="0" <?php if (!$revtheme_catalog_sorts_category['name_DESC']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-2">
				<input class="form-control" name="revtheme_catalog_sorts_category[name_DESC_text]" value="<?php echo isset($revtheme_catalog_sorts_category['name_DESC_text']) ? $revtheme_catalog_sorts_category['name_DESC_text'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">По Цене (возрастанию):</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[price_ASC]" value="1" <?php if ($revtheme_catalog_sorts_category['price_ASC']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[price_ASC]" value="0" <?php if (!$revtheme_catalog_sorts_category['price_ASC']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-2">
				<input class="form-control" name="revtheme_catalog_sorts_category[price_ASC_text]" value="<?php echo isset($revtheme_catalog_sorts_category['price_ASC_text']) ? $revtheme_catalog_sorts_category['price_ASC_text'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">По Цене (убыванию):</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[price_DESC]" value="1" <?php if ($revtheme_catalog_sorts_category['price_DESC']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[price_DESC]" value="0" <?php if (!$revtheme_catalog_sorts_category['price_DESC']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-2">
				<input class="form-control" name="revtheme_catalog_sorts_category[price_DESC_text]" value="<?php echo isset($revtheme_catalog_sorts_category['price_DESC_text']) ? $revtheme_catalog_sorts_category['price_DESC_text'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">По Рейтингу (убыванию):</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[rating_DESC]" value="1" <?php if ($revtheme_catalog_sorts_category['rating_DESC']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[rating_DESC]" value="0" <?php if (!$revtheme_catalog_sorts_category['rating_DESC']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-2">
				<input class="form-control" name="revtheme_catalog_sorts_category[rating_DESC_text]" value="<?php echo isset($revtheme_catalog_sorts_category['rating_DESC_text']) ? $revtheme_catalog_sorts_category['rating_DESC_text'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">По Рейтингу (возрастанию):</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[rating_ASC]" value="1" <?php if ($revtheme_catalog_sorts_category['rating_ASC']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[rating_ASC]" value="0" <?php if (!$revtheme_catalog_sorts_category['rating_ASC']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-2">
				<input class="form-control" name="revtheme_catalog_sorts_category[rating_ASC_text]" value="<?php echo isset($revtheme_catalog_sorts_category['rating_ASC_text']) ? $revtheme_catalog_sorts_category['rating_ASC_text'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">По Модели (A - Я):</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[model_ASC]" value="1" <?php if ($revtheme_catalog_sorts_category['model_ASC']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[model_ASC]" value="0" <?php if (!$revtheme_catalog_sorts_category['model_ASC']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-2">
				<input class="form-control" name="revtheme_catalog_sorts_category[model_ASC_text]" value="<?php echo isset($revtheme_catalog_sorts_category['model_ASC_text']) ? $revtheme_catalog_sorts_category['model_ASC_text'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">По Модели (Я - A):</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[model_DESC]" value="1" <?php if ($revtheme_catalog_sorts_category['model_DESC']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[model_DESC]" value="0" <?php if (!$revtheme_catalog_sorts_category['model_DESC']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-2">
				<input class="form-control" name="revtheme_catalog_sorts_category[model_DESC_text]" value="<?php echo isset($revtheme_catalog_sorts_category['model_DESC_text']) ? $revtheme_catalog_sorts_category['model_DESC_text'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">По дате поступления (старые > новые):</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[date_added_ASC]" value="1" <?php if ($revtheme_catalog_sorts_category['date_added_ASC']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[date_added_ASC]" value="0" <?php if (!$revtheme_catalog_sorts_category['date_added_ASC']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-2">
				<input class="form-control" name="revtheme_catalog_sorts_category[date_added_ASC_text]" value="<?php echo isset($revtheme_catalog_sorts_category['date_added_ASC_text']) ? $revtheme_catalog_sorts_category['date_added_ASC_text'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">По дате поступления (новые > старые):</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[date_added_DESC]" value="1" <?php if ($revtheme_catalog_sorts_category['date_added_DESC']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_sorts_category[date_added_DESC]" value="0" <?php if (!$revtheme_catalog_sorts_category['date_added_DESC']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-2">
				<input class="form-control" name="revtheme_catalog_sorts_category[date_added_DESC_text]" value="<?php echo isset($revtheme_catalog_sorts_category['date_added_DESC_text']) ? $revtheme_catalog_sorts_category['date_added_DESC_text'] : ''; ?>" />
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_cat_stikers">
		<div class="form-group">
			<label class="col-sm-2 control-label">Включить стикеры:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[status]" value="1" <?php if ($revtheme_catalog_stiker['status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[status]" value="0" <?php if (!$revtheme_catalog_stiker['status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Стикер "Новинка":</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[new_status]" value="1" <?php if ($revtheme_catalog_stiker['new_status']) { echo 'checked'; } ?> /> Вкл
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[new_status]" value="0" <?php if (!$revtheme_catalog_stiker['new_status']) { echo 'checked';} ?> /> Выкл
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Стикер "Хит":</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[best_status]" value="1" <?php if ($revtheme_catalog_stiker['best_status']) { echo 'checked'; } ?> /> Вкл
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[best_status]" value="0" <?php if (!$revtheme_catalog_stiker['best_status']) { echo 'checked';} ?> /> Выкл
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Стикер "Акция":</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[spec_status]" value="1" <?php if ($revtheme_catalog_stiker['spec_status']) { echo 'checked'; } ?> /> Вкл
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[spec_status]" value="0" <?php if (!$revtheme_catalog_stiker['spec_status']) { echo 'checked';} ?> /> Выкл
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Стикер "Нет в наличии":</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[stock_status]" value="1" <?php if ($revtheme_catalog_stiker['stock_status']) { echo 'checked'; } ?> /> Вкл
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[stock_status]" value="0" <?php if (!$revtheme_catalog_stiker['stock_status']) { echo 'checked';} ?> /> Выкл
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Поле UPC как стикер:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[upc]" value="1" <?php if ($revtheme_catalog_stiker['upc']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[upc]" value="0" <?php if (!$revtheme_catalog_stiker['upc']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Поле EAN как стикер:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[ean]" value="1" <?php if ($revtheme_catalog_stiker['ean']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[ean]" value="0" <?php if (!$revtheme_catalog_stiker['ean']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Поле JAN как стикер:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[jan]" value="1" <?php if ($revtheme_catalog_stiker['jan']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[jan]" value="0" <?php if (!$revtheme_catalog_stiker['jan']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Поле ISBN как стикер:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[isbn]" value="1" <?php if ($revtheme_catalog_stiker['isbn']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[isbn]" value="0" <?php if (!$revtheme_catalog_stiker['isbn']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Поле MPN как стикер:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[mpn]" value="1" <?php if ($revtheme_catalog_stiker['mpn']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_stiker[mpn]" value="0" <?php if (!$revtheme_catalog_stiker['mpn']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Стикер "Новинка":</label>
			<label class="col-sm-2 control-label">Цвет фона:</label>
			<div class="col-sm-2">
				<input class="jscolor form-control" name="revtheme_catalog_stiker[last_color]" value="<?php echo isset($revtheme_catalog_stiker['last_color']) ? $revtheme_catalog_stiker['last_color'] : ''; ?>" />
			</div>
			<label class="col-sm-2 control-label">Цвет текста:</label>
			<div class="col-sm-2">
				<input class="jscolor form-control" name="revtheme_catalog_stiker[last_color_text]" value="<?php echo isset($revtheme_catalog_stiker['last_color_text']) ? $revtheme_catalog_stiker['last_color_text'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Стикер "Хит":</label>
			<label class="col-sm-2 control-label">Цвет фона:</label>
			<div class="col-sm-2">
				<input class="jscolor form-control" name="revtheme_catalog_stiker[best_color]" value="<?php echo isset($revtheme_catalog_stiker['best_color']) ? $revtheme_catalog_stiker['best_color'] : ''; ?>" />
			</div>
			<label class="col-sm-2 control-label">Цвет текста:</label>
			<div class="col-sm-2">
				<input class="jscolor form-control" name="revtheme_catalog_stiker[best_color_text]" value="<?php echo isset($revtheme_catalog_stiker['best_color_text']) ? $revtheme_catalog_stiker['best_color_text'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Стикер "Акция":</label>
			<label class="col-sm-2 control-label">Цвет фона:</label>
			<div class="col-sm-2">
				<input class="jscolor form-control" name="revtheme_catalog_stiker[spec_color]" value="<?php echo isset($revtheme_catalog_stiker['spec_color']) ? $revtheme_catalog_stiker['spec_color'] : ''; ?>" />
			</div>
			<label class="col-sm-2 control-label">Цвет текста:</label>
			<div class="col-sm-2">
				<input class="jscolor form-control" name="revtheme_catalog_stiker[spec_color_text]" value="<?php echo isset($revtheme_catalog_stiker['spec_color_text']) ? $revtheme_catalog_stiker['spec_color_text'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Пользовательские стикеры:</label>
			<label class="col-sm-2 control-label">Цвет фона:</label>
			<div class="col-sm-2">
				<input class="jscolor form-control" name="revtheme_catalog_stiker[user_color]" value="<?php echo isset($revtheme_catalog_stiker['user_color']) ? $revtheme_catalog_stiker['user_color'] : ''; ?>" />
			</div>
			<label class="col-sm-2 control-label">Цвет текста:</label>
			<div class="col-sm-2">
				<input class="jscolor form-control" name="revtheme_catalog_stiker[user_color_text]" value="<?php echo isset($revtheme_catalog_stiker['user_color_text']) ? $revtheme_catalog_stiker['user_color_text'] : ''; ?>" />
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_cat_popuporder">
		<div class="form-group">
			<label class="col-sm-2 control-label">Включить:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_popuporder[status]" value="1" <?php if ($revtheme_catalog_popuporder['status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_popuporder[status]" value="0" <?php if (!$revtheme_catalog_popuporder['status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="input-firstname">Имя</label>
			<div class="col-sm-3">
			  <select name="revtheme_catalog_popuporder[firstname]" id="input-firstname" class="form-control">
				<?php if ($revtheme_catalog_popuporder['firstname'] == 1) { ?>
				<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } elseif ($revtheme_catalog_popuporder['firstname'] == 2) { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2" selected="selected">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
			  </select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="input-telephone">Телефон</label>
			<div class="col-sm-3">
			  <select name="revtheme_catalog_popuporder[telephone]" id="input-telephone" class="form-control">
				<?php if ($revtheme_catalog_popuporder['telephone'] == 1) { ?>
				<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } elseif ($revtheme_catalog_popuporder['telephone'] == 2) { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2" selected="selected">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
			  </select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Маска для телефона:</label>
			<div class="col-sm-3">
				<input class="form-control" name="revtheme_catalog_popuporder[telephone_mask]" value="<?php echo isset($revtheme_catalog_popuporder['telephone_mask']) ? $revtheme_catalog_popuporder['telephone_mask'] : ''; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="input-email">E-mail</label>
			<div class="col-sm-3">
			  <select name="revtheme_catalog_popuporder[email]" id="input-email" class="form-control">
				<?php if ($revtheme_catalog_popuporder['email'] == 1) { ?>
				<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } elseif ($revtheme_catalog_popuporder['email'] == 2) { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2" selected="selected">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
			  </select>
			</div>
		</div>		  
		<div class="form-group">
			<label class="col-sm-2 control-label" for="input-comment">Комментарий</label>
			<div class="col-sm-3">
			  <select name="revtheme_catalog_popuporder[comment]" id="input-comment" class="form-control">
				<?php if ($revtheme_catalog_popuporder['comment'] == 1) { ?>
				<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } elseif ($revtheme_catalog_popuporder['comment'] == 2) { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2" selected="selected">Включено и обязательно</option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="2">Включено и обязательно</option>
				<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
			  </select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Статус нового заказа:</label>
			<div class="col-sm-3">
			<select name="revtheme_catalog_popuporder[order_status]" class="form-control">
			  <?php foreach ($order_statuses as $order_status) { ?>
			  <?php if ($order_status['order_status_id'] == $revtheme_catalog_popuporder['order_status']) { ?>
			  <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
			  <?php } else { ?>
			  <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
			  <?php } ?>
			  <?php } ?>
			</select>
		  </div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_cat_compare">
		<div class="form-group">
			<label class="col-sm-2 control-label">Сортировка по категориям на странице сравнения:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_cat_compare[cat_sort]" value="1" <?php if ($revtheme_cat_compare['cat_sort']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_cat_compare[cat_sort]" value="0" <?php if (!$revtheme_cat_compare['cat_sort']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">У товаров используется "Главная категория":
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_cat_compare[main_cat]" value="1" <?php if ($revtheme_cat_compare['main_cat']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_cat_compare[main_cat]" value="0" <?php if (!$revtheme_cat_compare['main_cat']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Показывать на странице сравнения:
			</label>
		</div>	
		<div class="form-group" style="border-top: none; padding: 5px 0;">	
			<label class="col-sm-2 control-label">Цена:
			</label>
			<div class="col-sm-3">
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_price]" value="1" <?php if ($revtheme_cat_compare['compare_price']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_price]" value="0" <?php if (!$revtheme_cat_compare['compare_price']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group" style="border-top: none; padding: 5px 0;">	
			<label class="col-sm-2 control-label">Модель:
			</label>
			<div class="col-sm-3">
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_model]" value="1" <?php if ($revtheme_cat_compare['compare_model']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_model]" value="0" <?php if (!$revtheme_cat_compare['compare_model']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group" style="border-top: none; padding: 5px 0;">	
			<label class="col-sm-2 control-label">Артикул:
			</label>
			<div class="col-sm-3">
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_sku]" value="1" <?php if ($revtheme_cat_compare['compare_sku']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_sku]" value="0" <?php if (!$revtheme_cat_compare['compare_sku']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group" style="border-top: none; padding: 5px 0;">	
			<label class="col-sm-2 control-label">Производитель:
			</label>
			<div class="col-sm-3">
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_manuf]" value="1" <?php if ($revtheme_cat_compare['compare_manuf']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_manuf]" value="0" <?php if (!$revtheme_cat_compare['compare_manuf']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group" style="border-top: none; padding: 5px 0;">	
			<label class="col-sm-2 control-label">Наличие:
			</label>
			<div class="col-sm-3">
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_stock]" value="1" <?php if ($revtheme_cat_compare['compare_stock']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_stock]" value="0" <?php if (!$revtheme_cat_compare['compare_stock']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group" style="border-top: none; padding: 5px 0;">	
			<label class="col-sm-2 control-label">Рейтинг:
			</label>
			<div class="col-sm-3">
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_rate]" value="1" <?php if ($revtheme_cat_compare['compare_rate']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_rate]" value="0" <?php if (!$revtheme_cat_compare['compare_rate']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group" style="border-top: none; padding: 5px 0;">	
			<label class="col-sm-2 control-label">Краткое описание:
			</label>
			<div class="col-sm-3">
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_srtdesc]" value="1" <?php if ($revtheme_cat_compare['compare_srtdesc']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_srtdesc]" value="0" <?php if (!$revtheme_cat_compare['compare_srtdesc']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group" style="border-top: none; padding: 5px 0;">	
			<label class="col-sm-2 control-label">Вес:
			</label>
			<div class="col-sm-3">
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_weight]" value="1" <?php if ($revtheme_cat_compare['compare_weight']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_weight]" value="0" <?php if (!$revtheme_cat_compare['compare_weight']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group" style="border-top: none; padding: 5px 0;">	
			<label class="col-sm-2 control-label">Размеры:
			</label>
			<div class="col-sm-3">
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_razmer]" value="1" <?php if ($revtheme_cat_compare['compare_razmer']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_razmer]" value="0" <?php if (!$revtheme_cat_compare['compare_razmer']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group" style="border-top: none; padding: 5px 0;">	
			<label class="col-sm-2 control-label">Атрибуты:
			</label>
			<div class="col-sm-3">
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_atrib]" value="1" <?php if ($revtheme_cat_compare['compare_atrib']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_atrib]" value="0" <?php if (!$revtheme_cat_compare['compare_atrib']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group" style="border-top: none; padding: 5px 0;">	
			<label class="col-sm-2 control-label">Группы атрибутов:
			</label>
			<div class="col-sm-3">
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_atribgr]" value="1" <?php if ($revtheme_cat_compare['compare_atribgr']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="checkbox-inline">
					<input type="radio" name="revtheme_cat_compare[compare_atribgr]" value="0" <?php if (!$revtheme_cat_compare['compare_atribgr']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_cat_mods">
		<div class="form-group">
			<label class="col-sm-2 control-label">Просмотренные товары:
			</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_cat_mods[viewed_products]" value="1" <?php if ($revtheme_cat_mods['viewed_products']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_cat_mods[viewed_products]" value="0" <?php if (!$revtheme_cat_mods['viewed_products']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-3">
				<input type="text" name="revtheme_cat_mods[viewed_products_zagolovok]" value="<?php echo $revtheme_cat_mods['viewed_products_zagolovok']; ?>" class="form-control" />
			</div>
			<label class="col-sm-2 control-label">Лимит товаров:</label>
			<div class="col-sm-2">
				<input type="text" name="revtheme_cat_mods[viewed_products_limit]" value="<?php echo $revtheme_cat_mods['viewed_products_limit']; ?>" class="form-control" />
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_cat_attributes">
		<div class="form-group">
			<label class="col-sm-2 control-label">Заменить описание на свойства:
			</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" id="zavisimost5_radio" onChange="zavisimost5(this)" name="revtheme_cat_attributes[zamena_description]" value="1" <?php if ($revtheme_cat_attributes['zamena_description']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" id="zavisimost5_radio" onChange="zavisimost5(this)" name="revtheme_cat_attributes[zamena_description]" value="0" <?php if (!$revtheme_cat_attributes['zamena_description']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<hr>
		<div id="zavisimost5" style="display:none;">
			<div class="form-group">
				<label class="col-sm-2 control-label">Показывать модель:
				</label>
				<div class="col-sm-2">
					<label class="radio-inline">
						<input type="radio" name="revtheme_cat_attributes[model]" value="1" <?php if ($revtheme_cat_attributes['model']) { echo 'checked'; } ?> /> Да
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_cat_attributes[model]" value="0" <?php if (!$revtheme_cat_attributes['model']) { echo 'checked';} ?> /> Нет
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Показывать артикул (sku):
				</label>
				<div class="col-sm-2">
					<label class="radio-inline">
						<input type="radio" name="revtheme_cat_attributes[sku]" value="1" <?php if ($revtheme_cat_attributes['sku']) { echo 'checked'; } ?> /> Да
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_cat_attributes[sku]" value="0" <?php if (!$revtheme_cat_attributes['sku']) { echo 'checked';} ?> /> Нет
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Показывать наличие:
				</label>
				<div class="col-sm-2">
					<label class="radio-inline">
						<input type="radio" name="revtheme_cat_attributes[stock]" value="1" <?php if ($revtheme_cat_attributes['stock']) { echo 'checked'; } ?> /> Да
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_cat_attributes[stock]" value="0" <?php if (!$revtheme_cat_attributes['stock']) { echo 'checked';} ?> /> Нет
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Показывать вес:
				</label>
				<div class="col-sm-2">
					<label class="radio-inline">
						<input type="radio" name="revtheme_cat_attributes[weight]" value="1" <?php if ($revtheme_cat_attributes['weight']) { echo 'checked'; } ?> /> Да
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_cat_attributes[weight]" value="0" <?php if (!$revtheme_cat_attributes['weight']) { echo 'checked';} ?> /> Нет
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Показывать атрибуты:
				</label>
				<div class="col-sm-2">
					<label class="radio-inline">
						<input type="radio" id="zavisimost6_radio" onChange="zavisimost6(this)" name="revtheme_cat_attributes[attributes_status]" value="1" <?php if ($revtheme_cat_attributes['attributes_status']) { echo 'checked'; } ?> /> Да
					</label>
					<label class="radio-inline">
						<input type="radio" id="zavisimost6_radio" onChange="zavisimost6(this)" name="revtheme_cat_attributes[attributes_status]" value="0" <?php if (!$revtheme_cat_attributes['attributes_status']) { echo 'checked';} ?> /> Нет
					</label>
				</div>
			</div>
			<hr>
			<div id="zavisimost6" style="display:none;">
				<div class="form-group">
					<label class="col-sm-2 control-label">Показывать название атрибута:
					</label>
					<div class="col-sm-2">
						<label class="radio-inline">
							<input type="radio" name="revtheme_cat_attributes[show_name]" value="1" <?php if ($revtheme_cat_attributes['show_name']) { echo 'checked'; } ?> /> Да
						</label>
						<label class="radio-inline">
							<input type="radio" name="revtheme_cat_attributes[show_name]" value="0" <?php if (!$revtheme_cat_attributes['show_name']) { echo 'checked';} ?> /> Нет
						</label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Добавить теги к тексту:
					</label>
					<div class="col-sm-2">
						<label class="radio-inline">
							<input type="radio" name="revtheme_cat_attributes[show_tags]" value="1" <?php if ($revtheme_cat_attributes['show_tags']) { echo 'checked'; } ?> /> Да
						</label>
						<label class="radio-inline">
							<input type="radio" name="revtheme_cat_attributes[show_tags]" value="0" <?php if (!$revtheme_cat_attributes['show_tags']) { echo 'checked';} ?> /> Нет
						</label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Показывать количество атрибутов, 0 - показать все:
					</label>
					<div class="col-sm-2">
						<input type="text" name="revtheme_cat_attributes[count]" value="<?php echo $revtheme_cat_attributes['count']; ?>" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Разделитель атрибутов:
					</label>
					<div class="col-sm-2">
						<input type="text" name="revtheme_cat_attributes[separator]" value="<?php echo $revtheme_cat_attributes['separator']; ?>" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<td class="left">Группа</td>
								<td class="left">Атрибут</td>
								<td class="left">Показывать</td>
								<td class="left">Заменить значение атрибута на название</td>
							</tr>
						</thead>
						<tbody>
							<?php foreach($attributes as $attribute) {?>
								<tr>
									<td class="left"><?php echo $attribute['attribute_group']; ?></td>              
									<td class="left"><?php echo $attribute['name']; ?></td>              
									<td>
										<select class="form-control" name="revtheme_cat_attributes[attributes][<?php echo $attribute['attribute_id']; ?>][show]">
											<option value="0" <?php echo (isset($revtheme_cat_attributes['attributes'][$attribute['attribute_id']]['show']) && $revtheme_cat_attributes['attributes'][$attribute['attribute_id']]['show'] == '0') ? 'selected="selected"' : "" ;?>>Не показывать</option>
											<option value="1" <?php echo (isset($revtheme_cat_attributes['attributes'][$attribute['attribute_id']]['show']) && $revtheme_cat_attributes['attributes'][$attribute['attribute_id']]['show'] == '1') ? 'selected="selected"' : "" ;?>>Показывать</option>
											<option value="2" <?php echo (isset($revtheme_cat_attributes['attributes'][$attribute['attribute_id']]['show']) && $revtheme_cat_attributes['attributes'][$attribute['attribute_id']]['show'] == '2') ? 'selected="selected"' : "" ;?>>С заменой</option>
										</select>
									</td>
									<td class="left">
										<input type="text" class="form-control" name="revtheme_cat_attributes[attributes][<?php echo $attribute['attribute_id']; ?>][replace]" value="<?php echo isset($revtheme_cat_attributes['attributes'][$attribute['attribute_id']]['replace']) ? $revtheme_cat_attributes['attributes'][$attribute['attribute_id']]['replace'] : '';?>">
									</td>  
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!--
	<div class="tab-pane text-style" id="tab_cat_filter">
		<div class="form-group">
			<label class="col-sm-2 control-label">Включить:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_filter[status]" value="1" <?php if ($revtheme_catalog_filter['status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_catalog_filter[status]" value="0" <?php if (!$revtheme_catalog_filter['status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<div class="well well-sm" style="height: 150px; overflow: auto;">
				<?php foreach ($categories as $category) { ?>
					<div class="checkbox">
						<label>
						<?php if (in_array($category['category_id'], $revtheme_catalog_filter_categories)) { ?>
							<input type="checkbox" name="revtheme_catalog_filter_categories[]" value="<?php echo $category['category_id']; ?>" checked="checked" />
							<?php echo $category['name']; ?>
						<?php } else { ?>
							<input type="checkbox" name="revtheme_catalog_filter_categories[]" value="<?php echo $category['category_id']; ?>" />
							<?php echo $category['name']; ?>
						<?php } ?>
						</label>
					</div>
				<?php } ?>
			</div>
				<a onclick="selectAll(this);">Выбрать все</a> / <a onclick="deselectAll(this);">Снять выделения</a>
				<script type="text/javascript">
				function selectAll(element) {
					$(element).parent().find(':checkbox').attr('checked', true);
				}
				function deselectAll(element) {
					$(element).parent().find(':checkbox').attr('checked', false);
				}
				</script>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Налоги:</label>
			<div class="col-sm-3">
			  <input type="text" name="revtheme_catalog_filter[tax]" value="<?php echo $revtheme_catalog_filter['tax']; ?>" class="form-control" />
			</div>
		</div>
		<div class="form-group">
            <label class="col-sm-2 control-label" for="input-banner">Метод фильтрации:</label>
            <div class="col-sm-3">
              <select name="revtheme_catalog_filter[metod]" id="input-banner" class="form-control">
                <?php if ($revtheme_catalog_filter['metod'] == 'and') { ?>
                <option value="and" selected="selected">Только И</option>
				<option value="andor">И и ИЛИ</option>
                <?php } else { ?>
                <option value="and">Только И</option>
				<option value="andor" selected="selected">И и ИЛИ</option>
                <?php } ?>
              </select>
            </div>
        </div>
	</div>
	-->
</div>
</div>
<div class="tab-pane" id="tab_product">
<div class="col-sm-2">
		<nav class="nav-sidebar">
			<ul class="nav tabs">
				<li class="active"><a href="#tab_product_all" data-toggle="tab">Общие настройки</a></li>
				<li><a href="#tab_product_images" data-toggle="tab">Изображения</a></li>
				<li><a href="#tab_product_mods" data-toggle="tab">Модули</a></li>
			</ul>
		</nav>
	</div>	
	<div class="tab-content col-sm-10">
		<div class="tab-pane active text-style" id="tab_product_all">
			<div class="form-group">
			<label class="col-sm-2 control-label">Быстрый заказ в карточке товара:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[popup_purchase]" value="1" <?php if ($revtheme_product_all['popup_purchase']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[popup_purchase]" value="0" <?php if (!$revtheme_product_all['popup_purchase']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label">Возможность заказа при 0 кол-ве товара:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[zakaz]" value="1" <?php if ($revtheme_product_all['zakaz']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[zakaz]" value="0" <?php if (!$revtheme_product_all['zakaz']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label">Показывать счетчик до конца акции:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[countdown]" value="1" <?php if ($revtheme_product_all['countdown']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[countdown]" value="0" <?php if (!$revtheme_product_all['countdown']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label">Характеристики:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" id="atributs_zavisimost_radio" onChange="atributs_zavisimost(this)" name="revtheme_product_all[atributs]" value="1" <?php if ($revtheme_product_all['atributs']) { echo 'checked'; } ?> /> Справа от картинки
				</label>
				<label class="radio-inline">
					<input type="radio" id="atributs_zavisimost_radio" onChange="atributs_zavisimost(this)" name="revtheme_product_all[atributs]" value="0" <?php if (!$revtheme_product_all['atributs']) { echo 'checked';} ?> /> Во вкладке
				</label>
			</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label">Название группы в характеристиках:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[atributs_group_name]" value="1" <?php if ($revtheme_product_all['atributs_group_name']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[atributs_group_name]" value="0" <?php if (!$revtheme_product_all['atributs_group_name']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
			</div>	
			<hr>
			<div id="atributs_zavisimost" style="display:none;">
				<div class="form-group">
					<label class="col-sm-2 control-label">Группа атрибутов в кратком описании:</label>
					<div class="col-sm-4">
					<div class="well well-sm" style="height: 150px; overflow: auto; margin-bottom: 5px;">
						<?php foreach ($attribute_groups as $attribute_group) { ?>
							<div class="checkbox">
								<label>
								<?php if (in_array($attribute_group['attribute_group_id'], $revtheme_product_all_attribute_group)) { ?>
									<input type="checkbox" name="revtheme_product_all_attribute_group[]" value="<?php echo $attribute_group['attribute_group_id']; ?>" checked="checked" />
									<?php echo $attribute_group['name']; ?>
								<?php } else { ?>
									<input type="checkbox" name="revtheme_product_all_attribute_group[]" value="<?php echo $attribute_group['attribute_group_id']; ?>" />
									<?php echo $attribute_group['name']; ?>
								<?php } ?>
								</label>
							</div>
						<?php } ?>
					</div>
					<a onclick="selectAll2(this);">Выбрать все</a> / <a onclick="deselectAll2(this);">Снять выделения</a>
					<script type="text/javascript"><!--
					function selectAll2(element) {
						$(element).parent().find(':checkbox').attr('checked', true);
					}
					function deselectAll2(element) {
						$(element).parent().find(':checkbox').attr('checked', false);
					}
					--></script>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Выводить ссылку "Показать все характеристики":
					</label>
					<div class="col-sm-3">
						<label class="radio-inline">
							<input type="radio" name="revtheme_product_all[atributs_ssilka_all]" value="1" <?php if ($revtheme_product_all['atributs_ssilka_all']) { echo 'checked'; } ?> /> Вкл.
						</label>
						<label class="radio-inline">
							<input type="radio" name="revtheme_product_all[atributs_ssilka_all]" value="0" <?php if (!$revtheme_product_all['atributs_ssilka_all']) { echo 'checked';} ?> /> Выкл.
						</label>
					</div>
				</div>
				<hr>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label">Выводить производителя:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[manufacturer_status]" value="1" <?php if ($revtheme_product_all['manufacturer_status']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[manufacturer_status]" value="0" <?php if (!$revtheme_product_all['manufacturer_status']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label">Выводить модель (model):
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[model_status]" value="1" <?php if ($revtheme_product_all['model_status']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[model_status]" value="0" <?php if (!$revtheme_product_all['model_status']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label">Выводить артикул (sku):
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[sku_status]" value="1" <?php if ($revtheme_product_all['sku_status']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[sku_status]" value="0" <?php if (!$revtheme_product_all['sku_status']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label">Выводить бонусные балы:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[bonusbals_status]" value="1" <?php if ($revtheme_product_all['bonusbals_status']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[bonusbals_status]" value="0" <?php if (!$revtheme_product_all['bonusbals_status']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label">Выводить статус на складе:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[sklad_status]" value="1" <?php if ($revtheme_product_all['sklad_status']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[sklad_status]" value="0" <?php if (!$revtheme_product_all['sklad_status']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label">Выводить остаток товара:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[ostatok_status]" value="1" <?php if ($revtheme_product_all['ostatok_status']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[ostatok_status]" value="0" <?php if (!$revtheme_product_all['ostatok_status']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label">Выводить вес товара:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[weight_status]" value="1" <?php if ($revtheme_product_all['weight_status']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[weight_status]" value="0" <?php if (!$revtheme_product_all['weight_status']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label">Выводить размеры товара:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[razmers]" value="1" <?php if ($revtheme_product_all['razmers']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[razmers]" value="0" <?php if (!$revtheme_product_all['razmers']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label">Выводить соц. кнопки:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[share_status]" value="1" <?php if ($revtheme_product_all['share_status']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_all[share_status]" value="0" <?php if (!$revtheme_product_all['share_status']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
			</div>
		</div>
		<div class="tab-pane text-style" id="tab_product_images">
			<div class="form-group">
			<label class="col-sm-2 control-label">Зум (увеличение изображения):
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_images[zoom]" value="1" <?php if ($revtheme_product_images['zoom']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_images[zoom]" value="0" <?php if (!$revtheme_product_images['zoom']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label">Слайдер доп. изображений:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_images[slider]" value="1" <?php if ($revtheme_product_images['slider']) { echo 'checked'; } ?> /> Вкл.
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_product_images[slider]" value="0" <?php if (!$revtheme_product_images['slider']) { echo 'checked';} ?> /> Выкл.
				</label>
			</div>
			</div>
		</div>
		<div class="tab-pane text-style" id="tab_product_mods">
			<div class="form-group">
				<label class="col-sm-2 control-label">1) Просмотренные товары:
				</label>
				<div class="col-sm-2">
					<label class="radio-inline">
						<input type="radio" name="revtheme_product_mods[viewed_products]" value="1" <?php if ($revtheme_product_mods['viewed_products']) { echo 'checked'; } ?> /> Вкл.
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_product_mods[viewed_products]" value="0" <?php if (!$revtheme_product_mods['viewed_products']) { echo 'checked';} ?> /> Выкл.
					</label>
				</div>
			</div>
			<div class="form-group" style="border-top: none;">
				<label class="col-sm-2 control-label">Заголовок:</label>
				<div class="col-sm-3">
					<input type="text" name="revtheme_product_mods[viewed_products_zagolovok]" value="<?php echo $revtheme_product_mods['viewed_products_zagolovok']; ?>" class="form-control" />
				</div>
				<label class="col-sm-2 control-label">Лимит товаров:</label>
				<div class="col-sm-2">
					<input type="text" name="revtheme_product_mods[viewed_products_limit]" value="<?php echo $revtheme_product_mods['viewed_products_limit']; ?>" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">2) Текстовые блоки:
				</label>
				<div class="col-sm-2">
					<label class="radio-inline">
						<input type="radio" name="revtheme_product_mods[text_block]" value="1" <?php if ($revtheme_product_mods['text_block']) { echo 'checked'; } ?> /> Вкл.
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_product_mods[text_block]" value="0" <?php if (!$revtheme_product_mods['text_block']) { echo 'checked';} ?> /> Выкл.
					</label>
				</div>
				<label class="col-sm-2 control-label">Заголовок:</label>
				<div class="col-sm-3">
					<input type="text" name="revtheme_product_mods[text_block_zagolovok]" value="<?php echo $revtheme_product_mods['text_block_zagolovok']; ?>" class="form-control" />
				</div>
			</div>
			<div class="form-group"  style="border-top: none;">
				<label class="col-sm-2 control-label" for="input-text_block_cols">Блоков в ряд:</label>
				<div class="col-sm-1">
				  <select name="revtheme_product_mods[text_block_cols]" id="input-text_block_cols" class="form-control">
					<?php if ($revtheme_product_mods['text_block_cols'] == 'col-sm-12') { ?>
					<option value="col-sm-12" selected="selected">1</option>
					<option value="col-sm-6">2</option>
					<option value="col-sm-4">3</option>
					<option value="col-sm-1">12</option>
					<?php } elseif ($revtheme_product_mods['text_block_cols'] == 'col-sm-6') { ?>
					<option value="col-sm-12">1</option>
					<option value="col-sm-6" selected="selected">2</option>
					<option value="col-sm-4">3</option>
					<option value="col-sm-1">12</option>
					<?php } elseif ($revtheme_product_mods['text_block_cols'] == 'col-sm-4') { ?>
					<option value="col-sm-12">1</option>
					<option value="col-sm-6">2</option>
					<option value="col-sm-4" selected="selected">3</option>
					<option value="col-sm-1">12</option>
					<?php } else { ?>
					<option value="col-sm-12">1</option>
					<option value="col-sm-6">2</option>
					<option value="col-sm-4">3</option>
					<option value="col-sm-1" selected="selected">12</option>
					<?php } ?>
				  </select>
				</div>
			</div>
			<div class="form-group" style="border-top: none;">
				<div class="col-sm-12">
					<table id="t_product_blocks" class="table table-bordered">
						<thead>
							<tr>
								<td class="nowrap">Иконка</td>
								<td class="nowrap">Заголовок</td>
								<td class="nowrap">Текст</td>
								<td class="nowrap">Ссылка</td>
								<td class="nowrap" style="width:10%">Сортировка</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
						<?php $item_row_product_main = 10; ?>
						<?php foreach ($revtheme_blocks_product_items as $revtheme_blocks_product_item) { ?>
							<tr id="item-row-product-main<?php echo $item_row_product_main; ?>" class="item_row_product_main">						
								<td class="text-center">
									<span class="fa_icon" id="icon_block_product_<?php echo $item_row_product_main; ?>" onclick="fa_icons($(this).attr('id'))"><i class="<?php echo $revtheme_blocks_product_item['image']; ?>"></i></span>
									<input type="hidden" name="revtheme_blocks_product_item[<?php echo $item_row_product_main; ?>][image]" value="<?php echo $revtheme_blocks_product_item['image']; ?>" id="input-block-image<?php echo $item_row_product_main; ?>" />
								</td>		
								<td class="text-left">
									<input class="form-control" type="text" name="revtheme_blocks_product_item[<?php echo $item_row_product_main; ?>][title]" value="<?php echo $revtheme_blocks_product_item['title']; ?>" />
								</td>
								<td class="text-left">
									<input class="form-control" type="text" name="revtheme_blocks_product_item[<?php echo $item_row_product_main; ?>][description]" value="<?php echo $revtheme_blocks_product_item['description']; ?>" />
								</td>
								<td class="text-left">
									<input class="form-control" type="text" name="revtheme_blocks_product_item[<?php echo $item_row_product_main; ?>][link]" value="<?php echo $revtheme_blocks_product_item['link']; ?>" />
								</td>
								<td class="text-left">
									<input class="form-control" type="text" name="revtheme_blocks_product_item[<?php echo $item_row_product_main; ?>][sort]" value="<?php echo $revtheme_blocks_product_item['sort']; ?>" />
								</td>
								<td class="text-right">
									<a class="btn btn-danger" onclick="$('#t_product_blocks #item-row-product-main<?php echo $item_row_product_main; ?>').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
						<?php $item_row_product_main++; ?>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="5"></td>
								<td class="text-right"><a class="btn btn-primary" onclick="addBlockProductItem();" data-toggle="tooltip" title="Добавить"><i class="fa fa-plus-circle"></i></a></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="tab-pane" id="tab_footer">
	<div class="col-sm-2">
		<nav class="nav-sidebar">
			<ul class="nav tabs">
				<li class="active"><a href="#tab_footer_all" data-toggle="tab">Общие настройки</a></li>
				<li><a href="#tab_subscribe" data-toggle="tab">Подписка на новости</a></li>
				<li><a href="#tab_footer_links" data-toggle="tab">Доп. ссылки в подвале</a></li>
				<li><a href="#tab_soc" data-toggle="tab">Иконки социальных сетей</a></li>
				<li><a href="#tab_icons" data-toggle="tab">Иконки платежных систем</a></li>
			</ul>
		</nav>
	</div>
	<div class="tab-content col-sm-10">
		<div class="tab-pane active text-style" id="tab_footer_all">
			<div class="form-group">
				<label class="col-sm-2 control-label">Кнопка "Вверх":
				</label>
				<div class="col-sm-3">
					<label class="radio-inline">
						<input type="radio" name="revtheme_footer_all[in_top]" value="1" <?php if ($revtheme_footer_all['in_top']) { echo 'checked'; } ?> /> Вкл.
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_footer_all[in_top]" value="0" <?php if (!$revtheme_footer_all['in_top']) { echo 'checked';} ?> /> Выкл.
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Кнопка "Заказать звонок":
				</label>
				<div class="col-sm-3">
					<label class="radio-inline">
						<input type="radio" name="revtheme_footer_all[popup_phone]" value="1" <?php if ($revtheme_footer_all['popup_phone']) { echo 'checked'; } ?> /> Вкл.
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_footer_all[popup_phone]" value="0" <?php if (!$revtheme_footer_all['popup_phone']) { echo 'checked';} ?> /> Выкл.
					</label>
				</div>
			</div>
		</div>
		<div class="tab-pane text-style" id="tab_subscribe">
			<div class="form-group">
				<label class="col-sm-2 control-label">Включить:
				</label>
				<div class="col-sm-3">
					<label class="radio-inline">
						<input type="radio" name="revtheme_footer_subscribe[status]" value="1" <?php if ($revtheme_footer_subscribe['status']) { echo 'checked'; } ?> /> Да
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_footer_subscribe[status]" value="0" <?php if (!$revtheme_footer_subscribe['status']) { echo 'checked';} ?> /> Нет
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Заголовок:</label>
				<div class="col-sm-3">
				  <input type="text" name="revtheme_footer_subscribe[title]" value="<?php echo $revtheme_footer_subscribe['title']; ?>" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Текст:</label>
				<div class="col-sm-6">
				  <input type="text" name="revtheme_footer_subscribe[text]" value="<?php echo $revtheme_footer_subscribe['text']; ?>" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Текст при успешной подписке:</label>
				<div class="col-sm-10">
				  <input type="text" name="revtheme_footer_subscribe[text_uspeh]" value="<?php echo $revtheme_footer_subscribe['text_uspeh']; ?>" class="form-control" />
				</div>
			</div>
		</div>
		<div class="tab-pane text-style" id="tab_footer_links">
			<div class="form-group">
				<div class="col-sm-12">
					<table id="t_footer_links" class="table table-bordered">
						<thead>
							<tr>
								<td class="nowrap">Ссылка</td>
								<td class="nowrap">Заголовок</td>
								<td class="nowrap">Сортировка</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
						<?php $item_row_footer_link = 1; ?>
						<?php foreach ($revtheme_footer_links as $revtheme_footer_link) { ?>
							<tr id="item-row-main<?php echo $item_row_footer_link; ?>" class="item_row_footer_link">							
								<td class="text-left">
										<input class="form-control" type="text" name="revtheme_footer_link[<?php echo $item_row_footer_link; ?>][link]" value="<?php echo $revtheme_footer_link['link']; ?>" />
								</td>
								<td class="text-left">
										<input class="form-control" type="text" name="revtheme_footer_link[<?php echo $item_row_footer_link; ?>][title]" value="<?php echo $revtheme_footer_link['title']; ?>" />
								</td>
								<td class="text-left">
									<input class="form-control" type="text" name="revtheme_footer_link[<?php echo $item_row_footer_link; ?>][sort]" value="<?php echo $revtheme_footer_link['sort']; ?>" />
								</td>
								<td class="text-right">
									<a class="btn btn-danger" onclick="$('#t_footer_links #item-row-main<?php echo $item_row_footer_link; ?>').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
						<?php $item_row_footer_link++; ?>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3"></td>
								<td class="text-right"><a class="btn btn-primary" onclick="addBlockFooterLink();" data-toggle="tooltip" title="Добавить"><i class="fa fa-plus-circle"></i></a></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane text-style" id="tab_soc">
			<div class="form-group">
				<div class="col-sm-12">
					<table id="t_footer_socs" class="table table-bordered">
						<thead>
							<tr>
								<td class="nowrap">Иконка</td>
								<td class="nowrap">Ссылка</td>
								<td class="nowrap">Заголовок</td>
								<td class="nowrap">Сортировка</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
						<?php $item_row_main_soc = 1; ?>
						<?php foreach ($revtheme_footer_socs as $revtheme_footer_soc) { ?>
							<tr id="item-row-main<?php echo $item_row_main_soc; ?>" class="item_row_main_soc">						
								<td class="text-center">
									<span class="fa_icon" id="icon_banner_<?php echo $item_row_main_soc; ?>" onclick="fa_icons($(this).attr('id'))"><i class="<?php echo $revtheme_footer_soc['image']; ?>"></i></span>
									<input type="hidden" name="revtheme_footer_soc[<?php echo $item_row_main_soc; ?>][image]" value="<?php echo $revtheme_footer_soc['image']; ?>" id="input-soc-image<?php echo $item_row_main_soc; ?>" />
								</td>		
								<td class="text-left">
										<input class="form-control" type="text" name="revtheme_footer_soc[<?php echo $item_row_main_soc; ?>][link]" value="<?php echo $revtheme_footer_soc['link']; ?>" />
								</td>
								<td class="text-left">
										<input class="form-control" type="text" name="revtheme_footer_soc[<?php echo $item_row_main_soc; ?>][title]" value="<?php echo $revtheme_footer_soc['title']; ?>" />
								</td>
								<td class="text-left">
									<input class="form-control" type="text" name="revtheme_footer_soc[<?php echo $item_row_main_soc; ?>][sort]" value="<?php echo $revtheme_footer_soc['sort']; ?>" />
								</td>
								<td class="text-right">
									<a class="btn btn-danger" onclick="$('#t_footer_socs #item-row-main<?php echo $item_row_main_soc; ?>').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
						<?php $item_row_main_soc++; ?>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4"></td>
								<td class="text-right"><a class="btn btn-primary" onclick="addBlockItemSoc();" data-toggle="tooltip" title="Добавить"><i class="fa fa-plus-circle"></i></a></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane text-style" id="tab_icons">
			<div class="form-group">
				<label class="col-sm-2 control-label">Иконки:</label>
				<div class="col-sm-4">
					<table id="t_footer_icons" class="table table-bordered">
						<thead>
							<tr>
								<td class="nowrap"><i class="fa fa-edit fs14"></i>&nbsp;&nbsp;Иконка</td>
								<td class="nowrap"><i class="fa fa-sort fs14"></i>&nbsp;&nbsp;Сортировка</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
						<?php $item_row_main_icon = 1; ?>
						<?php foreach ($revtheme_footer_icons as $revtheme_footer_icon) { ?>
							<tr id="item-row-main<?php echo $item_row_main_icon; ?>" class="item_row_main_icon">
								<td class="text-center">
									<a href="" id="thumb-image<?php echo $item_row_main_icon; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $revtheme_footer_icon['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>"  /></a><input type="hidden" name="revtheme_footer_icon[<?php echo $item_row_main_icon; ?>][image]" value="<?php echo $revtheme_footer_icon['image']; ?>" id="input-icon-image<?php echo $item_row_main_icon; ?>" />
								</td>
								<td class="text-left">
									<input  class="form-control" type="text" name="revtheme_footer_icon[<?php echo $item_row_main_icon; ?>][sort]" value="<?php echo $revtheme_footer_icon['sort']; ?>" />
								</td>
								<td class="text-right">
									<a class="btn btn-danger" onclick="$('#t_footer_icons #item-row-main<?php echo $item_row_main_icon; ?>').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
						<?php $item_row_main_icon++; ?>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="2"></td>
								<td class="text-right"><a class="btn btn-primary" onclick="addBlockItemIcon();" data-toggle="tooltip" title="Добавить"><i class="fa fa-plus-circle"></i></a></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>	
</div>
<div class="tab-pane" id="tab_home">
	<div class="col-sm-2">
    <nav class="nav-sidebar">
		<ul class="nav tabs">
			<li class="active"><a href="#tab_home_all" data-toggle="tab">Общие настройки</a></li>
			<li><a href="#tab_slideshow" data-toggle="tab">Слайдшоу</a></li>
			<li><a href="#tab_blocks" data-toggle="tab">Текстовые блоки</a></li>
			<li><a href="#tab_sliders_mod" data-toggle="tab">Слайдеры: Последние, Хиты продаж, Спецпредложения</a></li>
			<li><a href="#tab_slider1" data-toggle="tab">Слайдер товаров 1</a></li>
			<li><a href="#tab_slider2" data-toggle="tab">Слайдер товаров 2</a></li>
			<li><a href="#tab_slider3" data-toggle="tab">Слайдер товаров 3</a></li>
			<li><a href="#tab_slider4" data-toggle="tab">Слайдер товаров 4</a></li>
			<li><a href="#tab_slider5" data-toggle="tab">Слайдер товаров 5</a></li>
			<li><a href="#tab_blog" data-toggle="tab">Виджет новостей</a></li>
			<li><a href="#tab_aboutstore" data-toggle="tab">О магазине</a></li>
			<li><a href="#tab_socv" data-toggle="tab">Виджет группы VK</a></li>
			<li><a href="#tab_storereview" data-toggle="tab">Отзывы о магазине</a></li>
			<li><a href="#tab_viewed_products" data-toggle="tab">Просмотренные товары</a></li>
		</ul>
	</nav>
	</div>
<div class="tab-content col-sm-10">
	<div class="tab-pane active text-style" id="tab_home_all">
		<div class="form-group">
            <label class="col-sm-2 control-label" for="input-width">Заголовок H1 на главной:</label>
            <div class="col-sm-3">
              <input type="text" name="revtheme_home_all[h1_home]" value="<?php echo $revtheme_home_all['h1_home']; ?>" id="input-width" class="form-control" />
            </div>
        </div>
	</div>		
	<div class="tab-pane text-style" id="tab_slideshow">
		<div class="form-group">
			<label class="col-sm-2 control-label">Включить:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_home_slideshow[status]" value="1" <?php if ($revtheme_home_slideshow['status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_home_slideshow[status]" value="0" <?php if (!$revtheme_home_slideshow['status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="input-banner">Баннеры:</label>
            <div class="col-sm-3">
				<input type="hidden" name="revtheme_home_slideshow[banner_id]" value="" />
				<select name="revtheme_home_slideshow[banner_id]" id="input-banner" class="form-control">
				<?php if ($banners) { ?>
					<?php foreach ($banners as $banner) { ?>
						<?php if ($banner['banner_id'] == $revtheme_home_slideshow['banner_id']) { ?>
							<option value="<?php echo $banner['banner_id']; ?>" selected="selected"><?php echo $banner['name']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $banner['banner_id']; ?>"><?php echo $banner['name']; ?></option>
						<?php } ?>
					<?php } ?>
				<?php } ?>
				</select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="input-width">Ширина:</label>
            <div class="col-sm-3">
              <input type="text" name="revtheme_home_slideshow[width]" value="<?php echo $revtheme_home_slideshow['width']; ?>" placeholder="Ширина" id="input-width" class="form-control" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="input-height">Высота:</label>
            <div class="col-sm-3">
              <input type="text" name="revtheme_home_slideshow[height]" value="<?php echo $revtheme_home_slideshow['height']; ?>" placeholder="Высота" id="input-height" class="form-control" />
            </div>
        </div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Цвет фона:</label>
			<div class="col-sm-3">
				<input class="jscolor form-control" name="revtheme_home_slideshow[b_color]" value="<?php echo isset($revtheme_home_slideshow['b_color']) ? $revtheme_home_slideshow['b_color'] : ''; ?>" />
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_blocks">
		<div class="form-group">
			<label class="col-sm-2 control-label">Включить:
			</label>
			<div class="col-sm-10">
				<label class="radio-inline">
					<input type="radio" name="revtheme_blocks_home[status]" value="1" <?php if ($revtheme_blocks_home['status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_blocks_home[status]" value="0" <?php if (!$revtheme_blocks_home['status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<table id="t_home_blocks" class="table table-bordered">
					<thead>
						<tr>
							<td class="nowrap">Иконка</td>
							<td class="nowrap">Заголовок</td>
							<td class="nowrap">Текст</td>
							<td class="nowrap">Ссылка</td>
							<td class="nowrap" style="width:10%">Сортировка</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					<?php $item_row_main = 10; ?>
					<?php foreach ($revtheme_blocks_home_items as $revtheme_blocks_home_item) { ?>
						<tr id="item-row-main<?php echo $item_row_main; ?>" class="item_row_main">						
							<td class="text-center">
								<span class="fa_icon" id="icon_banner_<?php echo $item_row_main; ?>" onclick="fa_icons($(this).attr('id'))"><i class="<?php echo $revtheme_blocks_home_item['image']; ?>"></i></span>
								<input type="hidden" name="revtheme_blocks_home_item[<?php echo $item_row_main; ?>][image]" value="<?php echo $revtheme_blocks_home_item['image']; ?>" id="input-block-image<?php echo $item_row_main; ?>" />
							</td>		
							<td class="text-left">
								<input class="form-control" type="text" name="revtheme_blocks_home_item[<?php echo $item_row_main; ?>][title]" value="<?php echo $revtheme_blocks_home_item['title']; ?>" />
							</td>
							<td class="text-left">
								<input class="form-control" type="text" name="revtheme_blocks_home_item[<?php echo $item_row_main; ?>][description]" value="<?php echo $revtheme_blocks_home_item['description']; ?>" />
							</td>
							<td class="text-left">
								<input class="form-control" type="text" name="revtheme_blocks_home_item[<?php echo $item_row_main; ?>][link]" value="<?php echo $revtheme_blocks_home_item['link']; ?>" />
							</td>
							<td class="text-left">
								<input class="form-control" type="text" name="revtheme_blocks_home_item[<?php echo $item_row_main; ?>][sort]" value="<?php echo $revtheme_blocks_home_item['sort']; ?>" />
							</td>
							<td class="text-right">
								<a class="btn btn-danger" onclick="$('#t_home_blocks #item-row-main<?php echo $item_row_main; ?>').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
					<?php $item_row_main++; ?>
					<?php } ?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="5"></td>
							<td class="text-right"><a class="btn btn-primary" onclick="addBlockItem();" data-toggle="tooltip" title="Добавить"><i class="fa fa-plus-circle"></i></a></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_sliders_mod">
		<div class="form-group">
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-sm-2 control-label">Слайдер последних товаров:
					</label>
					<div class="col-sm-2">
						<label class="radio-inline">
							<input type="radio" name="revtheme_home_last[status]" value="1" <?php if ($revtheme_home_last['status']) { echo 'checked'; } ?> /> Включен
						</label>
						<label class="radio-inline">
							<input type="radio" name="revtheme_home_last[status]" value="0" <?php if (!$revtheme_home_last['status']) { echo 'checked';} ?> /> Выключен
						</label>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-sm-2 control-label">Заголовок:</label>
					<div class="col-sm-3">
							<input class="form-control" type="text" name="revtheme_home_last[title]" value="<?php echo $revtheme_home_last['title']; ?>" />
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-sm-2 control-label">Лимит товаров:</label>
					<div class="col-sm-2">
							<input class="form-control width_initial" type="text" name="revtheme_home_last[limit]" value="<?php echo $revtheme_home_last['limit']; ?>" />
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-sm-2 control-label">Изображение (Ш x В):</label>
					<div class="col-sm-4">
						<input class="form-control width_initial" type="text" name="revtheme_home_last[image_width]" value="<?php echo $revtheme_home_last['image_width']; ?>" /> x 
						<input class="form-control width_initial" type="text" name="revtheme_home_last[image_height]" value="<?php echo $revtheme_home_last['image_height']; ?>" /> px
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="form-group">
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-sm-2 control-label">Слайдер хитов продаж:
					</label>
					<div class="col-sm-2">
						<label class="radio-inline">
							<input type="radio" name="revtheme_home_best[status]" value="1" <?php if ($revtheme_home_best['status']) { echo 'checked'; } ?> /> Включен
						</label>
						<label class="radio-inline">
							<input type="radio" name="revtheme_home_best[status]" value="0" <?php if (!$revtheme_home_best['status']) { echo 'checked';} ?> /> Выключен
						</label>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-sm-2 control-label">Заголовок:</label>
					<div class="col-sm-3">
							<input class="form-control" type="text" name="revtheme_home_best[title]" value="<?php echo $revtheme_home_best['title']; ?>" />
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-sm-2 control-label">Лимит товаров:</label>
					<div class="col-sm-2">
							<input class="form-control width_initial" type="text" name="revtheme_home_best[limit]" value="<?php echo $revtheme_home_best['limit']; ?>" />
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-sm-2 control-label">Изображение (Ш x В):</label>
					<div class="col-sm-4">
						<input class="form-control width_initial" type="text" name="revtheme_home_best[image_width]" value="<?php echo $revtheme_home_best['image_width']; ?>" /> x 
						<input class="form-control width_initial" type="text" name="revtheme_home_best[image_height]" value="<?php echo $revtheme_home_best['image_height']; ?>" /> px
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="form-group">
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-sm-2 control-label">Слайдер спецпредложений:
					</label>
					<div class="col-sm-2">
						<label class="radio-inline">
							<input type="radio" name="revtheme_home_spec[status]" value="1" <?php if ($revtheme_home_spec['status']) { echo 'checked'; } ?> /> Включен
						</label>
						<label class="radio-inline">
							<input type="radio" name="revtheme_home_spec[status]" value="0" <?php if (!$revtheme_home_spec['status']) { echo 'checked';} ?> /> Выключен
						</label>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-sm-2 control-label">Заголовок:</label>
					<div class="col-sm-3">
							<input class="form-control" type="text" name="revtheme_home_spec[title]" value="<?php echo $revtheme_home_spec['title']; ?>" />
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-sm-2 control-label">Лимит товаров:</label>
					<div class="col-sm-2">
							<input class="form-control width_initial" type="text" name="revtheme_home_spec[limit]" value="<?php echo $revtheme_home_spec['limit']; ?>" />
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-sm-2 control-label">Изображение (Ш x В):</label>
					<div class="col-sm-4">
						<input class="form-control width_initial" type="text" name="revtheme_home_spec[image_width]" value="<?php echo $revtheme_home_spec['image_width']; ?>" /> x 
						<input class="form-control width_initial" type="text" name="revtheme_home_spec[image_height]" value="<?php echo $revtheme_home_spec['image_height']; ?>" /> px
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_slider1">
		<div class="form-group">
			<label class="col-sm-2 control-label">Включен:
			</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_slider_1[status]" value="1" <?php if ($revtheme_slider_1['status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_slider_1[status]" value="0" <?php if (!$revtheme_slider_1['status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-3">
					<input class="form-control" type="text" name="revtheme_slider_1[title]" value="<?php echo $revtheme_slider_1['title']; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Иконка:</label>
			<div class="col-sm-3">
					<span style="cursor:pointer" class="fa_icon form-control width_initial" id="icon_slider_1" onclick="fa_icons($(this).attr('id'))"><i class="<?php echo $revtheme_slider_1['icon']; ?>"></i></span>
					<input type="hidden" name="revtheme_slider_1[icon]" value="<?php echo $revtheme_slider_1['icon']; ?>" id="input-icon_slider1" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Лимит товаров:</label>
			<div class="col-sm-2">
					<input class="form-control width_initial" type="text" name="revtheme_slider_1[count]" value="<?php echo $revtheme_slider_1['count']; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Изображение (Ш x В):</label>
			<div class="col-sm-4">
				<input class="form-control width_initial" type="text" name="revtheme_slider_1[image_width]" value="<?php echo $revtheme_slider_1['image_width']; ?>" /> x 
				<input class="form-control width_initial" type="text" name="revtheme_slider_1[image_height]" value="<?php echo $revtheme_slider_1['image_height']; ?>" /> px
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Источник товаров:</label>
			<div class="col-sm-4">
				<select name="revtheme_slider_1[category_id]" id="select" onchange="showhide();" class="form-control">
					<option value="0" <?php if ($revtheme_slider_1['category_id']=='0') { ?>selected="selected"<?php } ?>>Все товары</option>
					<option value="featured" <?php if ($revtheme_slider_1['category_id']=='featured') { ?>selected="selected"<?php } ?>>Выборочные товары</option>
				<?php foreach ($rootcats as $rootcat) { ?>
					<?php if ($rootcat['category_id'] == $revtheme_slider_1['category_id']) { ?>
						<option value="<?php echo $rootcat['category_id']; ?>" selected="selected"><?php echo $rootcat['name']; ?></option>
					<?php } else { ?>
						<option value="<?php echo $rootcat['category_id']; ?>"><?php echo $rootcat['name']; ?></option>
					<?php } ?>
				<?php } ?>
				</select>
			</div>
		</div>
		<?php if ($revtheme_slider_1['category_id']=='featured') { $featured_style="block";} else {$featured_style="none";}?>
		<div class="form-group" id="rowfeatured" style="display:<?php echo $featured_style; ?>;">
			<label class="col-sm-2 control-label">Товары (автозаполнение):</label>
			<div class="col-sm-4">
				<input type="text" name="product" value="" placeholder="Товары" id="input-product" class="form-control" />
				<br/>
				<div class="scrollbox well well-sm" id="featured-product" style="height: 150px; overflow: auto;">
					<?php $class = 'odd'; ?>
					<?php foreach ($products as $product) { ?>
						<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
						<div id="featured-product<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>"><?php echo $product['name']; ?> <i class="fa fa-minus-circle"></i>
							<input type="hidden" value="<?php echo $product['product_id']; ?>" />
						</div>
					<?php } ?>
				</div>
				<input type="hidden" name="revtheme_slider_1[featured]" value="<?php echo $revtheme_slider_1['featured']; ?>" />
			</div>
		</div>
		<?php if ($revtheme_slider_1['category_id']=='featured') { $featured_style="none";} else {$featured_style="block";}?>
		<div class="form-group" id="catonly" style="display:<?php echo $featured_style;?>;">
			<label class="col-sm-2 control-label">Фильтр по производителю:</label>
			<div class="col-sm-4 scrollbox" id="featured-product">
				<select name="revtheme_slider_1[manufacturer_id]" id="select" class="form-control">
					<option value="0" <?php if ($revtheme_slider_1['manufacturer_id']=='0') { ?>selected="selected"<?php } ?>>Все производители</option>
				<?php if (isset($manufacturers)){
					foreach ($manufacturers as $manufacturer) { ?>
						<?php if ($manufacturer['manufacturer_id'] == $revtheme_slider_1['manufacturer_id']) { ?>
							<option value="<?php echo $manufacturer['manufacturer_id']; ?>" selected="selected"><?php echo $manufacturer['name']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $manufacturer['manufacturer_id']; ?>"><?php echo $manufacturer['name']; ?></option>
						<?php } ?>
					<?php } ?>
				<?php } ?>
				</select>
			</div>
		</div>
		<?php if ($revtheme_slider_1['category_id']=='featured') { $featured_style3="block";} else {$featured_style3="none";}?>
		<div class="form-group" style="display:<?php echo $featured_style;?>;">
			<label class="col-sm-2 control-label">Сортировка и фильтрация:</label>
			<div class="col-sm-4">
				<select name="revtheme_slider_1[sort]" class="form-control">
				<?php if ($revtheme_slider_1['sort'] == 'p.date_added') { ?>
					<option value="p.date_added" selected="selected"><?php echo $text_sort_date_added; ?></option>
				<?php } else { ?>
					<option value="p.date_added"><?php echo $text_sort_date_added; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_1['sort'] == 'rating') { ?>
					<option value="rating" selected="selected"><?php echo $text_sort_rating; ?></option>
				<?php } else { ?>
					<option value="rating"><?php echo $text_sort_rating; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_1['sort'] == 'p.viewed') { ?>
					<option value="p.viewed" selected="selected"><?php echo $text_sort_viewed; ?></option>
				<?php } else { ?>
					<option value="p.viewed"><?php echo $text_sort_viewed; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_1['sort'] == 'p.sort_order') { ?>
					<option value="p.sort_order" selected="selected"><?php echo $text_sort_order; ?></option>
				<?php } else { ?>
					<option value="p.sort_order"><?php echo $text_sort_order; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_1['sort'] == 'topsellers') { ?>
					<option value="topsellers" selected="selected"><?php echo $text_sort_bestseller; ?></option>
				<?php } else { ?>
					<option value="topsellers"><?php echo $text_sort_bestseller; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_1['sort'] == 'special') { ?>
					<option value="special" selected="selected"><?php echo $text_sort_special; ?></option>
				<?php } else { ?>
					<option value="special"><?php echo $text_sort_special; ?></option>
				<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="autoscroll"><span data-toggle="tooltip" title="Период через который будет срабатывать авто прокрутка.<br/>Оставьте пустым чтобы отключть.">Автопрокрутка (мсек):</span></label>
			<div class="col-sm-4">
				<input type="text" name="revtheme_slider_1[autoscroll]" value="<?php echo $revtheme_slider_1['autoscroll']; ?>" class="form-control" />
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_slider2">
		<div class="form-group">
			<label class="col-sm-2 control-label">Включен:
			</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_slider_2[status]" value="1" <?php if ($revtheme_slider_2['status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_slider_2[status]" value="0" <?php if (!$revtheme_slider_2['status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-3">
					<input class="form-control" type="text" name="revtheme_slider_2[title]" value="<?php echo $revtheme_slider_2['title']; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Иконка:</label>
			<div class="col-sm-3">
					<span style="cursor:pointer" class="fa_icon form-control width_initial" id="icon_slider_2" onclick="fa_icons($(this).attr('id'))"><i class="<?php echo $revtheme_slider_2['icon']; ?>"></i></span>
					<input type="hidden" name="revtheme_slider_2[icon]" value="<?php echo $revtheme_slider_2['icon']; ?>" id="input-icon_slider2" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Лимит товаров:</label>
			<div class="col-sm-2">
					<input class="form-control width_initial" type="text" name="revtheme_slider_2[count]" value="<?php echo $revtheme_slider_2['count']; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Изображение (Ш x В):</label>
			<div class="col-sm-4">
				<input class="form-control width_initial" type="text" name="revtheme_slider_2[image_width]" value="<?php echo $revtheme_slider_2['image_width']; ?>" /> x 
				<input class="form-control width_initial" type="text" name="revtheme_slider_2[image_height]" value="<?php echo $revtheme_slider_2['image_height']; ?>" /> px
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Источник товаров:</label>
			<div class="col-sm-4">
				<select name="revtheme_slider_2[category_id]" id="select2" onchange="showhide2();" class="form-control">
					<option value="0" <?php if ($revtheme_slider_2['category_id']=='0') { ?>selected="selected"<?php } ?>>Все товары</option>
					<option value="featured" <?php if ($revtheme_slider_2['category_id']=='featured') { ?>selected="selected"<?php } ?>>Выборочные товары</option>
				<?php foreach ($rootcats2 as $rootcat) { ?>
					<?php if ($rootcat['category_id'] == $revtheme_slider_2['category_id']) { ?>
						<option value="<?php echo $rootcat['category_id']; ?>" selected="selected"><?php echo $rootcat['name']; ?></option>
					<?php } else { ?>
						<option value="<?php echo $rootcat['category_id']; ?>"><?php echo $rootcat['name']; ?></option>
					<?php } ?>
				<?php } ?>
				</select>
			</div>
		</div>
		<?php if ($revtheme_slider_2['category_id']=='featured') { $featured_style2="block";} else {$featured_style2="none";}?>
		<div class="form-group" id="rowfeatured2" style="display:<?php echo $featured_style2; ?>;">
			<label class="col-sm-2 control-label">Товары (автозаполнение):</label>
			<div class="col-sm-4">
				<input type="text" name="product2" value="" placeholder="Товары" id="input-product" class="form-control" />
				<br/>
				<div class="scrollbox2 well well-sm" id="featured-product2" style="height: 150px; overflow: auto;">
					<?php $class = 'odd'; ?>
					<?php foreach ($products2 as $product) { ?>
						<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
						<div id="featured-product2<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>"><?php echo $product['name']; ?> <i class="fa fa-minus-circle"></i>
							<input type="hidden" value="<?php echo $product['product_id']; ?>" />
						</div>
					<?php } ?>
				</div>
				<input type="hidden" name="revtheme_slider_2[featured]" value="<?php echo $revtheme_slider_2['featured']; ?>" />
			</div>
		</div>
		<?php if ($revtheme_slider_2['category_id']=='featured') { $featured_style2="none";} else {$featured_style2="block";}?>
		<div class="form-group" id="catonly2" style="display:<?php echo $featured_style2;?>;">
			<label class="col-sm-2 control-label">Фильтр по производителю:</label>
			<div class="col-sm-4 scrollbox2" id="featured-product2">
				<select name="revtheme_slider_2[manufacturer_id]" id="select2" class="form-control">
					<option value="0" <?php if ($revtheme_slider_2['manufacturer_id']=='0') { ?>selected="selected"<?php } ?>>Все производители</option>
				<?php if (isset($manufacturers)){
					foreach ($manufacturers as $manufacturer) { ?>
						<?php if ($manufacturer['manufacturer_id'] == $revtheme_slider_2['manufacturer_id']) { ?>
							<option value="<?php echo $manufacturer['manufacturer_id']; ?>" selected="selected"><?php echo $manufacturer['name']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $manufacturer['manufacturer_id']; ?>"><?php echo $manufacturer['name']; ?></option>
						<?php } ?>
					<?php } ?>
				<?php } ?>
				</select>
			</div>
		</div>
		<?php if ($revtheme_slider_2['category_id']=='featured') { $featured_style3="block";} else {$featured_style3="none";}?>
		<div class="form-group" style="display:<?php echo $featured_style2;?>;">
			<label class="col-sm-2 control-label">Сортировка и фильтрация:</label>
			<div class="col-sm-4">
				<select name="revtheme_slider_2[sort]" class="form-control">
				<?php if ($revtheme_slider_2['sort'] == 'p.date_added') { ?>
					<option value="p.date_added" selected="selected"><?php echo $text_sort_date_added; ?></option>
				<?php } else { ?>
					<option value="p.date_added"><?php echo $text_sort_date_added; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_2['sort'] == 'rating') { ?>
					<option value="rating" selected="selected"><?php echo $text_sort_rating; ?></option>
				<?php } else { ?>
					<option value="rating"><?php echo $text_sort_rating; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_2['sort'] == 'p.viewed') { ?>
					<option value="p.viewed" selected="selected"><?php echo $text_sort_viewed; ?></option>
				<?php } else { ?>
					<option value="p.viewed"><?php echo $text_sort_viewed; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_2['sort'] == 'p.sort_order') { ?>
					<option value="p.sort_order" selected="selected"><?php echo $text_sort_order; ?></option>
				<?php } else { ?>
					<option value="p.sort_order"><?php echo $text_sort_order; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_2['sort'] == 'topsellers') { ?>
					<option value="topsellers" selected="selected"><?php echo $text_sort_bestseller; ?></option>
				<?php } else { ?>
					<option value="topsellers"><?php echo $text_sort_bestseller; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_2['sort'] == 'special') { ?>
					<option value="special" selected="selected"><?php echo $text_sort_special; ?></option>
				<?php } else { ?>
					<option value="special"><?php echo $text_sort_special; ?></option>
				<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="autoscroll"><span data-toggle="tooltip" title="Период через который будет срабатывать авто прокрутка.<br/>Оставьте пустым чтобы отключть.">Автопрокрутка (мсек):</span></label>
			<div class="col-sm-4">
				<input type="text" name="revtheme_slider_2[autoscroll]" value="<?php echo $revtheme_slider_2['autoscroll']; ?>" class="form-control" />
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_slider3">
		<div class="form-group">
			<label class="col-sm-2 control-label">Включен:
			</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_slider_3[status]" value="1" <?php if ($revtheme_slider_3['status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_slider_3[status]" value="0" <?php if (!$revtheme_slider_3['status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-3">
					<input class="form-control" type="text" name="revtheme_slider_3[title]" value="<?php echo $revtheme_slider_3['title']; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Иконка:</label>
			<div class="col-sm-3">
					<span style="cursor:pointer" class="fa_icon form-control width_initial" id="icon_slider_3" onclick="fa_icons($(this).attr('id'))"><i class="<?php echo $revtheme_slider_3['icon']; ?>"></i></span>
					<input type="hidden" name="revtheme_slider_3[icon]" value="<?php echo $revtheme_slider_3['icon']; ?>" id="input-icon_slider3" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Лимит товаров:</label>
			<div class="col-sm-2">
					<input class="form-control width_initial" type="text" name="revtheme_slider_3[count]" value="<?php echo $revtheme_slider_3['count']; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Изображение (Ш x В):</label>
			<div class="col-sm-4">
				<input class="form-control width_initial" type="text" name="revtheme_slider_3[image_width]" value="<?php echo $revtheme_slider_3['image_width']; ?>" /> x 
				<input class="form-control width_initial" type="text" name="revtheme_slider_3[image_height]" value="<?php echo $revtheme_slider_3['image_height']; ?>" /> px
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Источник товаров:</label>
			<div class="col-sm-4">
				<select name="revtheme_slider_3[category_id]" id="select3" onchange="showhide3();" class="form-control">
					<option value="0" <?php if ($revtheme_slider_3['category_id']=='0') { ?>selected="selected"<?php } ?>>Все товары</option>
					<option value="featured" <?php if ($revtheme_slider_3['category_id']=='featured') { ?>selected="selected"<?php } ?>>Выборочные товары</option>
				<?php foreach ($rootcats3 as $rootcat) { ?>
					<?php if ($rootcat['category_id'] == $revtheme_slider_3['category_id']) { ?>
						<option value="<?php echo $rootcat['category_id']; ?>" selected="selected"><?php echo $rootcat['name']; ?></option>
					<?php } else { ?>
						<option value="<?php echo $rootcat['category_id']; ?>"><?php echo $rootcat['name']; ?></option>
					<?php } ?>
				<?php } ?>
				</select>
			</div>
		</div>
		<?php if ($revtheme_slider_3['category_id']=='featured') { $featured_style3="block";} else {$featured_style3="none";}?>
		<div class="form-group" id="rowfeatured3" style="display:<?php echo $featured_style3; ?>;">
			<label class="col-sm-2 control-label">Товары (автозаполнение):</label>
			<div class="col-sm-4">
				<input type="text" name="product3" value="" placeholder="Товары" id="input-product" class="form-control" />
				<br/>
				<div class="scrollbox2 well well-sm" id="featured-product3" style="height: 150px; overflow: auto;">
					<?php $class = 'odd'; ?>
					<?php foreach ($products as $product) { ?>
						<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
						<div id="featured-product3<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>"><?php echo $product['name']; ?> <i class="fa fa-minus-circle"></i>
							<input type="hidden" value="<?php echo $product['product_id']; ?>" />
						</div>
					<?php } ?>
				</div>
				<input type="hidden" name="revtheme_slider_3[featured]" value="<?php echo $revtheme_slider_3['featured']; ?>" />
			</div>
		</div>
		<?php if ($revtheme_slider_3['category_id']=='featured') { $featured_style3="none";} else {$featured_style3="block";}?>
		<div class="form-group" id="catonly3" style="display:<?php echo $featured_style3;?>;">
			<label class="col-sm-2 control-label">Фильтр по производителю:</label>
			<div class="col-sm-4 scrollbox2" id="featured-product3">
				<select name="revtheme_slider_3[manufacturer_id]" id="select3" class="form-control">
					<option value="0" <?php if ($revtheme_slider_3['manufacturer_id']=='0') { ?>selected="selected"<?php } ?>>Все производители</option>
				<?php if (isset($manufacturers)){
					foreach ($manufacturers as $manufacturer) { ?>
						<?php if ($manufacturer['manufacturer_id'] == $revtheme_slider_3['manufacturer_id']) { ?>
							<option value="<?php echo $manufacturer['manufacturer_id']; ?>" selected="selected"><?php echo $manufacturer['name']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $manufacturer['manufacturer_id']; ?>"><?php echo $manufacturer['name']; ?></option>
						<?php } ?>
					<?php } ?>
				<?php } ?>
				</select>
			</div>
		</div>
		<?php if ($revtheme_slider_3['category_id']=='featured') { $featured_style3="none";} else {$featured_style3="block";}?>
		<div class="form-group" style="display:<?php echo $featured_style3;?>;">
			<label class="col-sm-2 control-label">Сортировка и фильтрация:</label>
			<div class="col-sm-4">
				<select name="revtheme_slider_3[sort]" class="form-control">
				<?php if ($revtheme_slider_3['sort'] == 'p.date_added') { ?>
					<option value="p.date_added" selected="selected"><?php echo $text_sort_date_added; ?></option>
				<?php } else { ?>
					<option value="p.date_added"><?php echo $text_sort_date_added; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_3['sort'] == 'rating') { ?>
					<option value="rating" selected="selected"><?php echo $text_sort_rating; ?></option>
				<?php } else { ?>
					<option value="rating"><?php echo $text_sort_rating; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_3['sort'] == 'p.viewed') { ?>
					<option value="p.viewed" selected="selected"><?php echo $text_sort_viewed; ?></option>
				<?php } else { ?>
					<option value="p.viewed"><?php echo $text_sort_viewed; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_3['sort'] == 'p.sort_order') { ?>
					<option value="p.sort_order" selected="selected"><?php echo $text_sort_order; ?></option>
				<?php } else { ?>
					<option value="p.sort_order"><?php echo $text_sort_order; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_3['sort'] == 'topsellers') { ?>
					<option value="topsellers" selected="selected"><?php echo $text_sort_bestseller; ?></option>
				<?php } else { ?>
					<option value="topsellers"><?php echo $text_sort_bestseller; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_3['sort'] == 'special') { ?>
					<option value="special" selected="selected"><?php echo $text_sort_special; ?></option>
				<?php } else { ?>
					<option value="special"><?php echo $text_sort_special; ?></option>
				<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="autoscroll"><span data-toggle="tooltip" title="Период через который будет срабатывать авто прокрутка.<br/>Оставьте пустым чтобы отключть.">Автопрокрутка (мсек):</span></label>
			<div class="col-sm-4">
				<input type="text" name="revtheme_slider_3[autoscroll]" value="<?php echo $revtheme_slider_3['autoscroll']; ?>" class="form-control" />
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_slider4">
		<div class="form-group">
			<label class="col-sm-2 control-label">Включен:
			</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_slider_4[status]" value="1" <?php if ($revtheme_slider_4['status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_slider_4[status]" value="0" <?php if (!$revtheme_slider_4['status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-3">
					<input class="form-control" type="text" name="revtheme_slider_4[title]" value="<?php echo $revtheme_slider_4['title']; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Иконка:</label>
			<div class="col-sm-3">
					<span style="cursor:pointer" class="fa_icon form-control width_initial" id="icon_slider_4" onclick="fa_icons($(this).attr('id'))"><i class="<?php echo $revtheme_slider_4['icon']; ?>"></i></span>
					<input type="hidden" name="revtheme_slider_4[icon]" value="<?php echo $revtheme_slider_4['icon']; ?>" id="input-icon_slider4" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Лимит товаров:</label>
			<div class="col-sm-2">
					<input class="form-control width_initial" type="text" name="revtheme_slider_4[count]" value="<?php echo $revtheme_slider_4['count']; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Изображение (Ш x В):</label>
			<div class="col-sm-4">
				<input class="form-control width_initial" type="text" name="revtheme_slider_4[image_width]" value="<?php echo $revtheme_slider_4['image_width']; ?>" /> x 
				<input class="form-control width_initial" type="text" name="revtheme_slider_4[image_height]" value="<?php echo $revtheme_slider_4['image_height']; ?>" /> px
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Источник товаров:</label>
			<div class="col-sm-4">
				<select name="revtheme_slider_4[category_id]" id="select4" onchange="showhide4();" class="form-control">
					<option value="0" <?php if ($revtheme_slider_4['category_id']=='0') { ?>selected="selected"<?php } ?>>Все товары</option>
					<option value="featured" <?php if ($revtheme_slider_4['category_id']=='featured') { ?>selected="selected"<?php } ?>>Выборочные товары</option>
				<?php foreach ($rootcats4 as $rootcat) { ?>
					<?php if ($rootcat['category_id'] == $revtheme_slider_4['category_id']) { ?>
						<option value="<?php echo $rootcat['category_id']; ?>" selected="selected"><?php echo $rootcat['name']; ?></option>
					<?php } else { ?>
						<option value="<?php echo $rootcat['category_id']; ?>"><?php echo $rootcat['name']; ?></option>
					<?php } ?>
				<?php } ?>
				</select>
			</div>
		</div>
		<?php if ($revtheme_slider_4['category_id']=='featured') { $featured_style4="block";} else {$featured_style4="none";}?>
		<div class="form-group" id="rowfeatured4" style="display:<?php echo $featured_style4; ?>;">
			<label class="col-sm-2 control-label">Товары (автозаполнение):</label>
			<div class="col-sm-4">
				<input type="text" name="product4" value="" placeholder="Товары" id="input-product" class="form-control" />
				<br/>
				<div class="scrollbox4 well well-sm" id="featured-product4" style="height: 150px; overflow: auto;">
					<?php $class = 'odd'; ?>
					<?php foreach ($products as $product) { ?>
						<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
						<div id="featured-product4<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>"><?php echo $product['name']; ?> <i class="fa fa-minus-circle"></i>
							<input type="hidden" value="<?php echo $product['product_id']; ?>" />
						</div>
					<?php } ?>
				</div>
				<input type="hidden" name="revtheme_slider_4[featured]" value="<?php echo $revtheme_slider_4['featured']; ?>" />
			</div>
		</div>
		<?php if ($revtheme_slider_4['category_id']=='featured') { $featured_style4="none";} else {$featured_style4="block";}?>
		<div class="form-group" id="catonly4" style="display:<?php echo $featured_style4;?>;">
			<label class="col-sm-2 control-label">Фильтр по производителю:</label>
			<div class="col-sm-4 scrollbox4" id="featured-product3">
				<select name="revtheme_slider_4[manufacturer_id]" id="select4" class="form-control">
					<option value="0" <?php if ($revtheme_slider_4['manufacturer_id']=='0') { ?>selected="selected"<?php } ?>>Все производители</option>
				<?php if (isset($manufacturers)){
					foreach ($manufacturers as $manufacturer) { ?>
						<?php if ($manufacturer['manufacturer_id'] == $revtheme_slider_4['manufacturer_id']) { ?>
							<option value="<?php echo $manufacturer['manufacturer_id']; ?>" selected="selected"><?php echo $manufacturer['name']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $manufacturer['manufacturer_id']; ?>"><?php echo $manufacturer['name']; ?></option>
						<?php } ?>
					<?php } ?>
				<?php } ?>
				</select>
			</div>
		</div>
		<?php if ($revtheme_slider_4['category_id']=='featured') { $featured_style4="none";} else {$featured_style4="block";}?>
		<div class="form-group" style="display:<?php echo $featured_style4;?>;">
			<label class="col-sm-2 control-label">Сортировка и фильтрация:</label>
			<div class="col-sm-4">
				<select name="revtheme_slider_4[sort]" class="form-control">
				<?php if ($revtheme_slider_4['sort'] == 'p.date_added') { ?>
					<option value="p.date_added" selected="selected"><?php echo $text_sort_date_added; ?></option>
				<?php } else { ?>
					<option value="p.date_added"><?php echo $text_sort_date_added; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_4['sort'] == 'rating') { ?>
					<option value="rating" selected="selected"><?php echo $text_sort_rating; ?></option>
				<?php } else { ?>
					<option value="rating"><?php echo $text_sort_rating; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_4['sort'] == 'p.viewed') { ?>
					<option value="p.viewed" selected="selected"><?php echo $text_sort_viewed; ?></option>
				<?php } else { ?>
					<option value="p.viewed"><?php echo $text_sort_viewed; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_4['sort'] == 'p.sort_order') { ?>
					<option value="p.sort_order" selected="selected"><?php echo $text_sort_order; ?></option>
				<?php } else { ?>
					<option value="p.sort_order"><?php echo $text_sort_order; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_4['sort'] == 'topsellers') { ?>
					<option value="topsellers" selected="selected"><?php echo $text_sort_bestseller; ?></option>
				<?php } else { ?>
					<option value="topsellers"><?php echo $text_sort_bestseller; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_4['sort'] == 'special') { ?>
					<option value="special" selected="selected"><?php echo $text_sort_special; ?></option>
				<?php } else { ?>
					<option value="special"><?php echo $text_sort_special; ?></option>
				<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="autoscroll"><span data-toggle="tooltip" title="Период через который будет срабатывать авто прокрутка.<br/>Оставьте пустым чтобы отключть.">Автопрокрутка (мсек):</span></label>
			<div class="col-sm-4">
				<input type="text" name="revtheme_slider_4[autoscroll]" value="<?php echo $revtheme_slider_4['autoscroll']; ?>" class="form-control" />
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_slider5">
		<div class="form-group">
			<label class="col-sm-2 control-label">Включен:
			</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_slider_5[status]" value="1" <?php if ($revtheme_slider_5['status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_slider_5[status]" value="0" <?php if (!$revtheme_slider_5['status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-3">
					<input class="form-control" type="text" name="revtheme_slider_5[title]" value="<?php echo $revtheme_slider_5['title']; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Иконка:</label>
			<div class="col-sm-3">
					<span style="cursor:pointer" class="fa_icon form-control width_initial" id="icon_slider_5" onclick="fa_icons($(this).attr('id'))"><i class="<?php echo $revtheme_slider_5['icon']; ?>"></i></span>
					<input type="hidden" name="revtheme_slider_5[icon]" value="<?php echo $revtheme_slider_5['icon']; ?>" id="input-icon_slider5" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Лимит товаров:</label>
			<div class="col-sm-2">
					<input class="form-control width_initial" type="text" name="revtheme_slider_5[count]" value="<?php echo $revtheme_slider_5['count']; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Изображение (Ш x В):</label>
			<div class="col-sm-4">
				<input class="form-control width_initial" type="text" name="revtheme_slider_5[image_width]" value="<?php echo $revtheme_slider_5['image_width']; ?>" /> x 
				<input class="form-control width_initial" type="text" name="revtheme_slider_5[image_height]" value="<?php echo $revtheme_slider_5['image_height']; ?>" /> px
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Источник товаров:</label>
			<div class="col-sm-4">
				<select name="revtheme_slider_5[category_id]" id="select5" onchange="showhide5();" class="form-control">
					<option value="0" <?php if ($revtheme_slider_5['category_id']=='0') { ?>selected="selected"<?php } ?>>Все товары</option>
					<option value="featured" <?php if ($revtheme_slider_5['category_id']=='featured') { ?>selected="selected"<?php } ?>>Выборочные товары</option>
				<?php foreach ($rootcats5 as $rootcat) { ?>
					<?php if ($rootcat['category_id'] == $revtheme_slider_5['category_id']) { ?>
						<option value="<?php echo $rootcat['category_id']; ?>" selected="selected"><?php echo $rootcat['name']; ?></option>
					<?php } else { ?>
						<option value="<?php echo $rootcat['category_id']; ?>"><?php echo $rootcat['name']; ?></option>
					<?php } ?>
				<?php } ?>
				</select>
			</div>
		</div>
		<?php if ($revtheme_slider_5['category_id']=='featured') { $featured_style5="block";} else {$featured_style5="none";}?>
		<div class="form-group" id="rowfeatured5" style="display:<?php echo $featured_style5; ?>;">
			<label class="col-sm-2 control-label">Товары (автозаполнение):</label>
			<div class="col-sm-4">
				<input type="text" name="product5" value="" placeholder="Товары" id="input-product" class="form-control" />
				<br/>
				<div class="scrollbox5 well well-sm" id="featured-product5" style="height: 150px; overflow: auto;">
					<?php $class = 'odd'; ?>
					<?php foreach ($products as $product) { ?>
						<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
						<div id="featured-product5<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>"><?php echo $product['name']; ?> <i class="fa fa-minus-circle"></i>
							<input type="hidden" value="<?php echo $product['product_id']; ?>" />
						</div>
					<?php } ?>
				</div>
				<input type="hidden" name="revtheme_slider_5[featured]" value="<?php echo $revtheme_slider_5['featured']; ?>" />
			</div>
		</div>
		<?php if ($revtheme_slider_5['category_id']=='featured') { $featured_style5="none";} else {$featured_style5="block";}?>
		<div class="form-group" id="catonly5" style="display:<?php echo $featured_style5;?>;">
			<label class="col-sm-2 control-label">Фильтр по производителю:</label>
			<div class="col-sm-4 scrollbox5" id="featured-product3">
				<select name="revtheme_slider_5[manufacturer_id]" id="select5" class="form-control">
					<option value="0" <?php if ($revtheme_slider_5['manufacturer_id']=='0') { ?>selected="selected"<?php } ?>>Все производители</option>
				<?php if (isset($manufacturers)){
					foreach ($manufacturers as $manufacturer) { ?>
						<?php if ($manufacturer['manufacturer_id'] == $revtheme_slider_5['manufacturer_id']) { ?>
							<option value="<?php echo $manufacturer['manufacturer_id']; ?>" selected="selected"><?php echo $manufacturer['name']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $manufacturer['manufacturer_id']; ?>"><?php echo $manufacturer['name']; ?></option>
						<?php } ?>
					<?php } ?>
				<?php } ?>
				</select>
			</div>
		</div>
		<?php if ($revtheme_slider_5['category_id']=='featured') { $featured_style5="none";} else {$featured_style5="block";}?>
		<div class="form-group" style="display:<?php echo $featured_style5;?>;">
			<label class="col-sm-2 control-label">Сортировка и фильтрация:</label>
			<div class="col-sm-4">
				<select name="revtheme_slider_5[sort]" class="form-control">
				<?php if ($revtheme_slider_5['sort'] == 'p.date_added') { ?>
					<option value="p.date_added" selected="selected"><?php echo $text_sort_date_added; ?></option>
				<?php } else { ?>
					<option value="p.date_added"><?php echo $text_sort_date_added; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_5['sort'] == 'rating') { ?>
					<option value="rating" selected="selected"><?php echo $text_sort_rating; ?></option>
				<?php } else { ?>
					<option value="rating"><?php echo $text_sort_rating; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_5['sort'] == 'p.viewed') { ?>
					<option value="p.viewed" selected="selected"><?php echo $text_sort_viewed; ?></option>
				<?php } else { ?>
					<option value="p.viewed"><?php echo $text_sort_viewed; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_5['sort'] == 'p.sort_order') { ?>
					<option value="p.sort_order" selected="selected"><?php echo $text_sort_order; ?></option>
				<?php } else { ?>
					<option value="p.sort_order"><?php echo $text_sort_order; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_5['sort'] == 'topsellers') { ?>
					<option value="topsellers" selected="selected"><?php echo $text_sort_bestseller; ?></option>
				<?php } else { ?>
					<option value="topsellers"><?php echo $text_sort_bestseller; ?></option>
				<?php } ?>
				<?php if ($revtheme_slider_5['sort'] == 'special') { ?>
					<option value="special" selected="selected"><?php echo $text_sort_special; ?></option>
				<?php } else { ?>
					<option value="special"><?php echo $text_sort_special; ?></option>
				<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="autoscroll"><span data-toggle="tooltip" title="Период через который будет срабатывать авто прокрутка.<br/>Оставьте пустым чтобы отключть.">Автопрокрутка (мсек):</span></label>
			<div class="col-sm-4">
				<input type="text" name="revtheme_slider_5[autoscroll]" value="<?php echo $revtheme_slider_5['autoscroll']; ?>" class="form-control" />
			</div>
		</div>
	</div>
	<div class="tab-pane" id="tab_blog">
		<div class="form-group">
			<label class="col-sm-3 control-label">Показывать виджет новостей:</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_home_blog[status]" value="1" <?php if($revtheme_home_blog['status']) echo " checked='checked'"?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_home_blog[status]" value="0" <?php if(!$revtheme_home_blog['status']) echo " checked='checked'"?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
		  <label class="col-sm-3 control-label">Заголовок:</label>
		  <div class="col-sm-3">			
			<input type="text" name="revtheme_home_blog[title]" value="<?php echo (isset($revtheme_home_blog['title'])) ? $revtheme_home_blog['title'] : ''; ?>" class="form-control" />
		  </div>
		</div>
		<div class="form-group">
		  <label class="col-sm-3 control-label">Лимит новостей в виджете:</label>
		  <div class="col-sm-3">
			<input type="text" name="revtheme_home_blog[news_limit]" value="<?php echo (isset($revtheme_home_blog['news_limit'])) ? $revtheme_home_blog['news_limit'] : ''; ?>" class="form-control" />
		  </div>
		</div>
		<div class="form-group">
		  <label class="col-sm-3 control-label">Лимит символов описания:</label>
		  <div class="col-sm-3">
			<input type="text" name="revtheme_home_blog[desc_limit]" value="<?php echo (isset($revtheme_home_blog['desc_limit'])) ? $revtheme_home_blog['desc_limit'] : ''; ?>" class="form-control" />
		  </div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Размер картинки, px:</label>
			<div class="col-sm-9">
				<div class="row">
				  <div class="col-sm-2">
					<input data-toggle="tooltip" data-placement="top" title="Ширина" type="text" name="revtheme_home_blog[image_width]" value="<?php echo (isset($revtheme_home_blog['image_width'])) ? $revtheme_home_blog['image_width'] : ''; ?>" class="form-control" />
				  </div>
				  <div class="col-sm-2">
					<input data-toggle="tooltip" data-placement="top" title="Высота" type="text" name="revtheme_home_blog[image_height]" value="<?php echo (isset($revtheme_home_blog['image_height'])) ? $revtheme_home_blog['image_height'] : ''; ?>" class="form-control" />
				  </div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Цвет фона:</label>
			<div class="col-sm-2">
				<input class="jscolor form-control" name="revtheme_home_blog[b_color]" value="<?php echo isset($revtheme_home_blog['b_color']) ? $revtheme_home_blog['b_color'] : ''; ?>" />
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_aboutstore">
		<div class="form-group">
			<label class="col-sm-1 control-label">Включить:
			</label>
			<div class="col-sm-11">
				<label class="radio-inline">
					<input type="radio" name="revtheme_aboutstore_home[status]" value="1" <?php if ($revtheme_aboutstore_home['status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_aboutstore_home[status]" value="0" <?php if (!$revtheme_aboutstore_home['status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<hr>
		<div class="form-group">
			<label class="col-sm-1 control-label">Заголовок:</label>
			<div class="col-sm-11">
				<input class="form-control" type="text" name="revtheme_aboutstore_home[title]" value="<?php echo $revtheme_aboutstore_home['title']; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-1 control-label">Текст:</label>
			<div class="col-sm-11">
				<textarea name="revtheme_aboutstore_home[description]" placeholder="Текст о магазине" id="input-description" class="form-control"><?php echo isset($revtheme_aboutstore_home['description']) ? $revtheme_aboutstore_home['description'] : ''; ?></textarea>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="tab_socv">
		<div class="form-group">
			<label class="col-sm-3 control-label">Включить:
			</label>
			<div class="col-sm-9">
				<label class="radio-inline">
					<input type="radio" name="revtheme_home_socv[status]" value="1" <?php if ($revtheme_home_socv['status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_home_socv[status]" value="0" <?php if (!$revtheme_home_socv['status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Размеры виджета, px:</label>
			<div class="col-sm-9">
				<div class="row">
				  <div class="col-sm-2">
					<input data-toggle="tooltip" data-placement="top" title="Ширина" type="text" name="revtheme_home_socv[width]" value="<?php echo (isset($revtheme_home_socv['width'])) ? $revtheme_home_socv['width'] : ''; ?>" class="form-control" />
				  </div>
				  <div class="col-sm-2">
					<input data-toggle="tooltip" data-placement="top" title="Высота" type="text" name="revtheme_home_socv[height]" value="<?php echo (isset($revtheme_home_socv['height'])) ? $revtheme_home_socv['height'] : ''; ?>" class="form-control" />
				  </div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Id группы vk:</label>
			<div class="col-sm-3">
				<input type="text" name="revtheme_home_socv[id]" value="<?php echo (isset($revtheme_home_socv['id'])) ? $revtheme_home_socv['id'] : ''; ?>" class="form-control" />
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_storereview">
		<div class="form-group">
			<label class="col-sm-2 control-label">Включить:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_home_storereview[status]" value="1" <?php if ($revtheme_home_storereview['status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_home_storereview[status]" value="0" <?php if (!$revtheme_home_storereview['status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-3">
					<input class="form-control" type="text" name="revtheme_home_storereview[title]" value="<?php echo $revtheme_home_storereview['title']; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Кнопка "Все отзывы":
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_home_storereview[button_all]" value="1" <?php if ($revtheme_home_storereview['button_all']) { echo 'checked'; } ?> /> Включена
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_home_storereview[button_all]" value="0" <?php if (!$revtheme_home_storereview['button_all']) { echo 'checked';} ?> /> Выключена
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Текст кнопки "Все отзывы":</label>
			<div class="col-sm-3">
					<input class="form-control" type="text" name="revtheme_home_storereview[button_all_text]" value="<?php echo $revtheme_home_storereview['button_all_text']; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Лимит отзывов:</label>
			<div class="col-sm-3">
					<input class="form-control" type="text" name="revtheme_home_storereview[limit]" value="<?php echo $revtheme_home_storereview['limit']; ?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Сортировка отзывов:
			</label>
			<div class="col-sm-3">
				<label class="radio-inline">
					<input type="radio" name="revtheme_home_storereview[order]" value="1" <?php if ($revtheme_home_storereview['order']) { echo 'checked'; } ?> /> Случайно
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_home_storereview[order]" value="0" <?php if (!$revtheme_home_storereview['order']) { echo 'checked';} ?> /> По дате
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Лимит символов отзыва:</label>
			<div class="col-sm-3">
					<input class="form-control" type="text" name="revtheme_home_storereview[limit_text]" value="<?php echo $revtheme_home_storereview['limit_text']; ?>" />
			</div>
		</div>
	</div>
	<div class="tab-pane text-style" id="tab_viewed_products">
		<div class="form-group">
			<label class="col-sm-2 control-label">Включить:
			</label>
			<div class="col-sm-2">
				<label class="radio-inline">
					<input type="radio" name="revtheme_home_viewed_products[status]" value="1" <?php if ($revtheme_home_viewed_products['status']) { echo 'checked'; } ?> /> Да
				</label>
				<label class="radio-inline">
					<input type="radio" name="revtheme_home_viewed_products[status]" value="0" <?php if (!$revtheme_home_viewed_products['status']) { echo 'checked';} ?> /> Нет
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Заголовок:</label>
			<div class="col-sm-3">
				<input type="text" name="revtheme_home_viewed_products[zagolovok]" value="<?php echo $revtheme_home_viewed_products['zagolovok']; ?>" class="form-control" />
			</div>
			<label class="col-sm-2 control-label">Лимит товаров:</label>
			<div class="col-sm-2">
				<input type="text" name="revtheme_home_viewed_products[limit]" value="<?php echo $revtheme_home_viewed_products['limit']; ?>" class="form-control" />
			</div>
		</div>
	</div>
</div>
</div>
<div class="tab-pane" id="tab_user_set">
	<div class="col-sm-2">
    <nav class="nav-sidebar">
		<ul class="nav tabs">
			<li class="active"><a href="#tab_user_set_styles" data-toggle="tab">Стили css</a></li>
			<li><a href="#tab_user_set_scripts" data-toggle="tab">Скрипты</a></li>
		</ul>
	</nav>
	</div>
	<div class="tab-content col-sm-10">
		<div class="tab-pane active text-style" id="tab_user_set_styles">
			<div class="form-group">
				<label class="col-sm-1 control-label">Стили:</label>
				<div class="col-sm-11">
					<textarea name="revtheme_footer_user_set[styles]" class="form-control" rows = "20"><?php echo $revtheme_footer_user_set['styles']; ?></textarea>
				</div>
			</div>
		</div>
		<div class="tab-pane text-style" id="tab_user_set_scripts">
			<div class="form-group">
				<label class="col-sm-1 control-label">Скрипты:</label>
				<div class="col-sm-11">
					<textarea name="revtheme_footer_user_set[scripts]" class="form-control" rows = "20"><?php echo $revtheme_footer_user_set['scripts']; ?></textarea>
				</div>
			</div>
		</div>
	</div>	
</div>
<div class="tab-pane" id="tab_all_settings">
	<div class="col-sm-2">
    <nav class="nav-sidebar">
		<ul class="nav tabs">
			<li class="active"><a href="#tab_all_settings_osn" data-toggle="tab">Общие настройки</a></li>
			<li><a href="#tab_all_settings_colors" data-toggle="tab">Цвета</a></li>
			<li><a href="#tab_all_settings_modal" data-toggle="tab">Всплывающее окно</a></li>
			<li><a href="#tab_all_settings_microdata" data-toggle="tab">Микроразметка</a></li>
		</ul>
	</nav>
	</div>
	<div class="tab-content col-sm-10">
		<div class="tab-pane active text-style" id="tab_all_settings_osn">
			<div class="form-group">
				<label class="col-sm-3 control-label">Альтернативная страница 404:</label>
				<div class="col-sm-2">
					<label class="radio-inline">
						<input type="radio" name="revtheme_all_settings[error404]" value="1" <?php if ($revtheme_all_settings['error404']) { echo 'checked'; } ?> /> Да
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_all_settings[error404]" value="0" <?php if (!$revtheme_all_settings['error404']) { echo 'checked';} ?> /> Нет
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Индикатор загрузки страницы:</label>
				<div class="col-sm-2">
					<label class="radio-inline">
						<input type="radio" name="revtheme_all_settings[n_progres]" value="1" <?php if ($revtheme_all_settings['n_progres']) { echo 'checked'; } ?> /> Вкл.
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_all_settings[n_progres]" value="0" <?php if (!$revtheme_all_settings['n_progres']) { echo 'checked';} ?> /> Выкл.
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Своя карта в контактах:<span data-toggle="tooltip" title="Введите сюда код карты если хотите поменять карту, формируемую шаблоном или она не отображается."></span></label>
				<div class="col-sm-9">
					<textarea name="revtheme_all_settings[yamap]" class="form-control" rows = "5"><?php echo $revtheme_all_settings['yamap']; ?></textarea>
				</div>
			</div>
		</div>
		<div class="tab-pane text-style" id="tab_all_settings_colors">
			<div class="form-group">
				<label class="col-sm-3 control-label">Цвет подложки выделения текста:</label>
				<div class="col-sm-2">
					<input class="jscolor form-control" name="revtheme_all_settings[color_selecta]" value="<?php echo isset($revtheme_all_settings['color_selecta']) ? $revtheme_all_settings['color_selecta'] : ''; ?>" />
				</div>
			</div>
		</div>
		<div class="tab-pane text-style" id="tab_all_settings_modal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Включить:</label>
				<div class="col-sm-2">
					<label class="radio-inline">
						<input type="radio" name="revtheme_all_settings[modal_status]" value="1" <?php if ($revtheme_all_settings['modal_status']) { echo 'checked'; } ?> /> Да
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_all_settings[modal_status]" value="0" <?php if (!$revtheme_all_settings['modal_status']) { echo 'checked';} ?> /> Нет
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Показывать через:<span data-toggle="tooltip" title="Время в днях через которое снова будет показано окно. Если 0 - при следующем открытии браузера, 888 - всегда."></span></label>
				<div class="col-sm-3">
					<input type="text" name="revtheme_all_settings[modal_time]" value="<?php echo $revtheme_all_settings['modal_time']; ?>" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Заголовок:</label>
				<div class="col-sm-3">
					<input type="text" name="revtheme_all_settings[modal_header]" value="<?php echo $revtheme_all_settings['modal_header']; ?>" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Сообщение:</label>
				<div class="col-sm-10">
					<textarea name="revtheme_all_settings[modal_text]" id="input-modal-text" class="form-control"><?php echo isset($revtheme_all_settings['modal_text']) ? $revtheme_all_settings['modal_text'] : ''; ?></textarea>
				</div>
			</div>
		</div>
		<div class="tab-pane text-style" id="tab_all_settings_microdata">
			<div class="form-group">
				<label class="col-sm-2 control-label">Микроразметка данных о компании:</label>
				<div class="col-sm-2">
					<label class="radio-inline">
						<input type="radio" name="revtheme_all_settings[microdata_status]" value="1" <?php if ($revtheme_all_settings['microdata_status']) { echo 'checked'; } ?> /> Вкл.
					</label>
					<label class="radio-inline">
						<input type="radio" name="revtheme_all_settings[microdata_status]" value="0" <?php if (!$revtheme_all_settings['microdata_status']) { echo 'checked';} ?> /> Выкл.
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Почтовый индекс:</label>
				<div class="col-sm-3">
					<input type="text" name="revtheme_all_settings[microdata_postcode]" placeholder="119021" value="<?php echo $revtheme_all_settings['microdata_postcode']; ?>" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Город:</label>
				<div class="col-sm-3">
					<input type="text" name="revtheme_all_settings[microdata_city]" placeholder="Москва" value="<?php echo $revtheme_all_settings['microdata_city']; ?>" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Адрес:</label>
				<div class="col-sm-3">
					<input type="text" name="revtheme_all_settings[microdata_adress]" placeholder="Льва Толстого, 16" value="<?php echo $revtheme_all_settings['microdata_adress']; ?>" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Телефоны:<span data-toggle="tooltip" title="Если несколько номеров, то вводить через запятую"></span></label>
				<div class="col-sm-6">
					<textarea name="revtheme_all_settings[microdata_phones]" placeholder="+7 495 222–33–44, +7 495 555–66-77" class="form-control"><?php echo isset($revtheme_all_settings['microdata_phones']) ? $revtheme_all_settings['microdata_phones'] : ''; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Социальные сети:<span data-toggle="tooltip" title="Если несколько, то вводить через запятую"></span></label>
				<div class="col-sm-6">
					<textarea name="revtheme_all_settings[microdata_social]" placeholder="https://vk.com, http://ok.ru, https://www.youtube.com" class="form-control"><?php echo isset($revtheme_all_settings['microdata_social']) ? $revtheme_all_settings['microdata_social'] : ''; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">E-mail:</label>
				<div class="col-sm-3">
					<input type="text" name="revtheme_all_settings[microdata_email]" value="<?php echo $revtheme_all_settings['microdata_email']; ?>" class="form-control" />
				</div>
			</div>
		</div>
	</div>	
</div>
<hr>
</div>
        </form>
		<?php } ?>
      </div>
    </div>
	<div class="panel-footer">
		2016 © Revolution v.2.3
		<div class="pull-right">
		<?php if (in_array('revtheme', $extension)) { ?>
		<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#demo_auto">Демо-данные автотоваров</button>
		<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#demo_moda">Демо-данные одежды</button>
		<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#undo_theme">Удалить шаблон</button>
		<?php } ?>
		</div>
		<div class="modal fade" id="demo_auto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Установка демо-данных и настроек для магазна автотоваров</h4>
					</div>
					<div class="modal-body">
					Внимание! Все текущие данные и настройки сотрутся!<br>Устанавливайте демо-данные только на новых магазинах.
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
						<a href="<?php echo $install_demo_1; ?>" class="btn btn-primary">Установить</a>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="demo_moda" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Установка демо-данных и настроек для магазна одежды</h4>
					</div>
					<div class="modal-body">
					Внимание! Все текущие данные и настройки сотрутся!<br>Устанавливайте демо-данные только на новых магазинах.
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
						<a href="<?php echo $install_demo_2; ?>" class="btn btn-primary">Установить</a>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="undo_theme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Удалить настройки шаблона</h4>
					</div>
					<div class="modal-body">
					<?php echo $button_uninstall; ?><br>Будьте внимательны.
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
						<a href="<?php echo $uninstall; ?>" class="btn btn-danger">Удалить</a>
					</div>
				</div>
			</div>
		</div>
    </div>

  </div>
</div>

<script type="text/javascript" src="view/javascript/revolution/jscolor.min.js"></script>
<script type="text/javascript" src="view/javascript/revolution/popup_icons.js"></script>
<script type="text/javascript"><!--
var radiocheckedval = $("#ajax_search_status:checked").attr("value");
if (radiocheckedval == 1) {
	$("#ajax_search_status_zavisimost").css('display', 'block');
} else {
	$("#ajax_search_status_zavisimost").css('display', 'none');
}
function ajax_search_status_zavisimost(radio) {
  var valzn = radio.value;
    if (valzn == 1) {
       $("#ajax_search_status_zavisimost").fadeIn(100);
   } else {
       $("#ajax_search_status_zavisimost").fadeOut(100);
   } 
}

var radiocheckedval2 = $("#atributs_zavisimost_radio:checked").attr("value");
if (radiocheckedval2 == 0) {
	$("#atributs_zavisimost").css('display', 'block');
} else {
	$("#atributs_zavisimost").css('display', 'none');
}
function atributs_zavisimost(radio2) {
  var valzn2 = radio2.value;
    if (valzn2 == 0) {
       $("#atributs_zavisimost").fadeIn(100);
   } else {
       $("#atributs_zavisimost").fadeOut(100);
   } 
}

var radiocheckedval3 = $("#dop_contacts_zavisimost_radio:checked").attr("value");
if (radiocheckedval3 == 1) {
	$("#dop_contacts_zavisimost").css('display', 'block');
} else {
	$("#dop_contacts_zavisimost").css('display', 'none');
}
function dop_contacts_zavisimost(radio3) {
  var valzn3 = radio3.value;
    if (valzn3 == 1) {
       $("#dop_contacts_zavisimost").fadeIn(100);
   } else {
       $("#dop_contacts_zavisimost").fadeOut(100);
   } 
}

var radiocheckedval4 = $("#header_menu_zavisimost_radio:checked").attr("value");
if (radiocheckedval4 == 1) {
	$("#header_menu_zavisimost").css('display', 'block');
} else {
	$("#header_menu_zavisimost").css('display', 'none');
}
function header_menu_zavisimost(radio4) {
  var valzn4 = radio4.value;
    if (valzn4 == 1) {
       $("#header_menu_zavisimost").fadeIn(100);
   } else {
       $("#header_menu_zavisimost").fadeOut(100);
   } 
}

var radiocheckedval5 = $("#zavisimost5_radio:checked").attr("value");
if (radiocheckedval5 == 1) {
	$("#zavisimost5").css('display', 'block');
} else {
	$("#zavisimost5").css('display', 'none');
}
function zavisimost5(radio5) {
  var valzn5 = radio5.value;
    if (valzn5 == 1) {
       $("#zavisimost5").fadeIn(100);
   } else {
       $("#zavisimost5").fadeOut(100);
   }
}

var radiocheckedval6 = $("#zavisimost6_radio:checked").attr("value");
if (radiocheckedval6 == 1) {
	$("#zavisimost6").css('display', 'block');
} else {
	$("#zavisimost6").css('display', 'none');
}
function zavisimost6(radio6) {
  var valzn6 = radio6.value;
    if (valzn6 == 1) {
       $("#zavisimost6").fadeIn(100);
   } else {
       $("#zavisimost6").fadeOut(100);
   } 
}
//--></script>
<script type="text/javascript"><!--
<?php if ($ckeditor) { ?>
CKEDITOR.replace('input-description');
CKEDITOR.replace('input-modal-text');
<?php } else { ?>
$('#input-description').summernote({height: 300});
$('#input-modal-text').summernote({height: 300});
<?php } ?>
	function apply(){
		$(".alert").remove();
		$.post($("#form-revolution").attr('action'), $("#form-revolution").serialize(), function(html) {
			var $success = $(html).find(".alert-success, .alert-danger");
			if ($success.length > 0) {
				$(".alert-helper").before($success);
			}
		});
	}
//--></script>
<script type="text/javascript"><!--
function showhide() {
  var $select = $('#select');
  var selectedValue = $select.val();
  $("#rowfeatured")[selectedValue == 'featured' ? 'show' : 'hide'] ();
  $("#catonly")[selectedValue == 'featured' ? 'hide' : 'show'] ();
}
$('input[name=\'product\']').autocomplete({
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	select: function(item) {
      $('#featured-product' + item.value).remove();
      $('#featured-product').append('<div id="featured-product' +  item.value + '">' + item.label + ' <i class="fa fa-minus-circle"></i><input type="hidden" value="' + item.value + '" /></div>');
      $('#featured-product div:odd').attr('class', 'odd');
      $('#featured-product div:even').attr('class', 'even');
      var data = $.map($('#featured-product input'), function(element){
        return $(element).attr('value');
      });
      $('input[name=\'revtheme_slider_1[featured]\']').attr('value', data.join());
	}
});
$('.scrollbox').on('click', '.fa-minus-circle', function() {
	$(this).parent().remove();
	$('#featured-product div:odd').attr('class', 'odd');
	$('#featured-product div:even').attr('class', 'even');
	var data = $.map($('#featured-product input'), function(element){
		return $(element).attr('value');
	});
	$('input[name=\'revtheme_slider_1[featured]\']').attr('value', data.join());
});
function showhide2() {
  var $select = $('#select2');
  var selectedValue = $select.val();
  $("#rowfeatured2")[selectedValue == 'featured' ? 'show' : 'hide'] ();
  $("#catonly2")[selectedValue == 'featured' ? 'hide' : 'show'] ();
}
$('input[name=\'product2\']').autocomplete({
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	select: function(item) {
      $('#featured-product2' + item.value).remove();
      $('#featured-product2').append('<div id="featured-product2' +  item.value + '">' + item.label + ' <i class="fa fa-minus-circle"></i><input type="hidden" value="' + item.value + '" /></div>');
      $('#featured-product2 div:odd').attr('class', 'odd');
      $('#featured-product2 div:even').attr('class', 'even');
      var data = $.map($('#featured-product2 input'), function(element){
        return $(element).attr('value');
      });
      $('input[name=\'revtheme_slider_2[featured]\']').attr('value', data.join());
	}
});
$('.scrollbox2').on('click', '.fa-minus-circle', function() {
	$(this).parent().remove();
	$('#featured-product2 div:odd').attr('class', 'odd');
	$('#featured-product2 div:even').attr('class', 'even');
	var data = $.map($('#featured-product2 input'), function(element){
		return $(element).attr('value');
	});
	$('input[name=\'revtheme_slider_2[featured]\']').attr('value', data.join());
});
function showhide3() {
  var $select = $('#select3');
  var selectedValue = $select.val();
  $("#rowfeatured3")[selectedValue == 'featured' ? 'show' : 'hide'] ();
  $("#catonly3")[selectedValue == 'featured' ? 'hide' : 'show'] ();
}
$('input[name=\'product3\']').autocomplete({
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	select: function(item) {
      $('#featured-product3' + item.value).remove();
      $('#featured-product3').append('<div id="featured-product3' +  item.value + '">' + item.label + ' <i class="fa fa-minus-circle"></i><input type="hidden" value="' + item.value + '" /></div>');
      $('#featured-product3 div:odd').attr('class', 'odd');
      $('#featured-product3 div:even').attr('class', 'even');
      var data = $.map($('#featured-product3 input'), function(element){
        return $(element).attr('value');
      });
      $('input[name=\'revtheme_slider_3[featured]\']').attr('value', data.join());
	}
});
$('.scrollbox3').on('click', '.fa-minus-circle', function() {
	$(this).parent().remove();
	$('#featured-product3 div:odd').attr('class', 'odd');
	$('#featured-product3 div:even').attr('class', 'even');
	var data = $.map($('#featured-product3 input'), function(element){
		return $(element).attr('value');
	});
	$('input[name=\'revtheme_slider_3[featured]\']').attr('value', data.join());
});
function showhide4() {
  var $select = $('#select4');
  var selectedValue = $select.val();
  $("#rowfeatured4")[selectedValue == 'featured' ? 'show' : 'hide'] ();
  $("#catonly4")[selectedValue == 'featured' ? 'hide' : 'show'] ();
}
$('input[name=\'product4\']').autocomplete({
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	select: function(item) {
      $('#featured-product4' + item.value).remove();
      $('#featured-product4').append('<div id="featured-product4' +  item.value + '">' + item.label + ' <i class="fa fa-minus-circle"></i><input type="hidden" value="' + item.value + '" /></div>');
      $('#featured-product4 div:odd').attr('class', 'odd');
      $('#featured-product4 div:even').attr('class', 'even');
      var data = $.map($('#featured-product4 input'), function(element){
        return $(element).attr('value');
      });
      $('input[name=\'revtheme_slider_4[featured]\']').attr('value', data.join());
	}
});
$('.scrollbox4').on('click', '.fa-minus-circle', function() {
	$(this).parent().remove();
	$('#featured-product4 div:odd').attr('class', 'odd');
	$('#featured-product4 div:even').attr('class', 'even');
	var data = $.map($('#featured-product4 input'), function(element){
		return $(element).attr('value');
	});
	$('input[name=\'revtheme_slider_4[featured]\']').attr('value', data.join());
});
function showhide5() {
  var $select = $('#select5');
  var selectedValue = $select.val();
  $("#rowfeatured5")[selectedValue == 'featured' ? 'show' : 'hide'] ();
  $("#catonly5")[selectedValue == 'featured' ? 'hide' : 'show'] ();
}
$('input[name=\'product5\']').autocomplete({
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	select: function(item) {
      $('#featured-product5' + item.value).remove();
      $('#featured-product5').append('<div id="featured-product5' +  item.value + '">' + item.label + ' <i class="fa fa-minus-circle"></i><input type="hidden" value="' + item.value + '" /></div>');
      $('#featured-product5 div:odd').attr('class', 'odd');
      $('#featured-product5 div:even').attr('class', 'even');
      var data = $.map($('#featured-product5 input'), function(element){
        return $(element).attr('value');
      });
      $('input[name=\'revtheme_slider_5[featured]\']').attr('value', data.join());
	}
});
$('.scrollbox5').on('click', '.fa-minus-circle', function() {
	$(this).parent().remove();
	$('#featured-product5 div:odd').attr('class', 'odd');
	$('#featured-product5 div:even').attr('class', 'even');
	var data = $.map($('#featured-product5 input'), function(element){
		return $(element).attr('value');
	});
	$('input[name=\'revtheme_slider_5[featured]\']').attr('value', data.join());
});

var item_row_main = $('tr.item_row_main').size()+11;
function addBlockItem() {

	html  = '<tr id="item-row-main' + item_row_main + '" class="item_row_main">';
	html += '<td class="text-center"><span class="fa_icon" id="icon_banner_' + item_row_main + '" onclick="fa_icons($(this).attr(\'id\'))"><i class="fa fa-opencart"></i></span><input type="hidden" name="revtheme_blocks_home_item[' + item_row_main + '][image]" value="" id="input-block-image' + item_row_main + '" /></td>';
	html += '<td class="text-left">';
	html += '<input class="form-control" type="text" name="revtheme_blocks_home_item[' + item_row_main + '][title]" value="" />';
	html += '</td>';
	html += '<td class="text-left">';
	html += '<input class="form-control" type="text" name="revtheme_blocks_home_item[' + item_row_main + '][description]" value="" />';
	html += '</td>';	
	html += '<td class="text-left">';
	html += '<input class="form-control"  type="text" name="revtheme_blocks_home_item[' + item_row_main + '][link]" value="" />';
	html += '</td>';
	html += '<td class="text-left"><input class="form-control" type="text" name="revtheme_blocks_home_item[' + item_row_main + '][sort]" size="1" value="" /></td>';
	html += '<td class="text-right"><a class="btn btn-danger" onclick="$(\'#t_home_blocks #item-row-main' + item_row_main  + '\').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a></td>';
	html += '</tr>'; 
	
	$('#t_home_blocks tbody').append(html);
	
	item_row_main++;
}

var contact_row = $('tr.contact_row').size()+1;
function addDopContact() {
	
	html  = '<tr id="item-row-dop_contact' + contact_row + '" class="contact_row">';
	html += '<td class="text-center"><span class="fa_icon" id="icon_dop_contact_' + contact_row + '" onclick="fa_icons($(this).attr(\'id\'))"><i class="fa fa-opencart"></i></span><input type="hidden" name="revtheme_header_dop_contact[' + contact_row + '][icon]" value="" id="input-icon' + contact_row + '" /></td>';
	html += '<td class="text-left"><input class="form-control" type="text" name="revtheme_header_dop_contact[' + contact_row + '][number]" value="" placeholder="Телефон ' + contact_row + '" /></td>';
	html += '<td class="text-right"><a class="btn btn-danger" onclick="$(\'#t_dop_contacts #item-row-dop_contact' + contact_row  + '\').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a></td>';
	html += '</tr>'; 
		
	$('#t_dop_contacts tbody').append(html);
	
	contact_row++;
}

var item_row_main_icon = $('tr.item_row_main_icon').size()+1;
function addBlockItemIcon() {
	html  = '<tr id="item-row-main' + item_row_main_icon + '" class="item_row_main_icon">';
	html += '<td class="text-center"><a href="" id="thumb-image' + item_row_main_icon + '" data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>"  /></a><input type="hidden" name="revtheme_footer_icon[' + item_row_main_icon + '][image]" value="" id="input-icon-image' + item_row_main_icon + '" /></td>';
	html += '<td class="text-left"><input class="form-control" type="text" name="revtheme_footer_icon[' + item_row_main_icon + '][sort]" size="1" value="" /></td>';
	html += '<td class="text-right"><a class="btn btn-danger" onclick="$(\'#t_footer_icons #item-row-main' + item_row_main_icon  + '\').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a></td>';
	html += '</tr>'; 
	
	$('#t_footer_icons tbody').append(html);;	
	item_row_main_icon++;
}

var item_row_main_soc = $('tr.item_row_main_soc').size()+1;
function addBlockItemSoc() {
	html  = '<tr id="item-row-main' + item_row_main_soc + '" class="item_row_main_soc">';
	html += '<td class="text-center"><span class="fa_icon" id="icon_banner_' + item_row_main_soc + '" onclick="fa_icons($(this).attr(\'id\'))"><i class="fa fa-opencart"></i></span><input type="hidden" name="revtheme_footer_soc[' + item_row_main_soc + '][image]" value="" id="input-soc-image' + item_row_main_soc + '" /></td>';
	html += '<td class="text-left">';
	html += '<input class="form-control"  type="text" name="revtheme_footer_soc[' + item_row_main_soc + '][link]" value="" />';
	html += '</td>';
	html += '<td class="text-left">';
	html += '<input class="form-control"  type="text" name="revtheme_footer_soc[' + item_row_main_soc + '][title]" value="" />';
	html += '</td>';
	html += '<td class="text-left"><input class="form-control" type="text" name="revtheme_footer_soc[' + item_row_main_soc + '][sort]" size="1" value="" /></td>';
	html += '<td class="text-right"><a class="btn btn-danger" onclick="$(\'#t_footer_socs #item-row-main' + item_row_main_soc  + '\').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a></td>';
	html += '</tr>'; 
	
	$('#t_footer_socs tbody').append(html);;	
	item_row_main_soc++;
}

var item_row_header_link = $('tr.item_row_header_link').size()+1;
function addBlockHeaderLink() {
	html  = '<tr id="item-row-main' + item_row_header_link + '" class="item_row_header_link">';
	html += '<td class="text-left">';
	html += '<input class="form-control"  type="text" name="revtheme_header_link[' + item_row_header_link + '][link]" value="" />';
	html += '</td>';
	html += '<td class="text-left">';
	html += '<input class="form-control"  type="text" name="revtheme_header_link[' + item_row_header_link + '][title]" value="" />';
	html += '</td>';
	html += '<td class="text-left"><input class="form-control" type="text" name="revtheme_header_link[' + item_row_header_link + '][sort]" size="1" value="" /></td>';
	html += '<td class="text-right"><a class="btn btn-danger" onclick="$(\'#t_header_links #item-row-main' + item_row_header_link  + '\').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a></td>';
	html += '</tr>'; 
	
	$('#t_header_links tbody').append(html);;	
	item_row_header_link++;
}

var item_row_header_link2 = $('tr.item_row_header_link2').size()+1;
function addBlockHeaderLink2() {
	html  = '<tr id="item-row-main' + item_row_header_link2 + '" class="item_row_header_link2">';
	html += '<td class="text-left">';
	html += '<input class="form-control"  type="text" name="revtheme_header_link2[' + item_row_header_link2 + '][link]" value="" />';
	html += '</td>';
	html += '<td class="text-left">';
	html += '<input class="form-control"  type="text" name="revtheme_header_link2[' + item_row_header_link2 + '][title]" value="" />';
	html += '</td>';
	html += '<td class="text-left"><input class="form-control" type="text" name="revtheme_header_link2[' + item_row_header_link2 + '][sort]" size="1" value="" /></td>';
	html += '<td class="text-right"><a class="btn btn-danger" onclick="$(\'#t_header_links2 #item-row-main' + item_row_header_link2  + '\').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a></td>';
	html += '</tr>'; 
	
	$('#t_header_links2 tbody').append(html);;	
	item_row_header_link2++;
}

var item_row_footer_link = $('tr.item_row_footer_link').size()+1;
function addBlockFooterLink() {
	html  = '<tr id="item-row-main' + item_row_footer_link + '" class="item_row_footer_link">';
	html += '<td class="text-left">';
	html += '<input class="form-control"  type="text" name="revtheme_footer_link[' + item_row_footer_link + '][link]" value="" />';
	html += '</td>';
	html += '<td class="text-left">';
	html += '<input class="form-control"  type="text" name="revtheme_footer_link[' + item_row_footer_link + '][title]" value="" />';
	html += '</td>';
	html += '<td class="text-left"><input class="form-control" type="text" name="revtheme_footer_link[' + item_row_footer_link + '][sort]" size="1" value="" /></td>';
	html += '<td class="text-right"><a class="btn btn-danger" onclick="$(\'#t_footer_links #item-row-main' + item_row_footer_link  + '\').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a></td>';
	html += '</tr>'; 
	
	$('#t_footer_links tbody').append(html);;	
	item_row_footer_link++;
}

var item_row_product_main = $('tr.item_row_product_main').size()+11;
function addBlockProductItem() {

	html  = '<tr id="item-row-product-main' + item_row_product_main + '" class="item_row_product_main">';
	html += '<td class="text-center"><span class="fa_icon" id="icon_banner_' + item_row_product_main + '" onclick="fa_icons($(this).attr(\'id\'))"><i class="fa fa-opencart"></i></span><input type="hidden" name="revtheme_blocks_product_item[' + item_row_product_main + '][image]" value="" id="input-block-image' + item_row_product_main + '" /></td>';
	html += '<td class="text-left">';
	html += '<input class="form-control" type="text" name="revtheme_blocks_product_item[' + item_row_product_main + '][title]" value="" />';
	html += '</td>';
	html += '<td class="text-left">';
	html += '<input class="form-control" type="text" name="revtheme_blocks_product_item[' + item_row_product_main + '][description]" value="" />';
	html += '</td>';	
	html += '<td class="text-left">';
	html += '<input class="form-control"  type="text" name="revtheme_blocks_product_item[' + item_row_product_main + '][link]" value="" />';
	html += '</td>';
	html += '<td class="text-left"><input class="form-control" type="text" name="revtheme_blocks_product_item[' + item_row_product_main + '][sort]" size="1" value="" /></td>';
	html += '<td class="text-right"><a class="btn btn-danger" onclick="$(\'#t_product_blocks #item-row-product-main' + item_row_product_main  + '\').remove();" data-toggle="tooltip" title="Удалить"><i class="fa fa-trash-o"></i></a></td>';
	html += '</tr>'; 
	
	$('#t_product_blocks tbody').append(html);
	
	item_row_product_main++;
}
//--></script>
<input type="hidden" class="target" value="" />
<input type="hidden" class="class" value="" />
<div class="fa_icons">
<div class="fontawesome-icon-list">
<div class="clearfix" style="font-size:14px;padding-bottom:5px;">Web Application Icons</div>
<div class="col-xs-1"><i class="fa fa-adjust" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-american-sign-language-interpreting" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-anchor" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-archive" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-area-chart" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-arrows" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-arrows-h" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-arrows-v" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-asl-interpreting" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-assistive-listening-systems" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-asterisk" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-at" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-audio-description" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-automobile" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-balance-scale" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-ban" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bank" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bar-chart" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bar-chart-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-barcode" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bars" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-battery-0" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-battery-1" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-battery-2" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-battery-3" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-battery-4" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-battery-empty" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-battery-full" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-battery-half" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-battery-quarter" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-battery-three-quarters" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bed" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-beer" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bell" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bell-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bell-slash" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bell-slash-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bicycle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-binoculars" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-birthday-cake" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-blind" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bluetooth" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bluetooth-b" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bolt" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bomb" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-book" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bookmark" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bookmark-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-braille" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-briefcase" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bug" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-building" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-building-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bullhorn" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bullseye" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bus" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cab" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-calculator" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-calendar" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-calendar-minus-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-calendar-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-calendar-times-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-camera" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-camera-retro" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-car" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-caret-square-o-down" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-caret-square-o-left" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-caret-square-o-up" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cart-plus" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cc" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-certificate" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-check" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-check-circle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-check-circle-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-check-square" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-child" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-circle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-circle-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-circle-o-notch" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-circle-thin" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-clone" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-close" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cloud" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cloud-download" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cloud-upload" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-code" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-code-fork" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-coffee" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cog" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cogs" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-comment" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-comment-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-commenting" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-commenting-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-comments" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-comments-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-compass" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-copyright" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-creative-commons" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-credit-card" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-crop" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-crosshairs" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cube" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cubes" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cutlery" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-dashboard" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-database" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-deaf" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-deafness" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-desktop" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-diamond" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-dot-circle-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-download" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-edit" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-envelope" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-envelope-square" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-eraser" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-exchange" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-exclamation" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-external-link" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-external-link-square" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-eye" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-eye-slash" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-eyedropper" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-fax" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-feed" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-female" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-fighter-jet" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-archive-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-audio-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-code-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-excel-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-image-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-movie-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-photo-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-picture-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-powerpoint-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-sound-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-video-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-word-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-zip-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-film" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-filter" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-fire" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-fire-extinguisher" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-flag" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-flag-checkered" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-flag-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-flash" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-flask" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-folder" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-folder-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-folder-open" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-folder-open-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-frown-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-futbol-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-gamepad" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-gavel" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-gear" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-gears" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-gift" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-glass" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-globe" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-graduation-cap" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-group" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-grab-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-lizard-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-paper-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-peace-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-pointer-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-rock-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-scissors-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-spock-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-stop-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hard-of-hearing" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hashtag" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hdd-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-headphones" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-heart" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-heart-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-heartbeat" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-history" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-home" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hotel" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hourglass" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hourglass-1" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hourglass-2" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hourglass-3" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hourglass-end" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hourglass-half" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hourglass-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hourglass-start" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-i-cursor" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-image" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-inbox" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-industry" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-info" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-info-circle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-institution" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-key" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-keyboard-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-language" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-laptop" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-leaf" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-legal" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-lemon-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-level-down" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-level-up" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-life-bouy" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-life-buoy" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-life-ring" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-life-saver" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-line-chart" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-location-arrow" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-lock" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-low-vision" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-magic" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-magnet" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-mail-forward" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-mail-reply" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-mail-reply-all" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-male" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-map" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-map-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-map-pin" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-map-signs" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-meh-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-microphone" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-microphone-slash" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-minus" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-minus-circle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-minus-square" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-minus-square-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-mobile" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-mobile-phone" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-money" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-moon-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-mortar-board" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-motorcycle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-mouse-pointer" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-music" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-navicon" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-newspaper-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-object-group" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-object-ungroup" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-paint-brush" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-paper-plane" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-paw" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-pencil" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-pencil-square" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-percent" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-phone" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-phone-square" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-photo" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-picture-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-pie-chart" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-plane" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-plug" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-plus" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-plus-circle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-plus-square" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-plus-square-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-power-off" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-print" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-puzzle-piece" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-qrcode" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-question" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-question-circle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-question-circle-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-quote-right" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-random" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-recycle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-refresh" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-registered" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-remove" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-reorder" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-reply" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-reply-all" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-retweet" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-road" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-rocket" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-rss" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-rss-square" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-search" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-send" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-send-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-server" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-share" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-share-alt" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-share-alt-square" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-share-square" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-share-square-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-shield" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-ship" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-shopping-bag" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-shopping-basket" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-shopping-cart" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sign-in" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sign-language" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sign-out" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-signal" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-signing" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sitemap" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sliders" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-smile-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-soccer-ball-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sort" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sort-alpha-asc" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sort-alpha-desc" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sort-amount-asc" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sort-asc" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sort-desc" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sort-down" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sort-up" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-space-shuttle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-spinner" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-spoon" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-square" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-square-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-star" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-star-half" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-star-half-empty" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-star-half-full" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-star-half-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-star-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sticky-note" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sticky-note-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-street-view" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-suitcase" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sun-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-support" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-tablet" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-tachometer" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-tag" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-tags" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-tasks" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-taxi" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-television" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-terminal" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-thumb-tack" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-thumbs-down" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-thumbs-up" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-ticket" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-times" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-times-circle-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-tint" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-toggle-down" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-toggle-left" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-toggle-off" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-toggle-on" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-toggle-right" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-toggle-up" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-trademark" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-trash" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-trash-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-tree" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-trophy" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-truck" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-tty" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-tv" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-umbrella" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-universal-access" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-university" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-unlock" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-unlock-alt" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-unsorted" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-upload" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-user" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-user-plus" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-user-secret" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-user-times" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-users" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-video-camera" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-volume-control-phone" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-volume-down" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-volume-off" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-volume-up" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-warning" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-wheelchair" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-wheelchair-alt" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-wifi" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-wrench" aria-hidden="true"></i></div>
<hr style="clear: both;"><div class="clearfix" style="font-size:14px;padding-bottom:5px;">Accessibility Icons</div>
<div class="col-xs-1"><i class="fa fa-american-sign-language-interpreting" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-asl-interpreting" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-assistive-listening-systems" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-audio-description" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-blind" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-braille" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cc" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-deaf" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-deafness" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hard-of-hearing" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-low-vision" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-question-circle-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sign-language" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-signing" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-tty" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-universal-access" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-volume-control-phone" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-wheelchair" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-wheelchair-alt" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-grab-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-lizard-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-o-down" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-o-left" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-o-right" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-o-up" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-paper-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-peace-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-pointer-o" aria-hidden="true"></i></div>
<hr style="clear: both;"><div class="clearfix" style="font-size:14px;padding-bottom:5px;">Hand Icons</div>
<div class="col-xs-1"><i class="fa fa-hand-rock-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-scissors-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-spock-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-hand-stop-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-thumbs-down" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-thumbs-up" aria-hidden="true"></i></div>
<hr style="clear: both;"><div class="clearfix" style="font-size:14px;padding-bottom:5px;">Transportation Icons</div>
<div class="col-xs-1"><i class="fa fa-ambulance" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-automobile" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bicycle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bus" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cab" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-car" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-fighter-jet" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-motorcycle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-plane" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-rocket" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-ship" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-space-shuttle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-subway" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-taxi" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-train" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-truck" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-wheelchair" aria-hidden="true"></i></div>
<hr style="clear: both;"><div class="clearfix" style="font-size:14px;padding-bottom:5px;">Gender Icons</div>
<div class="col-xs-1"><i class="fa fa-genderless" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-intersex" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-mars" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-mars-double" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-mars-stroke" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-mars-stroke-h" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-mars-stroke-v" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-mercury" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-neuter" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-transgender" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-transgender-alt" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-venus" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-venus-double" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-venus-mars" aria-hidden="true"></i></div>
<hr style="clear: both;"><div class="clearfix" style="font-size:14px;padding-bottom:5px;">File Type Icons</div>
<div class="col-xs-1"><i class="fa fa-file" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-archive-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-audio-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-code-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-excel-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-image-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-movie-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-photo-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-picture-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-powerpoint-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-sound-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-text" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-text-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-video-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-word-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-file-zip-o" aria-hidden="true"></i></div>
<hr style="clear: both;"><div class="clearfix" style="font-size:14px;padding-bottom:5px;">Spinner Icons</div>
<div class="col-xs-1"><i class="fa fa-circle-o-notch" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cog" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-gear" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-refresh" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-spinner" aria-hidden="true"></i></div>
<hr style="clear: both;"><div class="clearfix" style="font-size:14px;padding-bottom:5px;">Form Control Icons</div>
<div class="col-xs-1"><i class="fa fa-check-square" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-circle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-circle-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-dot-circle-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-minus-square" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-minus-square-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-plus-square" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-plus-square-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-square" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-square-o" aria-hidden="true"></i></div>
<hr style="clear: both;"><div class="clearfix" style="font-size:14px;padding-bottom:5px;">Payment Icons</div>
<div class="col-xs-1"><i class="fa fa-cc-amex" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cc-diners-club" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cc-discover" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cc-jcb" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cc-mastercard" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cc-paypal" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cc-stripe" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cc-visa" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-credit-card" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-google-wallet" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-paypal" aria-hidden="true"></i></div>
<hr style="clear: both;"><div class="clearfix" style="font-size:14px;padding-bottom:5px;">Chart Icons</div>
<div class="col-xs-1"><i class="fa fa-area-chart" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bar-chart" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bar-chart-o" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-line-chart" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-pie-chart" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bitcoin" aria-hidden="true"></i></div>
<hr style="clear: both;"><div class="clearfix" style="font-size:14px;padding-bottom:5px;">Currency Icons</div>
<div class="col-xs-1"><i class="fa fa-btc" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-cny" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-dollar" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-eur" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-euro" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-gbp" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-gg" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-gg-circle" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-ils" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-inr" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-jpy" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-krw" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-money" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-rmb" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-rouble" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-rub" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-ruble" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-rupee" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-shekel" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-sheqel" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-try" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-turkish-lira" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-usd" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-won" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-yen" aria-hidden="true"></i></div>
<hr style="clear: both;"><div class="clearfix" style="font-size:14px;padding-bottom:5px;">Text Editor Icons</div>
<div class="col-xs-1"><i class="fa fa-align-center" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-align-justify" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-align-left" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-align-right" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-bold" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-chain" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-chain-broken" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-clipboard" aria-hidden="true"></i></div>
<div class="col-xs-1"><i class="fa fa-columns" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-copy" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-cut" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-dedent" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-eraser" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-file" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-file-o" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-file-text" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-file-text-o" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-files-o" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-floppy-o" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-font" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-header" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-indent" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-italic" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-link" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-list" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-list-alt" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-list-ol" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-list-ul" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-outdent" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-paperclip" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-paragraph" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-paste" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-repeat" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-rotate-left" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-rotate-right" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-save" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-scissors" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-strikethrough" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-subscript" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-superscript" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-table" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-text-height" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-text-width" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-th" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-th-large" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-th-list" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-underline" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-undo" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-unlink" aria-hidden="true"></i></div>

<hr style="clear: both;"><div class="clearfix" style="font-size:14px;padding-bottom:5px;">Directional Icons</div>

<div class="col-xs-1"><i class="fa fa-angle-double-down" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-angle-double-left" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-angle-double-right" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-angle-double-up" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-angle-down" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-angle-left" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-angle-right" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-angle-up" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrow-circle-down" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrow-left" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrow-right" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrow-up" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrows" aria-hidden="true"></i></div>

<hr style="clear: both;"><div class="clearfix" style="font-size:14px;padding-bottom:5px;">Video Player Icons</div>

<div class="col-xs-1"><i class="fa fa-arrows-alt" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrows-h" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrows-v" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-caret-down" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-caret-left" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-caret-right" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-caret-square-o-down" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-caret-square-o-left" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-caret-square-o-up" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-caret-up" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-exchange" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-hand-o-down" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-hand-o-left" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-hand-o-right" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-hand-o-up" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-long-arrow-down" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-toggle-down" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-toggle-left" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-toggle-right" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-toggle-up" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-arrows-alt" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-backward" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-compress" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-eject" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-expand" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-fast-backward" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-fast-forward" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-forward" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-pause" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-pause-circle" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-pause-circle-o" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-play" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-play-circle" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-play-circle-o" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-random" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-step-backward" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-step-forward" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-stop" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-stop-circle" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-stop-circle-o" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-youtube-play" aria-hidden="true"></i></div>

<hr style="clear: both;"><div class="clearfix" style="font-size:14px;padding-bottom:5px;">Brand Icons</div>

<div class="col-xs-1"><i class="fa fa-500px" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-adn" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-amazon" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-android" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-angellist" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-apple" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-behance" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-behance-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-bitbucket" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-bitbucket-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-bitcoin" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-black-tie" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-bluetooth" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-bluetooth-b" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-btc" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-buysellads" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-cc-amex" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-cc-diners-club" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-cc-discover" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-cc-jcb" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-cc-mastercard" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-cc-paypal" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-cc-stripe" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-cc-visa" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-chrome" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-codepen" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-codiepie" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-connectdevelop" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-contao" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-css3" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-dashcube" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-delicious" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-deviantart" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-digg" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-dribbble" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-dropbox" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-drupal" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-edge" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-empire" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-envira" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-expeditedssl" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-facebook" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-facebook-f" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-facebook-official" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-facebook-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-firefox" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-first-order" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-flickr" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-fonticons" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-fort-awesome" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-forumbee" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-foursquare" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-ge" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-get-pocket" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-gg" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-gg-circle" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-git" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-git-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-github" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-github-alt" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-github-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-gitlab" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-gittip" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-glide" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-glide-g" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-google" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-google-plus" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-google-plus-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-google-wallet" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-gratipay" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-hacker-news" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-houzz" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-html5" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-instagram" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-internet-explorer" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-ioxhost" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-joomla" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-jsfiddle" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-lastfm" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-lastfm-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-leanpub" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-linkedin" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-linkedin-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-linux" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-maxcdn" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-meanpath" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-medium" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-mixcloud" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-modx" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-odnoklassniki-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-opencart" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-openid" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-opera" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-optin-monster" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-pagelines" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-paypal" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-pied-piper" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-pied-piper-alt" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-pied-piper-pp" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-pinterest" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-pinterest-p" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-pinterest-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-product-hunt" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-qq" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-ra" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-rebel" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-reddit" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-reddit-alien" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-reddit-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-renren" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-resistance" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-safari" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-scribd" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-sellsy" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-share-alt" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-share-alt-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-simplybuilt" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-skyatlas" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-skype" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-slack" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-slideshare" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-snapchat" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-snapchat-ghost" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-snapchat-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-soundcloud" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-spotify" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-stack-exchange" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-stack-overflow" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-steam" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-steam-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-stumbleupon" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-stumbleupon-circle" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-tencent-weibo" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-themeisle" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-trello" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-tripadvisor" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-tumblr" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-tumblr-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-twitch" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-twitter" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-twitter-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-usb" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-viacoin" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-viadeo" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-viadeo-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-vimeo" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-vimeo-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-vine" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-vk" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-wechat" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-weibo" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-weixin" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-whatsapp" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-wikipedia-w" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-windows" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-wordpress" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-wpbeginner" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-wpforms" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-xing" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-xing-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-y-combinator" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-y-combinator-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-yahoo" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-yc" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-yc-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-yelp" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-yoast" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-youtube" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-youtube-play" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-youtube-square" aria-hidden="true"></i></div>

<hr style="clear: both;"><div class="clearfix" style="font-size:14px;padding-bottom:5px;">Medical Icons</div>

<div class="col-xs-1"><i class="fa fa-ambulance" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-h-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-heart" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-heart-o" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-heartbeat" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-hospital-o" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-medkit" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-plus-square" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-stethoscope" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-user-md" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-wheelchair" aria-hidden="true"></i></div>

<div class="col-xs-1"><i class="fa fa-meh-o" aria-hidden="true"></i></div>
</div>
</div>
<script>
	function fa_icons(id) {
		$('.fa_icons').popup('show');
		$('.target').val(id);
		
		$('.fa_icons .fa').on('click', function() {
			var this_class = $(this).attr('class');
			$('.class').val(this_class);
			save_icons();
			$('.fa_icons').popup('hide');
		});
	}
	
	function save_icons() {
		var target = $('.target').val();
		var this_class = $('.class').val();
		$('#'+target).html('<i class="'+this_class+'"></i>');
		$('#'+target).next().val(this_class);
	}
</script>
<?php echo $footer; ?>