<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
		<a onclick="apply()" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="Применить"><i class="fa fa-check"></i></a>
        <button type="submit" form="form-blog" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default alert-helper">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> Лицензия для шаблона Revolution (<?php echo $_SERVER['HTTP_HOST'] ?>)</h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-revthemel_license" class="form-horizontal">
			<div class="form-group">
			  <label class="col-sm-2 control-label">Введите лицензионный ключ:</label>
			  <div class="col-sm-3">
				<input type="text" name="revthemel_license" value="<?php echo (isset($revthemel_license)) ? $revthemel_license : ''; ?>" class="form-control" />
				<?php if ($error_revthemel_license) { ?>
				<div class="text-danger"><?php echo $error_revthemel_license; ?></div>
				<?php } ?>
			  </div>
			</div>				
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
	function apply(){
		$(".alert").remove();
		$.post($("#form-revthemel_license").attr('action'), $("#form-revthemel_license").serialize(), function(html) {
			var $success = $(html).find(".alert-success, .alert-danger");
			if ($success.length > 0) {
				$(".alert-helper").before($success);
			}
		});
	}
//--></script>
<?php echo $footer; ?>