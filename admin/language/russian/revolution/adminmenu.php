<?php
$_['text_revtheme']              	= 'Шаблон Revolution';
$_['text_revtheme_setting']         = 'Основные настройки';
$_['text_revblog']              	= 'Модуль новостей';
$_['text_revblog_setting'] 			= 'Настройки модуля';
$_['text_revblog_list'] 			= 'Список новостей';
$_['text_product_tab']              = 'Доп. вкладки для товаров';
$_['text_revpopupphone']            = 'Заказы обратного звонка';
$_['text_magazin']           		= 'Магазин';
$_['text_magazin_reviews']          = 'Отзывы на магазин';
$_['text_zakaz_obratn_zvonok']      = 'Заказы обратн. звонка';

///Доп вкладки для товара
$_['tab_extra_tab']          = 'Дополнительные вкладки';
$_['tab_module']          	 = 'Вкладка';
$_['button_module_add']      = 'Добавить';
$_['entry_heading']     	 = 'Заголовок';
$_['error_tab']              = 'Введите заголовок вкладки!';
//