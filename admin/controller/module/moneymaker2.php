<?php
class ControllerModuleMoneymaker2 extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/moneymaker2');

		$this->document->setTitle($this->language->get('heading_title'));

        $this->document->addStyle('../catalog/view/theme/moneymaker2/stylesheet/animate.min.css');
        $this->document->addStyle('view/javascript/jquery/colpick.moneymaker2/colpick.css');
        $this->document->addScript('view/javascript/jquery/colpick.moneymaker2/colpick.js');

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            $this->model_setting_setting->editSetting('moneymaker2', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

            if(isset($this->request->post['apply']) and $this->request->post['apply'])
                $this->response->redirect($this->url->link('module/moneymaker2', 'token=' . $this->session->data['token'], 'SSL'));
            else
                $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

        $text_strings = array(
            'heading_title',

            'text_edit',
            'text_enabled',
            'text_disabled',
            'text_select',
            'text_yes',
            'text_no',

            'entry_width',
            'entry_height',
            'entry_status',
            'entry_mode',
            'entry_text',
            'entry_header',
            'entry_title',
            'entry_description',
            'entry_tooltip',
            'entry_icon',
            'entry_color',
            'entry_style',
            'entry_style_help',
            'entry_transparency',
            'entry_glowing',
            'entry_button_title',
            'entry_caption',
            'entry_display_caption',
            'entry_divider',
            'entry_optional',
            'entry_px',
            'entry_width',
            'entry_height',
            'entry_size',
            'entry_links',
            'entry_quicklinks',
            'entry_display_button',
            'entry_sort_order',
            'entry_category',
            'entry_categories',
            'entry_telephone',
            'entry_columns',
            'entry_screen_large',
            'entry_screen_medium',
            'entry_screen_small',
            'entry_screen_xsmall',
            'entry_screen_xxsmall',
            'entry_screen_xxxsmall',
            'entry_static',
            'entry_hover',
            'entry_active',
            'entry_specials',
            'entry_popular',
            'entry_new',
            'entry_rated',
            'entry_discount',
            'entry_difference',
            'entry_percentage',
            'entry_limit',
            'entry_days',
            'entry_background',
            'entry_rating',
            'entry_views',
            'entry_custom',
            'entry_field',
            'entry_list',
            'entry_grid',
            'entry_instock',
            'entry_other',
            'entry_layout',
            'entry_options',
            'entry_categories_options',
            'entry_products_options',
            'entry_cart',
            'entry_search',
            'entry_menu',
            'entry_top_menu',
            'entry_categories_menu',
            'entry_display_image',
            'entry_display_images',
            'entry_display_charts',
            'entry_display_model',
            'entry_display_brand',
            'entry_display_availability',
            'entry_display_rating',
            'entry_display_description',
            'entry_display_weight',
            'entry_display_dimensions',
            'entry_display_product_code',
            'entry_display_map',
            'entry_products',
            'entry_dropdowns',
            'entry_animation',
            'entry_animation_in',
            'entry_animation_out',
            'entry_add_banner',
            'entry_compare',
            'entry_delimiter',
            'entry_chart_line',
            'entry_chart_bar',
            'entry_attribute',
            'entry_unit',
            'entry_stock_status',
            'entry_stock_status_color',
            'entry_product_code',
            'entry_product_code_field',
            'entry_photogallery',
            'entry_main_points',
            'entry_manufacturer',
            'entry_tabs',
            'entry_image_size',
            'entry_advanced',
            'entry_input_mask',
            'entry_phone_mask_help',
            'entry_socials',
            'entry_target_blank',
            'entry_contacts',
            'entry_copyrights',
            'entry_year_foundation',
            'entry_address',
            'entry_code',
            'entry_custom_css',
            //'entry_custom_js',

            'entry_tab_common',
            'entry_tab_common_help',

            'entry_tab_product',
            'entry_tab_product_help',
            'entry_product_metatitles_enabled',
            'entry_product_gallery_moved',
            'entry_product_gallery_round',
            'entry_product_thumbs',
            'entry_product_main_points_help',
            'entry_product_points_stock_default',
            'entry_product_points_add_point_help',
            'entry_product_options_hide',
            'entry_product_tabs_help',
            'entry_product_tabs_display',
            'entry_product_tabs_display_help',
            'entry_product_tabs_headers',
            'entry_product_tabs_headers_help',
            'entry_product_tabs_attributes_responsive',

            'entry_common_buy',
            'entry_common_wishlist',
            'entry_common_compare',
            'entry_common_cart_outofstock_disabled',
            'entry_common_price_detached',
            'entry_common_browser_warned',
            'entry_common_categories_help',
            'entry_common_categories_icons',
            'entry_common_categories_icons_help',
            'entry_common_products_help',
            'entry_common_products_title_overflow',
            'entry_common_products_title_overflow_help',
            'entry_common_products_title_overflow_gradient',
            'entry_common_products_title_overflow_2lines',
            'entry_common_products_title_overflow_3lines',
            'entry_common_products_title_overflow_4lines',

            'entry_tab_header',
            'entry_header_help',
            'entry_header_strip_expanded',
            'entry_header_strip_font_color',
            'entry_header_contacts_help',
            'entry_header_contacts_phone_help',
            'entry_header_contacts_phone_highlight',
            'entry_header_contacts_caption_help',
            'entry_header_categories_help',
            'entry_header_categories_caption_help',
            'entry_header_categories_mode_help',
            'entry_header_categories_showchilds',
            'entry_header_categories_add_banner_help',
            'entry_header_categories_menu_help',
            'entry_header_cart_help',
            'entry_header_cart_custom',
            'entry_header_search_help',
            'entry_header_search_ajax',
            'entry_header_search_ajax_price',
            'entry_header_menu_help',

            'entry_tab_footer',
            'entry_footer_help',
            'entry_footer_display_information',
            'entry_footer_display_customer',
            'entry_footer_display_extras',
            'entry_footer_display_account',
            'entry_footer_display_copyrights',
            'entry_footer_display_powered',

            'entry_tab_catalog',
            'entry_tab_catalog_help',
            'entry_catalog_layout_default',
            'entry_catalog_layout_switcher',
            'entry_catalog_categories_metatitles_enabled',
            'entry_catalog_categories_move_description',
            'entry_catalog_contacts_help',
            'entry_catalog_contacts_call_title',
            'entry_catalog_contacts_mail_title',
            'entry_catalog_contacts_map_help',
            'entry_catalog_compare_charts_price_display',
            'entry_catalog_compare_chart_add',
            'entry_catalog_compare_chart_add_help',
            'entry_catalog_compare_chart_functions',
            'entry_catalog_compare_chart_sort_help',

            'entry_tab_modules',
            'entry_tab_modules_help',

            'entry_tab_sticker',
            'entry_tab_stickers',
            'entry_modules_stickers_help',
            'entry_modules_stickers_specials_help',
            'entry_modules_stickers_popular_help',
            'entry_modules_stickers_rated_help',
            'entry_modules_stickers_new_help',
            'entry_modules_customstickers_help',
            'entry_modules_promostickers_help',

            'entry_tab_promo',
            'entry_modules_promo_help',
            'entry_modules_promo_add_help',

            'entry_tab_quickorder',
            'entry_modules_quickorder_help',
            'entry_modules_quickorder_clear_cart',

            'entry_tab_callback',
            'entry_modules_callback_help',

            'entry_tab_help',
            'entry_help_text',

            'button_apply',
            'button_save',
            'button_cancel',
            'button_add',
            'button_remove',
            'button_view',
            'button_close',
            'button_link',

            'tab_data',
            'tab_image',

        );

        foreach ($text_strings as $text) {
            $data[$text] = $this->language->get($text);
        }



		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        // loading languages
        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();

        // loading articles
        $this->load->model('catalog/information');
        $data['informations'] = $this->model_catalog_information->getInformations();

        // loading categories
        $this->load->model('catalog/category');
        $data['categories'] = $this->model_catalog_category->getCategories(0);

        // loading attributes
        $this->load->model('catalog/attribute');
        $data['attributes'] = $this->model_catalog_attribute->getAttributes();
        $this->load->model('catalog/attribute_group');
        $data['attribute_groups'] = $this->model_catalog_attribute_group->getAttributeGroups();

        // loading stock statuses
        $this->load->model('localisation/stock_status');
        $data['stock_statuses'] = $this->model_localisation_stock_status->getStockStatuses();

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/moneymaker2', 'token=' . $this->session->data['token'], 'SSL')
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/moneymaker2', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL')
			);
		}

        $data['action'] = $this->url->link('module/moneymaker2', 'token=' . $this->session->data['token'], 'SSL'); // URL to be directed when the save button is pressed

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        $this->load->model('tool/image');

        if (isset($this->request->post['moneymaker2_header_contacts'])) {
            $results = $this->request->post['moneymaker2_header_contacts'];
        } elseif ($this->config->get('moneymaker2_header_contacts')) {
            $results = $this->config->get('moneymaker2_header_contacts');
        } else {
            $results = array();
        }
        $data['moneymaker2_header_contacts'] = array();
        foreach ($results as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                $image = $result['image'];
                $thumb = $result['image'];
            } else {
                $image = '';
                $thumb = 'no_image.png';
            }
            $data['moneymaker2_header_contacts'][] = array(
                'image' => $image,
                'thumb' => $this->model_tool_image->resize($thumb, 100, 100),
                'text' => $result['text'],
                'mode'  => $result['mode'],
                'icon'  => $result['icon'],
                'link'  => $result['link'],
                'sort_order'  => $result['sort_order'],
            );
        }
        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

        if (isset($this->request->post['moneymaker2_header_banners'])) {
            $results = $this->request->post['moneymaker2_header_banners'];
        } elseif ($this->config->get('moneymaker2_header_banners')) {
            $results = $this->config->get('moneymaker2_header_banners');
        } else {
            $results = array();
        }
        $data['moneymaker2_header_banners'] = array();
        foreach ($results as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                $image = $result['image'];
                $thumb = $result['image'];
            } else {
                $image = '';
                $thumb = 'no_image.png';
            }
            $data['moneymaker2_header_banners'][] = array(
                'image' => $image,
                'icon' => $result['icon'],
                'thumb' => $this->model_tool_image->resize($thumb, 100, 100),
                'text' => $result['text'],
                'name' => $result['name'],
                'style'  => $result['style'],
                'link'  => $result['link'],
                'sort_order'  => $result['sort_order'],
            );
        }

        if (isset($this->request->post['moneymaker2_header_links'])) {
            $results = $this->request->post['moneymaker2_header_links'];
        } elseif ($this->config->get('moneymaker2_header_links')) {
            $results = $this->config->get('moneymaker2_header_links');
        } else {
            $results = array();
        }
        $data['moneymaker2_header_links'] = array();
        foreach ($results as $result) {
            $data['moneymaker2_header_links'][] = array(
                'icon' => $result['icon'],
                'caption' => $result['caption'],
                'link'  => $result['link'],
                'sort_order'  => $result['sort_order'],
            );
        }

        if (isset($this->request->post['moneymaker2_footer_contacts'])) {
            $results = $this->request->post['moneymaker2_footer_contacts'];
        } elseif ($this->config->get('moneymaker2_footer_contacts')) {
            $results = $this->config->get('moneymaker2_footer_contacts');
        } else {
            $results = array();
        }
        $data['moneymaker2_footer_contacts'] = array();
        foreach ($results as $result) {
            $data['moneymaker2_footer_contacts'][] = array(
                'caption' => $result['caption'],
                'link'  => $result['link'],
                'sort_order'  => $result['sort_order'],
            );
        }

        if (isset($this->request->post['moneymaker2_footer_links'])) {
            $results = $this->request->post['moneymaker2_footer_links'];
        } elseif ($this->config->get('moneymaker2_footer_links')) {
            $results = $this->config->get('moneymaker2_footer_links');
        } else {
            $results = array();
        }
        $data['moneymaker2_footer_links'] = array();
        foreach ($results as $result) {
            $data['moneymaker2_footer_links'][] = array(
                'caption' => $result['caption'],
                'link'  => $result['link'],
                'sort_order'  => $result['sort_order'],
            );
        }

        if (isset($this->request->post['moneymaker2_footer_socials'])) {
            $results = $this->request->post['moneymaker2_footer_socials'];
        } elseif ($this->config->get('moneymaker2_footer_socials')) {
            $results = $this->config->get('moneymaker2_footer_socials');
        } else {
            $results = array();
        }
        $data['moneymaker2_footer_socials'] = array();
        foreach ($results as $result) {
            $data['moneymaker2_footer_socials'][] = array(
                'icon' => $result['icon'],
                'caption' => $result['caption'],
                'link'  => $result['link'],
                'sort_order'  => $result['sort_order'],
            );
        }

        if (isset($this->request->post['moneymaker2_product_points'])) {
            $results = $this->request->post['moneymaker2_product_points'];
        } elseif ($this->config->get('moneymaker2_product_points')) {
            $results = $this->config->get('moneymaker2_product_points');
        } else {
            $results = array();
        }
        $data['moneymaker2_product_points'] = array();
        foreach ($results as $result) {
            $data['moneymaker2_product_points'][] = array(
                'icon' => $result['icon'],
                'categories' => isset($result['categories'])&&$result['categories'] ? $result['categories'] : array(0),
                'text' => $result['text'],
                'name' => $result['name'],
                'sort_order'  => $result['sort_order'],
            );
        }

        if (isset($this->request->post['moneymaker2_modules_promos'])) {
            $results = $this->request->post['moneymaker2_modules_promos'];
        } elseif ($this->config->get('moneymaker2_modules_promos')) {
            $results = $this->config->get('moneymaker2_modules_promos');
        } else {
            $results = array();
        }
        $data['moneymaker2_modules_promos'] = array();
        foreach ($results as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                $image = $result['image'];
                $thumb = $result['image'];
            } else {
                $image = '';
                $thumb = 'no_image.png';
            }
            $data['moneymaker2_modules_promos'][] = array(
                'image'         => $image,
                'thumb'         => $this->model_tool_image->resize($thumb, 100, 100),
                'categories'    => isset($result['categories'])&&$result['categories'] ? $result['categories'] : array(0),
                'sort_order'    => $result['sort_order'],
                'text'          => $result['text'],
                'title'         => $result['title'],
                'link'          => $result['link'],
                'description'   => $result['description'],
            );
        }

        if (isset($this->request->post['moneymaker2_catalog_compare_charts'])) {
            $results = $this->request->post['moneymaker2_catalog_compare_charts'];
        } elseif ($this->config->get('moneymaker2_catalog_compare_charts')) {
            $results = $this->config->get('moneymaker2_catalog_compare_charts');
        } else {
            $results = array();
        }
        $data['moneymaker2_catalog_compare_charts'] = array();
        foreach ($results as $result) {
            $data['moneymaker2_catalog_compare_charts'][] = array(
                'mode'          => $result['mode'],
                'fns'           => $result['fns'],
                'delimiter'     => $result['delimiter'],
                'attribute'     => $result['attribute'],
                'unit'          => $result['unit'],
            );
        }

        if (isset($this->request->post['moneymaker2_header_cart_image_src'])) {
            $data['moneymaker2_header_cart_image_src'] = $this->request->post['moneymaker2_header_cart_image_src'];
        } else {
            $data['moneymaker2_header_cart_image_src'] = $this->config->get('moneymaker2_header_cart_image_src');
        }
        if (isset($this->request->post['moneymaker2_header_cart_image_src']) && is_file(DIR_IMAGE . $this->request->post['moneymaker2_header_cart_image_src'])) {
            $data['moneymaker2_header_cart_thumb'] = $this->model_tool_image->resize($this->request->post['moneymaker2_header_cart_image_src'], 100, 100);
        } elseif ($this->config->get('moneymaker2_header_cart_image_src') && is_file(DIR_IMAGE . $this->config->get('moneymaker2_header_cart_image_src'))) {
            $data['moneymaker2_header_cart_thumb'] = $this->model_tool_image->resize($this->config->get('moneymaker2_header_cart_image_src'), 100, 100);
        } else {
            $data['moneymaker2_header_cart_thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }

        $config_data = array(
            'moneymaker2_name',

            'moneymaker2_common_buy_hide',
            'moneymaker2_common_wishlist_hide',
            'moneymaker2_common_wishlist_caption',
            'moneymaker2_common_compare_hide',
            'moneymaker2_common_compare_caption',
            'moneymaker2_common_css_enabled',
            'moneymaker2_common_css',
            //'moneymaker2_common_js_enabled',
            //'moneymaker2_common_js',
            'moneymaker2_common_cart_outofstock_disabled',
            'moneymaker2_common_price_detached',
            'moneymaker2_common_browser_warned',
            'moneymaker2_common_categories_icons_enabled',
            'moneymaker2_common_categories_icons',
            'moneymaker2_common_products_title_overflow',
            //'moneymaker2_common_dropdowns_animation',
            //'moneymaker2_common_dropdowns_animation_in',
            //'moneymaker2_common_dropdowns_animation_out',

            'moneymaker2_header_transparency',
            'moneymaker2_header_glowing',
            'moneymaker2_header_glowing_color',
            'moneymaker2_header_strip_expanded',
            'moneymaker2_header_strip_bg_color',
            'moneymaker2_header_strip_font_color',
            'moneymaker2_header_strip_font_color_hover',
            'moneymaker2_header_strip_font_color_active',
            'moneymaker2_header_contacts_icon',
            'moneymaker2_header_contacts_phone',
            'moneymaker2_header_contacts_phone_highlight',
            'moneymaker2_header_contacts_phone_highlight_color',
            'moneymaker2_header_contacts_title',
            'moneymaker2_header_categories_hide',
            'moneymaker2_header_categories_caption',
            'moneymaker2_header_categories_mode',
            'moneymaker2_header_categories_menu',
            'moneymaker2_header_categories_menu_icons',
            'moneymaker2_header_categories_hidechilds',
            'moneymaker2_header_categories_hidethumbs',
            'moneymaker2_header_categories_thumbs_width',
            'moneymaker2_header_categories_thumbs_height',
            'moneymaker2_header_categories_columns',
            'moneymaker2_header_cart_caption_hide',
            'moneymaker2_header_cart_custom',
            'moneymaker2_header_cart_effect',
            'moneymaker2_header_cart_effect_color',
            'moneymaker2_header_cart_caption',
            'moneymaker2_header_cart_image',
            'moneymaker2_header_cart_text',
            'moneymaker2_header_cart_link_enabled',
            'moneymaker2_header_cart_link_icon',
            'moneymaker2_header_cart_link_title',
            'moneymaker2_header_cart_link',
            'moneymaker2_header_search_caption_hide',
            'moneymaker2_header_search_ajax',
            'moneymaker2_header_search_ajax_image',
            'moneymaker2_header_search_ajax_image_height',
            'moneymaker2_header_search_ajax_image_width',
            'moneymaker2_header_search_ajax_price',
            'moneymaker2_header_search_ajax_code',
            'moneymaker2_header_search_ajax_code_field',
            'moneymaker2_header_menu_caption_hide',
            'moneymaker2_header_menu_links_enabled',
            'moneymaker2_header_menu_links_caption',

            'moneymaker2_footer_information_hide',
            'moneymaker2_footer_customer_hide',
            'moneymaker2_footer_extras_hide',
            'moneymaker2_footer_account_hide',
            'moneymaker2_footer_contacts_enabled',
            'moneymaker2_footer_contacts_title',
            'moneymaker2_footer_contacts_icon',
            'moneymaker2_footer_links_enabled',
            'moneymaker2_footer_links_title',
            'moneymaker2_footer_links_icon',
            'moneymaker2_footer_text1_enabled',
            'moneymaker2_footer_text1_title',
            'moneymaker2_footer_text1_icon',
            'moneymaker2_footer_text1',
            'moneymaker2_footer_text2_enabled',
            'moneymaker2_footer_text2_title',
            'moneymaker2_footer_text2_icon',
            'moneymaker2_footer_text2',
            'moneymaker2_footer_copyrights_hide',
            'moneymaker2_footer_powered_hide',
            'moneymaker2_footer_powered_custom_enabled',
            'moneymaker2_footer_powered_custom_text',
            'moneymaker2_footer_powered_custom_title',
            'moneymaker2_footer_powered_custom_year',
            'moneymaker2_footer_socials_enabled',
            'moneymaker2_footer_socials_target_blank',

            'moneymaker2_product_metatitles_enabled',
            'moneymaker2_product_socials_enabled',
            'moneymaker2_product_code',
            'moneymaker2_product_code_field',
            'moneymaker2_product_code_caption',
            'moneymaker2_product_gallery_moved',
            'moneymaker2_product_gallery_round',
            'moneymaker2_product_gallery_animation',
            'moneymaker2_product_gallery_animation_in',
            'moneymaker2_product_thumbs_limit',
            'moneymaker2_product_points_size',
            'moneymaker2_product_points_stock',
            'moneymaker2_product_points_stock_default',
            'moneymaker2_product_points_stock_icon',
            'moneymaker2_product_points_stock_color',
            'moneymaker2_product_points_stock_caption',
            'moneymaker2_product_points_manufacturer',
            'moneymaker2_product_points_manufacturer_image_width',
            'moneymaker2_product_points_manufacturer_image_height',
            'moneymaker2_product_points_manufacturer_caption',
            'moneymaker2_product_options_hide',
            'moneymaker2_product_options_hide_limit',
            'moneymaker2_product_tabs_hide',
            'moneymaker2_product_tabs_headers',
            'moneymaker2_product_tabs_headers_text',
            'moneymaker2_product_tabs_attributes_responsive',

            'moneymaker2_catalog_layout_default',
            'moneymaker2_catalog_layout_switcher_hide',
            'moneymaker2_catalog_categories_metatitles_enabled',
            'moneymaker2_catalog_categories_images_hide',
            'moneymaker2_catalog_categories_move_description',
            'moneymaker2_catalog_products_grid_stock_hide',
            'moneymaker2_catalog_products_list_stock_hide',
            'moneymaker2_catalog_products_stock_color',
            'moneymaker2_catalog_products_grid_code_hide',
            'moneymaker2_catalog_products_list_code_hide',
            'moneymaker2_catalog_products_code_field',
            'moneymaker2_catalog_contacts_quicklinks',
            'moneymaker2_catalog_contacts_call_title',
            'moneymaker2_catalog_contacts_mail_title',
            'moneymaker2_catalog_contacts_map',
            'moneymaker2_catalog_compare_images_hide',
            'moneymaker2_catalog_compare_model_hide',
            'moneymaker2_catalog_compare_brand_hide',
            'moneymaker2_catalog_compare_availability_hide',
            'moneymaker2_catalog_compare_rating_hide',
            'moneymaker2_catalog_compare_description_hide',
            'moneymaker2_catalog_compare_weight_hide',
            'moneymaker2_catalog_compare_dimensions_hide',
            'moneymaker2_catalog_compare_charts_enabled',
            'moneymaker2_catalog_compare_charts_price_enabled',
            'moneymaker2_catalog_compare_charts_columns',

            'moneymaker2_modules_stickers_mode',
            'moneymaker2_modules_stickers_background',
            'moneymaker2_modules_stickers_transparency',
            'moneymaker2_modules_stickers_size_catalog',
            'moneymaker2_modules_stickers_size_product',
            'moneymaker2_modules_stickers_specials_enabled',
            'moneymaker2_modules_stickers_specials_icon',
            'moneymaker2_modules_stickers_specials_color',
            'moneymaker2_modules_stickers_specials_caption',
            'moneymaker2_modules_stickers_specials_discount',
            'moneymaker2_modules_stickers_specials_discount_mode',
            'moneymaker2_modules_stickers_popular_enabled',
            'moneymaker2_modules_stickers_popular_icon',
            'moneymaker2_modules_stickers_popular_color',
            'moneymaker2_modules_stickers_popular_caption',
            'moneymaker2_modules_stickers_popular_limit',
            'moneymaker2_modules_stickers_rated_enabled',
            'moneymaker2_modules_stickers_rated_icon',
            'moneymaker2_modules_stickers_rated_color',
            'moneymaker2_modules_stickers_rated_caption',
            'moneymaker2_modules_stickers_rated_limit',
            'moneymaker2_modules_stickers_new_enabled',
            'moneymaker2_modules_stickers_new_icon',
            'moneymaker2_modules_stickers_new_color',
            'moneymaker2_modules_stickers_new_caption',
            'moneymaker2_modules_stickers_new_limit',
            'moneymaker2_modules_stickers_custom1_enabled',
            'moneymaker2_modules_stickers_custom1_icon',
            'moneymaker2_modules_stickers_custom1_color',
            'moneymaker2_modules_stickers_custom1_caption',
            'moneymaker2_modules_stickers_custom1_field',
            'moneymaker2_modules_stickers_custom2_enabled',
            'moneymaker2_modules_stickers_custom2_icon',
            'moneymaker2_modules_stickers_custom2_color',
            'moneymaker2_modules_stickers_custom2_caption',
            'moneymaker2_modules_stickers_custom2_field',
            'moneymaker2_modules_stickers_promo_enabled',
            'moneymaker2_modules_stickers_promo_icon',
            'moneymaker2_modules_stickers_promo_color',
            'moneymaker2_modules_stickers_promo_caption',

            'moneymaker2_modules_promo_enabled',
            'moneymaker2_modules_promo_style',

            'moneymaker2_modules_quickorder_enabled',
            'moneymaker2_modules_quickorder_clear_cart',
            'moneymaker2_modules_quickorder_image_width',
            'moneymaker2_modules_quickorder_image_height',
            'moneymaker2_modules_quickorder_button_title',
            'moneymaker2_modules_quickorder_phone_mask_enabled',
            'moneymaker2_modules_quickorder_phone_mask',

        );


        foreach ($config_data as $conf) {
            if (isset($this->request->post[$conf])) {
                $data[$conf] = $this->request->post[$conf];
            } else {
                $data[$conf] = $this->config->get($conf);
            }
        }

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/moneymaker2.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/moneymaker2')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}